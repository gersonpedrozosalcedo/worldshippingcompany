<?php

include "../header/header.php";

if ($_SESSION["id_admin"] == null) {
    echo "<script>alerta(); function alerta(){window.location.href = 'index';}</script>";
}

include '../database/database.php';

$consultar_tipo_identificacion = $conn->prepare("SELECT * FROM tipo_id");
$consultar_tipo_identificacion->execute();
$consultar_tipo_identificacion = $consultar_tipo_identificacion->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_rutas = $conn->prepare("SELECT * FROM rutas");
$consultar_tipo_rutas->execute();
$consultar_tipo_rutas = $consultar_tipo_rutas->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_vehiculo = $conn->prepare("SELECT * FROM tipo_vehiculo");
$consultar_tipo_vehiculo->execute();
$consultar_tipo_vehiculo = $consultar_tipo_vehiculo->fetchAll(PDO::FETCH_ASSOC);


$consultar_montacargas = $conn->prepare("SELECT * FROM vehiculos_montacarga WHERE estado = 1 ");
$consultar_montacargas->execute();
$consultar_montacargas = $consultar_montacargas->fetchAll(PDO::FETCH_ASSOC);


$consultar_v_transporte = $conn->prepare("SELECT * FROM vehiculos_transporte WHERE estado = 1 ");
$consultar_v_transporte->execute();
$consultar_v_transporte = $consultar_v_transporte->fetchAll(PDO::FETCH_ASSOC);


?>


<div class="pageheader pd-t-25 pd-b-35">
    <div class="pd-t-5 pd-b-5">
        <h1 class="pd-0 mg-0 tx-20">Conductores</h1>
    </div>
    <div class="breadcrumb pd-0 mg-0">
        <a class="breadcrumb-item" href="home"><i class="icon ion-ios-home-outline"></i> Inicio</a>
        <a class="breadcrumb-item" href="home">Dashboard</a>
        <span class="breadcrumb-item active">conductores</span>
    </div>
</div>

<div class="col-md-12 col-lg-12">
    <div class="card mg-b-20">
        <div class="card-header">
            <h4 class="card-header-title">
                Conductores agregados
            </h4>
            <center data-toggle="tooltip" data-trigger="hover" data-placement="top" title=""
                data-original-title="Agregar un nuevo conductor"><button type="button"
                    class="btn btn-brand btn-linkedin" data-toggle="modal" data-target="#m_modal_1_2">
                    <i data-feather="plus-circle"></i><span>Agregar un
                        conductor</span></center>
            <div class="card-header-btn" style="margin-left:5px;">
                <a href="#" data-toggle="collapse" class="btn card-collapse" data-target="#collapse3"
                    aria-expanded="true"><i class="ion-ios-arrow-down"></i></a>
                <a href="#" data-toggle="refresh" onclick="tabla_conductores()" class="btn card-refresh"><i
                        class="ion-android-refresh"></i></a>
                <a href="#" data-toggle="expand" class="btn card-expand"><i class="ion-android-expand"></i></a>
                <a href="#" data-toggle="remove" class="btn card-remove"><i class="ion-android-close"></i></a>
            </div>
        </div>
        <div class="card-body collapse show" id="collapse3">
            <div class="row">

                </button>
                <div class="mg-20 form-inline wd-100p">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Estado</label>
                            <select id="foo-filter-status" class="form-control">
                                <option value="">Mostrar todos</option>
                                <option value="Recien creado">Recien creado</option>
                                <option value="Activado">Activado</option>
                                <option value="Desactivado">Desactivado</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group ft-right">
                            <input id="foo-search" type="text" placeholder="Buscar conductor..." class="form-control"
                                autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div id="tabla_conductores"></div>
            <div id="estado_conductores"></div>
        </div>
    </div>
</div>

<!-- modales-->
<div class="modal" id="m_modal_1_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_2"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel_2">Crear conductor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ion-ios-close-empty"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div data-scrollbar-shown="true" data-scrollable="true" data-height="300"
                    style="height: 300px; overflow: hidden; overflow-y: auto;">

                    <form id="form_guardar_conductor" enctype="multipart/form-data">
                        <div class="row">

                            <div class="col-12 col-sm-6">
                                <label>Nombres: <span class="tx-danger">*</span></label>
                                <input type="text" name="nombres" class="form-control" placeholder="Nombres" required=""
                                    data-parsley-id="11">
                            </div>
                            <div class="col-12 col-sm-6">

                                <label>Apellidos: <span class="tx-danger">*</span></label>
                                <input type="text" name="apellidos" class="form-control" placeholder="Apellidos"
                                    required="" data-parsley-id="11">
                            </div>
                            <div class="col-12 col-sm-6">
                                <br>
                                <label>Tipo identifición: <span class="tx-danger">*</span></label>
                                <select name="tipo_id" id="tipo_id" class="form-control">
                                    <option value="" selected>Seleccione una opción</option>
                                    <?php
                                    foreach ($consultar_tipo_identificacion as $tipo_id) {
                                    ?>
                                    <option value="<?php echo $tipo_id["id"] ?>"> <?php echo $tipo_id["nombre"] ?>
                                    </option>

                                    <?php
                                    }
                                    ?>

                                </select>
                            </div>
                            <div class="col-12 col-sm-6">
                                <br>
                                <label>Número identificación: <span class="tx-danger">*</span></label>
                                <input type="tel" name="numero_identificacion" class="form-control"
                                    placeholder="Número identificación" required="" data-parsley-id="11">
                            </div>
                            <div class="col-12 col-sm-12">
                                <br>
                                <label for="">Tipo de vehículo</label>

                                <select name="tipo_vehiculo" id="tipo_vehiculo" onchange="seleccionar_placa_nombre_v()"
                                    class="form-control">
                                    <option value="">Seleccione una opción</option>
                                    <?php foreach ($consultar_tipo_vehiculo as $tipo) { ?>
                                    <option value="<?php echo $tipo["nombre"] ?>"> <?php echo $tipo["nombre"] ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Placa vehículo/nombre montacarga: <span class="tx-danger">*</span></label>
                                <select name="placa_vehiculo" class="form-control" onchange="mostrar_foto_v()"
                                    id="selector_placa_nombre" required="" data-parsley-id="11"></select>
                                <p id="foto_vehiculo"> </p>
                            </div>
                            <!-- <div class="col-12 col-sm-12">
                                <br>
                                <label>Ruta: <span class="tx-danger">*</span></label>
                                <select name="ruta" class="form-control" id="cargo">
                                    <option value="" selected>Seleccione una opción</option>
                                    <?php
                                    foreach ($consultar_tipo_rutas as $rutas) {
                                    ?>
                                    <option value="<?php echo $rutas["id"] ?>"> <?php echo $rutas["nombre_ruta"] ?>
                                    </option>

                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>-->
                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Estado: <span class="tx-danger">*</span></label>
                                <select name="estado" id="estado" class="form-control ">
                                    <option value="" selected>Seleccione una opción</option>
                                    <option value="0">Recien creado</option>
                                    <option value="1">Activar</option>
                                    <option value="2">Desactivar</option>

                                </select>
                            </div>
                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Firma digital (png, jpg) (100x150): <span class="tx-danger">*</span></label>
                                <div class="input-group">
                                    <div class="custom-file"> <input type="file" name="foto_firma"
                                            class="form-control form-control-sm" id="inputGroupFile04">
                                        <label class="" for="inputGroupFile04"></label>
                                    </div>
                                    <br>
                                    <div class="input-group-append">
                                        <span class="input-group-btn">
                                            <button class="btn btn-custom-primary file-browser" type="button"><i
                                                    class="fa fa-upload"></i></button>
                                        </span>
                                    </div>
                                </div>

                            </div>



                        </div>
                    </form>

                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps__rail-y" style="top: 0px; right: 4px;">
                        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                    </div>
                </div>
            </div>
            <div id="respuesta_form_conductor"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" onclick="guardar_conductor()" class="btn btn-primary">Guardar conductor</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="editar_conductor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_3"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel_3">Editar conductor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ion-ios-close-empty"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div data-scrollbar-shown="true" data-scrollable="true" data-height="300"
                    style="height: 300px; overflow: hidden; overflow-y: auto;">

                    <div id="editar_conductores"></div>

                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps__rail-y" style="top: 0px; right: 4px;">
                        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                    </div>
                </div>
            </div>
            <div id="respuesta_form_actualizar_conductor"></div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" onclick="actualizar_conductor()" class="btn btn-primary">Actualizar
                    conductor</button>
            </div>
        </div>
    </div>
</div>


<?php include "../footer/footer.php" ?>

<script>
window.load = tabla_conductores();
</script>
<script>
// ///////////////////////////////////////Row Toggler
$("#foo-row-toggler").footable();

// Accordion
$("#foo-accordion")
    .footable()
    .on("footable_row_expanded", function(e) {
        $("#foo-accordion tbody tr.footable-detail-show")
            .not(e.row)
            .each(function() {
                $("#foo-accordion").data("footable").toggleDetail(this);
            });
    });
// Filtering
var filtering = $("#foo-filtering");
filtering.footable().on("footable_filtering", function(e) {
    var selected = $("#foo-filter-status").find(":selected").val();
    e.filter += e.filter && e.filter.length > 0 ? " " + selected : selected;
    e.clear = !e.filter;
});

// Filter status
$("#foo-filter-status").change(function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});

// Search input
$("#foo-search").on("input", function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});

function seleccionar_placa_nombre_v() {

    var tipo_vehiculo = document.getElementById("tipo_vehiculo").value;

    if (tipo_vehiculo === "Transporte") {
        document.getElementById("selector_placa_nombre").innerHTML =
            '<option value="" selected>Seleccionar una opción</option><?php foreach ($consultar_v_transporte as $v_transporte) { ?><option value="<?php echo $v_transporte["nombre"]; ?> "> <?php echo $v_transporte["nombre"]; ?> </option> <?php } ?>';

    } else {
        document.getElementById("selector_placa_nombre").innerHTML =
            '<option value="" selected>Seleccionar una opción</option><?php foreach ($consultar_montacargas as $montacarga) { ?><option value = "<?php echo $montacarga["nombre"]; ?>"> <?php echo $montacarga["nombre"]; ?> </option> <?php } ?>';
    }

}

function mostrar_foto_v() {
    var vehiculo_t_m = document.getElementById("selector_placa_nombre").value;
    var strNewWebsiteName = vehiculo_t_m.replace("#", "");
    document.getElementById("foto_vehiculo").innerHTML =
        '<img src="http://worldshippingcompany.com.co/foto_vehiculos_m_t/' + strNewWebsiteName +
        '.jpeg" alt="Imgen vehículo" width="100" height="100">';
}
</script>

<script src="../assets/plugins/datepicker/js/datepicker.min.js"></script>
<script src="../assets/plugins/datepicker/js/datepicker.es.js"></script>