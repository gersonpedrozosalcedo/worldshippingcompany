<?php

include "../header/header.php";

if ($_SESSION["id_admin"] == null) {
    echo "<script>alerta(); function alerta(){window.location.href = 'index';}</script>";
}

?>

<div class="pageheader pd-t-25 pd-b-35">
    <div class="pd-t-5 pd-b-5">
        <h1 class="pd-0 mg-0 tx-20">Clientes</h1>
    </div>
    <div class="breadcrumb pd-0 mg-0">
        <a class="breadcrumb-item" href="home"><i class="icon ion-ios-home-outline"></i> Inicio</a>
        <a class="breadcrumb-item" href="home">Dashboard</a>
        <span class="breadcrumb-item active">clientes</span>
    </div>
</div>

<div class="col-md-12 col-lg-12">
    <div class="card mg-b-20">
        <div class="card-header">
            <h4 class="card-header-title">
                Clientes agregados
            </h4>
            <center data-toggle="tooltip" data-trigger="hover" data-placement="top" title=""
                data-original-title="Agregar un nuevo cliente"><button type="button" class="btn btn-brand btn-linkedin"
                    data-toggle="modal" data-target="#m_modal_1_2" onclick="ejecutar_email_cliente()">
                    <i data-feather="plus-circle"></i><span>Agregar un
                        cliente</span></center>
            <div class="card-header-btn" style="margin-left:5px;">
                <a href="#" data-toggle="collapse" class="btn card-collapse" data-target="#collapse3"
                    aria-expanded="true"><i class="ion-ios-arrow-down"></i></a>
                <a href="#" data-toggle="refresh" onclick="tabla_clientes()" class="btn card-refresh"><i
                        class="ion-android-refresh"></i></a>
                <a href="#" data-toggle="expand" class="btn card-expand"><i class="ion-android-expand"></i></a>
                <a href="#" data-toggle="remove" class="btn card-remove"><i class="ion-android-close"></i></a>
            </div>
        </div>
        <div class="card-body collapse show" id="collapse3">
            <div class="row">

                </button>
                <div class="mg-20 form-inline wd-100p">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Estado</label>
                            <select id="foo-filter-status" class="form-control">
                                <option value="">Mostrar todos</option>
                                <option value="Recien creado">Recien creado</option>
                                <option value="Activado">Activado</option>
                                <option value="Desactivado">Desactivado</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group ft-right">
                            <input id="foo-search" type="text" placeholder="Buscar cliente..." class="form-control"
                                autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div id="tabla_clientes"></div>
            <div id="estado_cliente"></div>
        </div>
    </div>
</div>

<!-- modales-->
<div class="modal" id="m_modal_1_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_2"
    aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel_2">Crear cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ion-ios-close-empty"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div data-scrollbar-shown="true" data-scrollable="true" data-height="300"
                    style="height: 300px; overflow: hidden; overflow-y: auto;">

                    <form id="form_guardar_cliente">
                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <label>Razón social: <span class="tx-danger">*</span></label>
                                <input type="text" name="razon_social" class="form-control" placeholder="Razón social"
                                    required="" data-parsley-id="11">
                            </div>
                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Nit: <span class="tx-danger">*</span></label>
                                <input type="tel" name="nit" class="form-control " placeholder="Nit" required=""
                                    data-parsley-id="20">
                            </div>

                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Email principal: <span class="tx-danger">*</span></label>
                                <input type="email" name="email" class="form-control " placeholder="Email" required=""
                                    data-parsley-id="11">
                            </div>

                            <div class="col-12 col-sm-12">
                                <input type="hidden" name="email_cliente" id="email_cliente" value="" readonly="true"
                                    required />
                                <div class="form-group">
                                    <br>
                                    <label>Email copias CCO: <span class="tx-danger">*</span></label>
                                    <div id="tabla_email_cliente"></div>

                                    <button type="button" class="btn btn-primary" onclick="ejecutar_email_cliente()"
                                        required>Agregar otro email copia CCO</button>'

                                </div>
                            </div>

                        </div>
                        <table width="100%">
                            <td>
                                <hr />
                            </td>
                            <td style="width:1px; padding: 0 10px; white-space: nowrap;"><b>Sub clientes</b>
                            </td>
                            <td>
                                <hr />
                            </td>
                        </table>
                        <div class="col-12 col-sm-12">
                            <input type="hidden" name="form_sub_cliente" id="form_sub_cliente" value="" readonly="true"
                                required />
                            <div class="form-group">

                                <div id="tabla_form_sub_cliente"></div><button type="button" style="width: 100%"
                                    name="add_form_cliente" id="add_form_cliente" class="btn btn-success">Añadir
                                    subcliente</button>

                            </div>
                        </div>

                    </form>

                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps__rail-y" style="top: 0px; right: 4px;">
                        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                    </div>
                </div>
            </div>
            <div id="respuesta_form_cliente"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" onclick="guardar_cliente()" class="btn btn-primary">Guardar cliente</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="editar_cliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_3"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel_3">Editar cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ion-ios-close-empty"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div data-scrollbar-shown="true" data-scrollable="true" data-height="300"
                    style="height: 300px; overflow: hidden; overflow-y: auto;">

                    <div id="editar_clientes"></div>

                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps__rail-y" style="top: 0px; right: 4px;">
                        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                    </div>
                </div>
            </div>

            <div id="respuesta_form_actualizar_cliente"></div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" onclick="actualizar_cliente()" class="btn btn-primary">Actualizar
                    cliente</button>
            </div>
        </div>
    </div>
</div>


<?php include "../footer/footer.php" ?>

<script>
window.load = tabla_clientes();
</script>
<script>
// ///////////////////////////////////////Row Toggler
$("#foo-row-toggler").footable();

// Accordion
$("#foo-accordion")
    .footable()
    .on("footable_row_expanded", function(e) {
        $("#foo-accordion tbody tr.footable-detail-show")
            .not(e.row)
            .each(function() {
                $("#foo-accordion").data("footable").toggleDetail(this);
            });
    });
// Filtering
var filtering = $("#foo-filtering");
filtering.footable().on("footable_filtering", function(e) {
    var selected = $("#foo-filter-status").find(":selected").val();
    e.filter += e.filter && e.filter.length > 0 ? " " + selected : selected;
    e.clear = !e.filter;
});

// Filter status
$("#foo-filter-status").change(function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});

// Search input
$("#foo-search").on("input", function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});


$(document).ready(function() {
    J = 0;
    aux = 0;
    $('#add_form_cliente').click(function() {
        J++;
        aux++;
        document.getElementById("form_sub_cliente").value = aux;
        $('#tabla_form_sub_cliente').append(

            '<table style="width:100%" id="tabla_form_sub_cliente' + J + '" >  ' + '<tr id="row' +
            J + '">' +
            '<td style="width:50%"><input type="text"  id="nombre_sub_cliente' + J +
            '" name="nombre_sub_cliente[]" placeholder="Nombre del subcliente" class="form-control"/>' +
            '<td style="width:50%"><input type="email"  id="email_sub_cliente' + J +
            '" name="email_sub_cliente[]" placeholder="Email del subcliente" class="form-control name_list"  required />' +
            '<td><button type="button" name="remove" id="' + J +
            '" class="btn btn-danger btn_remove">X</button></td>' +
            '</tr>' + '</table>');
    });
    $(document).on('click', '.btn_remove', function() {
        aux--;

        document.getElementById("form_sub_cliente").value = aux;

        var id = $(this).attr('id');
        $('#tabla_form_sub_cliente' + id).remove();
    });

})


var J = 0;
var aux = 0;

function ejecutar_email_cliente() {


    J++;
    aux++;
    document.getElementById("email_cliente").value = aux;
    $('#tabla_email_cliente').append(

        '<table style="width:100%" id="tabla_email_cliente' + J + '" >  ' + '<tr id="row' +
        J + '">' +
        '<td style="width:100%"><input type="text"  id="email_cliente' + J +
        '" name="email_copia_cliente[]" placeholder="Ingrese email(s) del cliente" class="form-control name_list"  required />' +
        '<td><button type="button" name="remove" id="' + J +
        '" class="btn btn-danger btn_remove" required>X</button></td>' +
        '</tr>' + '</table>');

}

$(document).on('click', '.btn_remove', function() {


    aux--;

    document.getElementById("email_cliente").value = aux;
    var id = $(this).attr('id');
    $('#tabla_email_cliente' + id).remove();


});
</script>

<script src="../assets/plugins/datepicker/js/datepicker.min.js"></script>
<script src="../assets/plugins/datepicker/js/datepicker.es.js"></script>