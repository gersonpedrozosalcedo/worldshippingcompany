<?php

include "../header/header.php";

if ($_SESSION["id_admin"] == null) {
    echo "<script>alerta(); function alerta(){window.location.href = 'index';}</script>";
}

include '../database/database.php';

$consultar_tipo_identificacion = $conn->prepare("SELECT * FROM tipo_id");
$consultar_tipo_identificacion->execute();
$consultar_tipo_identificacion = $consultar_tipo_identificacion->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_cargos = $conn->prepare("SELECT * FROM tipo_cargos");
$consultar_tipo_cargos->execute();
$consultar_tipo_cargos = $consultar_tipo_cargos->fetchAll(PDO::FETCH_ASSOC);


?>


<div class="pageheader pd-t-25 pd-b-35">
    <div class="pd-t-5 pd-b-5">
        <h1 class="pd-0 mg-0 tx-20">Empleados</h1>
    </div>
    <div class="breadcrumb pd-0 mg-0">
        <a class="breadcrumb-item" href="home"><i class="icon ion-ios-home-outline"></i> Inicio</a>
        <a class="breadcrumb-item" href="home">Dashboard</a>
        <span class="breadcrumb-item active">empleados</span>
    </div>
</div>

<div class="col-md-12 col-lg-12">
    <div class="card mg-b-20">
        <div class="card-header">
            <h4 class="card-header-title">
                Empleados agregados
            </h4>
            <center data-toggle="tooltip" data-trigger="hover" data-placement="top" title=""
                data-original-title="Agregar un nuevo empleado"><button type="button" class="btn btn-brand btn-linkedin"
                    data-toggle="modal" data-target="#m_modal_1_2">
                    <i data-feather="plus-circle"></i><span>Agregar un
                        empleado</span></center>
            <div class="card-header-btn" style="margin-left:5px;">
                <a href="#" data-toggle="collapse" class="btn card-collapse" data-target="#collapse3"
                    aria-expanded="true"><i class="ion-ios-arrow-down"></i></a>
                <a href="#" data-toggle="refresh" onclick="tabla_empleado()" class="btn card-refresh"><i
                        class="ion-android-refresh"></i></a>
                <a href="#" data-toggle="expand" class="btn card-expand"><i class="ion-android-expand"></i></a>
                <a href="#" data-toggle="remove" class="btn card-remove"><i class="ion-android-close"></i></a>
            </div>
        </div>
        <div class="card-body collapse show" id="collapse3">
            <div class="row">

                </button>
                <div class="mg-20 form-inline wd-100p">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Estado</label>
                            <select id="foo-filter-status" class="form-control">
                                <option value="">Mostrar todos</option>
                                <option value="Recien creado">Recien creado</option>
                                <option value="Activado">Activado</option>
                                <option value="Desactivado">Desactivado</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group ft-right">
                            <input id="foo-search" type="text" placeholder="Buscar empleado..." class="form-control"
                                autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div id="tabla_empleados"></div>
            <div id="estado_empleados"></div>
        </div>
    </div>
</div>

<!-- modales-->
<div class="modal" id="m_modal_1_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_2"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel_2">Crear empleado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ion-ios-close-empty"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div data-scrollbar-shown="true" data-scrollable="true" data-height="300"
                    style="height: 300px; overflow: hidden; overflow-y: auto;">

                    <form id="form_guardar_empleado" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <label>Nombres: <span class="tx-danger">*</span></label>
                                <input type="text" name="nombres" class="form-control" placeholder="Nombres" required=""
                                    data-parsley-id="11">
                            </div>
                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Apellidos: <span class="tx-danger">*</span></label>
                                <input type="text" name="apellidos" class="form-control" placeholder="Apellidos"
                                    required="" data-parsley-id="11">
                            </div>
                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Tipo identifición: <span class="tx-danger">*</span></label>
                                <select name="tipo_id" id="tipo_id" class="form-control">
                                    <option value="" selected>Seleccione una opción</option>
                                    <?php
                                    foreach ($consultar_tipo_identificacion as $tipo_id) {
                                    ?>
                                    <option value="<?php echo $tipo_id["id"] ?>"> <?php echo $tipo_id["nombre"] ?>
                                    </option>

                                    <?php
                                    }
                                    ?>

                                </select>
                            </div>
                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Número identificación: <span class="tx-danger">*</span></label>
                                <input type="tel" name="numero_identificacion" class="form-control"
                                    placeholder="Número identificación" required="" data-parsley-id="11">
                            </div>
                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Email: <span class="tx-danger">*</span></label>
                                <input type="email" name="email" class="form-control " placeholder="Email" required=""
                                    data-parsley-id="11">
                            </div>
                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Teléfono: <span class="tx-danger">*</span></label>
                                <input type="tel" name="telefono" class="form-control " placeholder="Teléfono"
                                    required="" data-parsley-id="11">
                            </div>

                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Contraseña: <span class="tx-danger">*</span></label>
                                <input type="password" name="contraseña" class="form-control" placeholder="Contraseña"
                                    required="" data-parsley-id="11">
                            </div>

                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Repetir contraseña: <span class="tx-danger">*</span></label>
                                <input type="password" name="contraseña1" class="form-control"
                                    placeholder="Repetir contraseña" required="" data-parsley-id="11">
                            </div>
                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Cargo: <span class="tx-danger">*</span></label>
                                <select name="cargo" class="form-control" id="cargo">
                                    <option value="" selected>Seleccione una opción</option>
                                    <?php
                                    foreach ($consultar_tipo_cargos as $cargos) {
                                    ?>
                                    <option value="<?php echo $cargos["id"] ?>"> <?php echo $cargos["nombre_cargo"] ?>
                                    </option>

                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Estado: <span class="tx-danger">*</span></label>
                                <select name="estado" id="estado" class="form-control ">
                                    <option value="" selected>Seleccione una opción</option>
                                    <option value="0">Recien creado</option>
                                    <option value="1">Activar</option>
                                    <option value="2">Desactivar</option>

                                </select>
                            </div>
                            <div class="col-12 col-sm-12">
                                <br>
                                <label>Firma digital (png, jpg) (100x150): <span class="tx-danger">*</span></label>
                                <div class="input-group">
                                    <div class="custom-file"> <input type="file" name="foto_firma"
                                            class="form-control form-control-sm" id="inputGroupFile04">
                                        <label class="" for="inputGroupFile04"></label>
                                    </div>
                                    <br>
                                    <div class="input-group-append">
                                        <span class="input-group-btn">
                                            <button class="btn btn-custom-primary file-browser" type="button"><i
                                                    class="fa fa-upload"></i></button>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>

                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps__rail-y" style="top: 0px; right: 4px;">
                        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                    </div>
                </div>
            </div>
            <div id="respuesta_form_empleado"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" onclick="guardar_empleado()" class="btn btn-primary">Guardar empleado</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="editar_empleado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_3"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel_3">Editar empleado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ion-ios-close-empty"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div data-scrollbar-shown="true" data-scrollable="true" data-height="300"
                    style="height: 300px; overflow: hidden; overflow-y: auto;">

                    <div id="editar_empleados"></div>

                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps__rail-y" style="top: 0px; right: 4px;">
                        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                    </div>
                </div>
            </div>
            <div id="respuesta_form_actualizar_empleado"></div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" onclick="actualizar_empleado()" class="btn btn-primary">Actualizar
                    empleado</button>
            </div>
        </div>
    </div>
</div>
<div id="estado_empleado"></div>

<?php include "../footer/footer.php" ?>

<script>
window.load = tabla_empleados();
</script>
<script>
// ///////////////////////////////////////Row Toggler
$("#foo-row-toggler").footable();

// Accordion
$("#foo-accordion")
    .footable()
    .on("footable_row_expanded", function(e) {
        $("#foo-accordion tbody tr.footable-detail-show")
            .not(e.row)
            .each(function() {
                $("#foo-accordion").data("footable").toggleDetail(this);
            });
    });
// Filtering
var filtering = $("#foo-filtering");
filtering.footable().on("footable_filtering", function(e) {
    var selected = $("#foo-filter-status").find(":selected").val();
    e.filter += e.filter && e.filter.length > 0 ? " " + selected : selected;
    e.clear = !e.filter;
});

// Filter status
$("#foo-filter-status").change(function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});

// Search input
$("#foo-search").on("input", function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});
</script>

<script src="../assets/plugins/datepicker/js/datepicker.min.js"></script>
<script src="../assets/plugins/datepicker/js/datepicker.es.js"></script>