<?php

include "../header/header.php";
if($_SESSION["id_admin"] == null) {
    echo "<script>alerta(); function alerta(){window.location.href = 'index';}</script>";
    }


?>


<div class="pageheader pd-t-25 pd-b-35">
    <div class="pd-t-5 pd-b-5">
        <h1 class="pd-0 mg-0 tx-20">Tipos de cargas</h1>
    </div>
    <div class="breadcrumb pd-0 mg-0">
        <a class="breadcrumb-item" href="home"><i class="icon ion-ios-home-outline"></i> Inicio</a>
        <a class="breadcrumb-item" href="home">Dashboard</a>
        <span class="breadcrumb-item active">tipo de cargas</span>
    </div>
</div>
<div class="col-md-12 col-lg-12">
    <div class="card mg-b-20">
        <div class="card-header">
            <h4 class="card-header-title">
                Tipos de cargas
            </h4>
            <center data-toggle="tooltip" data-trigger="hover" data-placement="top" title=""
                data-original-title="Agregar un nuevo tipo de carga"><button type="button"
                    class="btn btn-brand btn-linkedin" data-toggle="modal" data-target="#form_tipo_cargas">
                    <i data-feather="plus-circle"></i><span>Agregar un tipo de
                        carga</span></button></center>
            <div class="card-header-btn">
                <a href="#" data-toggle="collapse" class="btn card-collapse" data-target="#collapse2"
                    aria-expanded="true"><i class="ion-ios-arrow-down"></i></a>
                <a href="javascript:void(0)" onclick="cargar_tipos_de_cargas()" data-toggle="refresh"
                    class="btn card-refresh"><i class="ion-android-refresh"></i></a>
                <a href="#" data-toggle="expand" class="btn card-expand"><i class="ion-android-expand"></i></a>
                <a href="#" data-toggle="remove" class="btn card-remove"><i class="ion-android-close"></i></a>
            </div>
        </div>
        <div class="card-body collapse show" id="collapse2">
            <div class="row">

                </button>
                <div class="mg-20 form-inline wd-100p">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Estado</label>
                            <select id="foo-filter-status" class="form-control">
                                <option value="">Mostrar todos</option>
                                <option value="Activado">Activado</option>
                                <option value="Desactivado">Desactivado</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group ft-right">
                            <input id="foo-search" type="text" placeholder="Buscar tipos de cargas..."
                                class="form-control" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div id="tabla_tipos_cargas"></div>
        </div>
    </div>
</div>



<!----------- modals tipos cargas-->

<div class="modal" id="form_tipo_cargas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_5"
    style=" padding-right: 17px;">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel_5">Agregar tipo cargas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ion-ios-close-empty"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_tipo_carga">
                    <div class="form-group">
                        <label for="recipient-name-2" class="form-control-label">Nombre tipo carga</label>
                        <input type="text" class="form-control" name="nombre_carga" id=" recipient-name-2">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name-2" class="form-control-label">Estado carga</label>
                        <select name="estado" class="form-control" id="estado">
                            <option value="">Seleccionar una opción</option>
                            <option value="0">Desactiavo</option>
                            <option value="1">Activado</option>
                        </select>


                    </div>
                </form>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" onclick="guardar_tipo_carga()">Guardar</button>
                </div>
                <div id="respuesta_form_tipo_carga"></div>
            </div>

        </div>
    </div>
</div>



<div class="modal" id="editar_tipo_cargas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_5"
    style=" padding-right: 17px;">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel_5">Editar tipo cargas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ion-ios-close-empty"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div id="editar_tipo_carga"></div>
            </div>
            <div id="respuesta_actulizar_tipo_carga"></div>

        </div>
    </div>
</div>



<?php include "../footer/footer.php"?>
<script>
window.load = cargar_tipos_de_cargas();
</script>