<?php include "../header/header.php";
session_start();
if ($_SESSION["id_admin"] != null) {
    echo "<script>alerta(); function alerta(){window.location.href = 'home';}</script>";
}
?>



<body>
    <!--================================-->
    <!-- User Singin Start -->
    <!--================================-->
    <div class="ht-100v d-flex">
        <div class="card shadow-none pd-20 mx-auto wd-300 text-center bd-1 align-self-center">
            <center><img style="width:80%" src="../assets/images/logo.png" alt=""></center>
            <h5 class="card-title mt-3 text-center">Bienviendos a World Shipping Company</h5>
            <p class="text-center">Inicie sesión</p>
            <form id="form_inicio_sesion">
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text pd-x-9"> <i class="fa fa-envelope"></i> </span>
                    </div>
                    <input class="form-control form-control-sm" name="email" placeholder="Email" type="email">
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                    </div>
                    <input class="form-control form-control-sm" placeholder="Contraseña" name="contraseña"
                        type="password">
                </div>
                <div class="row  mg-t-10 mg-b-10">
                    <div class="col-md-6">
                        <div class="custom-control custom-radio">
                            <input name="radio" type="radio" value="admin" class="custom-control-input" id="radio1">
                            <label class="custom-control-label" for="radio1">Administrador</label>
                        </div>
                    </div>
                    <!-- col-3 -->
                    <div class="col-md-6 mg-t-20 mg-lg-t-0">
                        <div class="custom-control custom-radio">
                            <input name="radio" type="radio" value="operador" class="custom-control-input" checked=""
                                id="radio2">
                            <label class="custom-control-label" for="radio2">Operador</label>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <a href="javascript:void(0)" onclick="iniciar_sesion_admin()"
                        class="btn btn-custom-primary btn-block tx-13 hover-white"> Iniciar
                        sesión
                    </a>
                    <hr>
                    <center>
                        <a href="../inspeccion_vehiculo" class="badge badge-primary">Check list de inspección
                            vehículos</a>
                    </center>

                </div>
                <!--  <p class="text-center"><a href="../operador">Iniciar sesión operador</a></p>-->
                <div id="respuesta_login"></div>

                <!--<p class="text-center">Don't have an account?<br /> <a href="page-singup.html"></a> </p>-->
            </form>

        </div>
    </div>


    <?php include "../footer/footer.php" ?>