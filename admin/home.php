<?php

include "../header/header.php";
session_start();
include '../database/database.php';
if($_SESSION["id_admin"] == null) {
    echo "<script>alerta(); function alerta(){window.location.href = 'index';}</script>";
    }

    $consultar_servicios_iniciados = $conn->prepare("SELECT * FROM servicios_control_rutas WHERE estado = 0");
    $consultar_servicios_iniciados->execute();
    $consultar_servicios_iniciados = $consultar_servicios_iniciados->fetchAll(PDO::FETCH_ASSOC);
    $contador_servicio_iniciado = count($consultar_servicios_iniciados);

    $consultar_servicios_proceso = $conn->prepare("SELECT * FROM servicios_control_rutas WHERE estado = 1" );
    $consultar_servicios_proceso->execute();
    $consultar_servicios_proceso = $consultar_servicios_proceso->fetchAll(PDO::FETCH_ASSOC);
    $contador_servicio_proceso = count($consultar_servicios_proceso);
    
    $consultar_servicios_completos = $conn->prepare("SELECT * FROM servicios_control_rutas WHERE estado = 2" );
    $consultar_servicios_completos->execute();
    $consultar_servicios_completos = $consultar_servicios_completos->fetchAll(PDO::FETCH_ASSOC);
    $contador_servicio_completos= count($consultar_servicios_completos);

?>

<div class="pageheader pd-t-25 pd-b-35">
    <div class="pd-t-5 pd-b-5">
        <h1 class="pd-0 mg-0 tx-20">Bienvenido <?php echo $_SESSION["nombre_admin"].' ('.$_SESSION["nombre_cargo"].')'?>
        </h1>
    </div>
    <div class="breadcrumb pd-0 mg-0">
        <a class="breadcrumb-item" href="index.html"><i class="icon ion-ios-home-outline"></i> Inicio</a>
        <a class="breadcrumb-item" href="#">Dashboard</a>
        <span class="breadcrumb-item active"></span>
    </div>
</div>



<div class="col-md-12 col-lg-12">

    <div class="row row-xs">
        <div class="col-sm-6 col-xl-4">
            <div class="card mg-b-20">
                <div class="card-body">
                    <div class="d-flex clearfix">
                        <div class="text-left mt-3">
                            <p class="tx-uppercase tx-10 mg-b-10">Servicios iniciados</p>
                            <h2 class="tx-20 tx-sm-18 tx-md-24 mb-0 mt-2 mt-sm-0 tx-normal tx-rubik tx-dark"><span
                                    class="counter"><?php echo $contador_servicio_iniciado?></span></h2>
                        </div>
                        <div class="ml-auto"> <span
                                class="bg-soft-warning tx-warning wd-80 ht-80 d-flex align-items-center justify-content-center rounded-circle ">
                                <i class="tx-30 tx-primary  icon-pie-chart"></i> </span> </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-4">
            <div class="card mg-b-20">
                <div class="card-body">
                    <div class="d-flex clearfix">
                        <div class="text-left mt-3">
                            <p class="tx-uppercase tx-10 mg-b-10">Servicios en proceso</p>
                            <h2 class="tx-20 tx-sm-18 tx-md-24 mb-0 mt-2 mt-sm-0 tx-normal tx-rubik tx-dark"><span
                                    class="counter"><?php echo $contador_servicio_proceso ?></span></h2>
                        </div>
                        <div class="ml-auto"> <span
                                class="bg-soft-success tx-success wd-80 ht-80 d-flex align-items-center justify-content-center rounded-circle ">
                                <i class="ion-cube tx-40"></i> </span> </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-4">
            <div class="card mg-b-20">
                <div class="card-body">
                    <div class="d-flex clearfix">
                        <div class="text-left mt-3">
                            <p class="tx-uppercase tx-10 mg-b-10">Servicios completados</p>
                            <h2 class="tx-20 tx-sm-18 tx-md-24 mb-0 mt-2 mt-sm-0 tx-normal tx-rubik tx-dark"><span
                                    class="counter"><?php echo $contador_servicio_completos?></span></h2>
                        </div>
                        <div class="ml-auto"> <span
                                class="bg-soft-primary tx-primary wd-80 ht-80 d-flex align-items-center justify-content-center rounded-circle ">
                                <i class="icon-speedometer tx-success tx-40"></i> </span> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card mg-b-20">
        <div class="card-header">
            <h4 class="card-header-title">
                Ultimos 10 servicios agregados
            </h4>
            <div class="card-header-btn">
                <a href="#" data-toggle="collapse" class="btn card-collapse" data-target="#collapse2"
                    aria-expanded="true"><i class="ion-ios-arrow-down"></i></a>
                <a href="javascript:void(0)" onclick="cargar_ultimos_servicios()" data-toggle="refresh"
                    class="btn card-refresh"><i class="ion-android-refresh"></i></a>
                <a href="#" data-toggle="expand" class="btn card-expand"><i class="ion-android-expand"></i></a>
                <a href="#" data-toggle="remove" class="btn card-remove"><i class="ion-android-close"></i></a>
            </div>
        </div>
        <div class="card-body collapse show" id="collapse2">
            <div id="tabla_servicios"></div>
        </div>
    </div>


</div>



<?php include "../footer/footer.php"?>

<script>
window.load = cargar_ultimos_servicios_admin();
</script>