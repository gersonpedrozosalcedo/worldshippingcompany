<?php

include "../header/header.php";

if ($_SESSION["id_admin"] == null) {
    echo "<script>alerta(); function alerta(){window.location.href = 'index';}</script>";
}

?>


<div class="pageheader pd-t-25 pd-b-35">
    <div class="pd-t-5 pd-b-5">
        <h1 class="pd-0 mg-0 tx-20">Inspección de Vehículos</h1>
    </div>
    <div class="breadcrumb pd-0 mg-0">
        <a class="breadcrumb-item" href="home"><i class="icon ion-ios-home-outline"></i> Inicio</a>
        <a class="breadcrumb-item" href="home">Dashboard</a>
        <span class="breadcrumb-item active">Inspección vehículos</span>
    </div>
</div>

<div class="col-md-12 col-lg-12">
    <div class="card mg-b-20">
        <div class="card-header">
            <h4 class="card-header-title">
                Inspecciones de vehículos agregados
            </h4>
            <a href="../inspeccion_vehiculo" target="_blank" class="btn btn-brand btn-linkedin">
                <center data-toggle="tooltip" data-trigger="hover" data-placement="top" title=""
                    data-original-title="Agregar un nueva inspección"> <i data-feather="plus-circle"></i><span>Agregar
                        inspección</span></center>
            </a>

            <div class="card-header-btn" style="margin-left:5px;">
                <a href="#" data-toggle="collapse" class="btn card-collapse" data-target="#collapse3"
                    aria-expanded="true"><i class="ion-ios-arrow-down"></i></a>

                <a href="#" data-toggle="refresh" onclick="tabla_inspeccion_vehiculo()" class="btn card-refresh"><i
                        class="ion-android-refresh"></i></a>

                <a href="#" data-toggle="expand" class="btn card-expand"><i class="ion-android-expand"></i></a>
                <a href="#" data-toggle="remove" class="btn card-remove"><i class="ion-android-close"></i></a>
            </div>
        </div>
        <div class="card-body collapse show" id="collapse3">
            <div class="row">

                </button>
                <div class="mg-20 form-inline wd-100p">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Estado</label>
                            <select id="foo-filter-status" class="form-control">
                                <option value="">Mostrar todos</option>
                                <option value="Sin revisar">Sin revisar</option>
                                <option value="Revisado">Revisados</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group ft-right">
                            <input id="foo-search" type="text" placeholder="Buscar inspección..." class="form-control"
                                autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div id="tabla_inspeccion"></div>
            <div id="estado_inspeccion"></div>
        </div>
    </div>
</div>


<div class="modal show" id="ver_inspeccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_2">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel_2">Inspección completa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ion-ios-close-empty"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div data-scrollbar-shown="true" data-scrollable="true" data-height="300"
                    style="height: 300px; overflow: hidden; overflow-y: auto;">

                    <div id="editar_inspeccion"></div>

                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps__rail-y" style="top: 0px; right: 4px;">
                        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                    </div>
                </div>
            </div>
            <div id="respuesta_form_empleado"></div>
            <div class="modal-footer">
                <div id="respuesta_actulizar_inspeccion"></div>

                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>

<?php include "../footer/footer.php" ?>

<script>
// ///////////////////////////////////////Row Toggler
$("#foo-row-toggler").footable();

// Accordion
$("#foo-accordion")
    .footable()
    .on("footable_row_expanded", function(e) {
        $("#foo-accordion tbody tr.footable-detail-show")
            .not(e.row)
            .each(function() {
                $("#foo-accordion").data("footable").toggleDetail(this);
            });
    });
// Filtering
var filtering = $("#foo-filtering");
filtering.footable().on("footable_filtering", function(e) {
    var selected = $("#foo-filter-status").find(":selected").val();
    e.filter += e.filter && e.filter.length > 0 ? " " + selected : selected;
    e.clear = !e.filter;
});

// Filter status
$("#foo-filter-status").change(function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});

// Search input
$("#foo-search").on("input", function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});
</script>


<script>
window.load = tabla_inspeccion_vehiculo();

////////// Inspeccion vehiculos //////////////

function editar_inspeccion(id) {
    var url = "../../actions/actions_admin/editar_inspeccion_vehiculos.php?id_inspeccion=" + id;

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#editar_inspeccion").html("Cargando...");
        },
        success: function(data) {
            $("#editar_inspeccion").html(data);
        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });
}

function actualizar_inspeccion_estado(id_inspeccion, id_empleado, nombre_empleado) {

    var opcion = confirm("¿Estás seguro de realizar esta acción?");

    if (opcion == true) {
        var url = "../../actions/actions_admin/actualizar_inspeccion_vehiculo.php?id_inspeccion=" + id_inspeccion +
            "&idetificacion_empleado=" + id_empleado + '&nombre_empleado=' + nombre_empleado;

        $.ajax({
            cache: false,
            async: false,
            url: url,
            beforeSend: function() {
                $("#editar_inspeccion").html("Cargando...");
            },
            success: function(data) {
                $("#editar_inspeccion").html(data);
            },
            error: function() {
                alert("Error, por favor intentalo más tarde.");
            },
        });
    } else {
        fadeOut();
    }
}


function tabla_inspeccion_vehiculo() {
    var url = "../../actions/actions_admin/inspeccion_vehiculos.php";

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#tabla_inspeccion").html("Cargando...");
        },
        success: function(data) {
            $("#tabla_inspeccion").html(data);
        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });
}
</script>



<script src="../assets/plugins/datepicker/js/datepicker.min.js"></script>
<script src="../assets/plugins/datepicker/js/datepicker.es.js"></script>