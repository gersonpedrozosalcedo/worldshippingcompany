<?php

use function PHPSTORM_META\type;

include "../header/header.php";
include '../database/database.php';
$id_servicios = $_GET["id_servicios"];
$type = $_GET["type"];

if ($type == 3) {
    $consultar_servicio_evidencia = $conn->prepare("SELECT * FROM evidencia_servicios WHERE id_servicio = '$id_servicios'");
    $consultar_servicio_evidencia->execute();
    $consultar_servicio_evidencia = $consultar_servicio_evidencia->fetchAll(PDO::FETCH_ASSOC);
    $ruta = "foto_evidencia";
} else if ($type == 5) {
    $consultar_servicio_evidencia = $conn->prepare("SELECT * FROM evidencia_servicios_devolucion_v WHERE id_servicio = '$id_servicios'");
    $consultar_servicio_evidencia->execute();
    $consultar_servicio_evidencia = $consultar_servicio_evidencia->fetchAll(PDO::FETCH_ASSOC);
    $ruta = "foto_evidencia_devolucion_vacio";
}

?>

<link type="text/css" rel="stylesheet" href="../assets/plugins/baguetteBox/baguetteBox.min.css">
<link type="text/css" rel="stylesheet" href="../assets/plugins/viewer/css/viewer.css">
<link type="text/css" rel="stylesheet" href="../assets/plugins/viewer/css/main.css">
<link type="text/css" rel="stylesheet" href="../assets/plugins/photoswipe/photoswipe.css">
<link type="text/css" rel="stylesheet" href="../assets/plugins/photoswipe/default-skin/default-skin.css">


<div class="card mg-b-20">
    <div class="card-header">
        <center>Evidencias del servicio</center>
    </div>
    <div class="card-body collapse show" id="collapse3">
        <div id="eliminando_evidencia"></div>
        <div class="tz-gallery">
            <div style="height: 90vh; overflow-y: auto; overflow-x: auto;">
                <div class="row">

                    <?php foreach ($consultar_servicio_evidencia as $evidencia_servicio) { ?>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail">
                            <a class="lightbox"
                                href="../<?php echo $ruta ?>/<?php echo $evidencia_servicio['id_servicio'] ?>/<?php echo $evidencia_servicio['foto'] ?>">
                                <img class="img-thumbnail img-fluid mg-5"
                                    style="height: 350px;width: 350px;object-fit: cover;"
                                    src="../<?php echo $ruta ?>/<?php echo $evidencia_servicio['id_servicio'] ?>/<?php echo $evidencia_servicio['foto'] ?>"
                                    alt="">
                            </a>
                            <div class="caption">
                                <h3
                                    style=" text-overflow: ellipsis; /* puntos suspensivos */ white-space: nowrap; /* no permite multilinea */ overflow: hidden; /* ocultar el resto */">
                                    <?php echo $evidencia_servicio['foto'] ?></h3>
                                <a href="javascript:void(0)" class="btn btn-danger btn-sm"
                                    onclick="eliminar_evidencia(<?php echo $evidencia_servicio['id'] ?>,<?php echo $type ?>)">Eliminar</a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>

</div>



<script src="../assets/plugins/baguetteBox/baguetteBox.min.js"></script>
<script src="../assets/plugins/viewer/js/viewer.js"></script>
<script src="../assets/plugins/viewer/js/common.js"></script>
<script src="../assets/plugins/viewer/js/main.js"></script>
<script src="../assets/plugins/photoswipe/photoswipe.min.js"></script>
<script src="../assets/plugins/photoswipe/photoswipe-ui-default.min.js"></script>

<script>
//baguetteBox Gallery 
baguetteBox.run('.tz-gallery', {
    noScrollbars: true
});

function eliminar_evidencia(id_evidencia, type) {
    var opcion = confirm("¿Estás seguro de realizar esta acción?");

    if (opcion == true) {
        var url =
            "../actions/actions_admin/eliminar_evidencia_servicio.php?id_evidencia=" +
            id_evidencia + '&type=' + type;

        $.ajax({
            cache: false,
            async: false,
            url: url,
            beforeSend: function() {

                $("#eliminando_evidencia").html(
                    '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Eliminando, por favor espere...</button>'
                );
            },
            success: function(data) {

                setTimeout(function() {
                    $("#eliminando_evidencia").html(data);
                }, 3000);
            },
            error: function() {
                alert("Error, por favor intentalo más tarde.");
            },
        });
    } else {
        fadeOut();
    }
}
</script>
<?php include "../footer/footer.php" ?>