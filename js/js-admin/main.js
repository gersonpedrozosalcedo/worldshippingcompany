/*************************   INICIO */

function iniciar_sesion_admin() {
  var url = "../../actions/actions_admin/login_action.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    data: $("#form_inicio_sesion").serialize(),
    beforeSend: function () {
      $("#respuesta_login").html("Cargando...");
      toastr.error("POR FAVOR ESPERE...", "HOLA!");
    },
    success: function (data) {
      $("#respuesta_login").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function cargar_ultimos_servicios_admin() {
  // alert("entro");
  var accion = "ultimo10";
  var url =
    "../../actions/actions_admin/consultar_servicios.php?accion=" + accion;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#tabla_servicios").html("Cargando...");
    },
    success: function (data) {
      $("#tabla_servicios").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}
/*************************   CLIENTES */

function tabla_clientes() {
  var url = "../../actions/actions_admin/consultar_clientes.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#tabla_clientes").html("Cargando...");
    },
    success: function (data) {
      $("#tabla_clientes").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function guardar_cliente() {
  var url = "../../actions/actions_admin/guardar_cliente.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    type: "POST",
    data: new FormData($("#form_guardar_cliente")[0]),
    contentType: false,
    processData: false,
    beforeSend: function () {
      $("#respuesta_form_cliente").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_form_cliente").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function editar_cliente(id_cliente) {
  var url =
    "../../actions/actions_admin/editar_clientes.php?id_cliente=" + id_cliente;

  $.ajax({
    cache: false,
    async: false,
    url: url,

    beforeSend: function () {
      $("#editar_clientes").html("Cargando...");
    },
    success: function (data) {
      $("#editar_clientes").html(data);
      ejecutar_email_clientes();
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}
function actualizar_estado_sub_cliente(id_sub_cliente, estado) {
  var url =
    "../../actions/actions_admin/cambiar_estado_sub_cliente.php?id_sub_cliente=" +
    id_sub_cliente +
    "&estado=" +
    estado;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#cambiar_estado_sub_clientes").html("Cargando...");
    },
    success: function (data) {
      $("#cambiar_estado_sub_clientes").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function actualizar_estado_email_cco(id_email_cco, estado) {
  var url =
    "../../actions/actions_admin/cambiar_estado_email_cco.php?id_email_cco=" +
    id_email_cco +
    "&estado=" +
    estado;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#cambiar_estado_sub_clientes").html("Cargando...");
    },
    success: function (data) {
      $("#cambiar_estado_sub_clientes").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}
function actualizar_cliente() {
  var url = "../../actions/actions_admin/actualizar_cliente.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    type: "POST",
    data: new FormData($("#form_editar_cliente")[0]),
    contentType: false,
    processData: false,
    beforeSend: function () {
      $("#respuesta_form_actualizar_cliente").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_form_actualizar_cliente").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function cambiar_estado_empleado(id_cliente, estado) {
  var mensaje;
  var opcion = confirm("¿Estás seguro de realizar esta acción?");

  if (opcion == true) {
    var url =
      "../../actions/actions_admin/cambiar_estado_empleado.php?id_cliente=" +
      id_cliente +
      "&estado_empleado=" +
      estado;

    $.ajax({
      cache: false,
      async: false,
      url: url,
      beforeSend: function () {
        $("#estado_empleado").html("Cargando...");
      },
      success: function (data) {
        $("#estado_empleado").html(data);
      },
      error: function () {
        alert("Error, por favor intentalo más tarde.");
      },
    });
  } else {
    fadeOut();
  }
}

///// email copia ////
function eliminar_email_copia(id_email_copia) {
  //alert(id_email_copia);

  var url =
    "../../actions/actions_admin/eliminar_email_copia.php?id_email_copia=" +
    id_email_copia;

  var opcion = confirm("¿Estás seguro de realizar esta acción?");

  if (opcion == true) {
    $.ajax({
      cache: false,
      async: false,
      url: url,
      beforeSend: function () {
        $("#eliminando_email_copia").html(
          '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Eliminando, por favor espere...</button>'
        );
        $("#eliminando_email_copia").hide();
      },
      success: function (data) {
        $("#eliminando_email_copia").html(data);
      },
      error: function () {
        alert("Error, por favor intentalo más tarde.");
      },
    });
  } else {
    fadeOut();
  }
}

/// subcliente ////////
function eliminar_sub_cliente(id_sub_cliente) {
  //alert(id_sub_cliente);

  var url =
    "../../actions/actions_admin/eliminar_sub_cliente.php?id_sub_cliente=" +
    id_sub_cliente;

  var opcion = confirm("¿Estás seguro de realizar esta acción?");

  if (opcion == true) {
    $.ajax({
      cache: false,
      async: false,
      url: url,
      beforeSend: function () {
        $("#eliminando_sub_cliente").html(
          '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Eliminando, por favor espere...</button>'
        );
        $("#eliminando_sub_cliente").hide();
      },
      success: function (data) {
        $("#eliminando_sub_cliente").html(data);
      },
      error: function () {
        alert("Error, por favor intentalo más tarde.");
      },
    });
  } else {
    fadeOut();
  }
}

/////////////// eliminar subclientes ////////////////

function eliminar_cliente(id_cliente) {
  // alert(id_cliente);
  var url =
    "../../actions/actions_admin/eliminar_cliente.php?id_cliente=" + id_cliente;

  var opcion = confirm("¿Estás seguro de realizar esta acción?");

  if (opcion == true) {
    $.ajax({
      cache: false,
      async: false,
      url: url,
      beforeSend: function () {
        $("#eliminando_cliente").html(
          '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Eliminando, por favor espere...</button>'
        );
        $("#eliminando_cliente").hide();
      },
      success: function (data) {
        $("#eliminando_cliente").html(data);
      },
      error: function () {
        alert("Error, por favor intentalo más tarde.");
      },
    });
  } else {
    fadeOut();
  }
}

/*************************   EMPLEADOS */
function tabla_empleados() {
  var url = "../../actions/actions_admin/consultar_empleados.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#tabla_empleados").html("Cargando...");
    },
    success: function (data) {
      $("#tabla_empleados").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function guardar_empleado() {
  var url = "../../actions/actions_admin/guardar_empleado.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    type: "POST",
    data: new FormData($("#form_guardar_empleado")[0]),
    contentType: false,
    processData: false,
    beforeSend: function () {
      $("#respuesta_form_empleado").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_form_empleado").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}
function editar_empleado(id_empleado) {
  var url =
    "../../actions/actions_admin/editar_empleados.php?id_empleado=" +
    id_empleado;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#editar_empleados").html("Cargando...");
    },
    success: function (data) {
      $("#editar_empleados").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function actualizar_empleado() {
  var url = "../../actions/actions_admin/actualizar_empleado.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    type: "POST",
    data: new FormData($("#form_editar_empleado")[0]),
    contentType: false,
    processData: false,
    beforeSend: function () {
      $("#respuesta_form_actualizar_empleado").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_form_actualizar_empleado").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function cambiar_estado_empleado(id_empleado, estado) {
  var mensaje;
  var opcion = confirm("¿Estás seguro de realizar esta acción?");

  if (opcion == true) {
    var url =
      "../../actions/actions_admin/cambiar_estado_empleado.php?id_empleado=" +
      id_empleado +
      "&estado_empleado=" +
      estado;

    $.ajax({
      cache: false,
      async: false,
      url: url,
      beforeSend: function () {
        $("#estado_empleado").html("Cargando...");
      },
      success: function (data) {
        $("#estado_empleado").html(data);
      },
      error: function () {
        alert("Error, por favor intentalo más tarde.");
      },
    });
  } else {
    fadeOut();
  }
}

/////////////// eliminar empleado ////////////////

function eliminar_empleado(id_empleado) {
  // alert(id_cliente);
  var url =
    "../../actions/actions_admin/eliminar_empleado.php?id_empleado=" +
    id_empleado;

  var opcion = confirm("¿Estás seguro de realizar esta acción?");

  if (opcion == true) {
    $.ajax({
      cache: false,
      async: false,
      url: url,
      beforeSend: function () {
        $("#eliminando_empleado").html(
          '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Eliminando, por favor espere...</button>'
        );
        $("#eliminando_empleado").hide();
      },
      success: function (data) {
        $("#eliminando_empleado").html(data);
      },
      error: function () {
        alert("Error, por favor intentalo más tarde.");
      },
    });
  } else {
    fadeOut();
  }
}

/*************************   CONDUCTORES */
function tabla_conductores() {
  var url = "../../actions/actions_admin/consultar_conductores.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#tabla_conductores").html("Cargando...");
    },
    success: function (data) {
      $("#tabla_conductores").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function guardar_conductor() {
  var url = "../../actions/actions_admin/guardar_conductor.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    type: "POST",
    data: new FormData($("#form_guardar_conductor")[0]),
    contentType: false,
    processData: false,
    beforeSend: function () {
      $("#respuesta_form_conductor").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_form_conductor").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}
function editar_conductor(id_conductor) {
  var url =
    "../../actions/actions_admin/editar_conductor.php?id_conductor=" +
    id_conductor;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#editar_conductores").html("Cargando...");
    },
    success: function (data) {
      $("#editar_conductores").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function actualizar_conductor() {
  var url = "../../actions/actions_admin/actualizar_conductor.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    type: "POST",
    data: new FormData($("#form_editar_conductor")[0]),
    contentType: false,
    processData: false,
    beforeSend: function () {
      $("#respuesta_form_actualizar_conductor").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_form_actualizar_conductor").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function cambiar_estado_conductor(id_conductor, estado) {
  var mensaje;
  var opcion = confirm("¿Estás seguro de realizar esta acción?");

  if (opcion == true) {
    var url =
      "../../actions/actions_admin/cambiar_estado_conductor.php?id_conductor=" +
      id_conductor +
      "&estado_conductor=" +
      estado;

    $.ajax({
      cache: false,
      async: false,
      url: url,
      beforeSend: function () {
        $("#estado_conductores").html("Cargando...");
      },
      success: function (data) {
        $("#estado_conductores").html(data);
      },
      error: function () {
        alert("Error, por favor intentalo más tarde.");
      },
    });
  } else {
    fadeOut();
  }
}

/////////////// eliminar empleado ////////////////

function eliminar_conductor(id_conductor) {
  // alert(id_cliente);
  var url =
    "../../actions/actions_admin/eliminar_conductor.php?id_conductor=" +
    id_conductor;

  var opcion = confirm("¿Estás seguro de realizar esta acción?");

  if (opcion == true) {
    $.ajax({
      cache: false,
      async: false,
      url: url,
      beforeSend: function () {
        $("#eliminando_conductor").html(
          '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Eliminando, por favor espere...</button>'
        );
        $("#eliminando_conductor").hide();
      },
      success: function (data) {
        $("#eliminando_conductor").html(data);
      },
      error: function () {
        alert("Error, por favor intentalo más tarde.");
      },
    });
  } else {
    fadeOut();
  }
}

/*************************   SERVICIOS */
function tabla_servicios() {
  var url = "../../actions/actions_admin/consultar_servicios.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#tabla_servicios").html("Cargando...");
    },
    success: function (data) {
      $("#tabla_servicios").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function formulario_añadir_servicio() {
  var url = "../../actions/actions_admin/formulario_servicios.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#formulario_añadir_servicio").html("Cargando...");
    },
    success: function (data) {
      $("#formulario_añadir_servicio").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function consultar_subcliente() {
  var id_cliente = document.getElementById("cliente").value;
  var url =
    "../../actions/actions_admin/consultar_subcliente.php?id_cliente=" +
    id_cliente;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#sub_clientes").html("Cargando...");
    },
    success: function (data) {
      $("#sub_clientes").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function eliminar_servicio(id_servicio) {
  var url =
    "../../actions/actions_admin/eliminar_servicios.php?id_servicio=" +
    id_servicio;

  var opcion = confirm("¿Estás seguro de realizar esta acción?");

  if (opcion == true) {
    $.ajax({
      cache: false,
      async: false,
      url: url,
      beforeSend: function () {
        $("#eliminando_servicio").html(
          '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Eliminando, por favor espere...</button>'
        );
      },
      success: function (data) {
        setTimeout(function () {
          $("#eliminando_servicio").html(data);
        }, 2000);
      },
      error: function () {
        alert("Error, por favor intentalo más tarde.");
      },
    });
  } else {
    fadeOut();
  }
}

function finalizar_servicio(id_servicio) {
  var costo = prompt("Ingrese el costo del servicio", "");
  var url =
    "../../actions/actions_admin/actualizar_estado_servicio.php?id_servicio=" +
    id_servicio +
    "&costo=" +
    costo;

  var opcion = confirm("¿Estás seguro de realizar esta acción?");

  if (opcion == true) {
    $.ajax({
      cache: false,
      async: false,
      url: url,
      beforeSend: function () {
        $("#respuesta_form_servicio_edit").html(
          '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Cargando, por favor espere...</button>'
        );
      },
      success: function (data) {
        setTimeout(function () {
          $("#respuesta_form_servicio_edit").html(data);
        }, 2000);
      },
      error: function () {
        alert("Error, por favor intentalo más tarde.");
      },
    });
  } else {
    fadeOut();
  }
}

//////////////// ADMINISTRADOR //////////

function guardar_conductor() {
  var url = "../../actions/actions_admin/guardar_conductor.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    type: "POST",
    data: new FormData($("#form_guardar_conductor")[0]),
    contentType: false,
    processData: false,
    beforeSend: function () {
      $("#respuesta_form_conductor").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_form_conductor").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}
function editar_conductor(id_conductor) {
  var url =
    "../../actions/actions_admin/editar_conductor.php?id_conductor=" +
    id_conductor;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#editar_conductores").html("Cargando...");
    },
    success: function (data) {
      $("#editar_conductores").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function actualizar_conductor() {
  var url = "../../actions/actions_admin/actualizar_conductor.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    type: "POST",
    data: new FormData($("#form_editar_conductor")[0]),
    contentType: false,
    processData: false,
    beforeSend: function () {
      $("#respuesta_form_actualizar_conductor").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_form_actualizar_conductor").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function cambiar_estado_conductor(id_conductor, estado) {
  var mensaje;
  var opcion = confirm("¿Estás seguro de realizar esta acción?");

  if (opcion == true) {
    var url =
      "../../actions/actions_admin/cambiar_estado_conductor.php?id_conductor=" +
      id_conductor +
      "&estado_conductor=" +
      estado;

    $.ajax({
      cache: false,
      async: false,
      url: url,
      beforeSend: function () {
        $("#estado_conductores").html("Cargando...");
      },
      success: function (data) {
        $("#estado_conductores").html(data);
      },
      error: function () {
        alert("Error, por favor intentalo más tarde.");
      },
    });
  } else {
    fadeOut();
  }
}
///////////////////////
function guardar_form_1() {
  var url = "../../actions/actions_admin/guardar_servicio_form_1.php";

  $.ajax({
    cache: false,
    async: false,
    type: "POST",
    url: url,
    data: new FormData($("#form_1")[0]),
    contentType: false,
    processData: false,
    beforeSend: function () {
      $("#respuesta_form_servicio_inicio").html(
        '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Guardando, por favor espere...</button>'
      );
    },
    success: function (data) {
      setTimeout(function () {
        $("#respuesta_form_servicio_inicio").html(data);
        console.log(data);
      }, 3000);
      //document.getElementById("id_servicio_form2").click();
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function guardar_form_2() {
  // alert();
  var url = "../../actions/actions_admin/guardar_servicio_form_2.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    data: $("#form_2").serialize(),
    beforeSend: function () {
      $("#respuesta_form_servicio_edit").html(
        '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Guardando, por favor espere...</button>'
      );
      /* document.getElementById("id_servicio_form3").click();
      document.getElementById("id_servicio_form5").click();*/
    },
    success: function (data) {
      setTimeout(function () {
        $("#respuesta_form_servicio_edit").html(data);
      }, 3000);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function guardar_form_3() {
  var url = "../../actions/actions_admin/guardar_servicio_form_3.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    type: "POST",
    data: new FormData($("#form_3")[0]),
    contentType: false,
    processData: false,
    beforeSend: function () {
      $("#respuesta_form_servicio_edit").html(
        '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Guardando, por favor espere...</button>'
      );
    },
    success: function (data) {
      setTimeout(function () {
        $("#respuesta_form_servicio_edit").html(data);
      }, 3000);
      document.getElementById("id_servicio_form4").click();
      pallets();
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function guardar_form_4() {
  var url = "../../actions/actions_admin/guardar_servicio_form_4.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    type: "POST",
    data: new FormData($("#form_4")[0]),
    contentType: false,
    processData: false,
    beforeSend: function () {
      $("#respuesta_form_servicio_edit").html(
        '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Guardando, por favor espere...</button>'
      );
    },
    success: function (data) {
      setTimeout(function () {
        $("#respuesta_form_servicio_edit").html(data);
        document.getElementById("id_servicio_form5").click();
      }, 3000);
      pallets();
      bultos();
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function guardar_form_5() {
  var url = "../../actions/actions_admin/guardar_servicio_form_5.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    type: "POST",
    data: new FormData($("#form_5")[0]),
    contentType: false,
    processData: false,
    beforeSend: function () {
      $("#respuesta_form_servicio_edit").html(
        '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Guardando, por favor espere...</button>'
      );
    },
    success: function (data) {
      setTimeout(function () {
        $("#respuesta_form_servicio_edit").html(data);
      }, 3000);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function despachos() {
  var url = "../../actions/actions_admin/acordeon_despacho_parcial_total.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#despacho_parcial_totals").html("Cargando...");
    },
    success: function (data) {
      $("#despacho_parcial_totals_servicios").html(data);
      //console.log(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function resumen() {
  var url = "../../actions/actions_admin/resumen_servicio.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#resumen_servicio").html("Cargando...");
    },
    success: function (data) {
      $("#resumen_servicio").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function editar_servicio(id_servicio) {
  //alert();
  var modals = document.getElementById("editar_servicio");
  modals.style.display = "block";
  var url =
    "../../actions/actions_admin/editar_servicios.php?id_servicio=" +
    id_servicio;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#editar_servicioss").html("Cargando...");
    },
    success: function (data) {
      $("#editar_servicioss").html(data);

      despachos_servicios(id_servicio);
      pallets();
      tipos_despachos();
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
  resumen_servicios(id_servicio);
  /* validar_cantidad_despachos();
  validar_cantidad_pallets();
  validar_cantidad_pesos();*/
}

function pallets() {
  var suma_pallets_despachados = document.getElementById(
    "suma_pallets_despachados"
  ).value;

  var cantidad_p_total = document.getElementById("cantidad_bpallets").value;
  var pallets_disponibles_por_despachar =
    cantidad_p_total - suma_pallets_despachados;

  if (pallets_disponibles_por_despachar == 0) {
    document.getElementById("pallets").innerHTML = "Todo ha sido despachado.";
  } else {
    document.getElementById("pallets").innerHTML =
      "Cantidad restante por despachar: <b>(" +
      pallets_disponibles_por_despachar +
      ")</b>";
  }
  bultos(pallets_disponibles_por_despachar);
}

function bultos(pallets_disponibles_por_despachar) {
  var suma_bultos_despachados = document.getElementById(
    "suma_bultos_despachados"
  ).value;
  // alert(suma_bultos_despachados);
  var cantidad_b_total = document.getElementById("cantidad_bultos").value;
  var bultos_disponibles_por_despachar =
    cantidad_b_total - suma_bultos_despachados;

  if (bultos_disponibles_por_despachar == 0) {
    document.getElementById("bultos_contador").innerHTML =
      "Todo ha sido despachado.";
  } else {
    document.getElementById("bultos_contador").innerHTML =
      "Cantidad restante por despachar: <b>(" +
      bultos_disponibles_por_despachar +
      ")</b>";
  }

  if (
    pallets_disponibles_por_despachar == 0 &&
    bultos_disponibles_por_despachar == 0
  ) {
    document.getElementById("btn_agregar_datos").disabled = true;
  }
}

function actualizar_form_1() {
  //  alert();
  var url = "../../actions/actions_admin/actualizar_formularios_actions.php";

  $.ajax({
    cache: false,
    async: false,
    type: "POST",
    url: url,
    data: $("#form_editar_1").serialize(),
    beforeSend: function () {
      $("#respuesta_form_servicio_edit").html(
        '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Guardando, por favor espere...</button>'
      );
    },
    success: function (data) {
      setTimeout(function () {
        $("#respuesta_form_servicio_edit").html(data);
      }, 3000);
      document.getElementById("id_servicio_form5").click();
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function despachos_servicios(id_servicio) {
  var url =
    "../../actions/actions_admin/acordeon_despacho_parcial_total_servicio.php?id_servicio=" +
    id_servicio;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#despacho_parcial_totals_servicios").html("Cargando...");
    },
    success: function (data) {
      $("#despacho_parcial_totals_servicios").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function resumen_servicios(id_servicio) {
  var url =
    "../../actions/actions_admin/resumen_servicio_editar.php?id_servicio=" +
    id_servicio;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#resumen_servicio_editar").html("Cargando...");
    },
    success: function (data) {
      $("#resumen_servicio_editar").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function ocultar_capo_cantidad() {
  var tipo_carga = document.getElementById("tipo_carga").value;
  // alert(tipo_carga);

  if (tipo_carga == 5) {
    document.getElementById("tipo_carga_form_2_campos").innerHTML =
      '<div class="row"><div class="col-12 col-sm-6"> <label>Cantidad pallets recibidos</label>' +
      '<input type="number" class="form-control" id="cantidad_pallets_despacho_item" name="cantidad_pallets_despacho" placeholder=""> </div>' +
      '<div class="col-12 col-sm-6"><label>Cantidad bultos recibidos</label>' +
      '<input type="text" class="form-control" name="cantidad_bulto_despachado" id="cantidad_bulto_despachado"></div></div>';
  } else if (tipo_carga == 4) {
    document.getElementById("tipo_carga_form_2_campos").innerHTML =
      '<div class="row"><div class="col-12 col-sm-12"> <label>Cantidad paquetes recibidos</label>' +
      '<input type="number" class="form-control" id="cantidad_pallets_despacho_item" name="cantidad_pallets_despacho" placeholder=""> </div></div>';
  } else if (tipo_carga == 3) {
    document.getElementById("tipo_carga_form_2_campos").innerHTML =
      '<div class="row"><div class="col-12 col-sm-12"> <label>Cantidad carga suelta recibidos</label>' +
      '<input type="number" class="form-control" id="cantidad_pallets_despacho_item" name="cantidad_pallets_despacho" placeholder=""> </div></div>';
  } else if (tipo_carga == 2) {
    document.getElementById("tipo_carga_form_2_campos").innerHTML =
      '<div class="row"> <div class="col-12 col-sm-12"> <label>Cantidad bultos recibidos</label>' +
      '<input type="text" class="form-control" name="cantidad_bulto_despachado" id="cantidad_bulto_despachado"></div></div>';
  } else if (tipo_carga == 1) {
    document.getElementById("tipo_carga_form_2_campos").innerHTML =
      '<div class="row"><div class="col-12 col-sm-12"> <label>Cantidad pallets recibidos</label>' +
      '<input type="number" class="form-control" id="cantidad_pallets_despacho_item" name="cantidad_pallets_despacho" placeholder=""> </div></div>';
  }
}

/********* desahabilitar forms */

function desahabilitar_campos(tipo_form) {
  if (tipo_form == "form_1") {
    document.getElementById("fecha_recepcion_doc").disabled = true;
    document.getElementById("cliente").disabled = true;
    document.getElementById("sub_cliente").disabled = true;
    document.getElementById("dons").disabled = true;
    document.getElementById("tipo_servicios").disabled = true;
    document.getElementById("tipo_carga").disabled = true;
    document.getElementById("contenedor").disabled = true;
    document.getElementById("tamaño_contenedor").disabled = true;
    document.getElementById("linea_naviera").disabled = true;
    document.getElementById("recepcion_doc").disabled = true;
    document.getElementById("origen").disabled = true;
    document.getElementById("lugar_descargue").disabled = true;
    document.getElementById("fecha_vencimiento_b_p").disabled = true;
    //  document.getElementById("fecha_vencimiento_t_zf").disabled = true;
    //  document.getElementById("fecha_hora_r_p").disabled = true;
    document.getElementById("fecha_devolucion_v").disabled = true;
    document.getElementById("impoexpo").disabled = true;
    resumen();
    resumen_servicios();
  } else if (tipo_form == "form_2") {
    document.getElementById("placa_vehiculo_c").disabled = true;
    document.getElementById("nombre_conductor").disabled = true;
    document.getElementById("identificacion_conductor").disabled = true;
    document.getElementById("ruta").disabled = true;
    resumen();
    resumen_servicios();
  } else if (tipo_form == "form_3") {
    document.getElementById("fecha_hora_descargue").disabled = true;
    document.getElementById("fecha_hora_t_descargue").disabled = true;
    // document.getElementById("cantidad_bultos").disabled = true;
    document.getElementById("cantidad_bpallets").disabled = true;
    document.getElementById("aplica_almacenamiento").disabled = true;
    document.getElementById("numero_dia_almacenaje").disabled = true;
    document.getElementById("dia_almacenaje_libre").disabled = true;
    document.getElementById("cubicaje").disabled = true;
    document.getElementById("observaciones_carga").disabled = true;
    /* despachos(); */
    resumen();
    resumen_servicios();
  } else if (tipo_form == "form_4") {
    /* document.getElementById("despacho_parcial_total").disabled = true;*/
    document.getElementById("fecha_hora_despacho").value = "";
    document.getElementById("transportadora").value = "";
    document.getElementById("nombre_conductor_t").value = "";
    document.getElementById("doc_conductor_t").value = "";
    document.getElementById("placa_v_conductor_t").value = "";
    document.getElementById("cantidad_pallets_despacho_item").value = "";

    //document.getElementById("form_4").reset();
    despachos();
    var id_servicio = document.getElementById("id_servicio_form4").value;
    despachos_servicios(id_servicio);
    //alert(id_servicio);
    resumen();
    resumen_servicios(id_servicio);
    pallets();
    validar_cantidad_pallets();
  } else if (tipo_form == "form_5") {
    document.getElementById("lugar_devolucion_v").disabled = true;
    document.getElementById("fecha_cita_devolucion_v").disabled = true;
    document.getElementById("nombre_condcutor_v").disabled = true;
    document.getElementById("doc_condcutor_v").disabled = true;
    document.getElementById("palca_condcutor_v").disabled = true;
    resumen();
    var id_servicio = document.getElementById("id_servicio_form2").value;
    resumen_servicios(id_servicio);
  }
}

/***************** tipos de servicios */

function cargar_tipos_de_servicios() {
  var url = "../../actions/actions_admin/tipos_servicio.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#tabla_tipos_servicios").html("Cargando...");
    },
    success: function (data) {
      $("#tabla_tipos_servicios").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
  document.getElementById("form_tipo_servicio").reset();
}

function guardar_tipo_servicio() {
  var url = "../../actions/actions_admin/guardar_tipo_servicio.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    data: $("#form_tipo_servicio").serialize(),
    beforeSend: function () {
      $("#respuesta_form_tipo_servicio").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_form_tipo_servicio").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function editar_tipo_servicio(id_tipo_servicio) {
  var url =
    "../../actions/actions_admin/editar_tipo_servicio.php?id_tipo_servicio=" +
    id_tipo_servicio;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#editar_tipo_servicio").html("Cargando...");
    },
    success: function (data) {
      $("#editar_tipo_servicio").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function actualizar_tipos_servicios() {
  var url = "../../actions/actions_admin/actualizar_tipo_servicio.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    data: $("#editar_form_tipo_servicio").serialize(),
    beforeSend: function () {
      $("#respuesta_actulizar_tipo_servicio").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_actulizar_tipo_servicio").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

/***************** tipos de cargas */

function cargar_tipos_de_cargas() {
  var url = "../../actions/actions_admin/tipos_carga.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#tabla_tipos_cargas").html("Cargando...");
    },
    success: function (data) {
      $("#tabla_tipos_cargas").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
  document.getElementById("form_tipo_carga").reset();
}

function guardar_tipo_carga() {
  var url = "../../actions/actions_admin/guardar_tipo_carga.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    data: $("#form_tipo_carga").serialize(),
    beforeSend: function () {
      $("#respuesta_form_tipo_carga").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_form_tipo_carga").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function editar_tipo_carga(id_tipo_carga) {
  var url =
    "../../actions/actions_admin/editar_tipo_carga.php?id_tipo_carga=" +
    id_tipo_carga;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#editar_tipo_carga").html("Cargando...");
    },
    success: function (data) {
      $("#editar_tipo_carga").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function actualizar_tipos_cargas() {
  var url = "../../actions/actions_admin/actualizar_tipo_carga.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    data: $("#editar_form_tipo_carga").serialize(),
    beforeSend: function () {
      $("#respuesta_actulizar_tipo_carga").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_actulizar_tipo_carga").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

/***************** tipos de rutas */

function cargar_tipos_de_rutas() {
  var url = "../../actions/actions_admin/tipos_ruta.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#tabla_tipos_rutas").html("Cargando...");
    },
    success: function (data) {
      $("#tabla_tipos_rutas").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
  document.getElementById("form_tipo_ruta").reset();
}

function guardar_tipo_ruta() {
  var url = "../../actions/actions_admin/guardar_tipo_ruta.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    data: $("#form_tipo_ruta").serialize(),
    beforeSend: function () {
      $("#respuesta_form_tipo_ruta").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_form_tipo_ruta").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function editar_tipo_ruta(id_tipo_ruta) {
  var url =
    "../../actions/actions_admin/editar_tipo_ruta.php?id_tipo_ruta=" +
    id_tipo_ruta;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#editar_tipo_ruta").html("Cargando...");
    },
    success: function (data) {
      $("#editar_tipo_ruta").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function actualizar_tipos_rutas() {
  var url = "../../actions/actions_admin/actualizar_tipo_ruta.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    data: $("#editar_form_tipo_ruta").serialize(),
    beforeSend: function () {
      $("#respuesta_actulizar_tipo_ruta").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_actulizar_tipo_ruta").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

////////////////////// reporte en excel ////////

function generar_excel_servicios() {
  // alert(id_servicio);
  var fecha = document.getElementById("datePicker4").value;
  var id_servicio = document.getElementById("select_servicios").value;

  if (id_servicio === " ") {
    toastr.error("Por favor selecciona un servicio", "Hola!");
  } else if (fecha === "") {
    toastr.error("Por favor seleccina un rango de fechas", "Hola!");
  } else {
    toastr.success("Por favor espere...", "Hola!");
    setTimeout(function () {
      window.location.href =
        "../../actions/actions_admin/descargar_reporte.php?rango_fecha=" +
        fecha +
        "&id_servicio=" +
        id_servicio;
    }, 3000);
  }

  /*
  var url =
    "../../actions/actions_admin/descargar_reporte.php?rango_fecha=" +
    fecha +
    "&id_servicio=" +
    id_servicio;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#respuesta_generador").html(
        '<button class="btn btn-primary btn-sm"  type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Generando, por favor espere...</button>'
      );
    },
    success: function (data) {
      let blob = new Blob([data], {
        type: "application/octetstream",
      });

      let a = document.createElement("a");
      a.href = window.URL.createObjectURL(blob);
      a.download = "Reporte_servicios.xls";

      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
      window.URL.removeObjectURL(a.href);

      setTimeout(function () {
        $("#respuesta_generador").html(data);
      }, 3000);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
  */
}

function generar_excel_servicios_unico(id_servicio) {
  // alert(id_servicio);
  if (id_servicio === " ") {
    toastr.error("Por favor selecciona un servicio", "Hola!");
  } else {
    toastr.success("Por favor espere...", "Hola!");
    setTimeout(function () {
      window.location.href =
        "../../actions/actions_admin/descargar_reporte_unico.php?id_servicio=" +
        id_servicio;
    }, 3000);
  }
}

///// inspecciones /////////////////

function eliminar_inspeccion(id_inspeccion) {
  var url =
    "../../actions/actions_admin/eliminar_inspeccion.php?id_inspeccion=" +
    id_inspeccion;

  var opcion = confirm("¿Estás seguro de realizar esta acción?");

  if (opcion == true) {
    $.ajax({
      cache: false,
      async: false,
      url: url,
      beforeSend: function () {
        $("#eliminando_inspeccion").html(
          '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Eliminando, por favor espere...</button>'
        );
        $("#btn_eliminar_inspeccion").hide();
      },
      success: function (data) {
        $("#eliminando_inspeccion").html(data);
      },
      error: function () {
        alert("Error, por favor intentalo más tarde.");
      },
    });
  } else {
    fadeOut();
  }
}
