<?php 
include "../header/header.php";


if($_SESSION["id_operador"] == null){
 
}else{
      echo '
      <script>redireccionar()
      function redireccionar(){
          window.location.href = "home";
      }
      </script>';
}
?>


<body>
    <!--================================-->
    <!-- User Singin Start -->
    <!--================================-->
    <div class="ht-100v d-flex">
        <div class="card shadow-none pd-20 mx-auto wd-300 text-center bd-1 align-self-center">
            <center><img style="width:100%" src="../assets/images/logo.png" alt=""></center>
            <h4 class="card-title mt-3 text-center">Bienviendos a World Shipping Comapny</h4>
            <p class="text-center">Inicie sesión con su cuenta operador</p>
            <form id="form_inicio_sesion">
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text pd-x-9"> <i data-feather="hash"
                                style="width:17px; height:17px;"></i> </span>
                    </div>
                    <input class="form-control form-control-sm" name="numero_documento"
                        placeholder="Número de documento" type="tel">
                </div>
                <!-- <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                    </div>
                    <input class="form-control form-control-sm" placeholder="Contraseña" type="password">
                </div>
               <p class="text-center"><a href="page-password.html">Has olvidado la contraseña?</a></p>-->
                <div class="form-group">
                    <a href="javascript:void(0)" onclick="iniciar_sesion_operador()"
                        class="btn btn-custom-primary btn-block tx-13 hover-white"> Iniciar
                        sesión
                    </a>
                </div>
                <p class="text-center"><a href="../admin">Iniciar sesión administrador</a></p>
            </form>
            <div id="respuesta_login"></div>
        </div>
    </div>

    <?php include "../footer/footer.php"?>