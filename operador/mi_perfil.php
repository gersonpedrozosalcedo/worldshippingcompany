<?php


include "../header/header.php";

if($_SESSION["id_operador"] == null){
    echo '
    <script>redireccionar()
    function redireccionar(){
        window.location.href = "index";
    }
    </script>';
}
?>


<div class="pageheader pd-t-25 pd-b-35">
    <div class="pd-t-5 pd-b-5">
        <h1 class="pd-0 mg-0 tx-20">Mi perfil</h1>
    </div>
    <div class="breadcrumb pd-0 mg-0">
        <a class="breadcrumb-item" href="index.html"><i class="icon ion-ios-home-outline"></i> Inicio</a>
        <a class="breadcrumb-item" href="#">Dashboard</a>
        <span class="breadcrumb-item active">Mi perfil</span>
    </div>
</div>

<div class="page-inner pd-0-force mg-0-force bg-white">
    <!--================================-->
    <!-- User Profile Start -->
    <!--================================-->
    <div class="row no-gutters">
        <div class="col-12">
            <div class="card bd-l-0-force bd-t-0-force bd-r-0-force">
                <div class="card-body bg-primary pd-y-50">
                    <div class="row no-gutters">
                        <div class="col-md-6 mg-t-20">
                            <div class="d-flex align-items-center">
                                <div class="mr-3">
                                    <span class="avatar avatar-lg avatar-online pd-b-20">
                                        <img src="assets/images/avatar/avatar1.png" class="img-fluid wd-100" alt="">
                                    </span>
                                </div>
                                <div class="mg-b-0">
                                    <h5 class="tx-gray-100 tx-15 mg-b-0">John Deo</h5>
                                    <p class="mg-b-10 tx-gray-300">@johndeo352</p>
                                    <a href="#" class="btn btn-sm btn-danger flex-fill mg-r-10">Follow</a>
                                    <a href="#" class="btn btn-sm btn-warning flex-fill">Message</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mg-t-10 mg-l-auto">
                            <ul class="list-unstyled tx-gray-100 mb-0">
                                <li><i class="ti-target mr-2 font-18"></i> <b>Gender </b>: Meal</li>
                                <li class="mt-2"><i class="ti-mobile mr-2 font-18"></i> <b>phone </b>: +91 23456 78910
                                </li>
                                <li class="mt-2"><i class="ti-headphone-alt mr-2 font-18"></i> <b>phone </b>: (800)
                                    477-1477</li>
                                <li class="mt-2"><i class="ti-email mr-2 font-18"></i> <b>Email </b>: example@email.com
                                </li>
                                <li class="mt-2"><i class="ti-map mr-2 font-18"></i> <b>Location</b> : California</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!--/ User Profile End -->
    </div>

    <?php include "../footer/footer.php"?>