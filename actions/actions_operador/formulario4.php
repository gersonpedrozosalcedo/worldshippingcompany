<?php 
$id_servicio = $_GET["id_servicio"];
?>

<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalToggleLabel3">ID SERVICIO: #<?php echo $id_servicio?> - Formulario (4)
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>
    </div>
    <div class="modal-body">
        <form action="">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <label for="">¿Despacho parcial o total?</label>
                    <select name="" class="form-control" id="">
                        <option value="" selected>Despacho parcial</option>
                        <option value="">Despacho total</option>
                    </select>
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Fecha y hora de despacho</label>
                    <input type="text" class="form-control datepicker-here" placeholder="Select Date &amp; Time"
                        data-timepicker="true" data-time-format="hh:ii aa">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Transportadora</label>
                    <input type="text" class="form-control">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Nombre conductor (transportadora)</label>
                    <input type="text" class="form-control">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Identificación del conductor (transportadora)</label>
                    <input type="text" class="form-control">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Placa del vehículo (transportadora)</label>
                    <input type="text" class="form-control">
                </div>

            </div>

    </div>
    </form>
    <div class="modal-footer">
        <button class="btn btn-dark" data-dismiss="modal" aria-label="Close">Regresar</button>
        <button class="btn btn-primary" data-target="#exampleModalToggle5"
            onclick="formulario5(<?php echo $id_servicio?>)" data-toggle="modal">Continuar</button>
    </div>
</div>
<script src="../assets/plugins/datepicker/js/datepicker.min.js"></script>
<script src="../assets/plugins/datepicker/js/datepicker.es.js"></script>