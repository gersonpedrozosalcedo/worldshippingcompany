<?php 
$id_servicio = $_GET["id_servicio"];
?>

<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalToggleLabel5">ID SERVICIO: #<?php echo $id_servicio?> - Formulario (5)
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>
    </div>
    <div class="modal-body">
        <form action="">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <label for="">Lugar devolucion del vacío</label>
                    <input type="text" class="form-control" name="" id="">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Fecha devolución del vacío</label>
                    <input type="text" class="form-control datepicker-here" placeholder="Select Date &amp; Time"
                        data-timepicker="true" data-time-format="hh:ii aa">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Nombre conductor (devolución del vacío)</label>
                    <input type="text" class="form-control">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Identificación del conductor (devolución del vacío)</label>
                    <input type="text" class="form-control">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Placa del vehículo (devolución del vacío)</label>
                    <input type="text" class="form-control">
                </div>

            </div>

    </div>
    </form>
    <div class="modal-footer">
        <button class="btn btn-dark" data-dismiss="modal" aria-label="Close">Regresar</button>
        <button class="btn btn-primary" data-target="#exampleModalToggle6"
            onclick="resumen_control(<?php echo $id_servicio?>)" data-toggle="modal">Siguiente</button>
    </div>
</div>


<script src="../assets/plugins/datepicker/js/datepicker.min.js"></script>
<script src="../assets/plugins/datepicker/js/datepicker.es.js"></script>