<?php
include '../../database/database.php';
date_default_timezone_set('America/Bogota');
$accion = $_GET["accion"];
$fecha_actual = date("Y-m-d");
$fecha_restada =  date("Y-m-d",strtotime($fecha_actual."- 1 week")); 
$fecha_sumada = date("Y-m-d",strtotime($fecha_actual."+ 1 day")); 
if($accion == 'ultimo10'){
$consultar_servicios = $conn->prepare("SELECT * FROM ordendeservicio ORDER BY oid DESC LIMIT 10 ");
$consultar_servicios->execute();
$consultar_servicios = $consultar_servicios->fetchAll(PDO::FETCH_ASSOC);

}else if($accion == 'todos'){
    $consultar_servicios = $conn->prepare("SELECT * FROM ordendeservicio WHERE fecha 
    BETWEEN '$fecha_restada' AND '$fecha_sumada' ORDER BY oid DESC");

    $consultar_servicios->execute();
    $consultar_servicios = $consultar_servicios->fetchAll(PDO::FETCH_ASSOC);
    foreach($consultar_servicios as $servicios){
    }
}
?>


<?php
            if($accion == 'ultimo10'){
                
            }else{
            ?>
<div class="row">
    <div class="mg-20 form-inline wd-100p">
        <div class="col-sm-4">
            <div class="form-group">

                <label class="control-label">Estado</label>
                <select id="foo-filter-status" class="form-control">
                    <option value="">Mostrar todos</option>
                    <option value="Realizado">Realizados</option>
                    <option value="No realizado">No realizados</option>

                </select>
            </div>
        </div>
        <div class="col-sm-4">

            <div class="input-group">
                <label for=""></label>

                <input id="datePicker4" type="text" class="form-control datepicker-here" name="fechas"
                    placeholder="Buscar servicios por rango de fechas" data-range="true"
                    data-multiple-dates-separator=" - ">

                <div class="input-group-append">
                    <label for="datePicker2" class="input-group-text"> <a href="javascript:void(0)"
                            onclick="buscar_servicio()"><i class="fa fa-search"></i></a></label>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group ft-right">
                <input id="foo-search" type="text" placeholder="Search" class="form-control" autocomplete="off">
            </div>
        </div>
    </div>
</div>
<?php }?>


<div id="sin_buscar_servicios">
    <table id="foo-filtering" class="table table-bordered table-hover toggle-circle" data-page-size="7">
        <thead>
            <tr>
                <th data-toggle="true">Id servicio</th>
                <th>Cliente</th>
                <th data-hide="phone, tablet">Instrucción</th>
                <th data-hide="phone, tablet">Fecha</th>
                <th data-hide="phone, tablet">Estado</th>
                <?php
            if($accion == 'ultimo10'){
                
            }else{
                echo '<th data-hide="phone, tablet">Acción</th>';
            }
            ?>

            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($consultar_servicios as $servicios){
            ?> <tr>
                <td><?php echo $servicios["oid"]?></td>
                <td><?php $nit_cliente = $servicios["cliente"];
            $consultar_cliente = $conn->prepare("SELECT razon_social FROM clientes WHERE nit = '$nit_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);

            foreach($consultar_cliente as $cliente){
                echo $cliente["razon_social"];
            }
            ?></td>
                <td><?php echo $servicios["instruciones"]?></td>
                <td><?php echo $servicios["fecha"]?></td>
                <td>
                    <?php

                if($servicios["estado"] == 0){
                    echo ' <span class="label label-table label-danger">No realizado</span>';
                }else if($servicios["estado"] == 1){
                    echo ' <span class="label label-table label-success">Realizado</span>';
                }
               ?>
                </td>
                <?php
            if($accion == 'ultimo10'){
                
            }else{
            ?>
                <td>
                    <button type="button" data-toggle="modal" href="#formulario1" role="button"
                        onclick="formulario1(<?php echo $servicios['oid']?>)"
                        class="btn btn-outline-primary btn-icon mg-r-5"><i data-feather="plus-circle"
                            data-toggle="tooltip" data-trigger="hover" data-placement="top" title=""
                            data-original-title="Ver/Añadir información sobre el control de vehículo en ruta"></i></button>
                </td>
                <?php } ?>
            </tr>
            <?php
            }
            ?>

        </tbody>
        <tfoot>
            <tr>
                <td colspan="5">
                    <div class="ft-right">
                        <ul class="pagination"></ul>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
</div>

<div id="con_buscador_servicios">
    <div id="respuesta_buscador"></div>
</div>




<script>
// Row Toggler
$("#foo-row-toggler").footable();

// Accordion
$("#foo-accordion")
    .footable()
    .on("footable_row_expanded", function(e) {
        $("#foo-accordion tbody tr.footable-detail-show")
            .not(e.row)
            .each(function() {
                $("#foo-accordion").data("footable").toggleDetail(this);
            });
    });
// Filtering
var filtering = $("#foo-filtering");
filtering.footable().on("footable_filtering", function(e) {
    var selected = $("#foo-filter-status").find(":selected").val();
    e.filter += e.filter && e.filter.length > 0 ? " " + selected : selected;
    e.clear = !e.filter;
});

// Filter status
$("#foo-filter-status").change(function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});

// Search input
$("#foo-search").on("input", function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});
</script>

<script src="http://worldshippingcompany.com.co/assets/plugins/datepicker/js/datepicker.min.js"></script>
<script src="http://worldshippingcompany.com.co/assets/plugins/datepicker/js/datepicker.es.js"></script>

<script src="http://worldshippingcompany.com.co/js/js-operador/main.js"></script>