<?php

include '../../database/database.php';

$consultar_clientes = $conn->prepare("SELECT * FROM clientes ORDER BY id desc");
$consultar_clientes->execute();
$consultar_clientes = $consultar_clientes->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipos_estados = $conn->prepare("SELECT * FROM estados ORDER BY id desc");
$consultar_tipos_estados->execute();
$consultar_tipos_estados = $consultar_tipos_estados->fetchAll(PDO::FETCH_ASSOC);
?>


<div id="eliminando_cliente"></div>

<table id="foo-filtering" class="table table-bordered table-hover toggle-circle" data-page-size="7">
    <thead>
        <tr>
            <th data-toggle="true">Razón social</th>
            <th>Nit</th>
            <th data-hide="phone, tablet">Email</th>
            <th data-hide="phone, tablet">Estado</th>
            <th data-hide="phone, tablet">Acción</th>
        </tr>
    </thead>
    <tbody>
        <tr>

            <?php foreach ($consultar_clientes as $clientes) { ?>
            <td id="responsive"><?php echo $clientes["razon_social"] ?></td>
            <td id="responsive"><?php echo $clientes["nit"] ?></td>
            <td id="responsive"><?php echo $clientes["email"] ?></td>
            <td id="responsive">
                <?php
                    if ($clientes["estado"] == 0) {
                        echo '<span class="label label-table label-dark ">Recien creado</span>';
                    } else if ($clientes["estado"] == 1) {
                        echo '<span class="label label-table label-success">Activado</span>';
                    } else if ($clientes["estado"] == 2) {
                        echo '<span class="label label-table label-danger">Desactivado</span>';
                    }
                    ?>


            </td>
            <td id="responsive">
                <button type="button" data-toggle="modal" data-target="#editar_cliente" role="button"
                    onclick="editar_cliente(<?php echo $clientes['id'] ?>)"
                    class="btn btn-outline-primary btn-icon mg-r-5"><i data-feather="edit-3" data-toggle="tooltip"
                        data-trigger="hover" data-placement="top" title=""
                        data-original-title="Ver/Editar información sobre el cliente"></i></button>
                <button type="button" data-toggle="modal" href="#formulario1" role="button"
                    onclick="eliminar_cliente(<?php echo $clientes['id'] ?>)"
                    class="btn btn-outline-danger btn-icon mg-r-5"><i data-toggle="tooltip" data-trigger="hover"
                        data-placement="top" title="" data-original-title="Eliminar cliente"
                        data-feather="x-circle"></i></button>


                <?php /*
                               if($clientes["estado"] == 0){
                                echo ' <button type="button" data-toggle="modal" href="#formulario1" role="button"
                                onclick="cambiar_estado_cliente('.$clientes["id"].',0)"
                class="btn btn-outline-danger btn-icon mg-r-5"><i data-toggle="tooltip" data-trigger="hover"
                    data-placement="top" title="" data-original-title="Eliminar cliente"
                    data-feather="x-circle"></i></button>';
                }else if($clientes["estado"] == 1){
                echo ' <button type="button" data-toggle="modal" href="#formulario1" role="button"
                    onclick="cambiar_estado_cliente('.$clientes["id"].',2)"
                class="btn btn-outline-warning btn-icon mg-r-5"><i data-feather="user-x" data-toggle="tooltip"
                    data-trigger="hover" data-placement="top" title="" data-original-title="Desactivar cliente"></i>
                </button>';
                }else if($clientes["estado"] == 2){
                echo ' <button type="button" data-toggle="modal" href="#formulario1" role="button"
                    onclick="cambiar_estado_cliente('.$clientes["id"].',1)"
                    class="btn btn-outline-success btn-icon mg-r-5"><i data-feather="user-check" data-toggle="tooltip" data-trigger="hover"
                        data-placement="top" title="" data-original-title="Activar cliente" ></i></button>';
                } */
                    ?>
            </td>
        </tr>
        <?php } ?>

    </tbody>
    <tfoot>
        <tr>
            <td colspan="5">
                <div class="ft-right">
                    <ul class="pagination"></ul>
                </div>
            </td>
        </tr>
    </tfoot>
</table>