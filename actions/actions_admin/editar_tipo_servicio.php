<?php 

include '../../database/database.php';

$id_tipo_servicio = $_GET["id_tipo_servicio"];

$consultar_tipos_servicios = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio' ");
$consultar_tipos_servicios->execute();
$consultar_tipos_servicios = $consultar_tipos_servicios->fetchAll(PDO::FETCH_ASSOC);
foreach($consultar_tipos_servicios as $tipo_servicio){
    
}
?>

<form id="editar_form_tipo_servicio">

    <input type="hidden" name="id_tipo_servicio" value="<?php echo $id_tipo_servicio?>">
    <div class="form-group">
        <label for="recipient-name-2" class="form-control-label">Nombre servicio</label>
        <input type="text" class="form-control" name="nombre_servicio"
            value="<?php echo $tipo_servicio["nombre_servicio"]?>" id="recipient-name-2">
    </div>
    <div class="form-group">
        <label for="recipient-name-2" class="form-control-label">Costo servicio</label>
        <input type="text" class="form-control" name="costo_servicio"
            value="<?php echo $tipo_servicio["costo_servicio"]?>" id="recipient-name-2">
    </div>
    <div class="form-group">
        <label for="recipient-name-2" class="form-control-label">ITR</label>
        <select name="itritre" class="form-control" id="itritre">

            <option value="<?php echo $tipo_servicio["itreitri"]?>"><?php  
            if($tipo_servicio["itreitri"] == 0){
echo "ITR de Importación";
            }else{
                echo "ITR de Exportación";
            }
            ?></option>
            <option value="0">ITR de Importación</option>
            <option value="1">ITR de Exportación</option>

        </select>


    </div>

    <div class="form-group">
        <label for="recipient-name-2" class="form-control-label">Estado servicio</label>
        <select name="estado" class="form-control" id="estado">

            <option value="<?php echo $tipo_servicio["estado"]?>">
                <?php 
            if($tipo_servicio["estado"] == 0){
                echo "Desactivado";

            }else{
                echo "Activado";
            }
            ?>

            </option>
            <option value="0">Desactivar</option>
            <option value="1">Activar</option>

        </select>


    </div>
</form>

<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
    <button type="button" class="btn btn-primary" onclick="actualizar_tipos_servicios()">Actualizar</button>
</div>