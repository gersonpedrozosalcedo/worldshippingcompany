<?php

require "../../vendor/autoload.php";
date_default_timezone_set('America/Bogota');
include '../../database/database.php';

$id_inspeccion = $_GET["id_inspeccion"];

use PhpOffice\PhpSpreadsheet\Spreadsheet;
//use PhpOffice\PhpSpreadsheet\writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;

$fecha_generado = date("Y-m-d H:i");

$nombre_archivo = 'AP-CO-FO ' . $id_inspeccion . ' INSPECCION MONTACARGA ' . $fecha_generado;

$spreadsheet = new SpreadSheet();
$spreadsheet->getProperties()->setCreator("WORLDSHIPPINGCOMPANYSAS")->setTitle($nombre_archivo);

$spreadsheet->setActiveSheetIndex(0);
$hojaActiva = $spreadsheet->getActiveSheet();


$spreadsheet->getDefaultStyle()->getFont()->setName("Calibri");
$spreadsheet->getDefaultStyle()->getFont()->setSize(8);
$spreadsheet->getActiveSheet()->getSheetView()->setZoomScale(100);
$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(70);
//$spreadsheet->getActiveSheet()->getRowDimension('3')->setRowHeight(5);
$spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(60);
$spreadsheet->getActiveSheet()->getRowDimension('27')->setRowHeight(30);
$spreadsheet->getActiveSheet()->getRowDimension('33')->setRowHeight(30);
for ($i = 8; $i <= 26; $i++) {
    $spreadsheet->getActiveSheet()->getRowDimension($i)->setRowHeight(20);
}
for ($i = 28; $i <= 32; $i++) {
    $spreadsheet->getActiveSheet()->getRowDimension($i)->setRowHeight(15);
}

////altura automatica celdas de observacion
//$spreadsheet->getActiveSheet()->getRowDimension('8,32')->setRowHeight(-1);
//$spreadsheet->getActiveSheet()->getStyle('F')->getAlignment()->setWrapText(true);
////////////////
$hojaActiva->getColumnDimension('A')->setWidth(9);
$hojaActiva->getColumnDimension('B')->setWidth(12);
$hojaActiva->getColumnDimension('C')->setWidth(12);
$hojaActiva->getColumnDimension('D')->setWidth(7);
$hojaActiva->getColumnDimension('E')->setWidth(7);
$hojaActiva->getColumnDimension('F')->setWidth(7);
$hojaActiva->getColumnDimension('G')->setWidth(7);
$hojaActiva->getColumnDimension('H')->setWidth(7);
$hojaActiva->getColumnDimension('I')->setWidth(7);
$hojaActiva->getColumnDimension('J')->setWidth(7);
$hojaActiva->getColumnDimension('K')->setWidth(7);
$hojaActiva->getColumnDimension('L')->setWidth(7);
$hojaActiva->getColumnDimension('M')->setWidth(7);
$hojaActiva->getColumnDimension('N')->setWidth(7);
$hojaActiva->getColumnDimension('M')->setWidth(7);
$hojaActiva->getColumnDimension('O')->setWidth(7);
$hojaActiva->getColumnDimension('P')->setWidth(7);
$hojaActiva->getColumnDimension('Q')->setWidth(7);

//bordes

$styleArray1 = array(
    'borders' => array(
        'allBorders' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => array('argb' => '0000'),
        ),
    ),

);
$styleArray2 = array(
    'borders' => array(
        'outline' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => array('argb' => '0000'),
        ),
    ),
    'font' => array(
        'bold' => true
    )

);
$styleArray3 = array(
    'borders' => array(
        'allBorders' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => array('argb' => '0000'),
        ),
    ),
    'font' => array(
        'bold' => true
    )

);
$style_titles = array(
    'font' => array(
        'bold' => true
    )
);



$hojaActiva->getStyle('A1:Q1')->applyFromArray($styleArray2);
$hojaActiva->getStyle('A2:Q2')->applyFromArray($styleArray2);
$hojaActiva->getStyle('A3:G3')->applyFromArray($styleArray2);
$hojaActiva->getStyle('H3:M3')->applyFromArray($styleArray2);
$hojaActiva->getStyle('N3:Q3')->applyFromArray($styleArray2);
$hojaActiva->getStyle('A4:C4')->applyFromArray($styleArray2);
$hojaActiva->getStyle('D4:E4')->applyFromArray($styleArray2);
$hojaActiva->getStyle('F4:K4')->applyFromArray($styleArray2);
$hojaActiva->getStyle('L4:M4')->applyFromArray($styleArray2);
$hojaActiva->getStyle('N4:Q4')->applyFromArray($styleArray2);

$hojaActiva->getStyle('A5:Q7')->applyFromArray($styleArray3);
$hojaActiva->getStyle('A8:Q33')->applyFromArray($styleArray3);
$hojaActiva->getStyle('B28:Q32')->applyFromArray($styleArray2);


/*$hojaActiva->getStyle('A8:F39')->applyFromArray($styleArray1);*/


$spreadsheet->getActiveSheet()->getStyle('A1:Q1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFFF');


///////

$hojaActiva->mergeCells('D1:f1');
$hojaActiva->mergeCells('A2:Q2');
$hojaActiva->mergeCells('A3:G3');
$hojaActiva->mergeCells('H3:M3');
$hojaActiva->mergeCells('N3:Q3');
$hojaActiva->mergeCells('A4:C4');
$hojaActiva->mergeCells('D4:E4');
$hojaActiva->mergeCells('F4:K4');
$hojaActiva->mergeCells('L4:M4');
$hojaActiva->mergeCells('N4:Q4');
$hojaActiva->mergeCells('A5:A7');
$hojaActiva->mergeCells('A8:A12');
$hojaActiva->mergeCells('A13:A14');
$hojaActiva->mergeCells('A15:A25');

$hojaActiva->mergeCells('B5:C6');
$hojaActiva->mergeCells('B7:C7');

$hojaActiva->mergeCells('D5:E6');
$hojaActiva->mergeCells('F5:G6');
$hojaActiva->mergeCells('H5:I6');
$hojaActiva->mergeCells('J5:K6');
$hojaActiva->mergeCells('L5:M6');
$hojaActiva->mergeCells('N5:O6');
$hojaActiva->mergeCells('P5:Q6');
$hojaActiva->mergeCells('A27:C27');

$hojaActiva->mergeCells('D27:E27');
$hojaActiva->mergeCells('F27:G27');
$hojaActiva->mergeCells('H27:I27');
$hojaActiva->mergeCells('J27:K27');
$hojaActiva->mergeCells('L27:M27');
$hojaActiva->mergeCells('N27:O27');
$hojaActiva->mergeCells('P27:Q27');

$hojaActiva->mergeCells('A28:A32');
$hojaActiva->mergeCells('A33:C33');
$hojaActiva->mergeCells('D33:Q33');

for ($i = 8; $i <= 26; $i++) {
    $hojaActiva->mergeCells('B' . $i . ':C' . $i . '');
}
for ($i = 28; $i <= 32; $i++) {
    $hojaActiva->mergeCells('B' . $i . ':Q' . $i . '');
}

//consultas
$consultar_inspeccion = $conn->prepare("SELECT * FROM control_inspeccion_vehiculo WHERE id = '$id_inspeccion'");
$consultar_inspeccion->execute();
$consultar_inspeccion = $consultar_inspeccion->fetchAll(PDO::FETCH_ASSOC);

$consultar_control_inspeccion_vehiculo_relacion = $conn->prepare("SELECT * FROM control_inspeccion_vehiculo_relacion WHERE id_inspeccion = '$id_inspeccion'");
$consultar_control_inspeccion_vehiculo_relacion->execute();
$consultar_control_inspeccion_vehiculo_relacion = $consultar_control_inspeccion_vehiculo_relacion->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_inspeccion as $value) {
    $fecha_creacion = $value["fecha_creacion"];
    $nombre_conductor = $value["conductor"];
    $identificacion = $value["identificacion_conductor"];
    $placa_vehiculo = $value["placa_vehiculo"];
    $nombre_empleado = $value["nombre_empleado"];
    $identificacion_empleado = $value["identificacion_empleado"];
}
$separar_fecha_hora = (explode(" ", $fecha_creacion));
$fecha_inspeccion = "Fecha: " . $separar_fecha_hora[0];
$hora_inspeccion = 'Hora: ' . $separar_fecha_hora[1];

//consultas conductor

$consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE numero_identificacion = '$identificacion'");
$consultar_conductor->execute();
$consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_conductor as $conductor) {
    $id_conductor = $conductor["id"];
    $foto_firma_conductor = $conductor["firma_digital"];
}
//consultas empleado

$consultar_empleado = $conn->prepare("SELECT * FROM empleados WHERE numero_identificacion = '$identificacion_empleado'");
$consultar_empleado->execute();
$consultar_empleado = $consultar_empleado->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_empleado as $empleado) {
    $id_empleado = $empleado["id"];
    $foto_firma_empleado = $empleado["firma_digital"];
}

///// logo titulo

$hojaActiva->setCellValue('J1', 'INSPECCIÓN DE MONTACARGAS')->getStyle('J1')->getAlignment()->setHorizontal('center');
$hojaActiva->setCellValue('J1', 'INSPECCIÓN DE MONTACARGAS')->getStyle('J1')->getAlignment()->setVertical('center');

$logo = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
$logo->setName('Logo');
$logo->setDescription('Logo');
$logo->setPath('../../assets/images/logo.png'); // put your path and image here
$logo->setCoordinates('E1');
$logo->setOffsetX(80);
$logo->setOffsetY(8);
$logo->setRotation(0);
$logo->setWidthAndHeight(70, 100);
/*$logo->getShadow()->setVisible(true);
$logo->getShadow()->setDirection(45);*/
$logo->setWorksheet($spreadsheet->getActiveSheet());

///////
$hojaActiva->setCellValue('A2', 'VERSIÓN: 5           CODIGO: AP-CO-FO' . $id_inspeccion . '           FECHA: ' . $fecha_generado . '           PAGINA 1 DE 1')->getStyle('A2')->getAlignment()->setHorizontal('center');
////// 
$hojaActiva->setCellValue('A3', 'Conductor: ' . $nombre_conductor . '')->getStyle('A3')->getAlignment()->setHorizontal('left');
$hojaActiva->setCellValue('H3', 'Coord/aux: ' . $nombre_empleado . '')->getStyle('H3')->getAlignment()->setHorizontal('left');
$hojaActiva->setCellValue('N3', 'Placa: ' . $placa_vehiculo)->getStyle('N3')->getAlignment()->setHorizontal('left');
//////////

$hojaActiva->setCellValue('A4', 'B si el estado es bueno o en la columna de M si el estado es Malo')->getStyle('A4')->getAlignment()->setHorizontal('center')->setVertical('center')->setWrapText(true);
$hojaActiva->setCellValue('D4', 'Maquina')->getStyle('D4')->getAlignment()->setHorizontal('center')->setVertical('center');
$hojaActiva->setCellValue('F4', $placa_vehiculo)->getStyle('F4')->getAlignment()->setHorizontal('center')->setVertical('center');
$hojaActiva->setCellValue('L4', 'Mes/Año')->getStyle('L4')->getAlignment()->setHorizontal('center')->setVertical('center');
$hojaActiva->setCellValue('N4', date("m/Y", strtotime($separar_fecha_hora[0])))->getStyle('N4')->getAlignment()->setHorizontal('center')->setVertical('center');
/////////////////
$hojaActiva->setCellValue('A5', 'ITEM')->getStyle('A5')->getAlignment()->setHorizontal('center')->setVertical('center');
$hojaActiva->setCellValue('A8', 'SEGURIDAD')->getStyle('A8')->getAlignment()->setHorizontal('center')->setVertical('center')->setTextRotation(90);
$hojaActiva->setCellValue('A13', 'LUCES')->getStyle('A13')->getAlignment()->setHorizontal('center')->setVertical('center')->setTextRotation(90);
$hojaActiva->setCellValue('A13', 'LUCES')->getStyle('A13')->getAlignment()->setHorizontal('center')->setVertical('center')->setTextRotation(90);
$hojaActiva->setCellValue('A15', 'GENERALES')->getStyle('A15')->getAlignment()->setHorizontal('center')->setVertical('center')->setTextRotation(90);
$hojaActiva->setCellValue('A26', 'LLANTAS')->getStyle('A26')->getAlignment()->setHorizontal('center')->setVertical('center');


////
$hojaActiva->setCellValue('B5', 'DÍA')->getStyle('B5')->getAlignment()->setHorizontal('center')->setVertical('center');
$hojaActiva->setCellValue('B7', 'CONCEPTO')->getStyle('B7')->getAlignment()->setHorizontal('center')->setVertical('center');


//// imprimiendo conceptos en el excel

$i = 0;
$j = 8;

foreach ($consultar_control_inspeccion_vehiculo_relacion as $inspeccion_relacion) {

    if ($i <= 18) {

        $fechaComoEntero = strtotime($fecha_creacion);
        $dia = date("d", $fechaComoEntero);
        $mess = date("m", $fechaComoEntero);
        $año = date("Y", strtotime($separar_fecha_hora[0]));
        $meses = array(
            1 => 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO',
            'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'
        );

        $mes = $meses[$mess];

        $hojaActiva->setCellValue('D5', $dia . ' DE ' . $mes . ' DEL ' . $año)->getStyle('D5')->getAlignment()->setHorizontal('center')->setVertical('center')->setWrapText(true);
        $hojaActiva->setCellValue('D7', 'B')->getStyle('D7')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hojaActiva->setCellValue('E7', 'M')->getStyle('E7')->getAlignment()->setHorizontal('center')->setVertical('center');

        $hojaActiva->setCellValue('B' . $j, $inspeccion_relacion["concepto"]);
        if ($inspeccion_relacion["seleccion"] == "Bueno") {
            $hojaActiva->setCellValue('D' . $j, 'X')->getStyle('D' . $j)->getAlignment()->setHorizontal('center')->setVertical('center');
        } else if ($inspeccion_relacion["seleccion"] == "Malo") {
            $hojaActiva->setCellValue('E' . $j, 'X')->getStyle('E' . $j)->getAlignment()->setHorizontal('center')->setVertical('center');
        }

        /////// firmas operador

        /* foto*/
        $logo = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $logo->setName('firma_conductor');
        $logo->setDescription('firma_conductor');
        $logo->setPath('../../foto_firma_conductores/' . $id_conductor . '/' . $foto_firma_conductor); // put your path and image here
        $logo->setCoordinates('D27');
        $logo->setOffsetX(13);
        $logo->setOffsetY(8);
        $logo->setRotation(0);
        $logo->setWidthAndHeight(50, 60);
        /*$logo->getShadow()->setVisible(true);
        $logo->getShadow()->setDirection(45);*/
        $logo->setWorksheet($spreadsheet->getActiveSheet());
    } else if ($i >= 19 && $i <= 37) {
        $fecha_creacions = date("d-m-Y", strtotime($fecha_creacion . "+ 1 days"));
        $fechaComoEntero = strtotime($fecha_creacions);
        $dia = date("d", $fechaComoEntero);
        $mess = date("m", $fechaComoEntero);
        $año = date("Y", strtotime($separar_fecha_hora[0]));
        $meses = array(
            1 => 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO',
            'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'
        );

        $mes = $meses[$mess];

        $hojaActiva->setCellValue('F5',  $dia . ' DE ' . $mes . ' DEL ' . $año)->getStyle('F5')->getAlignment()->setHorizontal('center')->setVertical('center')->setWrapText(true);
        $hojaActiva->setCellValue('F7', 'B')->getStyle('F7')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hojaActiva->setCellValue('G7', 'M')->getStyle('G7')->getAlignment()->setHorizontal('center')->setVertical('center');

        if ($inspeccion_relacion["seleccion"] == "Bueno") {
            $hojaActiva->setCellValue('F' . $resta1, 'X')->getStyle('F' . $p)->getAlignment()->setHorizontal('center')->setVertical('center');
        } else if ($inspeccion_relacion["seleccion"] == "Malo") {
            $hojaActiva->setCellValue('G' . $resta1, 'X')->getStyle('G' . $p)->getAlignment()->setHorizontal('center')->setVertical('center');
        }

        /////// firmas operador

        /* foto*/
        $logo = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $logo->setName('firma_conductor');
        $logo->setDescription('firma_conductor');
        $logo->setPath('../../foto_firma_conductores/' . $id_conductor . '/' . $foto_firma_conductor); // put your path and image here
        $logo->setCoordinates('F27');
        $logo->setOffsetX(13);
        $logo->setOffsetY(8);
        $logo->setRotation(0);
        $logo->setWidthAndHeight(50, 60);
        /*$logo->getShadow()->setVisible(true);
        $logo->getShadow()->setDirection(45);*/
        $logo->setWorksheet($spreadsheet->getActiveSheet());
    } else if ($i >= 38 && $i <= 56) {
        $fecha_creacions =  date("d-m-Y", strtotime($fecha_creacion . "+ 2 days"));
        $fechaComoEntero = strtotime($fecha_creacions);
        $dia = date("d", $fechaComoEntero);
        $mess = date("m", $fechaComoEntero);
        $año = date("Y", strtotime($separar_fecha_hora[0]));
        $meses = array(
            1 => 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO',
            'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'
        );

        $mes = $meses[$mess];
        $hojaActiva->setCellValue('H5', $dia . ' DE ' . $mes . ' DEL ' . $año)->getStyle('H5')->getAlignment()->setHorizontal('center')->setVertical('center')->setWrapText(true);
        $hojaActiva->setCellValue('H7', 'B')->getStyle('H7')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hojaActiva->setCellValue('I7', 'M')->getStyle('I7')->getAlignment()->setHorizontal('center')->setVertical('center');

        if ($inspeccion_relacion["seleccion"] == "Bueno") {
            $hojaActiva->setCellValue('H' . $resta2, 'X')->getStyle('H' . $p)->getAlignment()->setHorizontal('center')->setVertical('center');
        } else if ($inspeccion_relacion["seleccion"] == "Malo") {
            $hojaActiva->setCellValue('I' . $resta2, 'X')->getStyle('I' . $p)->getAlignment()->setHorizontal('center')->setVertical('center');
        }

        /////// firmas operador

        /* foto*/
        $logo = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $logo->setName('firma_conductor');
        $logo->setDescription('firma_conductor');
        $logo->setPath('../../foto_firma_conductores/' . $id_conductor . '/' . $foto_firma_conductor); // put your path and image here
        $logo->setCoordinates('H27');
        $logo->setOffsetX(13);
        $logo->setOffsetY(8);
        $logo->setRotation(0);
        $logo->setWidthAndHeight(50, 60);
        /*$logo->getShadow()->setVisible(true);
        $logo->getShadow()->setDirection(45);*/
        $logo->setWorksheet($spreadsheet->getActiveSheet());
    } else if ($i >= 57 && $i <= 75) {
        $fecha_creacions =  date("d-m-Y", strtotime($fecha_creacion . "+ 3 days"));
        $fechaComoEntero = strtotime($fecha_creacions);
        $dia = date("d", $fechaComoEntero);
        $mess = date("m", $fechaComoEntero);
        $año = date("Y", strtotime($separar_fecha_hora[0]));
        $meses = array(
            1 => 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO',
            'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'
        );

        $mes = $meses[$mess];
        $hojaActiva->setCellValue('J5',  $dia . ' DE ' . $mes . ' DEL ' . $año)->getStyle('J5')->getAlignment()->setHorizontal('center')->setVertical('center')->setWrapText(true);
        $hojaActiva->setCellValue('J7', 'B')->getStyle('J7')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hojaActiva->setCellValue('K7', 'M')->getStyle('K7')->getAlignment()->setHorizontal('center')->setVertical('center');

        if ($inspeccion_relacion["seleccion"] == "Bueno") {
            $hojaActiva->setCellValue('J' . $resta3, 'X')->getStyle('J' . $p)->getAlignment()->setHorizontal('center')->setVertical('center');
        } else if ($inspeccion_relacion["seleccion"] == "Malo") {
            $hojaActiva->setCellValue('K' . $resta3, 'X')->getStyle('K' . $p)->getAlignment()->setHorizontal('center')->setVertical('center');
        }

        /////// firmas operador

        /* foto*/
        $logo = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $logo->setName('firma_conductor');
        $logo->setDescription('firma_conductor');
        $logo->setPath('../../foto_firma_conductores/' . $id_conductor . '/' . $foto_firma_conductor); // put your path and image here
        $logo->setCoordinates('J27');
        $logo->setOffsetX(13);
        $logo->setOffsetY(8);
        $logo->setRotation(0);
        $logo->setWidthAndHeight(50, 60);
        /*$logo->getShadow()->setVisible(true);
        $logo->getShadow()->setDirection(45);*/
        $logo->setWorksheet($spreadsheet->getActiveSheet());
    } else if ($i >= 76 && $i <= 94) {
        $fecha_creacions =  date("d-m-Y", strtotime($fecha_creacion . "+ 4 days"));
        $fechaComoEntero = strtotime($fecha_creacions);
        $dia = date("d", $fechaComoEntero);
        $mess = date("m", $fechaComoEntero);
        $año = date("Y", strtotime($separar_fecha_hora[0]));
        $meses = array(
            1 => 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO',
            'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'
        );

        $mes = $meses[$mess];
        $hojaActiva->setCellValue('L5',  $dia . ' DE ' . $mes . ' DEL ' . $año)->getStyle('L5')->getAlignment()->setHorizontal('center')->setVertical('center')->setWrapText(true);
        $hojaActiva->setCellValue('L7', 'B')->getStyle('L7')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hojaActiva->setCellValue('M7', 'M')->getStyle('M7')->getAlignment()->setHorizontal('center')->setVertical('center');

        if ($inspeccion_relacion["seleccion"] == "Bueno") {
            $hojaActiva->setCellValue('L' . $resta4, 'X')->getStyle('L' . $p)->getAlignment()->setHorizontal('center')->setVertical('center');
        } else if ($inspeccion_relacion["seleccion"] == "Malo") {
            $hojaActiva->setCellValue('M' . $resta4, 'X')->getStyle('M' . $p)->getAlignment()->setHorizontal('center')->setVertical('center');
        }

        /////// firmas operador

        /* foto*/
        $logo = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $logo->setName('firma_conductor');
        $logo->setDescription('firma_conductor');
        $logo->setPath('../../foto_firma_conductores/' . $id_conductor . '/' . $foto_firma_conductor); // put your path and image here
        $logo->setCoordinates('L27');
        $logo->setOffsetX(13);
        $logo->setOffsetY(8);
        $logo->setRotation(0);
        $logo->setWidthAndHeight(50, 60);
        /*$logo->getShadow()->setVisible(true);
        $logo->getShadow()->setDirection(45);*/
        $logo->setWorksheet($spreadsheet->getActiveSheet());
    } else if ($i >= 94 && $i <= 113) {

        $fecha_creacions =  date("d-m-Y", strtotime($fecha_creacion . "+ 5 days"));
        $fechaComoEntero = strtotime($fecha_creacions);
        $dia = date("d", $fechaComoEntero);
        $mess = date("m", $fechaComoEntero);
        $año = date("Y", strtotime($separar_fecha_hora[0]));
        $meses = array(
            1 => 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO',
            'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'
        );

        $mes = $meses[$mess];
        $hojaActiva->setCellValue('N5',  $dia . ' DE ' . $mes . ' DEL ' . $año)->getStyle('N5')->getAlignment()->setHorizontal('center')->setVertical('center')->setWrapText(true);
        $hojaActiva->setCellValue('N7', 'B')->getStyle('N7')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hojaActiva->setCellValue('O7', 'M')->getStyle('O7')->getAlignment()->setHorizontal('center')->setVertical('center');

        if ($inspeccion_relacion["seleccion"] == "Bueno") {
            $hojaActiva->setCellValue('N' . $resta5, 'X')->getStyle('N' . $p)->getAlignment()->setHorizontal('center')->setVertical('center');
        } else if ($inspeccion_relacion["seleccion"] == "Malo") {
            $hojaActiva->setCellValue('O' . $resta5, 'X')->getStyle('O' . $p)->getAlignment()->setHorizontal('center')->setVertical('center');
        }

        /////// firmas operador

        /* foto*/
        $logo = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $logo->setName('firma_conductor');
        $logo->setDescription('firma_conductor');
        $logo->setPath('../../foto_firma_conductores/' . $id_conductor . '/' . $foto_firma_conductor); // put your path and image here
        $logo->setCoordinates('N27');
        $logo->setOffsetX(13);
        $logo->setOffsetY(8);
        $logo->setRotation(0);
        $logo->setWidthAndHeight(50, 60);
        /*$logo->getShadow()->setVisible(true);
        $logo->getShadow()->setDirection(45);*/
        $logo->setWorksheet($spreadsheet->getActiveSheet());
    } else if ($i >= 112 && $i <= 133) {

        $fecha_creacions =  date("d-m-Y", strtotime($fecha_creacion . "+ 6 days"));
        $fechaComoEntero = strtotime($fecha_creacions);
        $dia = date("d", $fechaComoEntero);
        $mess = date("m", $fechaComoEntero);
        $año = date("Y", strtotime($separar_fecha_hora[0]));
        $meses = array(
            1 => 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO',
            'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'
        );

        $mes = $meses[$mess];
        $hojaActiva->setCellValue('P5',  $dia . ' DE ' . $mes . ' DEL ' . $año)->getStyle('P5')->getAlignment()->setHorizontal('center')->setVertical('center')->setWrapText(true);
        $hojaActiva->setCellValue('P7', 'B')->getStyle('P7')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hojaActiva->setCellValue('Q7', 'M')->getStyle('Q7')->getAlignment()->setHorizontal('center')->setVertical('center');

        if ($inspeccion_relacion["seleccion"] == "Bueno") {
            $hojaActiva->setCellValue('P' . $resta6, 'X')->getStyle('P' . $p)->getAlignment()->setHorizontal('center')->setVertical('center');
        } else if ($inspeccion_relacion["seleccion"] == "Malo") {
            $hojaActiva->setCellValue('Q' . $resta6, 'X')->getStyle('Q' . $p)->getAlignment()->setHorizontal('center')->setVertical('center');
        }

        /////// firmas operador

        /* foto*/
        $logo = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $logo->setName('firma_conductor');
        $logo->setDescription('firma_conductor');
        $logo->setPath('../../foto_firma_conductores/' . $id_conductor . '/' . $foto_firma_conductor); // put your path and image here
        $logo->setCoordinates('P27');
        $logo->setOffsetX(13);
        $logo->setOffsetY(8);
        $logo->setRotation(0);
        $logo->setWidthAndHeight(50, 60);
        /*$logo->getShadow()->setVisible(true);
        $logo->getShadow()->setDirection(45);*/
        $logo->setWorksheet($spreadsheet->getActiveSheet());
    }

    if ($inspeccion_relacion["observacion"] == '') {
        $observavcion_concat =  $inspeccion_relacion["observacion"];
    } else {
        $observavcion_concat =  $inspeccion_relacion["observacion"] . ',';
    }

    $observcion .= $observavcion_concat;

    $hojaActiva->setCellValue('B28', $observcion)->getStyle('B28')->getAlignment()->setHorizontal('left')->setVertical('center');
    $resta1 = $j - 18;
    $resta2 = $j - 37;
    $resta3  = $j - 56;
    $resta4  = $j - 75;
    $resta5  = $j - 94;
    $resta6  = $j - 113;

    $i++;
    $j++;
}
$hojaActiva->setCellValue('A27', 'FIRMA OPERADOR')->getStyle('A27')->getAlignment()->setHorizontal('center')->setVertical('center');

$hojaActiva->setCellValue('A28', 'OBSERVACIONES')->getStyle('A28')->getAlignment()->setHorizontal('center')->setVertical('center')->setTextRotation(90);
$hojaActiva->setCellValue('A33', 'FIRMA COORD/AUX')->getStyle('A33')->getAlignment()->setHorizontal('center')->setVertical('center');




$logo = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
$logo->setName('firma_conductor');
$logo->setDescription('firma_conductor');
$logo->setPath('../../foto_firma_empleados/' . $id_empleado . '/' . $foto_firma_empleado); // put your path and image here
$logo->setCoordinates('D33');
$logo->setOffsetX(13);
$logo->setOffsetY(8);
$logo->setRotation(0);
$logo->setWidthAndHeight(50, 60);
$logo->setWorksheet($spreadsheet->getActiveSheet());

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $nombre_archivo . '.xlsx"');
header('Cache-Control: max-age=0');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');