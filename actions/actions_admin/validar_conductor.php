<?php

include '../../database/database.php';
$documento = $_POST["documento"];

if ($documento == null) {
    $error = "error";
    $mensaje = "Por favor ingresar número documento.";
} else {
    $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE numero_identificacion = '$documento' ");
    $consultar_conductor->execute();
    $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

    $contador = count($consultar_conductor);

    if ($contador == 1) {

        foreach ($consultar_conductor as $value) {
            $nombre_conductor = $value["nombres_conductor"] . ' ' . $value["apellidos_conductor"];
            $placa_vehiculo = $value["placa_vehiculo"];
            $tipo_vehiculo = $value["tipo_vehiculo"];
        }

        $error = "success";
        $mensaje = "Hola " . $nombre_conductor . "!";
    } else {
        $error = "error";
        $mensaje = "Conductor no encontrado, por favor verificar el número documento.";
    }
}

echo "<script> 

ejecutar_boton();
function ejecutar_boton() {
   if('$error' == 'success'){
    toastr.success('$mensaje','Hola!')
    setTimeout(function() {
 document.getElementById('btn_cerrar_modal').click();
llenar_campos('$documento','$nombre_conductor','$placa_vehiculo','$tipo_vehiculo');
    }, 1700);
    
   }else{
    toastr.error('$mensaje','Hola!')
   }
}

</script>";