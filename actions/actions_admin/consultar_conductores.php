<?php

include '../../database/database.php';

$consultar_conductor = $conn->prepare("SELECT * FROM conductores ORDER BY id desc");
$consultar_conductor->execute();
$consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipos_estados = $conn->prepare("SELECT * FROM estados ORDER BY id desc");
$consultar_tipos_estados->execute();
$consultar_tipos_estados = $consultar_tipos_estados->fetchAll(PDO::FETCH_ASSOC);


?>

<div id="eliminando_conductor"></div>

<table id="foo-filtering" class="table table-bordered table-hover toggle-circle" data-page-size="7">
    <thead>
        <tr>
            <th data-toggle="true">Nombres</th>
            <th>Apellidos</th>
            <th data-hide="phone, tablet">Número identificación</th>
            <th data-hide="phone, tablet">Placa vehiculo</th>
            <!--<th data-hide="phone, tablet">Ruta</th>-->
            <th>Tipo vehículo</th>
            <th data-hide="phone, tablet">Estado</th>
            <th data-hide="phone, tablet">Acción</th>
        </tr>
    </thead>
    <tbody>
        <tr>

            <?php foreach ($consultar_conductor as $conductor) { ?>
            <td id="responsive"><?php echo $conductor["nombres_conductor"] ?></td>
            <td id="responsive"><?php echo $conductor["apellidos_conductor"] ?></td>
            <td id="responsive">
                <?php
                    $id_tipo = $conductor["tipo_identificacion"];
                    $consultar_identificacion = $conn->prepare("SELECT codigo FROM tipo_id WHERE id = '$id_tipo'");
                    $consultar_identificacion->execute();
                    $consultar_identificacion = $consultar_identificacion->fetch(PDO::FETCH_OBJ);

                    echo $consultar_identificacion->codigo;
                    echo ' ';
                    echo $conductor["numero_identificacion"];
                    ?>

            </td>
            <td id="responsive"><?php echo $conductor["placa_vehiculo"] ?></td>
            <td id="responsive">
                <?php echo $conductor['tipo_vehiculo'] ?>
            </td>
            <!--<td id="responsive">
                <?php
                $id_ruta = $conductor["id_ruta"];
                $consultar_rutas = $conn->prepare("SELECT nombre_ruta FROM rutas WHERE id = '$id_ruta'");
                $consultar_rutas->execute();
                $consultar_rutas = $consultar_rutas->fetch(PDO::FETCH_OBJ);

                echo $consultar_rutas->nombre_ruta;
                ?>
            </td>-->
            <td id="responsive">
                <?php
                    if ($conductor["estado"] == 0) {
                        echo '<span class="label label-table label-dark ">Recien creado</span>';
                    } else if ($conductor["estado"] == 1) {
                        echo '<span class="label label-table label-success">Activado</span>';
                    } else if ($conductor["estado"] == 2) {
                        echo '<span class="label label-table label-danger">Desactivado</span>';
                    }
                    ?>


            </td>

            <td id="responsive">
                <button type="button" data-toggle="modal" data-target="#editar_conductor" role="button"
                    onclick="editar_conductor(<?php echo $conductor['id'] ?>)"
                    class="btn btn-outline-primary btn-icon mg-r-5"><i data-feather="edit-3" data-toggle="tooltip"
                        data-trigger="hover" data-placement="top" title=""
                        data-original-title="Ver/Editar información sobre el conductor"></i></button>


                <?php
                    if ($conductor["estado"] == 0) {
                        echo ' <button type="button" data-toggle="modal" href="#formulario1" role="button"
                                onclick="cambiar_estado_conductor(' . $conductor["id"] . ',0)"
                class="btn btn-outline-danger btn-icon mg-r-5"><i data-toggle="tooltip" data-trigger="hover"
                    data-placement="top" title="" data-original-title="Eliminar conductor"
                    data-feather="x-circle"></i></button>';
                    } else if ($conductor["estado"] == 1) {
                        echo ' <button type="button" data-toggle="modal" href="#formulario1" role="button"
                    onclick="cambiar_estado_conductor(' . $conductor["id"] . ',2)"
                class="btn btn-outline-warning btn-icon mg-r-5"><i data-feather="user-x" data-toggle="tooltip"
                    data-trigger="hover" data-placement="top" title="" data-original-title="Desactivar conductor"></i>
                </button>';
                    } else if ($conductor["estado"] == 2) {
                        echo ' <button type="button" data-toggle="modal" href="#formulario1" role="button"
                    onclick="cambiar_estado_conductor(' . $conductor["id"] . ',1)"
                    class="btn btn-outline-success btn-icon mg-r-5"><i data-feather="user-check" data-toggle="tooltip" data-trigger="hover"
                        data-placement="top" title="" data-original-title="Activar conductor" ></i></button>';
                    }
                    ?>
                <button type="button" data-toggle="modal" href="#" role="button"
                    onclick="eliminar_conductor(<?php echo $conductor['id'] ?>)"
                    class="btn btn-outline-danger btn-icon mg-r-5"><i data-toggle="tooltip" data-trigger="hover"
                        data-placement="top" title="" data-original-title="Eliminar conductor"
                        data-feather="x-circle"></i></button>
            </td>
        </tr>
        <?php } ?>

    </tbody>
    <tfoot>
        <tr>
            <td colspan="5">
                <div class="ft-right">
                    <ul class="pagination"></ul>
                </div>
            </td>
        </tr>
    </tfoot>
</table>