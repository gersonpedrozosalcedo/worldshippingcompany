<?php
include '../../database/database.php';

$accion = $_GET["accion"];

if ($accion == 'ultimo10') {
    $consultar_servicios = $conn->prepare("SELECT * FROM servicios_control_rutas ORDER BY id DESC LIMIT 10");
    $consultar_servicios->execute();
    $consultar_servicios = $consultar_servicios->fetchAll(PDO::FETCH_ASSOC);
} else {
    $consultar_servicios = $conn->prepare("SELECT * FROM servicios_control_rutas ORDER BY id DESC ");
    $consultar_servicios->execute();
    $consultar_servicios = $consultar_servicios->fetchAll(PDO::FETCH_ASSOC);
}
?>
<div id="eliminando_servicio"></div>
<table id="foo-filtering" class="table table-bordered table-hover toggle-circle table-responsive-sm" data-page-size="7">
    <thead>
        <tr>
            <th data-toggle="true">Id servicio</th>
            <th data-toggle="true">Cliente</th>
            <th>Do/Ns</th>
            <th data-toggle="true">Tipo servicio</th>
            <th data-toggle="true">Fecha recepción</th>
            <th data-toggle="true">Estado</th>
            <?php if ($accion == 'ultimo10') {
            } else { ?>
            <th data-toggle="true">Acción</th>
            <?php } ?>

        </tr>
    </thead>
    <tbody>

        <?php
        foreach ($consultar_servicios as $servicios) {
        ?>
        <tr>
            <td id="responsive"><?php echo $servicios["id"]; ?></td>
            <td id="responsive">
                <?php
                    $id_cliente = $servicios["id_cliente"];
                    $consultar_cliente = $conn->prepare("SELECT razon_social FROM clientes WHERE id = '$id_cliente'");
                    $consultar_cliente->execute();
                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($consultar_cliente as $cliente) {
                        echo $cliente["razon_social"];
                    }
                    ?></td>
            <td id="responsive"><?php echo $servicios["dons"] ?></td>
            <td id="responsive"><?php
                                    $id_ripo_servicio =  $servicios["tipo_servicio"];
                                    $consultar_tipo_servicio = $conn->prepare("SELECT nombre_servicio FROM tipo_servicios WHERE id = '$id_ripo_servicio'");
                                    $consultar_tipo_servicio->execute();
                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);

                                    foreach ($consultar_tipo_servicio as $tipo_servicios) {
                                        echo $tipo_servicios["nombre_servicio"];
                                    }

                                    ?></td>
            <td id="responsive"><?php echo $servicios["fecha_recepcion_doc"] ?></td>
            <td id="responsive">
                <?php

                    if ($servicios["estado"] == 0) {
                        echo '<a href="javascript:void(0)" class="badge badge-pill  badge-primary">Recien creado</a>';
                    } else if ($servicios["estado"] == 1) {
                        echo '<a href="javascript:void(0)" class="badge badge-pill  badge-warning">En proceso</a>';
                    } else if ($servicios["estado"] == 2) {
                        echo '<a href="javascript:void(0)" class="badge badge-pill  badge-success">Completado</a>';
                    }
                    ?>
            </td>

            <td id="responsive">
                <?php if ($accion == 'ultimo10') {
                    } else {
                    ?>
                <span style="margin: 0px; padding:0px" onclick="editar_servicio(<?php echo $servicios['id'] ?>)"
                    data-toggle="tooltip" data-trigger="hover" data-placement="top" title=""
                    data-original-title="Ver/editar información sobre el control de vehículo en ruta">
                    <button type="button" id="" class="btn btn-outline-primary btn-sm btn-icon mg-r-5">

                        <i class="fa fa-plus-circle"></i>
                    </button></span>

                <span style="margin: 0px; padding:0px"
                    onclick="generar_excel_servicios_unico(<?php echo $servicios['id'] ?>)" data-toggle="tooltip"
                    data-trigger="hover" data-placement="top" title="" data-original-title="Descargar excel">
                    <button type="button" id="" class="btn btn-outline-success  btn-sm btn-icon mg-r-5">

                        <i class="fa fa-file-excel-o"></i>
                    </button></span>

                <a href="javascript:void(0)" onclick="eliminar_servicio(<?php echo $servicios['id'] ?>)"
                    class="btn btn-danger btn-sm">Eliminar</a>
                <?php } ?>

            </td>
        </tr>
        <?php
        }
        ?>


    </tbody>
    <tfoot>
        <tr>
            <td colspan="5">
                <div class="ft-right">
                    <ul class="pagination"></ul>
                </div>
            </td>
        </tr>
    </tfoot>
</table>

<script>
// ///////////////////////////////////////Row Toggler
$("#foo-row-toggler").footable();

// Accordion
$("#foo-accordion")
    .footable()
    .on("footable_row_expanded", function(e) {
        $("#foo-accordion tbody tr.footable-detail-show")
            .not(e.row)
            .each(function() {
                $("#foo-accordion").data("footable").toggleDetail(this);
            });
    });
// Filtering
var filtering = $("#foo-filtering");
filtering.footable().on("footable_filtering", function(e) {
    var selected = $("#foo-filter-status").find(":selected").val();
    e.filter += e.filter && e.filter.length > 0 ? " " + selected : selected;
    e.clear = !e.filter;
});

// Filter status
$("#foo-filter-status").change(function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});

// Search input
$("#foo-search").on("input", function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});
</script>