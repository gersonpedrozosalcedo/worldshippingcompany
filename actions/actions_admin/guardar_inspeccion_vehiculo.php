<?php
session_start();
include '../../database/database.php';


$conductor = $_POST["conductor"];
$identificacion = $_POST["identificacion_conductor"];
$placa_vehiculo = $_POST["placa_vehiculo"];
$tipo_vehiculo = $_POST["tipo_vehiculo"];


require '../../vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$dirPrincipal = '../../vendor/phpmailer';
require "$dirPrincipal/phpmailer/src/Exception.php";
require "$dirPrincipal/phpmailer/src/PHPMailer.php";
require "$dirPrincipal/phpmailer/src/SMTP.php";
date_default_timezone_set('America/Bogota');


$id_inspeccions = $_POST["id_inspeccion"];


if ($id_inspeccions != null) {

    $id_inspeccion = $id_inspeccions;
    $concepto = $_POST["concepto"];
    $seleccion = $_POST["seleccion"];
    $observacion = $_POST["observacion"];
    $evidencias_name = $_FILES["evidencias"]["name"];
    $evidencias_tmp = $_FILES["evidencias"]["tmp_name"];

    $url_encode = base64_encode("http://worldshippingcompany.com.co/actions/actions_admin/generar_excel_inspeccion.php?id_inspeccion=" + $id_inspeccion);

    for ($i = 0; $i < count($concepto); $i++) {
        $conceptos =  $concepto[$i];
        $selecciones = $seleccion[$i];
        $observaciones = $observacion[$i];

        $guardando_inspeccion_relacion = $conn->prepare("INSERT INTO control_inspeccion_vehiculo_relacion (id_inspeccion,concepto,seleccion,observacion,fecha_creacion) VALUES ('$id_inspeccion','$conceptos','$selecciones','$observaciones',NOW())");
        $guardando_inspeccion_relacion->execute();

        $id_inspeccion_relacion = $conn->lastInsertId();


        $directorio = "../../foto_evidencia_inspeccion_vehiculo/$id_inspeccion_relacion";
        if (!file_exists($directorio)) {
            mkdir($directorio, 0777) or die("No se puede crear el directorio de extracci&oacute;n");
        }
        $dir = opendir($directorio);

        $filename = $evidencias_name[$i];
        $source = $evidencias_tmp[$i];
        $target_path = $directorio . '/' . $filename;
        move_uploaded_file($source, $target_path);

        if ($filename != null) {
            $guardar_evidencias = $conn->prepare("INSERT INTO control_inspeccion_vehiculo_evidencias (id_inspeccion,nombre_file,fecha_creacion) VALUES ('$id_inspeccion_relacion','$filename',NOW())");
            $guardar_evidencias->execute();
        }
    }

    $consultar_inspeccion = $conn->prepare("SELECT * FROM control_inspeccion_vehiculo WHERE id = $id_inspeccion");
    $consultar_inspeccion->execute();
    $consultar_inspeccion = $consultar_inspeccion->fetchAll(PDO::FETCH_ASSOC);

    foreach ($consultar_inspeccion as $inspeccion_montacarga) {

        $conductor_m = $inspeccion_montacarga["conductor"];
        $identificacion_m = $inspeccion_montacarga["identificacion_conductor"];
        $placa_vehiculo_m = $inspeccion_montacarga["placa_vehiculo"];
        $tipo_vehiculo_m = $inspeccion_montacarga["tipo_vehiculo"];
    }

    $consultar_emails_copia_empleado = $conn->prepare("SELECT * FROM empleados WHERE cargo = 1 || cargo = 5 ");
    $consultar_emails_copia_empleado->execute();
    $consultar_emails_copia_empleado = $consultar_emails_copia_empleado->fetchAll(PDO::FETCH_ASSOC);

    $email_empelado = 'admin@worldshippingcompany.com.co';
    // $email_empelado = 'gpedrozos@unicartagena.edu.co';
    $mailBody = '<html>
    
    <head>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            max-width: 100%;
            margin-top: 20px;
            margin-left: 10px;
            margin-right: 10px;
            text-align: center;
            font-family: arial;
            background: #ffffff;
        }
    
        .title {
            margin: 20px;
            color: grey;
            font-size: 14x;
            text-align: center;
        }
    
        .button {
            border: none;
            border-radius: 20px;
            outline: 0;
            display: inline-block;
            padding: 8px;
            color: white;
            background-color: #28a49c;
            text-align: center;
            cursor: pointer;
            width: 80%;
            margin: 8px;
            font-size: 14px;
        }
    
        .button:hover,
        a:hover {
            opacity: 0.7;
        }
    
        .flex-container {
            display: flex;
    
            margin-left: auto;
            margin-right: auto;
            width: 53%;
        }
    
        @media (min-width: 600px) {
            .flex-container {
                display: flex;
    
                margin-left: auto;
                margin-right: auto;
                width: 20%;
            }
        }
    
        .flex-container>div {
    
            margin: 10px;
    
            color: white;
        }
    
        .flex-container2 {
            display: flex;
    
        }
    
        .flex-container2>div {
    
            margin: 10px;
    
            color: white;
        }
        </style>
    </head>
    
    <body style="background:#0b6ea7;">
    
        <div style="background: white; border-radius: 0px 0px 20px 20px; ">
            <center> <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:100px"></center>
        </div>
    
        <div class="card" style=" padding:10px; border-radius:20px;">
            <center> <img src="http://worldshippingcompany.com.co/assets/images/mensaje.png" style="width:100px"></center>
    
            <p class="title">¡Hola, nueva actualización sobre la inspección del vehículo (' . $tipo_vehiculo_m . ') placa (' . $placa_vehiculo_m . ') registrada!.</p>
            <hr>
            <p class="title" style="text-align: justify">
                <b>Detalles del servicio:</b><br>
                <br>
                <b>Id inspección:</b>' . $id_inspeccion . ' <br>
                <b>Tipo de vehiculo:</b>' . $tipo_vehiculo_m . '<br>
                <b>Nombre conductor:</b> ' . $conductor_m . '<br>
                <b>Número identificación:</b> ' . $identificacion_m . '<br>
                <b>Placa vehículo:</b> ' . $placa_vehiculo_m . '<br>
            </p>
            <hr>
            <center class="title">Puedes ver los detalles completos de la inspección descargando el siguiente excel <br><a href="http://worldshippingcompany.com.co/descargar_excel_inspeccion?url_excel=' . $url_encode . '&t_v=' . $tipo_vehiculo . ' " style=" background-color: #0b6ea7;;
                            border: 1px solid;
                            color: white;
                            padding: 7px 14px;  
                            text-align: center;
                            border-radius: 10px 10px 10px 10px; 
                            text-decoration: none;
                            display: inline-block;
                            font-size: 16px;
                            margin: 4px 2px;
                            cursor: pointer;" ><b>¡Descargar!</b></a> </center>
            <hr>
            <p class="title" style="font-size:12px;"><b>Correo generado en la siguiente fecha y hora: ' . date("Y-m-d H:i:s") . '</b></p>
        </div>
    
    
    
        <hr>
        <center style="color:white"> Copyright © World Shipping Company S.A.S 2022 </center>
    </body>
    
    </html>';

    $mailSubject = '(Actulización de inspección del vehículo) Conductor: ' . $conductor_m . ' - Vehículo (' . $tipo_vehiculo_m . '): ' . $placa_vehiculo_m . '.'; /// consecutivo + nombre_cliente + do +
    $mail = new PHPMailer(true);
    $mailHost = "a2plcpnl0093.prod.iad2.secureserver.net";
    $mailUsername   = 'notificaciones@worldshippingcompany.com.co';                     //SMTP username
    $mailPassword   = 'Worldshipping@';
    $mailFrom = 'notificaciones@worldshippingcompany.com.co';
    $mail->SMTPDebug = 0; //SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();
    $mail->CharSet = 'UTF-8';                         //Send using SMTP
    $mail->Host       = $mailHost;                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = $mailUsername;                     //SMTP username
    $mail->Password   = $mailPassword;
    $mail->SMTPSecure = 'ssl';
    $mail->Port = 465;
    $mail->setFrom($mailFrom, 'World Shipping Company S.A.S');
    //  $mail->addAddress($email_empelado);     //Add a recipient
    // $mail->addAddress('gersonpedrozosalcedo@gmail.com'); //pruebas 
    $mail->addAddress('jmontiel@worldshippingcompany.com.co'); //produccion
    /* foreach($consultar_emails_copia as $email_copias){
      $email_copia_cliente = $email_copias["email"];
        $mail->addCC($email_copia_cliente);  // correo en copias
  } */
    $mail->addCC('rrico@worldshippingcompany.com.co'); //produccion 
    $mail->addCC('ngonzalez@worldshippingcompany.com.co'); //produccion 
    $mail->addCC('gersonpedrozosalcedo@gmail.com'); //pruebas 
    /* foreach($consultar_emails_copia_empleado as $email_copias_empleado){
      $email_copia_empleado = $email_copias_empleado["email"];
        $mail->addCC($email_copia_empleado);  // correo en copias
  }*/
    $mail->isHTML(true);
    $mail->Subject = $mailSubject;
    $mail->Body    = $mailBody;
    $mail->send();

    $error = "success";
    $mensaje = "Inspección guardada correctamente.";
} else {

    if ($conductor == null) {
        $error = "error";
        $mensaje = "Ingresar un nombre conductor";
    } else if ($identificacion == null) {
        $error = "error";
        $mensaje = "Ingresar idetificación conductor";
    } else if ($placa_vehiculo == null) {
        $error = "error";
        $mensaje = "Ingresar placa del vehículo";
    } else {

        $guardando_inspeccion = $conn->prepare("INSERT INTO control_inspeccion_vehiculo (conductor,identificacion_conductor,placa_vehiculo,fecha_creacion,tipo_vehiculo,estado) VALUES ('$conductor','$identificacion','$placa_vehiculo',NOW(),'$tipo_vehiculo',0)");
        $guardando_inspeccion->execute();

        $id_inspeccion = $conn->lastInsertId();
        $url_encode = base64_encode("http://worldshippingcompany.com.co/actions/actions_admin/generar_excel_inspeccion.php?id_inspeccion=" + $id_inspeccion);

        $concepto = $_POST["concepto"];
        $seleccion = $_POST["seleccion"];
        $observacion = $_POST["observacion"];
        $evidencias_name = $_FILES["evidencias"]["name"];
        $evidencias_tmp = $_FILES["evidencias"]["tmp_name"];


        for ($i = 0; $i < count($concepto); $i++) {
            $conceptos =  $concepto[$i];
            $selecciones = $seleccion[$i];
            $observaciones = $observacion[$i];

            $guardando_inspeccion_relacion = $conn->prepare("INSERT INTO control_inspeccion_vehiculo_relacion (id_inspeccion,concepto,seleccion,observacion,fecha_creacion) VALUES ('$id_inspeccion','$conceptos','$selecciones','$observaciones',NOW())");
            $guardando_inspeccion_relacion->execute();

            $id_inspeccion_relacion = $conn->lastInsertId();


            $directorio = "../../foto_evidencia_inspeccion_vehiculo/$id_inspeccion_relacion";
            if (!file_exists($directorio)) {
                mkdir($directorio, 0777) or die("No se puede crear el directorio de extracci&oacute;n");
            }
            $dir = opendir($directorio);

            $filename = $evidencias_name[$i];
            $source = $evidencias_tmp[$i];
            $target_path = $directorio . '/' . $filename;
            move_uploaded_file($source, $target_path);

            if ($filename != null) {
                $guardar_evidencias = $conn->prepare("INSERT INTO control_inspeccion_vehiculo_evidencias (id_inspeccion,nombre_file,fecha_creacion) VALUES ('$id_inspeccion_relacion','$filename',NOW())");
                $guardar_evidencias->execute();
            }
        }

        $consultar_emails_copia_empleado = $conn->prepare("SELECT * FROM empleados WHERE cargo = 1 || cargo = 5 ");
        $consultar_emails_copia_empleado->execute();
        $consultar_emails_copia_empleado = $consultar_emails_copia_empleado->fetchAll(PDO::FETCH_ASSOC);

        $email_empelado = 'admin@worldshippingcompany.com.co';
        // $email_empelado = 'gpedrozos@unicartagena.edu.co';
        $mailBody = '<html>
        
        <head>
            <meta charset="UTF-8" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <style>
            .card {
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
                max-width: 100%;
                margin-top: 20px;
                margin-left: 10px;
                margin-right: 10px;
                text-align: center;
                font-family: arial;
                background: #ffffff;
            }
        
            .title {
                margin: 20px;
                color: grey;
                font-size: 14x;
                text-align: center;
            }
        
            .button {
                border: none;
                border-radius: 20px;
                outline: 0;
                display: inline-block;
                padding: 8px;
                color: white;
                background-color: #28a49c;
                text-align: center;
                cursor: pointer;
                width: 80%;
                margin: 8px;
                font-size: 14px;
            }
        
            .button:hover,
            a:hover {
                opacity: 0.7;
            }
        
            .flex-container {
                display: flex;
        
                margin-left: auto;
                margin-right: auto;
                width: 53%;
            }
        
            @media (min-width: 600px) {
                .flex-container {
                    display: flex;
        
                    margin-left: auto;
                    margin-right: auto;
                    width: 20%;
                }
            }
        
            .flex-container>div {
        
                margin: 10px;
        
                color: white;
            }
        
            .flex-container2 {
                display: flex;
        
            }
        
            .flex-container2>div {
        
                margin: 10px;
        
                color: white;
            }
            </style>
        </head>
        
        <body style="background:#0b6ea7;">
        
            <div style="background: white; border-radius: 0px 0px 20px 20px; ">
                <center> <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:100px"></center>
            </div>
        
            <div class="card" style=" padding:10px; border-radius:20px;">
                <center> <img src="http://worldshippingcompany.com.co/assets/images/mensaje.png" style="width:100px"></center>
        
                <p class="title">¡Hola, nueva inspección de vehículo (' . $tipo_vehiculo . ') placa (' . $placa_vehiculo . ') registrada!.</p>
                <hr>
                <p class="title" style="text-align: justify">
                    <b>Detalles del servicio:</b><br>
                    <br>
                    <b>Id inspección:</b>' . $id_inspeccion . ' <br>
                    <b>Tipo de vehiculo:</b>' . $tipo_vehiculo . '<br>
                    <b>Nombre conductor:</b> ' . $conductor . '<br>
                    <b>Número identificación:</b> ' . $identificacion . '<br>
                    <b>Placa vehículo:</b> ' . $placa_vehiculo . '<br>
                </p>
                <hr>
                <center class="title">Puedes ver los detalles completos de la inspección descargando el siguiente excel <br><a href="http://worldshippingcompany.com.co/descargar_excel_inspeccion?url_excel=' . $url_encode . '&t_v=' . $tipo_vehiculo . '" style=" background-color: #0b6ea7;;
                                border: 1px solid;
                                color: white;
                                padding: 7px 14px;  
                                text-align: center;
                                border-radius: 10px 10px 10px 10px; 
                                text-decoration: none;
                                display: inline-block;
                                font-size: 16px;
                                margin: 4px 2px;
                                cursor: pointer;" ><b>¡Descargar!</b></a> </center>
                <hr>
                <p class="title" style="font-size:12px;"><b>Correo generado en la siguiente fecha y hora: ' . date("Y-m-d H:i:s") . '</b></p>
            </div>
        
        
        
            <hr>
            <center style="color:white"> Copyright © World Shipping Company S.A.S 2022 </center>
        </body>
        
        </html>';

        $mailSubject = '(Nueva inspección de vehículo) Conductor: ' . $conductor . ' - Vehículo (' . $tipo_vehiculo . '): ' . $placa_vehiculo . '.'; /// consecutivo + nombre_cliente + do +
        $mail = new PHPMailer(true);
        $mailHost = "a2plcpnl0093.prod.iad2.secureserver.net";
        $mailUsername   = 'notificaciones@worldshippingcompany.com.co';                     //SMTP username
        $mailPassword   = 'Worldshipping@';
        $mailFrom = 'notificaciones@worldshippingcompany.com.co';
        $mail->SMTPDebug = 0; //SMTP::DEBUG_SERVER;                      //Enable verbose debug output
        $mail->isSMTP();
        $mail->CharSet = 'UTF-8';                         //Send using SMTP
        $mail->Host       = $mailHost;                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = $mailUsername;                     //SMTP username
        $mail->Password   = $mailPassword;
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->setFrom($mailFrom, 'World Shipping Company S.A.S');
        //  $mail->addAddress($email_empelado);     //Add a recipient
        // $mail->addAddress('gersonpedrozosalcedo@gmail.com'); //pruebas 
        $mail->addAddress('jmontiel@worldshippingcompany.com.co'); //produccion
        /* foreach($consultar_emails_copia as $email_copias){
         $email_copia_cliente = $email_copias["email"];
           $mail->addCC($email_copia_cliente);  // correo en copias
     } */
        $mail->addCC('rrico@worldshippingcompany.com.co'); //produccion 
        $mail->addCC('ngonzalez@worldshippingcompany.com.co'); //produccion 
        //  $mail->addCC('gersonpedrozosalcedo@gmail.com'); //pruebas 
        $mail->addCC('gersonpedrozosalcedo@gmail.com'); //pruebas 
        /* foreach($consultar_emails_copia_empleado as $email_copias_empleado){
         $email_copia_empleado = $email_copias_empleado["email"];
           $mail->addCC($email_copia_empleado);  // correo en copias
     }*/
        $mail->isHTML(true);
        $mail->Subject = $mailSubject;
        $mail->Body    = $mailBody;
        $mail->send();

        $error = "success";
        $mensaje = "Inspección guardada correctamente.";
    }
}



echo "<script> 

ejecutar_boton();
function ejecutar_boton() {
   if('$error' == 'success'){
    toastr.success('$mensaje','Hola!')
    setTimeout(function() {

    if('$id_inspeccions' === ''){
        redirect();
    }else{
        tabla_inspeccion_vehiculo();
    }
    }, 1700);
    
   }else{
    toastr.error('$mensaje','Hola!')
   }
}

</script>";