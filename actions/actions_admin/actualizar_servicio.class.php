<?php


include '../../database/database.php';
require '../../vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$dirPrincipal = '../../vendor/phpmailer';
require "$dirPrincipal/phpmailer/src/Exception.php";
require "$dirPrincipal/phpmailer/src/PHPMailer.php";
require "$dirPrincipal/phpmailer/src/SMTP.php";

class actualizar_servicio
{

    public static $tablename = "servicios_control_rutas";

    public static function  update_servicio($datos, $conn, $id_servicio)
    {

        unset($datos['id_servicio']); //eliminando el id_servicio del array _POST
        unset($datos['evidencias']);
        unset($datos['mensaje_correo']);
        $complemento = "";
        foreach ($datos as $key => $value) {
            $complemento .= $key . " ='" . $value . "',";
            $formateado = trim($complemento, ',');
            $id_servicio_formateado = "'" . $id_servicio . "'";
            $sql = 'UPDATE servicios_control_rutas SET ' . $formateado . ' ' . 'WHERE id=' . $id_servicio_formateado;
        }
        $actualizar_servicio = $conn->prepare($sql);
        $actualizar_servicio->execute();

        $sql_actualizar = "UPDATE  servicios_control_rutas  SET estado = 1 WHERE id = '$id_servicio' ";
        $actualizar_servicio = $conn->prepare($sql_actualizar);
        $actualizar_servicio->execute();

        $count = $actualizar_servicio->rowCount(); // validando que s ehaya ejecutado la sentencia.



        if ($count === '0') {
            $confirmacion = [
                "error" => "error",
                "mensaje" => "Hubo un error, intentelo más tarde o ya has ingresado estos datos.",
                "mensaje_error" => $sql
            ];
        } else {
            $confirmacion = [
                "error" => "success",
                "mensaje" => "Servicio actualizado correctamente.",
                "mensaje_error" => $sql
            ];
        }

        return $confirmacion;
    }

    public  static function enviar_correo($datos, $conn, $mensaje_correo)
    {

        $id_servicio = $datos["id_servicio"];
        $consultar_servicio = $conn->prepare("SELECT * FROM servicios_control_rutas WHERE id = '$id_servicio'");
        $consultar_servicio->execute();
        $consultar_servicio = $consultar_servicio->fetchAll(PDO::FETCH_ASSOC);

        foreach ($consultar_servicio as $servicio) {
            $id_cliente = $servicio["id_cliente"];
            $id_sub_cliente = $servicio["id_sub_cliente"];
            $fecha_recepcion = $servicio["fecha_recepcion_doc"];
            $contenedor = $servicio["contenedor"];
            $tamaño_contenedor = $servicio["tamaño_contenedor"];
            $tipo_carga = $servicio["tipo_carga"];
            $dons = $servicio["dons"];
            $tipo_servicio = $servicio["tipo_servicio"];
        }

        $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
        $consultar_cliente->execute();
        $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);

        foreach ($consultar_cliente as $clientes) {
            $razon_social = $clientes["razon_social"];
            $email_cliente = $clientes["email"];
        }
        $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$tipo_servicio'");
        $consultar_tipo_servicio->execute();
        $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);


        $consultar_emails_copia = $conn->prepare("SELECT * FROM email_copia_clientes WHERE  id_cliente = '$id_cliente' AND estado = 1 ");
        $consultar_emails_copia->execute();
        $consultar_emails_copia = $consultar_emails_copia->fetchAll(PDO::FETCH_ASSOC);

        foreach ($consultar_tipo_servicio as $tipo_servicio) {
            $nombre_servicio = $tipo_servicio["nombre_servicio"];
        }
        $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$tipo_carga'");
        $consultar_tipo_carga->execute();
        $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
        foreach ($consultar_tipo_carga as $tipo_carga) {
            $nombre_carga = $tipo_carga["nombre_carga"];
        }

        $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
        $consultar_sub_cliente->execute();
        $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
        foreach ($consultar_sub_cliente as $sub_cliente) {
            $nombre_sub_cliente = $sub_cliente["nombre_sub_cliente"];
        }

        if ($id_sub_cliente != 0) {
            $nombre_sub_cliente = $sub_cliente["nombre_sub_cliente"];
        } else {
            $nombre_sub_cliente = 'Sin registrar.';
        }

        if ($nombre_carga == null) {
            $nombre_cargas = 'Sin registrar';
        } else {
            $nombre_cargas = $nombre_carga;
        }

        if ($contenedor == null) {
            $contenedors = 'Sin registrar';
        } else {
            $contenedors = $contenedor;
        }

        if ($tamaño_contenedor == null) {
            $tamaño_contenedors = 'Sin registrar';
        } else {
            $tamaño_contenedors = $tamaño_contenedor;
        }
        $consultar_emails_copia_empleado = $conn->prepare("SELECT * FROM empleados");
        $consultar_emails_copia_empleado->execute();
        $consultar_emails_copia_empleado = $consultar_emails_copia_empleado->fetchAll(PDO::FETCH_ASSOC);

        $mailBody =  $mailBody = '<html>
            
            <head>
                <meta charset="UTF-8" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                <style>
                .card {
                    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
                    max-width: 100%;
                    margin-top: 20px;
                    margin-left: 10px;
                    margin-right: 10px;
                    text-align: center;
                    font-family: arial;
                    background: #ffffff;
                }
            
                .title {
                    margin: 20px;
                    color: grey;
                    font-size: 14x;
                    text-align: center;
                }
            
                .button {
                    border: none;
                    border-radius: 20px;
                    outline: 0;
                    display: inline-block;
                    padding: 8px;
                    color: white;
                    background-color: #28a49c;
                    text-align: center;
                    cursor: pointer;
                    width: 80%;
                    margin: 8px;
                    font-size: 14px;
                }
            
                .button:hover,
                a:hover {
                    opacity: 0.7;
                }
            
                .flex-container {
                    display: flex;
            
                    margin-left: auto;
                    margin-right: auto;
                    width: 53%;
                }
            
                @media (min-width: 600px) {
                    .flex-container {
                        display: flex;
            
                        margin-left: auto;
                        margin-right: auto;
                        width: 20%;
                    }
                }
            
                .flex-container>div {
            
                    margin: 10px;
            
                    color: white;
                }
            
                .flex-container2 {
                    display: flex;
            
                }
            
                .flex-container2>div {
            
                    margin: 10px;
            
                    color: white;
                }
                </style>
            </head>
            
            <body style="background:#0b6ea7;">
            
                <div style="background: white; border-radius: 0px 0px 20px 20px; ">
                    <center> <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:100px"></center>
                </div>
            
                <div class="card" style=" padding:10px; border-radius:20px;">
                <center> <img src="http://worldshippingcompany.com.co/assets/images/mensaje.png" style="width:100px"></center>
            
                    <p class="title">¡Hola ' . $razon_social . '!, el servicio #' . $id_servicio . ' ha tomado un nuevo estado (actualizado).</p>
                    <hr>
                    <p class="title" style="text-align: justify">
                        <b>Detalles del servicio:</b><br>
                        <br>
                        <b>Fecha recepción de los documentos:</b> ' . $fecha_recepcion . '<br>
                        <b>Cliente:</b> ' . $razon_social . '<br>
                        <b>Sub-cliente:</b> ' . $nombre_sub_cliente . '<br>
                        <b>Do/Ns:</b> ' . $dons . '<br>
                        <b>Tipo de servicio:</b> ' . $nombre_servicio . '<br>

                        <hr>
                        <b>NOTA*: ' . $mensaje_correo . '</b>
                        <hr>
                        
                        <center class="title">Puedes ver los detalles completos del servicio presionando <br><a href="worldshippingcompany.com.co/estado_servicio?service=' . base64_encode($id_servicio) . '" style=" background-color: #0b6ea7;;
                        border: 1px solid;
                        color: white;
                        padding: 7px 14px;  
                        text-align: center;
                        border-radius: 10px 10px 10px 10px; 
                        text-decoration: none;
                        display: inline-block;
                        font-size: 16px;
                        margin: 4px 2px;
                        cursor: pointer;" ><b>¡aquí!</b></a> </center>

                    </p>
                    <hr>
                    <p class="title" style="font-size:12px;"><b>Correo generado en la siguiente fecha y hora: ' . date("Y-m-d H:i:s") . '</b></p>
                </div><br>
                <center style="color:white"> Copyright © World Shipping Company S.A.S 2022 </center>
            </body>
            
            </html>';
        $mailSubject = '(SERVICIO ACTUALIZADO) ID: ' . $id_servicio . ' - #SERVICIO DO/NS/RESERVA: ' . $dons . ' - CLIENTE: ' . $razon_social . ' - # CONTENEDOR: ' . $contenedors . '.'; /// consecutivo + nombre_cliente + do +
        $mail = new PHPMailer(true);
        $mailHost = "a2plcpnl0093.prod.iad2.secureserver.net";
        $mailUsername   = 'notificaciones@worldshippingcompany.com.co';                     //SMTP username
        $mailPassword   = 'Worldshipping@';
        $mailFrom = 'notificaciones@worldshippingcompany.com.co';
        $mail->SMTPDebug = 0; //SMTP::DEBUG_SERVER;                      //Enable verbose debug output
        $mail->isSMTP();
        $mail->CharSet = 'UTF-8';                         //Send using SMTP
        $mail->Host       = $mailHost;                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = $mailUsername;                     //SMTP username
        $mail->Password   = $mailPassword;
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->setFrom($mailFrom, 'World Shipping Company S.A.S');
        $mail->addAddress($email_cliente);     //Add a recipient
        // $mail->addAddress("gersonpedrozosalcedo@gmail.com");
        foreach ($consultar_emails_copia as $email_copias) {
            $email_copia_cliente = $email_copias["email"];
            $mail->addCC($email_copia_cliente);  // correo en copias
        }
        /*  $mail->addCC('jmontiel@worldshippingcompany.com.co'); //produccion
        $mail->addCC('rrico@worldshippingcompany.com.co'); //produccion 
        $mail->addCC('ngonzalez@worldshippingcompany.com.co'); //produccion 
        $mail->addCC('facturacion@serviportuarios.com'); //produccion 
        $mail->addCC('contabilidad@serviportuarios.com'); //produccion 

         $mail->addCC('gersonsal15@hotmail.com'); //pruebas 
                    $mail->addCC('gersonflow1@hotmail.com'); //pruebas
                    $mail->addCC('coordinador@serviportuarios.com'); 
                    $mail->addCC('transporte@serviportuarios.com'); 
                    $mail->addCC('contabilidad@serviportuarios.com'); 
                    $mail->addCC('gcoordinadorpuertos@serviportuarios.com'); */
        $mail->isHTML(true);
        $mail->Subject = $mailSubject;
        $mail->Body    = $mailBody;
        $mail->send();

        /////////////////////////////

        $email_empelado = 'admin@worldshippingcompany.com.co';
        //  $email_empelado = 'gpedrozos@unicartagena.edu.co';
        $mailBody = '<html>
                    
                    <head>
                        <meta charset="UTF-8" />
                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                        <style>
                        .card {
                            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
                            max-width: 100%;
                            margin-top: 20px;
                            margin-left: 10px;
                            margin-right: 10px;
                            text-align: center;
                            font-family: arial;
                            background: #ffffff;
                        }
                    
                        .title {
                            margin: 20px;
                            color: grey;
                            font-size: 14x;
                            text-align: center;
                        }
                    
                        .button {
                            border: none;
                            border-radius: 20px;
                            outline: 0;
                            display: inline-block;
                            padding: 8px;
                            color: white;
                            background-color: #28a49c;
                            text-align: center;
                            cursor: pointer;
                            width: 80%;
                            margin: 8px;
                            font-size: 14px;
                        }
                    
                        .button:hover,
                        a:hover {
                            opacity: 0.7;
                        }
                    
                        .flex-container {
                            display: flex;
                    
                            margin-left: auto;
                            margin-right: auto;
                            width: 53%;
                        }
                    
                        @media (min-width: 600px) {
                            .flex-container {
                                display: flex;
                    
                                margin-left: auto;
                                margin-right: auto;
                                width: 20%;
                            }
                        }
                    
                        .flex-container>div {
                    
                            margin: 10px;
                    
                            color: white;
                        }
                    
                        .flex-container2 {
                            display: flex;
                    
                        }
                    
                        .flex-container2>div {
                    
                            margin: 10px;
                    
                            color: white;
                        }
                        </style>
                    </head>
                    
                    <body style="background:#0b6ea7;">
            
                    <div style="background: white; border-radius: 0px 0px 20px 20px; ">
                        <center> <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:100px"></center>
                    </div>
                
                    <div class="card" style=" padding:10px; border-radius:20px;">
                    <center> <img src="http://worldshippingcompany.com.co/assets/images/mensaje.png" style="width:100px"></center>
                
                        <p class="title">¡Hola!, el servicio #' . $id_servicio . ' ha tomado un nuevo estado (actualizado).</p>
                        <hr>
                        <p class="title" style="text-align: justify">
                            <b>Detalles del servicio:</b><br>
                            <br>
                            <b>Fecha recepción de los documentos:</b> ' . $fecha_recepcion . '<br>
                            <b>Cliente:</b> ' . $razon_social . '<br>
                            <b>Sub-cliente:</b> ' . $nombre_sub_cliente . '<br>
                            <b>Do/Ns:</b> ' . $dons . '<br>
                            <b>Tipo de servicio:</b> ' . $nombre_servicio . '<br>

                            <hr>
                            <b>NOTA*: ' . $mensaje_correo . '</b>
                            <hr>

                            <center class="title">Puedes ver los detalles completos del servicio presionando <br><a href="worldshippingcompany.com.co/estado_servicio?service=' . base64_encode($id_servicio) . '" style=" background-color: #0b6ea7;;
                            border: 1px solid;
                            color: white;
                            padding: 7px 14px;  
                            text-align: center;
                            border-radius: 10px 10px 10px 10px; 
                            text-decoration: none;
                            display: inline-block;
                            font-size: 16px;
                            margin: 4px 2px;
                            cursor: pointer;" ><b>¡Aquí!</b></a> </center>
    
                        </p>
                        <hr>
                        <p class="title" style="font-size:12px;"><b>Correo generado en la siguiente fecha y hora: ' . date("Y-m-d H:i:s") . '</b></p>
                    </div><br>
                    <center style="color:white"> Copyright © World Shipping Company S.A.S 2022 </center>
                </body>
                    
                    </html>';

        $mailSubject = '(SERVICIO ACTUALIZADO) ID: ' . $id_servicio . ' - #SERVICIO DO/NS/RESERVA: ' . $dons . ' - CLIENTE: ' . $razon_social . ' - # CONTENEDOR: ' . $contenedors . '.'; /// consecutivo + nombre_cliente + do +
        $mail = new PHPMailer(true);
        $mailHost = "a2plcpnl0093.prod.iad2.secureserver.net";
        $mailUsername   = 'notificaciones@worldshippingcompany.com.co';                     //SMTP username
        $mailPassword   = 'Worldshipping@';
        $mailFrom = 'notificaciones@worldshippingcompany.com.co';
        $mail->SMTPDebug = 0; //SMTP::DEBUG_SERVER;                      //Enable verbose debug output
        $mail->isSMTP();
        $mail->CharSet = 'UTF-8';                         //Send using SMTP
        $mail->Host       = $mailHost;                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = $mailUsername;                     //SMTP username
        $mail->Password   = $mailPassword;
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->setFrom($mailFrom, 'World Shipping Company S.A.S');
        $mail->addAddress($email_empelado);     //Add a recipient

        $mail->addCC('jmontiel@worldshippingcompany.com.co'); //produccion
        $mail->addCC('rrico@worldshippingcompany.com.co'); //produccion 
        $mail->addCC('ngonzalez@worldshippingcompany.com.co'); //produccion 
        $mail->addCC('facturacion@serviportuarios.com'); //produccion 


        // $mail->addAddress("gersonpedrozosalcedo@gmail.com");
        /* foreach($consultar_emails_copia as $email_copias){
                            $email_copia_cliente = $email_copias["email"];
                              $mail->addCC($email_copia_cliente);  // correo en copias
                        } */
        /* $mail->addCC('gersonsal15@hotmail.com'); //pruebas 
                        $mail->addCC('facturacion@serviportuarios.com'); //pruebas */
        /* foreach ($consultar_emails_copia_empleado as $email_copias_empleado) {
            $email_copia_empleado = $email_copias_empleado["email"];
            $mail->addCC($email_copia_empleado);  // correo en copias
        }*/
        $mail->isHTML(true);
        $mail->Subject = $mailSubject;
        $mail->Body    = $mailBody;
        // $mail->send();

        /* if ($count == '0') {
            $confirmacion = [
                "error" => "error",
                "mensaje" => "Hubo un error, intentelo más tarde o ya has ingresado estos datos.",
                "mensaje_error" => 
            ];
        } else {
            $confirmacion = [
                "error" => "success",
                "mensaje" => "Servicio actualizado correctamente."
            ];
        }

        return $confirmacion;*/
    }
}