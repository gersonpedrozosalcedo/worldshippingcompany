<?php

include '../../database/database.php';


$nombres = $_POST["nombres"];
$apellidos = $_POST["apellidos"];
$tipo_id = $_POST["tipo_id"];
$numero_identificacion = $_POST["numero_identificacion"];
$placa_vehiculo = $_POST["placa_vehiculo"];
//$ruta = $_POST["ruta"];
$tipo_vehiculo = $_POST["tipo_vehiculo"];
$foto_firma = $_FILES["foto_firma"]["name"];
$estado = $_POST["estado"];
if ($nombres == null) {
   $error = "error";
   $mensaje = "Por favor ingrese el nombre del empleado.";
} else if ($apellidos == null) {
   $error = "error";
   $mensaje = "Por favor ingrese los apellidos del empleado.";
} else if ($tipo_id == null) {
   $error = "error";
   $mensaje = "Por favor ingrese el tipo de documento.";
} else if ($numero_identificacion == null) {
   $error = "error";
   $mensaje = "Por favor ingrese un número de identificación.";
} else if ($placa_vehiculo == null) {
   $error = "error";
   $mensaje = "Por favor ingrese la placa del vehiculo.";
}/*else if($ruta == null){
   $error= "error";
   $mensaje = "Por favor seleccione una rutas.";
}*/ else if ($estado == null) {
   $error = "error";
   $mensaje = "Por favor seleccione el estado del usuario.";
} else if ($foto_firma == null) {
   $error = "error";
   $mensaje = "Por favor ingrese la firma digital del conductor.";
} else if ($tipo_vehiculo == null) {
   $error = "error";
   $mensaje = "Por favor ingrese el tipo de vehículo.";
} else {


   $guardar_conductor = $conn->prepare("INSERT INTO conductores (nombres_conductor,apellidos_conductor,tipo_identificacion,numero_identificacion,placa_vehiculo/*,id_ruta*/,estado,tipo_vehiculo,fecha_creacion)
     VALUES ('$nombres','$apellidos','$tipo_id','$numero_identificacion','$placa_vehiculo'/*,'$ruta'*/,'$estado','$tipo_vehiculo',NOW())");

   $guardar_conductor->execute();

   if ($guardar_conductor === null) {
      $error = "error";
      $mensaje = "Hubo un error, inténtelo más tarde.";
   } else {
      $error = "success";
      $mensaje = "Conductor guardado exitosamente.";
      $id_conductor = $conn->lastInsertId();
      $directorio = "../../foto_firma_conductores/$id_conductor";
      if (!file_exists($directorio)) {
         mkdir($directorio, 0777) or die("No se puede crear el directorio de extracci&oacute;n");
      }

      $dir = opendir($directorio);
      $filename = $foto_firma;
      $source = $_FILES["foto_firma"]["tmp_name"];
      $target_path = $directorio . '/' . $filename;
      move_uploaded_file($source, $target_path);

      if ($filename != null) {
         $guardar_foto = $conn->prepare("UPDATE conductores SET firma_digital = '$foto_firma'  where id= '$id_conductor'");
         $guardar_foto->execute();
      }
   }
}



echo "<script> 

 ejecutar_boton();
 function ejecutar_boton() {
    if('$error' == 'success'){
     toastr.success('$mensaje','Hola!')
     setTimeout(function() {
 
     window.location.href = 'conductores';
     
     }, 1700);
    }else{
     toastr.error('$mensaje','Hola!')
    }
 }
 
 </script>";