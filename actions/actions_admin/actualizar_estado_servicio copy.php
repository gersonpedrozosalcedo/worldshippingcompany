<?php
include '../../database/database.php';
require '../../vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$dirPrincipal = '../../vendor/phpmailer';
require "$dirPrincipal/phpmailer/src/Exception.php";
require "$dirPrincipal/phpmailer/src/PHPMailer.php";
require "$dirPrincipal/phpmailer/src/SMTP.php";

$id_servicio = $_GET["id_servicio"];
//$cantidad_despacho = $_GET["cantidad_despacho"];


$actualizar_estado_servicio = $conn->prepare("UPDATE  servicios_control_rutas SET estado = 2 WHERE id = '$id_servicio'");
$actualizar_estado_servicio->execute();

if ($actualizar_estado_servicio == null) {
    $error = "error";
    $mensaje = "Hubo un error, intentalo más tarde";
} else {

    $error = "success";
    $mensaje = "Servicio completado correctamente.";


    $consultar_servicio = $conn->prepare("SELECT * FROM servicios_control_rutas WHERE id = '$id_servicio'");
    $consultar_servicio->execute();
    $consultar_servicio = $consultar_servicio->fetchAll(PDO::FETCH_ASSOC);

    foreach ($consultar_servicio as $servicio) {
        $id_cliente = $servicio["id_cliente"];
        $id_sub_cliente = $servicio["id_sub_cliente"];
        $fecha_recepcion = $servicio["fecha_recepcion_doc"];
        $contenedor = $servicio["contenedor"];
        $tamaño_contenedor = $servicio["tamaño_contenedor"];
        $tipo_carga = $servicio["tipo_carga"];
        $dons = $servicio["dons"];
        $tipo_servicio = $servicio["tipo_servicio"];
        $nombre_condcutor_v = $servicio["nombre_condcutor_v"];
        $cantidad_carga_recibidos = $servicio["cantidad_carga_recibidos"];
        $tipo_contenedor = $servicio["tipo_contenedor"];
        $numero_contenedor = $servicio["contenedor"];
        $tamaño_contenedor = $servicio["tamaño_contenedor"];
        $linea_naviera = $servicio["linea_naviera"];
        $puerto_origen = $servicio["puerto_origen"];
        $fecha_hora_r_p = $servicio["fecha_hora_r_p"];
        $fecha_vencimiento_b_p_zf = $servicio["fecha_vencimiento_b_p_zf"];
        $observaciones = $servicio["observacion1"];

        $placa_vehiculo_c = $servicio["numero_placa_vehiculo"];
        $nombre_conductor = $servicio["nombre_conductor"];
        $identificacion_conductor = $servicio["identificacion_conductor"];
        $ruta = $servicio["ruta"];
        $contenedor_bajado_piso = $servicio["contenedor_bajado_piso"];
        $arim = $servicio["arim"];
        $fecha_hora_r_p = $servicio["fecha_r_p_p_o"];
        $observacion2 = $servicio["observacion2"];

        $fecha_hora_descargue = $servicio["fecha_hora_incio_descargue"];
        $fecha_hora_t_descargue = $servicio["fecha_hora_terminacion_descargue"];
        $cantidad_bultos = $servicio["cantidad_bultos"];
        $cantidad_bpallets = $servicio["cantidad_pallets"];
        $aplica_almacenamiento = $servicio["aplica_almacenamiento"];
        $numero_dia_almacenaje = $servicio["numero_dia_almacenaje"];
        $dia_almacenaje_libre = $servicio["dia_almacenaje_libre"];
        $cubicaje = $servicio["cubicaje"];
        $observaciones_carga = $servicio["observaciones_carga"];


        $despacho_parcial_total = $servicio["despacho_parcial_total"];


        $lugar_devolucion_v = $servicio["lugar_devolucion_vacio"];
        $fecha_cita_devolucion_v = $servicio["fecha_devolucion"];
        $id_conductor_d = $servicio["id_conductor_d"];
        $lugar_inspeccion_vacio = $servicio["lugar_inspeccion_vacio"];
        $fecha_hora_inspeccion = $servicio["fecha_hora_inspeccion"];
        $patelizo = $servicio["patelizo"];
        $fecha_bajado_piso = $servicio["fecha_bajado_piso"];
        $observacion3 = $servicio["observacion3"];
    }

    $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
    $consultar_cliente->execute();
    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);

    foreach ($consultar_cliente as $clientes) {
        $razon_social = $clientes["razon_social"];
        $email_cliente = $clientes["email"];
    }
    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$tipo_servicio'");
    $consultar_tipo_servicio->execute();
    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);


    $consultar_emails_copia = $conn->prepare("SELECT * FROM email_copia_clientes WHERE id_cliente = '$cliente' AND estado = 1 ");
    $consultar_emails_copia->execute();
    $consultar_emails_copia = $consultar_emails_copia->fetchAll(PDO::FETCH_ASSOC);

    foreach ($consultar_tipo_servicio as $tipo_servicio) {
        $nombre_servicio = $tipo_servicio["nombre_servicio"];
    }
    $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$tipo_carga'");
    $consultar_tipo_carga->execute();
    $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
    foreach ($consultar_tipo_carga as $tipo_carga) {
        $nombre_carga = $tipo_carga["nombre_carga"];
    }

    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
    $consultar_sub_cliente->execute();
    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
    foreach ($consultar_sub_cliente as $sub_cliente) {
        $nombre_sub_cliente = $sub_cliente["nombre_sub_cliente"];
    }

    $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor_d'");
    $consultar_conductor->execute();
    $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);
    foreach ($consultar_conductor as $conductor) {
        $nombre_conductors = $conductor["nombres_conductor"] . ' ' . $conductor["apellidos_conductor"];
        $doc_condcutor_v = $conductor["numero_identificacion"];
        $placa_condcutor_v = $conductor["placa_vehiculo"];
    }

    //entrega de mercancia
    $consultar_mercancia = $conn->prepare("SELECT * FROM despachos WHERE id_servicio = '$id_servicio'");
    $consultar_mercancia->execute();
    $consultar_mercancia = $consultar_mercancia->fetchAll(PDO::FETCH_ASSOC);
    foreach ($consultar_mercancia as $e_mercancia) {
        $fecha_hora_despacho = $e_mercancia["fecha_hora_despacho"];
        $transportadora = $e_mercancia["nombre_transportadora"];
        $nombre_conductor_t = $e_mercancia["nombres_conductor"];
        $doc_conductor_t = $e_mercancia["numero_identificacion"];
        $placa_v_conductor_t = $e_mercancia["placa_vehiculo"];
        $cantidad_pallets_despacho = $e_mercancia["cantidad_pallets_despacho"];
        $cantidad_bulto_despachado = $e_mercancia["cantidad_bulto_despachado"];
        $observacion = $e_mercancia["observacion"];
        $peso_aprox = $e_mercancia["peso_aprox"];
        $destino = $e_mercancia["destino"];
        $tel_conductor = $e_mercancia["telefono"];
    }

    if ($id_sub_cliente != 0) {
        $nombre_sub_cliente = $sub_cliente["nombre_sub_cliente"];
    } else {
        $nombre_sub_cliente = 'Sin registrar.';
    }
    if ($nombre_carga == null) {
        $nombre_cargas = 'Sin registrar';
    } else {
        $nombre_cargas = $nombre_carga;
    }

    if ($contenedor == null) {
        $contenedors = 'Sin registrar';
    } else {
        $contenedors = $contenedor;
    }

    if ($tamaño_contenedor == null) {
        $tamaño_contenedors = 'Sin registrar';
    } else {
        $tamaño_contenedors = $tamaño_contenedor;
    }

    if ($contenedor_bajado_piso == 0) {
        $contenedor_bajado_pisos = 'No';
    } else {
        $contenedor_bajado_pisos = 'Sí';
    }

    if ($aplica_almacenamiento == 0) {
        $aplica_almacenamientos = 'No';
    } else {
        $aplica_almacenamientos = 'Sí';
    }

    if ($despacho_parcial_total == 0) {
        $despacho_parcial_totals = 'Despacho parcial';
    } else {
        $despacho_parcial_totals = 'Despacho total';
    }

    $consultar_emails_copia_empleado = $conn->prepare("SELECT * FROM empleados");
    $consultar_emails_copia_empleado->execute();
    $consultar_emails_copia_empleado = $consultar_emails_copia_empleado->fetchAll(PDO::FETCH_ASSOC);

    $mailBody =  $mailBody = '<html>
                    
                    <head>
                        <meta charset="UTF-8" />
                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                        <style>
                        .card {
                            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
                            max-width: 100%;
                            margin-top: 20px;
                            margin-left: 10px;
                            margin-right: 10px;
                            text-align: center;
                            font-family: arial;
                            background: #ffffff;
                        }
                    
                        .title {
                            margin: 20px;
                            color: grey;
                            font-size: 14x;
                            text-align: center;
                        }
                    
                        .button {
                            border: none;
                            border-radius: 20px;
                            outline: 0;
                            display: inline-block;
                            padding: 8px;
                            color: white;
                            background-color: #28a49c;
                            text-align: center;
                            cursor: pointer;
                            width: 80%;
                            margin: 8px;
                            font-size: 14px;
                        }
                    
                        .button:hover,
                        a:hover {
                            opacity: 0.7;
                        }
                    
                        .flex-container {
                            display: flex;
                    
                            margin-left: auto;
                            margin-right: auto;
                            width: 53%;
                        }
                    
                        @media (min-width: 600px) {
                            .flex-container {
                                display: flex;
                    
                                margin-left: auto;
                                margin-right: auto;
                                width: 20%;
                            }
                        }
                    
                        .flex-container>div {
                    
                            margin: 10px;
                    
                            color: white;
                        }
                    
                        .flex-container2 {
                            display: flex;
                    
                        }
                    
                        .flex-container2>div {
                    
                            margin: 10px;
                    
                            color: white;
                        }
                        </style>
                    </head>
                    
                    <body style="background:#0b6ea7;">
                    
                        <div style="background: white; border-radius: 0px 0px 20px 20px; ">
                            <center> <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:100px"></center>
                        </div>
                    
                        <div class="card" style=" padding:10px; border-radius:20px;">
                            <center> <img src="https://midailycare.com/assets/images/image-5.png" style="width:100px"></center>
                    
                            <p class="title">¡Hola ' . $razon_social . '!, el servicio #' . $id_servicio . ' ha sido completado.</p>
                            <hr>
                            <p class="title" style="text-align: justify">
                                <b>Detalles del servicio:</b><br>
                                <br>
                                <b>Fecha recepción de los documentos:</b> ' . date("Y-m-d H:i:s") . '<br>
                                <b>Cliente: </b> ' . $razon_social . '<br>
                                <b>Sub-cliente:</b> ' . $nombre_sub_cliente . '<br>
                                <b>Do/Ns:</b> ' . $dons . '<br>
                                <b>Tipo de servicio: </b> ' . $nombre_servicio . '<br>
                                <b>Tipo de carga: </b> ' . $nombre_cargas . '<br>
                                <b>Cantidad total recibidos: </b>' . $cantidad_carga_recibidos . '<br>
                                <b>Tipo de contenedor: </b> ' . $tipo_contenedor . '<br>
                                <b>Tamaño del contenedor: </b> ' . $tamaño_contenedor . '<br>
                                <b>Número de contenedor: </b> ' . $numero_contenedor . '<br>
                                <b>Linea naviera: </b> ' . $linea_naviera . '<br>
                                <b>Puerto de retiro: </b>' . $puerto_origen . '<br>
                                <b>Fecha retiro puerto: </b>' . $fecha_hora_r_p . '<br>
                                <b>Fecha vencimiento vacío: </b>' . $fecha_vencimiento_b_p_zf . '<br>
                                <b>Observaciones: </b>' . $observaciones . '
                                <br><br>

                                <b>Transporte</b>
                                <br><br>
                                
                                <b>Fecha actualización: </b> ' . date("Y-m-d H:i:s") . '<br>
                                <b>Número de placa del vehículo: </b> ' . $placa_vehiculo_c . '<br>
                                <b>Nombre del conductor del vehículo: </b> ' . $nombre_conductor . '<br>
                                <b>Identificación del conductor: </b> ' . $identificacion_conductor . '<br>
                                <b>Ruta: </b> ' . $ruta . '<br>
                                <b>¿Contenedor bajado a piso?: </b> ' . $contenedor_bajado_pisos . '<br>
                                <b>Arim:</b> ' . $arim . '<br>
                                <b>Fecha y hora de descargue: </b> ' . $fecha_hora_r_p . '<br>
                                <b>Observaciones: </b> ' . $observacion2 . '

                                <br><br>
                                <b>Descargue mercancía</b>
                                <br><br>
                                <b>Fecha actualización: </b> ' . date("Y-m-d H:i:s") . '<br>
                                <b>Fecha y hora de inicio descargue: </b> ' . $fecha_hora_descargue . '<br>
                                <b>Fecha y hora de terminación descargue: </b> ' . $fecha_hora_t_descargue . '<br>
                                <b>Cantidad pallets total: </b> ' . $cantidad_bpallets . '<br>
                                <b>Aplica almacenamiento: </b> ' . $aplica_almacenamientos . '<br>
                                <b>Número de día de almacenaje: </b> ' . $numero_dia_almacenaje . '<br>
                                <b>Días de almacenaje libre: </b> ' . $dia_almacenaje_libre . '<br>
                                <b>Cubicaje: </b> ' . $cubicaje . '<br>
                                <b>Observaciones: </b> ' . $observaciones_carga . '

                                <br><br>
                                <b>Entrega mercancía</b>
                                <br><br>
                                <b>Fecha actualización: </b> ' . date("Y-m-d H:i:s") . '<br>
                                <b>¿Despacho parcial o total?:</b> ' . $despacho_parcial_totals . '<br>
                                <b>Fecha y hora de despacho:</b> ' . $fecha_hora_despacho . '<br>
                                <b>Cantidad pallets recibidos:</b> ' . $cantidad_pallets_despacho . '<br>
                                <b>Peso aprox:</b> ' . $peso_aprox . '<br>
                                <b>Destino:</b> ' . $destino . '<br>
                                <b>Transportadora:</b> ' . $transportadora . '<br>
                                <b>Nombre conductor (transportadora):</b> ' . $nombre_conductor_t . '<br>
                                <b>Identificación del conductor (transportadora):</b> ' . $doc_conductor_t . '<br>
                                <b>Teléfono conductor:</b> ' . $tel_conductor . '<br>
                                <b>Placa del vehículo (transportadora):</b> ' . $placa_v_conductor_t . '<br>
                                <b>Observación:</b> ' . $observacion . '

                                <br><br>
                                <b>Devolución vacío</b>
                                <br><br>

                                <b>¿Contenedor fue bajado a piso en patio aliado?:</b> ' . $patelizo . '<br>
                                <b>Fecha bajado a piso:</b> ' . $fecha_bajado_piso . '<br>
                                <b>Lugar de inspección del vacío:</b> ' . $lugar_inspeccion_vacio . '<br>
                                <b>Fecha y hora de inspección del vacío:</b> ' . $fecha_hora_inspeccion . '<br>
                                <b>Lugar devolucion del vacío:</b> ' . $lugar_devolucion_v . '<br>
                                <b>Fecha y hora devolucion del vacío:</b> ' . $fecha_cita_devolucion_v . '<br>
                                <b>Nombre conductor (devolución del vacío):</b> ' . $nombre_conductors . '<br>
                                <b>Identificación del conductor (devolución del vacío):</b> ' . $doc_condcutor_v . '<br>
                                <b>Placa del vehículo (devolución del vacío):</b> ' . $placa_condcutor_v . '<br>
                                <b>Observaciones:</b> ' . $observacion3 . '<br>
                                
                                <hr>
                                <center class="title">Puedes ver los detalles completos del servicio presionando <br><a href="worldshippingcompany.com.co/estado_servicio?service=' . base64_encode($id_servicio) . '" style=" background-color: #0b6ea7;;
                                border: 1px solid;
                                color: white;
                                padding: 7px 14px;  
                                text-align: center;
                                border-radius: 10px 10px 10px 10px; 
                                text-decoration: none;
                                display: inline-block;
                                font-size: 16px;
                                margin: 4px 2px;
                                cursor: pointer;" ><b>¡aquí!</b></a> </center>

                            </p>
                            <hr>
                            <p class="title" style="font-size:12px;"><b>Correo generado en la siguiente fecha y hora: ' . date("Y-m-d H:i:s") . '</b></p>
                        </div><br>
                        <center style="color:white"> Copyright © World Shipping Company S.A.S 2022 </center>
                    </body>
                    
                    </html>';
    $mailSubject = '(Servicio completado) Id pedido: ' . $id_servicio . ' => #servicio Do: ' . $dons . ' => Cliente: ' . $razon_social . ' => # Numero contenedor: ' . $contenedors . '.'; /// consecutivo + nombre_cliente + do +
    $mail = new PHPMailer(true);
    $mailHost = "a2plcpnl0093.prod.iad2.secureserver.net";
    $mailUsername   = 'notificaciones@worldshippingcompany.com.co';                     //SMTP username
    $mailPassword   = 'Worldshipping@';
    $mailFrom = 'notificaciones@worldshippingcompany.com.co';
    $mail->SMTPDebug = 0; //SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();
    $mail->CharSet = 'UTF-8';                         //Send using SMTP
    $mail->Host       = $mailHost;                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = $mailUsername;                     //SMTP username
    $mail->Password   = $mailPassword;
    $mail->SMTPSecure = 'ssl';
    $mail->Port = 465;
    $mail->setFrom($mailFrom, 'World Shipping Company S.A.S');
    $mail->addAddress($email_cliente);     //Add a recipient
    /*foreach($consultar_emails_copia as $email_copias){
                        $email_copia_cliente = $email_copias["email"];
                          $mail->addCC($email_copia_cliente);  // correo en copias
                    } */
    /* $mail->addCC('gersonsal15@hotmail.com'); //pruebas 
                    $mail->addCC('gersonflow1@hotmail.com'); //pruebas
                  $mail->addCC('coordinador@serviportuarios.com'); 
                    $mail->addCC('transporte@serviportuarios.com'); 
                    $mail->addCC('contabilidad@serviportuarios.com'); 
                    $mail->addCC('gcoordinadorpuertos@serviportuarios.com'); */
    $mail->isHTML(true);
    $mail->Subject = $mailSubject;
    $mail->Body    = $mailBody;
    //$mail->send();

    ////////////////////// empleados

    /////////////////////////////

    $email_empelado = 'admin@worldshippingcompany.com.co';
    // $email_empelado = 'gpedrozos@unicartagena.edu.co';
    $mailBody = '<html>
                   
                   <head>
                       <meta charset="UTF-8" />
                       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                       <style>
                       .card {
                           box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
                           max-width: 100%;
                           margin-top: 20px;
                           margin-left: 10px;
                           margin-right: 10px;
                           text-align: center;
                           font-family: arial;
                           background: #ffffff;
                       }
                   
                       .title {
                           margin: 20px;
                           color: grey;
                           font-size: 14x;
                           text-align: center;
                       }
                   
                       .button {
                           border: none;
                           border-radius: 20px;
                           outline: 0;
                           display: inline-block;
                           padding: 8px;
                           color: white;
                           background-color: #28a49c;
                           text-align: center;
                           cursor: pointer;
                           width: 80%;
                           margin: 8px;
                           font-size: 14px;
                       }
                   
                       .button:hover,
                       a:hover {
                           opacity: 0.7;
                       }
                   
                       .flex-container {
                           display: flex;
                   
                           margin-left: auto;
                           margin-right: auto;
                           width: 53%;
                       }
                   
                       @media (min-width: 600px) {
                           .flex-container {
                               display: flex;
                   
                               margin-left: auto;
                               margin-right: auto;
                               width: 20%;
                           }
                       }
                   
                       .flex-container>div {
                   
                           margin: 10px;
                   
                           color: white;
                       }
                   
                       .flex-container2 {
                           display: flex;
                   
                       }
                   
                       .flex-container2>div {
                   
                           margin: 10px;
                   
                           color: white;
                       }
                       </style>
                   </head>
                   
                   <body style="background:#0b6ea7;">
           
                   <div style="background: white; border-radius: 0px 0px 20px 20px; ">
                       <center> <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:100px"></center>
                   </div>
               
                   <div class="card" style=" padding:10px; border-radius:20px;">
                       <center> <img src="https://midailycare.com/assets/images/image-5.png" style="width:100px"></center>
               
                       <p class="title">¡Hola!, el servicio #' . $id_servicio . ' ha sido completado.</p>
                       <hr>
                       <p class="title" style="text-align: justify">
                           <b>Detalles del servicio:</b><br>
                           <br>
                           <b>Fecha recepción de los documentos:</b> ' . $fecha_recepcion . '<br>
                           <b>Cliente:</b> ' . $razon_social . '<br>
                           <b>Sub-cliente:</b> ' . $nombre_sub_cliente . '<br>
                           <b>Do/Ns:</b> ' . $dons . '<br>
                           <b>Tipo de servicio:</b> ' . $nombre_servicio . '<br>
                           <b>Tipo de carga:</b> ' . $nombre_cargas . '<br>
                           <b>Contenedor:</b> ' . $contenedors . '<br>
                           <b>Tamaño del contenedor:</b> ' . $tamaño_contenedors . '<br>
   
                           <hr>
                           <center class="title">Puedes ver los detalles completos del servicio presionando <br><a href="worldshippingcompany.com.co/estado_servicio?service=' . base64_encode($id_servicio) . '" style=" background-color: #0b6ea7;;
                           border: 1px solid;
                           color: white;
                           padding: 7px 14px;  
                           text-align: center;
                           border-radius: 10px 10px 10px 10px; 
                           text-decoration: none;
                           display: inline-block;
                           font-size: 16px;
                           margin: 4px 2px;
                           cursor: pointer;" ><b>¡aquí!</b></a> </center>
   
                       </p>
                       <hr>
                       <p class="title" style="font-size:12px;"><b>Correo generado en la siguiente fecha y hora: ' . date("Y-m-d H:i:s") . '</b></p>
                   </div><br>
                   <center style="color:white"> Copyright © World Shipping Company S.A.S 2022 </center>
               </body>
                   
                   </html>';

    $mailSubject = '(Servicio compleado) Id: ' . $id_servicio . ' - #servicio Do: ' . $dons . ' - Cliente: ' . $razon_social . ' - # Numero contenedors: ' . $contenedors . '.'; /// consecutivo + nombre_cliente + do +
    $mail = new PHPMailer(true);
    $mailHost = "a2plcpnl0093.prod.iad2.secureserver.net";
    $mailUsername   = 'notificaciones@worldshippingcompany.com.co';                     //SMTP username
    $mailPassword   = 'Worldshipping@';
    $mailFrom = 'notificaciones@worldshippingcompany.com.co';
    $mail->SMTPDebug = 0; //SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();
    $mail->CharSet = 'UTF-8';                         //Send using SMTP
    $mail->Host       = $mailHost;                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = $mailUsername;                     //SMTP username
    $mail->Password   = $mailPassword;
    $mail->SMTPSecure = 'ssl';
    $mail->Port = 465;
    $mail->setFrom($mailFrom, 'World Shipping Company S.A.S');
    $mail->addAddress($email_empelado);     //Add a recipient
    /* foreach($consultar_emails_copia as $email_copias){
                           $email_copia_cliente = $email_copias["email"];
                             $mail->addCC($email_copia_cliente);  // correo en copias
                       } */
    /* $mail->addCC('gersonsal15@hotmail.com'); //pruebas 
                       $mail->addCC('gersonflow1@hotmail.com'); //pruebas */
    foreach ($consultar_emails_copia_empleado as $email_copias_empleado) {
        $email_copia_empleado = $email_copias_empleado["email"];
        $mail->addCC($email_copia_empleado);  // correo en copias
    }
    $mail->isHTML(true);
    $mail->Subject = $mailSubject;
    $mail->Body    = $mailBody;
    // $mail->send();


}
echo "<script> 
ejecutar_boton();
function ejecutar_boton() {
   if('$error' == 'success'){
    toastr.success('$mensaje','Hola!')
    setTimeout(function() {
        editar_servicio($id_servicio);
        tabla_servicios();
    }, 1000);
    
   }else{
    toastr.error('$mensaje','Hola!')
   }
}

</script>";