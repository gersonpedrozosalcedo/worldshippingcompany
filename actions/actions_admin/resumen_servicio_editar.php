<?php

include '../../database/database.php';
session_start();
$id_servicio = $_GET["id_servicio"];

$consultar_servicio = $conn->prepare("SELECT * FROM servicios_control_rutas WHERE id = '$id_servicio'");
$consultar_servicio->execute();
$consultar_servicio = $consultar_servicio->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_servicio as $servicio) {
    $id_tipo_servicio = $servicio["tipo_servicio"];
    $costo_servicios = $servicio["costo_servicio"];
}

$consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
$consultar_tipo_servicio->execute();
$consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
foreach ($consultar_tipo_servicio as $tipo_servicio) {
    $nombre_tipo_servicio = $tipo_servicio["nombre_servicio"];
}

$consultar_despachos = $conn->prepare("SELECT * FROM despachos WHERE id_servicio = '$id_servicio'");
$consultar_despachos->execute();
$consultar_despachos = $consultar_despachos->fetchAll(PDO::FETCH_ASSOC);

?>

<div class="col-12">
    <h6 class="tx-dark tx-13 tx-semibold">ID SERVICIO: #<?php echo $id_servicio ?> - Resumen del servicio</h6>
    <ul class="list-unstyled">
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Cliente:</b>
                <?php $id_cliente = $servicio["id_cliente"];
                $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
                $consultar_cliente->execute();
                $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                foreach ($consultar_cliente as $cliente) {
                    echo $cliente["razon_social"];
                }
                ?>
            </a>
        </li>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Sub cliente:</b>
                <?php $id_cliente = $servicio["id_cliente"];
                $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_cliente WHERE id_cliente = '$id_cliente'");
                $consultar_sub_cliente->execute();
                $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                foreach ($consultar_sub_cliente as $sub_cliente) {
                    echo $sub_cliente["nombre_sub_cliente"];
                }
                ?>
            </a>
        </li>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Do/Ns: </b>
                <?php echo $servicio["dons"] ?>
            </a>
        </li>

        <?php if ($id_tipo_servicio == 3) { ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Número contenedor: </b>
                <?php echo $servicio["contenedor"] ?>
            </a>
        </li>
        <?php
            if ($servicio["fecha_lugar_entrega_v"] == "0000-00-00") {
            ?>
        <li>

            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>
                Acarreo de <?= $servicio["puerto_origen"] ?> el <?= $servicio["fecha_hora_r_p"] ?> a
                <?= $servicio["patio_almacenaje"] ?>,
                descargue y cargue del contenedor a piso con montacargas de 7 toneladas,
                <?php
                        $date1 = new DateTime($servicio["fecha_ingreso_patio_almacenaje"]);
                        $date2 = new DateTime($servicio["fecha_ingreso_patio_naviero"]);
                        $diff = $date1->diff($date2);
                        // this will output 4 days                           
                        echo $diff->days;
                        ?> día(s) de almacenamiento y acarreo a <?= $servicio["lugar_entrega"] ?> el
                <?= $servicio["fecha_hora_r_p"] ?>.
            </a>
        </li>
        <?php } else { ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>
                Acarreo de <?= $servicio["puerto_origen"] ?> el <?= $servicio["fecha_hora_r_p"] ?> a
                <?= $servicio["lugar_entrega"] ?>
            </a>
        </li>
        <?php }
        } ?>

        <?php if ($id_tipo_servicio == 4) { ?>

        <?php
            if ($servicio["patio_almacenaje"] != "") {
            ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Número contenedor: </b>
                <?php echo $servicio["contenedor"] ?>
            </a>
        </li>
        <li>

            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>
                Acarreo de <?= $servicio["patio_retiro_vacio"] ?> el <?= $servicio["fecha_patio_retiro_vacio"] ?> a
                <?= $servicio["patio_almacenaje"] ?>,
                descargue y cargue del contenedor a piso con montacargas de 7 toneladas,
                <?php
                        $date1 = new DateTime($servicio["fecha_ingreso_puerto"]);
                        $date2 = new DateTime($servicio["fecha_ingreso_patio_almacenaje"]);
                        $diff = $date1->diff($date2);
                        // this will output 4 days                           
                        echo $diff->days;
                        ?> día(s) de almacenamiento y acarreo a <?= $servicio["puerto_entrega_vacio"] ?> el
                <?= $servicio["fecha_ingreso_puerto"] ?>.
            </a>
        </li>

        <?php } else { ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>
                Acarreo de <?= $servicio["patio_retiro_vacio"] ?> el <?= $servicio["fecha_patio_retiro_vacio"] ?> a
                <?= $servicio["fecha_ingreso_puerto"] ?>

            </a>
        </li>
        <?php }
        } ?>

        <?php if ($id_tipo_servicio == 5) { ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Número contenedor: </b>
                <?php echo $servicio["contenedor"] ?>
            </a>
        </li>
        <?php if ($servicio["lugar_devolucion_vacio"] != "") { ?>
        <li>

            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>
                Acarreo de <?= $servicio["puerto_origen"] ?> el <?= $servicio["fecha_hora_r_p"] ?> a
                <?= $servicio["lugar_entrega_zona_f"] ?>, de <?= $servicio["lugar_entrega_zona_f"] ?> a
                <?= $servicio["lugar_devolucion_vacio"] ?> el <?= $servicio["fecha_devolucion"] ?>.
                <?php if ($servicio["lugar_devolucion_vacio"] == "") { ?>descargue y cargue
                del contenedor a piso con montacargas de 7 toneladas.<?php } ?>
            </a>
        </li>
        <?php } else { ?>

        <a class="menu-item pl-0 tx-13 tx-normal" href="#">
            <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>
            Acarreo de <?= $servicio["puerto_origen"] ?> el <?= $servicio["fecha_hora_r_p"] ?> a
            <?= $servicio["lugar_entrega_zona_f"] ?>,
            de <?= $servicio["lugar_entrega_zona_f"] ?> a
            <?= $servicio["patio_almacenaje"] ?> el <?= $servicio["fecha_ingreso_patio_almacenaje"] ?>,
            <?php if ($servicio["lugar_devolucion_vacio"] == "") { ?>descargue y cargue
            del contenedor a piso con montacargas de 7 toneladas<?php } ?>, <?php
                                                                                    $date1 = new DateTime($servicio["fecha_ingreso_patio_naviero"]);
                                                                                    $date2 = new DateTime($servicio["fecha_ingreso_patio_almacenaje"]);
                                                                                    $diff = $date1->diff($date2);
                                                                                    // this will output 4 days                           
                                                                                    echo $diff->days;
                                                                                    ?> día(s) de almacenamiento y
            acarreo a <?= $servicio["lugar_entrega"] ?> el
            <?= $servicio["fecha_ingreso_patio_naviero"] ?>.
        </a>
        <?php } ?>
        <?php } ?>

        <?php if ($id_tipo_servicio == 6) { ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Cantidad de pallets: </b>
                <?php echo $servicio["cantidad_pallets"] ?>
            </a>
        </li>
        <a class="menu-item pl-0 tx-13 tx-normal" href="#">
            <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>
            Traslado de <?= $servicio["puerto_origen"] ?> el <?= $servicio["fecha_hora_r_p"] ?> a
            <?= $servicio["lugar_entrega"] ?>
        </a>

        <?php } ?>

        <?php if ($id_tipo_servicio == 7) { ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Cantidad peso: </b>
                <?php echo $servicio["peso_retiro"] ?>
            </a>
        </li>
        <a class="menu-item pl-0 tx-13 tx-normal" href="#">
            <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>
            Acarreo de <?php echo $servicio["peso_retiro"] ?> (peso retirado) con <?= $servicio["vehiculo_acarreo"] ?>
            desde
            <?= $servicio["puerto_origen"] ?> el <?= $servicio["fecha_hora_r_p"] ?> a <?= $servicio["lugar_ingreso"] ?>.
        </a>
        <hr>
        <center>Despachos pesos retidados</center>
        <hr>
        <div id="accordion">
            <?php
                foreach ($consultar_despachos as $despachos) {
                    $suma_de_pallets_despachados += $despachos["cantidad_pallets_despacho"];
                ?>
            <div class="card mb-2">
                <div class="card-header">
                    <a class="text-dark collapsed" data-toggle="collapse"
                        href="#accordion_resumen<?php echo $despachos["id"] ?>" aria-expanded="false"
                        data-original-title="" title="" data-init="true">
                        <?php echo $despachos["nombre_transportadora"] . ' / ' . $despachos["fecha_hora_despacho"] . ' / # despachados: ' . $despachos["cantidad_pallets_despacho"] ?>
                    </a>
                </div>
                <div id="accordion_resumen<?php echo $despachos["id"] ?>" class="collapse show"
                    data-parent="#accordion_resumen">
                    <div class="card-body">
                        <div class="col-12">
                            <h6 class="tx-dark tx-13 tx-semibold">Detalles</h6>
                            <ul class="list-unstyled">
                                <li>
                                    <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                        <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Fecha y hora de
                                            despacho:</b>
                                        <?php echo $despachos["fecha_hora_despacho"] ?>
                                    </a>
                                </li>
                                <li>
                                    <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                        <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Transportadora: </b>
                                        <?php echo $despachos["nombre_transportadora"] ?>
                                    </a>
                                </li>
                                <li>
                                    <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                        <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Nombre condcutor:
                                        </b><?php echo $despachos["nombres_conductor"] ?>
                                    </a>
                                </li>
                                <li>
                                    <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                        <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Identificación conductor:
                                        </b><?php echo $despachos["numero_identificacion"] ?>
                                    </a>
                                </li>
                                <li>
                                    <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                        <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Placa vehículo:
                                        </b><?php echo $despachos["placa_vehiculo"] ?>
                                    </a>
                                </li>
                                <li>
                                    <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                        <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Cantidad despachados:
                                        </b><?php echo $despachos["cantidad_pallets_despacho"] ?>
                                    </a>
                                </li>
                                <li>
                                    <a class="menu-item pl-0 tx-13 tx-normal" href="#" data-toggle="modal"
                                        data-target="#ver_evidencias_despachos"
                                        onclick="ver_evidencias(<?php echo $despachos['id'] ?>)">
                                        <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Ver evidencias
                                        </b></a><br>
                                    <!-- <a href="javascript:void(0)" class="btn btn-danger btn-sm"
                                        onclick="eliminar_despacho(<?php echo $despachos['id'] ?>,<?php echo $id_servicio ?>)">Eliminar
                                        despacho</a>-->
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php } ?>


        <?php if ($id_tipo_servicio == 8) { ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Número contenedor: </b>
                <?php echo $servicio["contenedor"] ?>
            </a>
        </li>
        <?php if ($servicio["patio_almacenaje"] == "") { ?>
        <li>

            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>

                Acarreo de <?= $servicio["puerto_origen"] ?> el <?= $servicio["fecha_hora_r_p"] ?> a
                <?= $servicio["lugar_entrega_zona_f"] ?>, de <?= $servicio["lugar_entrega_zona_f"] ?> a
                <?= $servicio["lugar_devolucion_vacio"] ?> el <?= $servicio["fecha_devolucion"] ?>.
                <?php if ($servicio["lugar_devolucion_vacio"] == "") { ?>descargue y cargue
                del contenedor a piso con montacargas de 7 toneladas.<?php } ?>
            </a>
        </li>
        <?php } else { ?>

        <a class="menu-item pl-0 tx-13 tx-normal" href="#">
            <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>

            Acarreo de <?= $servicio["puerto_origen"] ?> el <?= $servicio["fecha_hora_r_p"] ?> a
            <?= $servicio["lugar_entrega_zona_f"] ?>,
            de <?= $servicio["lugar_entrega_zona_f"] ?> a
            <?= $servicio["patio_almacenaje"] ?> el <?= $servicio["fecha_ingreso_patio_almacenaje"] ?>,
            <?php if ($servicio["lugar_devolucion_vacio"] == "") { ?>descargue y cargue
            del contenedor a piso con montacargas de 7 toneladas<?php } ?>, <?php
                                                                                    $date1 = new DateTime($servicio["fecha_ingreso_patio_naviero"]);
                                                                                    $date2 = new DateTime($servicio["fecha_ingreso_patio_almacenaje"]);
                                                                                    $diff = $date1->diff($date2);
                                                                                    // this will output 4 days                           
                                                                                    echo $diff->days;
                                                                                    ?> día(s) de almacenamiento y
            acarreo a <?= $servicio["lugar_entrega"] ?> el
            <?= $servicio["fecha_ingreso_patio_naviero"] ?>.
        </a>
        <?php } ?>
        <?php } ?>

        <?php if ($id_tipo_servicio == 10 || $id_tipo_servicio == 11) { ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Número contenedor: </b>
                <?php echo $servicio["contenedor"] ?>
            </a>

        </li>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>

                <?= $nombre_tipo_servicio ?> el <?= $servicio["fecha_operacion"] ?> en
                <?= $servicio["lugar_operacion"] ?> por <?= $servicio["hora_operacion"] ?> hora(s)
            </a>
        </li>
        <?php } ?>

        <?php if ($id_tipo_servicio == 12 || $id_tipo_servicio == 13) { ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Número contenedor: </b>
                <?php echo $servicio["contenedor"] ?>
            </a>

        </li>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>
                Suministro de <?= $servicio["cantidad_estiba_entregada"] ?> estibas
                <?php if ($id_tipo_servicio == 12) {
                        echo "certificadas";
                    } else {
                        echo "no certificadas";
                    } ?>.
                <?php if ($servicio["lleno_contenedor"] == "Sí") {
                        echo 'Se realizó palletizado de las estibas y llenado del contenedor, ' . $servicio["hora_montacarga"] . ' hora(s) utilizada(s).';
                    } else {
                    }
                    ?>
            </a>
        </li>
        <?php } ?>
        <?php if ($id_tipo_servicio == 14) { ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Número contenedor: </b>
                <?php echo $servicio["contenedor"] ?>
            </a>

        </li>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>
                Suministro de <?= $servicio["cantidad_estiba_entregada"] ?> pencas con sus raches en
                <?= $servicio["lugar_entrega_estiba"] ?>
                <?php if ($servicio["lleno_contenedor"] == "Sí") {
                        echo ', ' . $servicio["hora_montacarga"] . ' hora(s) utilizada(s).';
                    } else {
                        echo "";
                    } ?>
            </a>
        </li>
        <?php } ?>

        <?php if ($id_tipo_servicio == 15) { ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Número contenedor: </b>
                <?php echo $servicio["contenedor"] ?>
            </a>

        </li>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>
                Suministro de <?= $servicio["cantidad_estiba_entregada"] ?> de cadenas con sus monas en
                <?= $servicio["lugar_entrega_estiba"] ?>
                <?php if ($servicio["lleno_contenedor"] == "Sí") {
                        echo ', ' . $servicio["hora_montacarga"] . ' hora(s) utilizada(s).';
                    } else {
                        echo "";
                    } ?>
            </a>
        </li>
        <?php } ?>
        <?php if ($id_tipo_servicio == 16 || $id_tipo_servicio == 17) { ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Número contenedor: </b>
                <?php echo $servicio["contenedor"] ?>
            </a>

        </li>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>
                Suministro de <?= $servicio["hora_operacion"] ?> hora(s) de <?= $nombre_tipo_servicio ?>
            </a>
        </li>
        <?php } ?>


        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Costo del servicios: </b>
                <?php if ($costo_servicios != null) {
                    echo '$' . number_format($costo_servicios);
                } else {
                    echo "No se ha definido el costo del servicio.";
                }
                ?>
            </a>


        </li>

        <!-- <li>
            <span style="margin: 0px; padding:0px" onclick="generar_excel(<?php echo $id_servicio ?>)"
                data-toggle="tooltip" data-trigger="hover" data-placement="top" title=""
                data-original-title="Descargar excel"> Descargar excel &nbsp; &nbsp;
                <button type="button" id="" class="btn btn-outline-success btn-icon mg-r-5">

                    <i class="fa fa-file-excel-o"></i>
                </button></span>
        </li>-->
    </ul>
</div>
<div id="respuesta_buscador"></div>