<?php

include '../../database/database.php';
$consultar_inspeccion_vehiculo = $conn->prepare("SELECT * FROM control_inspeccion_vehiculo ORDER BY id DESC ");
$consultar_inspeccion_vehiculo->execute();
$consultar_inspeccion_vehiculo = $consultar_inspeccion_vehiculo->fetchAll(PDO::FETCH_ASSOC);
session_start();
$identificacion_empleado = $_SESSION["numero_identificacion"];
$nombre_empleado = $_SESSION["nombre_admin"];

if ($identificacion_empleado == null) {
    $is_admin_identificacion = 0;
} else {
    $is_admin_identificacion = $_SESSION["numero_identificacion"];
}

?>
<table id="foo-filtering" class="table table-bordered table-hover toggle-circle table-responsive-sm"
    data-page-size="10">

    <thead>
        <tr>
            <th data-toggle="true" title="Id inspección">Id</th>
            <th data-toggle="true">Nombre conductor</th>
            <th data-hide="phone, tablet">Número de identificación</th>
            <th data-toggle="true">Placa vehículo</th>
            <th data-hide="phone, tablet">Coordinador/aux</th>
            <th data-hide="phone, tablet">Fecha/hora</th>
            <th data-toggle="true">Tipo vehículo</th>
            <th data-toggle="true">Estado</th>
            <th data-toggle="true">Acción</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $contador = 0;
        foreach ($consultar_inspeccion_vehiculo as $inspeccion) {
            $tipo_vehiuclo = $inspeccion["tipo_vehiculo"];
            $id_inspeccion = $inspeccion["id"];
            $consultar_inspeccion_vehiculo_relacion = $conn->prepare("SELECT * FROM control_inspeccion_vehiculo_relacion WHERE id_inspeccion = '$id_inspeccion' AND concepto = 'Pito' ");
            $consultar_inspeccion_vehiculo_relacion->execute();
            $consultar_inspeccion_vehiculo_relacion = $consultar_inspeccion_vehiculo_relacion->fetchAll(PDO::FETCH_ASSOC);

            $contador = count($consultar_inspeccion_vehiculo_relacion);
            if ($contador == 1) {
                $pregreso = "10";
            } else if ($contador == 2) {
                $pregreso = "20";
            } else if ($contador == 3) {
                $pregreso = "40";
            } else if ($contador == 4) {
                $pregreso = "50";
            } else if ($contador == 5) {
                $pregreso = "60";
            } else if ($contador == 6) {
                $pregreso = "80";
            } else if ($contador == 7) {
                $pregreso = "100";
            }
        ?>
        <tr>
            <td><?php echo $inspeccion["id"] ?> <img
                    src="http://worldshippingcompany.com.co/foto_vehiculos_m_t/<?php echo str_replace("#", '', $inspeccion['placa_vehiculo']); ?>.jpeg"
                    alt="Imgen vehículo" width="100" height="100"> </td>
            <td id="responsive"><?php echo $inspeccion["conductor"] ?></td>
            <td><?php echo $inspeccion["identificacion_conductor"] ?></td>
            <td><?php echo $inspeccion["placa_vehiculo"] ?></td>
            <td id="responsive"><?php echo $inspeccion["nombre_empleado"] ?></td>
            <td id="responsive"><?php echo $inspeccion["fecha_creacion"] ?></td>
            <td id="responsive"><?php echo $inspeccion["tipo_vehiculo"] ?></td>
            <td>
                <?php
                    if ($inspeccion['estado'] == 0) {
                        echo '<span class="badge badge-outlined badge-danger">Sin revisar</span>';
                    } else {
                        echo '<span class="badge badge-outlined badge-success">Revisado</span>';
                    } ?>

                <?php
                    if ($tipo_vehiuclo == "Montacarga") {
                        echo '
                        <div class="clearfix"> 
                                 <span class="float-left small tx-gray-500">' . $pregreso . '% </span> 
                                 <span class="float-right">
                                 <i class="ion-android-arrow-up mr-1 tx-success"></i><span class="tx-dark">' . $pregreso . '</span><span class="small mg-b-0">/100</span>
                                 </span>
                              </div>
                        <div class="progress ht-3 op-5">
                        <div class="progress-bar bg-primary wd-' . $pregreso . 'p" role="progressbar" aria-valuenow="' . $pregreso . '" aria-valuemin="0" aria-valuemax="' . $pregreso . '"></div>
                     </div>';
                    } ?>
            </td>

            <td>
                <span style="margin: 0px; padding:0px" data-toggle="tooltip" data-trigger="hover" data-placement="top"
                    title="" data-original-title="Ver detalles de inspección completa"
                    onclick="editar_inspeccion(<?php echo $inspeccion['id'] ?>)">
                    <button type="button" data-toggle="modal" data-target="#ver_inspeccion"
                        class="btn btn-outline-primary btn-sm btn-icon mg-r-5">
                        <i class="fa fa-eye"></i>
                    </button>
                </span>

                <?php
                    if ($inspeccion['estado'] == 0) {
                    ?>
                <span style="margin: 0px; padding:0px" data-toggle="tooltip" data-trigger="hover" data-placement="top"
                    title="" data-original-title="Cambiar estado de inspección (Revisado)">


                    <a href="javascript:void(0)"
                        onclick="actualizar_inspeccion_estado(<?php echo $inspeccion['id'] ?>,<?php echo $is_admin_identificacion ?>,'<?php echo $nombre_empleado ?>')"
                        class="btn btn-outline-success  btn-sm btn-icon mg-r-5">

                        <i class="fa fa-check-circle"></i>
                    </a>
                </span>
                <?php } else {
                    } ?>


                <?php if ($tipo_vehiuclo == 'Montacarga') { ?>

                <?php if ($pregreso == '100') {
                        } else { ?>
                <!--<a href="javascript:void(0)" onclick="añadir_mas_check_list(<?php echo $inspeccion['id'] ?>)"
                    data-toggle="tooltip" data-trigger="hover" data-placement="top" title=""
                    data-original-title="Anañir mas check list" class="btn btn-outline-primary btn-sm btn-icon mg-r-5">
                    <i class="fa fa-plus"></i>
                </a>-->
                <?php } ?>

                <span style="margin: 0px; padding:0px" data-toggle="tooltip" data-trigger="hover" data-placement="top"
                    title="" data-original-title="Descargar excel">
                    <a href="../actions/actions_admin/generar_excel_inspeccion_montacarga.php?id_inspeccion=<?php echo $inspeccion["id"] ?>"
                        class="btn btn-outline-success  btn-sm btn-icon mg-r-5">

                        <i class="fa fa-file-excel-o"></i>
                    </a>
                </span>
                <?php } else { ?>
                <span style="margin: 0px; padding:0px" data-toggle="tooltip" data-trigger="hover" data-placement="top"
                    title="" data-original-title="Descargar excel">
                    <a href="../actions/actions_admin/generar_excel_inspeccion.php?id_inspeccion=<?php echo $inspeccion["id"] ?>"
                        class="btn btn-outline-success  btn-sm btn-icon mg-r-5">

                        <i class="fa fa-file-excel-o"></i>
                    </a>
                </span>
                <?php } ?>

                <span style="margin: 0px; padding:0px" data-toggle="tooltip" data-trigger="hover" data-placement="top"
                    title="" data-original-title="Eliminar inspección">
                    <a id="btn_eliminar_inspeccion" href="#"
                        onclick="eliminar_inspeccion(<?php echo $inspeccion['id'] ?>)"
                        class="btn btn-outline-danger  btn-sm btn-icon mg-r-5">Eliminar
                    </a>
                    <p id="eliminando_inspeccion"></p>
                </span>

            </td>

        </tr>
        <?php
        }
        ?>


    </tbody>
    <tfoot>
        <tr>
            <td colspan="5">
                <div class="ft-right">
                    <ul class="pagination"></ul>
                </div>
            </td>
        </tr>
    </tfoot>
</table>




<script>
// ///////////////////////////////////////Row Toggler
$("#foo-row-toggler").footable();

// Accordion
$("#foo-accordion")
    .footable()
    .on("footable_row_expanded", function(e) {
        $("#foo-accordion tbody tr.footable-detail-show")
            .not(e.row)
            .each(function() {
                $("#foo-accordion").data("footable").toggleDetail(this);
            });
    });
// Filtering
var filtering = $("#foo-filtering");
filtering.footable().on("footable_filtering", function(e) {
    var selected = $("#foo-filter-status").find(":selected").val();
    e.filter += e.filter && e.filter.length > 0 ? " " + selected : selected;
    e.clear = !e.filter;
});

// Filter status
$("#foo-filter-status").change(function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});

// Search input
$("#foo-search").on("input", function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});
</script>