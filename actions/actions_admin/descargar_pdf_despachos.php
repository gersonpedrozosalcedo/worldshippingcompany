<?php

include '../../database/database.php';
session_start();

$id_despacho = $_GET["id_despacho"];


$consultar_despachos = $conn->prepare("SELECT * FROM despachos WHERE id = '$id_despacho' ");
$consultar_despachos->execute();
$consultar_despachos = $consultar_despachos->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_despachos as $despachos) {
    $id_servicio = $despachos["id_servicio"];
}


$consultar_servicios = $conn->prepare("SELECT * FROM servicios_control_rutas WHERE id = '$id_servicio' ");
$consultar_servicios->execute();
$consultar_servicios = $consultar_servicios->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_servicios as  $servicios) {
    $tipo_cargas = $servicios["tipo_carga"];
    $id_cliente = $servicios["id_cliente"];
}

$consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente' ");
$consultar_cliente->execute();
$consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_cliente as  $cliente) {
}

$consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$tipo_cargas' ");
$consultar_tipo_carga->execute();
$consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_tipo_carga as  $tipo_carga) {
    $nombre_tipo_carga = $tipo_carga["nombre_carga"];
}

/*
if($tipo_carga == 3){
$
}else if($tipo_carga == 1){

}else if($tipo_carga == 4){

}*/

$numero_aleatorio = rand(0, 100000) . '-' . $id_despacho;
$nombre_pdf = 'CARTA DE PORTE N°' . $numero_aleatorio . ' PARA DESPACHO MERCANCÍA';

?>

<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keyword" content="">
    <meta name="author" content="" />
    <!-- Page Title -->
    <title>World Shipping Company</title>
    <!-- Main CSS -->
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/bootstrap/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/flag-icon/flag-icon.min.css" />
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/simple-line-icons/css/simple-line-icons.css">
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/ionicons/css/ionicons.css">
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/footable/footable.core.css">
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/toastr/toastr.min.css">
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/chartist/chartist.css">
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/apex-chart/apexcharts.css">
    <link type="text/css" rel="stylesheet" href="../../assets/css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="../../assets/css/style.min.css" />
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/datepicker/css/datepicker.min.css">
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/bootstrap-select/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/steps/jquery.steps.css">
    <link rel="icon" href="../../assets/images/favicon.ico" type="image/x-icon">


    <link type="text/css" rel="stylesheet" href="../../assets/plugins/spinkit/spinkit.min.css">

</head>


<body>

    <style>
    p {
        color: #006fae !important;
    }
    </style>
    <!--================================-->
    <!-- Page Container Start -->
    <!--================================-->

    <div class="pd-y-50">
        <div class="sk-chasing-dots">
            <div class="sk-child sk-dot1"></div>
            <div class="sk-child sk-dot2"></div>
        </div>
        <center>
            <h6>Pdf generado correctamente, esta ventana se cierra en 8 segundos...</h6>
        </center>
        <center>
            <h6>Por favor guarde el pdf</h6>
        </center>
    </div>



    <div style="display:none">


        <div id="pdf" style="margin:100px; color: #006fae;">

            <div class="row">
                <div class="col-6 col-sm-6">
                    <img src="../../../../assets/images/logo.png" alt="" style="width:200px; margin-top: -37px;">
                </div>
                <div class="col-6 col-sm-6" style="text-align: right">
                    <h5><b>WORLD SHIPPING COMPANY S.A.S</b></h5>
                    <h6><b>TRANSPORTADORES DE CARGA</b></h6>
                    <h6><b>NIT.900011819-1 RÉGIMEN COMÚN</b></h6>

                    <h6>Bosque callejón Cano, Transversal 40 N° 21A-10</h6>
                    <h6>Tel: 662 1378 - 662 1349 - Cartagena de Indias - Colombia</h6>
                </div>
            </div>

            <br><br>

            <div class="row" style="border: 1px solid #006fae; border-radius: 10px">
                <div class="col-12" style="margin:10px;">
                    <div style="display: flex">
                        <h5><b>CARTA DE PORTE N° </b></h5> &nbsp;&nbsp; &nbsp;&nbsp;
                        <h5 style="color:#ff2f2f"><b> <?php echo $numero_aleatorio ?></b></h5>&nbsp;&nbsp; &nbsp;&nbsp;
                        <h5><b> PARA DESPACHO MERCANCÍA</b></h5>
                    </div>

                </div>
                <div class="col-12">
                    <div class="row"
                        style="background-color: #3781ad33; border: 1px solid black;  border-radius: 10px; margin: 10px;padding: 10px;border-color: #3781ad00;">
                        <div class="col-4"><b style="font-size: 15px;">DO.
                                &nbsp;&nbsp;<?php echo $servicios["dons"] ?></b>
                        </div>
                        <div class="col-4"><b style="font-size: 15px;">PEDIDO &nbsp;&nbsp;</b></div>
                        <div class="col-4"><b style="font-size: 15px;">FECHA
                                &nbsp;&nbsp;<?php echo $despachos["fecha_hora_despacho"] ?>
                            </b></div>
                    </div>
                </div>
                <br>
                <div class="col-12"
                    style="margin-left: 12px; margin-top: 10px; color: #006fae; background-image: url('../../../../assets/images/logo_pdf.png');  background-size:35%; background-repeat: no-repeat; /*filter: opacity(50%);*/ background-position: center; ">
                    <div style="display:flex">
                        <p style="width:15%"><b>MANIFIESTO N°: </b> </p>
                        <p style="width: 81%; border-bottom: 1px solid;"> </p>
                    </div>
                    <div style="display:flex">
                        <p style="width:15%"><b>TIPO DE CARGA: </b></p>
                        <p style="width: 81%; border-bottom: 1px solid;"><?php echo $tipo_carga["nombre_carga"]; ?></p>
                    </div>
                    <div style="display:flex">
                        <p style="width:15%"><b>TRANSPORTADOR: </b></p>
                        <p style="width: 81%; border-bottom: 1px solid;">
                            <?php echo $despachos["nombre_transportadora"] ?>
                        </p>
                    </div>
                    <div style="display:flex">
                        <p style="width:15%"><b>IMPORTADOR: </b></p>
                        <p style="width: 81%; border-bottom: 1px solid;"><?php echo $cliente["razon_social"] ?>
                        </p>
                    </div>
                    <div style="display:flex">
                        <p style="width:15%"><b>DESTINO: </b></p>
                        <p style="width: 81%; border-bottom: 1px solid;"><?php echo $despachos["destino"] ?></p>
                    </div>
                    <div style="display:flex">
                        <p style="width:15%"><b>DIRECCIÓN: </b> </p>
                        <p style="width: 81%; border-bottom: 1px solid;"> </p>
                    </div>
                    <?php if ($tipo_cargas == 5) { ?>
                    <div style="display:flex">
                        <p style="width:15%"><b>PALLETS: </b></p>
                        <p style="width: 81%; border-bottom: 1px solid;">
                            <?php echo $despachos["cantidad_pallets_despacho"] ?></p>
                    </div>
                    <div style="display:flex">
                        <p style="width:15%"><b>BULTOS: </b></p>
                        <p style="width: 81%; border-bottom: 1px solid;">
                            <?php echo $despachos["cantidad_bultos_despachados"] ?></p>
                    </div>
                    <?php } else if ($tipo_cargas == 2) { ?>
                    <div style="display:flex">
                        <p style="width:15%"><b>BULTOS: </b></p>
                        <p style="width: 81%; border-bottom: 1px solid;">
                            <?php echo $despachos["cantidad_bultos_despachados"] ?></p>
                    </div>
                    <?php } else { ?>
                    <div style="display:flex">
                        <p style="width:15%"><b><?php echo strtoupper($nombre_tipo_carga) ?> </b></p>
                        <p style="width: 81%; border-bottom: 1px solid;">
                            <?php echo $despachos["cantidad_pallets_despacho"] ?></p>
                    </div>
                    <?php } ?>
                    <div style="display:flex">
                        <p style="width:15%"><b>CONTENIDO: </b> </p>
                        <p style="width: 81%; border-bottom: 1px solid;"></p>
                    </div>
                    <!-- <div style="display:flex">
                    <p style="width:15%"><b>IMPORTADOR: </b></p>
                    <p style="width: 81%; border-bottom: 1px solid;"></p>
                </div>-->
                    <div style="display:flex">
                        <p style="width:15%"><b>PESO APROX: </b></p>
                        <p style="width: 81%; border-bottom: 1px solid;"><?php echo $despachos["peso_aprox"] . ' Kg' ?>
                        </p>
                    </div>
                    <div style="display:flex">
                        <p style="width:15%"><b>CONDUCTOR: </b></p>
                        <p style="width: 81%; border-bottom: 1px solid;"><?php echo $despachos["nombres_conductor"]  ?>
                        </p>
                    </div>
                    <div style="display:flex">
                        <p style="width:15%"><b>C.C.: </b></p>
                        <p style="width: 81%; border-bottom: 1px solid;">
                            <?php echo $despachos["numero_identificacion"] ?>
                        </p>
                    </div>
                    <div style="display:flex">
                        <p style="width:15%"><b>CEL: </b></p>
                        <p style="width: 81%; border-bottom: 1px solid;"> <?php echo $despachos["telefono"] ?></p>
                    </div>
                    <div style="display:flex">
                        <p style="width:15%"><b>PLACA: </b></p>
                        <p style="width: 81%; border-bottom: 1px solid;"><?php echo $despachos["placa_vehiculo"] ?></p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row"
                        style="background-color: #3781ad33; border: 1px solid black;  border-radius: 10px; margin: 10px;padding: 10px;border-color: #3781ad00;">
                        <div style="display:flex">
                            <p>&nbsp;&nbsp;<b>OBSERVACIONES: </b> <?php echo $despachos["observacion"] ?></p>

                        </div>
                    </div>
                </div>

                <div class="col-12" style="margin-left: 12px; margin-top: 10px;">
                    <h6><b>CONDICIONES ESPECIALES:</b> La empresa transportadora se compromete a transportar el
                        cargamento
                        detallado anteriormente bajo la responsabilidad mancomunada y solidaria del conductor y dueno
                        del
                        camión y la enteegará en su destino de acuerdo con las instrucciones contenidas en la presenta
                        Carta
                        de Porte, cuyo fiel entendimiento da por medio de su firma y selli al pie </h6>
                </div>

                <div class="row" style="margin-left: 12px; margin-top: 10px; width: 98%;  margin-bottom: 32px;">
                    <div class="col-6">
                        <br><br>
                        <p style="width:100%; border: 0.5px solid  #006fae; border-bottom: 0px solid  #006fae"></p>
                        <h6>Firma y sello del Consignatario</h6>
                    </div>
                    <div class="col-6" style="text-align: right">
                        <br>
                        <br>
                        <p style="width:100%; border:  0.5px solid  #006fae; border-bottom: 0px solid  #006fae"></p>
                        <h6>Firma y sello del Transportador</h6>
                    </div>
                </div>

            </div>


        </div>

    </div>

    <script src=" ../../assets/plugins/jquery/jquery.min.js">
    </script>
    <script src="../../assets/plugins/jquery-ui/jquery-ui.js"></script>
    <script src="../../assets/plugins/popper/popper.js"></script>
    <script src="../../assets/plugins/feather-icon/feather.min.js"></script>
    <script src="../../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../assets/plugins/pace/pace.min.js"></script>
    <script src="../../assets/plugins/toastr/toastr.min.js"></script>
    <script src="../../assets/plugins/countup/counterup.min.js"></script>
    <script src="../../assets/plugins/waypoints/waypoints.min.js"></script>
    <script src="../../assets/plugins/chartjs/chartjs.js"></script>
    <script src="../../assets/plugins/apex-chart/apexcharts.min.js"></script>
    <script src="../../assets/plugins/apex-chart/irregular-data-series.js"></script>
    <script src="../../assets/plugins/simpler-sidebar/jquery.simpler-sidebar.min.js"></script>
    <script src="../../assets/js/dashboard/sales-dashboard-init.js"></script>
    <script src="../../assets/js/jquery.slimscroll.min.js"></script>
    <script src="../../assets/js/highlight.min.js"></script>
    <script src="../../assets/js/app.js"></script>
    <script src="../../assets/js/custom.js"></script>

    <script src="../../assets/plugins/footable/footable.all.min.js"></script>
    <script src="../../assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="../../assets/plugins/steps/jquery.steps.js"></script>
    <script src="../../assets/plugins/pace/pace.min.js"></script>

    <script src="../../assets/plugins/parsleyjs/parsley.js"></script>

    <script src="js/js-admin/main.js"></script>
    <script src="js/js-operador/main.js"></script>





</body>

<!-- Mirrored from colorlib.net/metrical/light/page-singin.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jan 2020 21:19:37 GMT -->

</html>

<script src="../../assets/plugins/html2pdf.bundle.min.js"></script>
<script>
pdf()

function pdf() {

    var element = document.getElementById("pdf"); // <-- Aquí puedes elegir cualquier elemento del DOM
    var opt = {
        margin: 0,
        filename: '<?php echo $nombre_pdf ?>.pdf',
        image: {
            type: 'jpeg',
            quality: 0.98
        },
        html2canvas: {
            scale: 3, // A mayor escala, mejores gráficos, pero más peso
            letterRendering: true,
        },
        jsPDF: {
            unit: "in",
            format: "a3",
            orientation: 'portrait' // landscape o portrait
        },
        pagebreak: {
            mode: ['avoid-all', 'css', 'legacy']
        } //It determines how HTML elements should be split.
    };

    // New Promise-based usage:
    html2pdf().set(opt).from(element).save();

    // Old monolithic-style usage:
    //html2pdf(element, opt);
    redirec();
}

function redirec() {
    setTimeout(function() {
        window.close()
    }, 8000);

}
</script>