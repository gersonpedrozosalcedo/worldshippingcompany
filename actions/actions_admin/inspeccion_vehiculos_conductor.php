<?php

include '../../database/database.php';

$id_conductor = $_GET["id_conductor"];
$tipo_vehiuclo = $_GET["tipo_vehiculo"];

//$consultar_inspeccion_vehiculo = $conn->prepare("SELECT * FROM control_inspeccion_vehiculo WHERE identificacion_conductor = '$id_conductor' ORDER BY id DESC ");
$consultar_inspeccion_vehiculo = $conn->prepare("SELECT * FROM control_inspeccion_vehiculo WHERE tipo_vehiculo = '$tipo_vehiuclo' ORDER BY id DESC ");
$consultar_inspeccion_vehiculo->execute();
$consultar_inspeccion_vehiculo = $consultar_inspeccion_vehiculo->fetchAll(PDO::FETCH_ASSOC);
session_start();
$identificacion_empleado = $_SESSION["numero_identificacion"];
$nombre_empleado = $_SESSION["nombre_admin"];

if ($identificacion_empleado == null) {
    $is_admin_identificacion = 0;
} else {
    $is_admin_identificacion = $_SESSION["numero_identificacion"];
}

$consultar_conceptos_inspeccion_montacarga = $conn->prepare("SELECT * FROM control_concepto_inspeccion_montacarga");
$consultar_conceptos_inspeccion_montacarga->execute();
$consultar_conceptos_inspeccion_montacarga = $consultar_conceptos_inspeccion_montacarga->fetchAll(PDO::FETCH_ASSOC);
?>
<div id="tabla_inspecciones">
    <div class="row">
        <div class="mg-20 form-inline wd-100p">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">Estado</label>
                    <select id="foo-filter-status" class="form-control">
                        <option value="">Mostrar todos</option>
                        <option value="Sin revisar">Sin revisar</option>
                        <option value="Revisado">Revisados</option>

                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group ft-right">
                    <input id="foo-search" type="text" placeholder="Buscar inspección..." class="form-control"
                        autocomplete="off">
                </div>
            </div>
        </div>
    </div>
    <table id="foo-filtering" class="table table-bordered table-hover toggle-circle table-responsive-sm"
        data-page-size="7">

        <thead>
            <tr>
                <th data-toggle="true" title="Id inspección">Id</th>
                <th data-toggle="true">Nombre conductor</th>
                <th data-hide="phone, tablet">Número de identificación</th>
                <th data-toggle="true">Placa vehículo</th>
                <th data-hide="phone, tablet">Coordinador/aux</th>
                <th data-hide="phone, tablet">Fecha/hora</th>
                <th data-toggle="true">Estado</th>
                <th data-toggle="true">Acción</th>
            </tr>
        </thead>
        <tbody>

            <?php
            $contador = 0;
            foreach ($consultar_inspeccion_vehiculo as $inspeccion) {
                $id_inspeccion = $inspeccion["id"];
                $consultar_inspeccion_vehiculo_relacion = $conn->prepare("SELECT * FROM control_inspeccion_vehiculo_relacion WHERE id_inspeccion = '$id_inspeccion' AND concepto = 'Pito' ");
                $consultar_inspeccion_vehiculo_relacion->execute();
                $consultar_inspeccion_vehiculo_relacion = $consultar_inspeccion_vehiculo_relacion->fetchAll(PDO::FETCH_ASSOC);

                $contador = count($consultar_inspeccion_vehiculo_relacion);
                if ($contador == 1) {
                    $pregreso = "10";
                } else if ($contador == 2) {
                    $pregreso = "20";
                } else if ($contador == 3) {
                    $pregreso = "40";
                } else if ($contador == 4) {
                    $pregreso = "50";
                } else if ($contador == 5) {
                    $pregreso = "60";
                } else if ($contador == 6) {
                    $pregreso = "80";
                } else if ($contador == 7) {
                    $pregreso = "100";
                }
            ?>
            <tr>
                <td><?php echo $inspeccion["id"] ?><img
                        src="http://worldshippingcompany.com.co/foto_vehiculos_m_t/<?php echo str_replace("#", '', $inspeccion['placa_vehiculo']); ?>.jpeg"
                        alt="Imgen vehículo" width="100" height="100"></td>
                <td id="responsive"><?php echo $inspeccion["conductor"] ?></td>
                <td><?php echo $inspeccion["identificacion_conductor"] ?></td>
                <td><?php echo $inspeccion["placa_vehiculo"] ?></td>
                <td id="responsive"><?php echo $inspeccion["nombre_empleado"] ?></td>
                <td id="responsive"><?php echo $inspeccion["fecha_creacion"] ?></td>
                <td>
                    <?php
                        if ($inspeccion['estado'] == 0) {
                            echo '<span class="badge badge-outlined badge-danger">Sin revisar</span>';
                        } else {
                            echo '<span class="badge badge-outlined badge-success">Revisado</span>';
                        } ?>
                    <?php
                        if ($tipo_vehiuclo == "Montacarga") {
                            echo '
                        <div class="clearfix"> 
                                 <span class="float-left small tx-gray-500">' . $pregreso . '% </span> 
                                 <span class="float-right">
                                 <i class="ion-android-arrow-up mr-1 tx-success"></i><span class="tx-dark">' . $pregreso . '</span><span class="small mg-b-0">/100</span>
                                 </span>
                              </div>
                        <div class="progress ht-3 op-5">
                        <div class="progress-bar bg-primary wd-' . $pregreso . 'p" role="progressbar" aria-valuenow="' . $pregreso . '" aria-valuemin="0" aria-valuemax="' . $pregreso . '"></div>
                     </div>';
                        } ?>
                </td>
                <td>

                    <?php if ($tipo_vehiuclo == 'Montacarga') { ?>

                    <?php if ($pregreso == '100') {
                            } else { ?>
                    <a href="javascript:void(0)" onclick="añadir_mas_check_list(<?php echo $inspeccion['id'] ?>)"
                        data-toggle="tooltip" data-trigger="hover" data-placement="top" title=""
                        data-original-title="Anañir mas check list"
                        class="btn btn-outline-primary btn-sm btn-icon mg-r-5">
                        <i class="fa fa-plus"></i>
                    </a>
                    <?php } ?>

                    <span style="margin: 0px; padding:0px" data-toggle="tooltip" data-trigger="hover"
                        data-placement="top" title="" data-original-title="Descargar excel">
                        <a href="../actions/actions_admin/generar_excel_inspeccion_montacarga.php?id_inspeccion=<?php echo $inspeccion["id"] ?>"
                            class="btn btn-outline-success  btn-sm btn-icon mg-r-5">

                            <i class="fa fa-file-excel-o"></i>
                        </a>
                    </span>
                    <?php } else { ?>
                    <span style="margin: 0px; padding:0px" data-toggle="tooltip" data-trigger="hover"
                        data-placement="top" title="" data-original-title="Descargar excel">
                        <a href="../actions/actions_admin/generar_excel_inspeccion.php?id_inspeccion=<?php echo $inspeccion["id"] ?>"
                            class="btn btn-outline-success  btn-sm btn-icon mg-r-5">

                            <i class="fa fa-file-excel-o"></i>
                        </a>
                    </span>
                    <?php } ?>
                    <span style="margin: 0px; padding:0px" data-toggle="tooltip" data-trigger="hover"
                        data-placement="top" title="" data-original-title="Eliminar inspección">
                        <a id="btn_eliminar_inspeccion" href="#"
                            onclick="eliminar_inspeccion(<?php echo $inspeccion['id'] ?>)"
                            class="btn btn-outline-danger  btn-sm btn-icon mg-r-5">Eliminar
                        </a>
                        <p id="eliminando_inspeccion"></p>
                    </span>
                </td>

            </tr>
            <?php
            }
            ?>


        </tbody>
        <tfoot>
            <tr>
                <td colspan="5">
                    <div class="ft-right">
                        <ul class="pagination"></ul>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
</div>

<div id="añadir_inspeccion_vehiculo_montacarga" style="display:none;">

    <form id="form_inspeccion_montacarga" enctype="multipart/form-data" style="margin-bottom:80px; ">
        <input type="hidden" name="id_inspeccion" id="id_inspeccion">
        <div class="form-layout form-layout-2">
            <div class="row no-gutters">

                <?php
                foreach ($consultar_conceptos_inspeccion_montacarga as $value) {
                ?>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label active">¿<?php echo $value["concepto"] ?>?
                            <span class="tx-danger">*</span></label>
                        <input class="form-control" type="hidden" name="concepto[]"
                            value="<?php echo $value["concepto"] ?>">
                        <select name="seleccion[]" class="form-control form-control-sm" id="">
                            <option value="Bueno">Bueno</option>
                            <option value="Malo">Malo</option>
                        </select>
                    </div>
                </div>
                <!-- col-4 -->
                <div class="col-md-4 mg-t--1 mg-md-t-0">
                    <div class="form-group mg-md-l--1">
                        <label class="form-control-label active">Observaciones: <span class="tx-danger">*</span></label>
                        <textarea name="observacion[]" rows="2" class="form-control form-control-sm"
                            placeholder=" Observaciones" value="Ninguna"></textarea>
                    </div>
                </div>
                <!-- col-4 -->
                <div class="col-md-4 mg-t--1 mg-md-t-0">
                    <div class="form-group mg-md-l--1">
                        <label for="">Subir evidencia (opcional)</label>
                        <div class="input-group">
                            <div class="custom-file"> <input type="file" name="evidencias[]"
                                    class="form-control form-control-sm" id="inputGroupFile04" multiple="">
                                <label class="" for="inputGroupFile04"></label>
                            </div>
                            <br>
                            <div class="input-group-append">
                                <span class="input-group-btn">
                                    <button class="btn btn-custom-primary file-browser" type="button"><i
                                            class="fa fa-upload"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>


            </div>


        </div>
    </form>
    <div class="form-layout-footer bd pd-20 bd-t-0" id="footers">
        <!--<a href="#" class="btn btn-danger" onclick="borrar_forms()">Borrar todo</a>-->
        <div id="respuesta_servidors"></div>
        <a href="javascript:void(0)" class="btn btn-secondary btn-sm" onclick="cerrar_añadir_mas_check_list()">Atrás</a>
        <a href="javascript:void(0)" onclick="guardar_inspeccion_vehiculo_montacarga()"
            class="btn btn-custom-primary btn-sm">Guardar</a>

    </div>

</div>


<script>
// ///////////////////////////////////////Row Toggler
$("#foo-row-toggler").footable();

// Accordion
$("#foo-accordion")
    .footable()
    .on("footable_row_expanded", function(e) {
        $("#foo-accordion tbody tr.footable-detail-show")
            .not(e.row)
            .each(function() {
                $("#foo-accordion").data("footable").toggleDetail(this);
            });
    });
// Filtering
var filtering = $("#foo-filtering");
filtering.footable().on("footable_filtering", function(e) {
    var selected = $("#foo-filter-status").find(":selected").val();
    e.filter += e.filter && e.filter.length > 0 ? " " + selected : selected;
    e.clear = !e.filter;
});

// Filter status
$("#foo-filter-status").change(function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});

// Search input
$("#foo-search").on("input", function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});


function añadir_mas_check_list(id) {
    $("#añadir_inspeccion_vehiculo_montacarga").show();
    $("#tabla_inspecciones").hide();

    $("#id_inspeccion").val(id);

}

function cerrar_añadir_mas_check_list() {
    $("#añadir_inspeccion_vehiculo_montacarga").hide();
    $("#tabla_inspecciones").show();
}

function guardar_inspeccion_vehiculo_montacarga() {
    var url =
        "actions/actions_admin/guardar_inspeccion_vehiculo.php";

    var opcion = confirm("¿Estás seguro de realizar esta acción?");

    if (opcion == true) {
        $.ajax({
            cache: false,
            async: false,
            type: 'POST',
            data: new FormData($("#form_inspeccion_montacarga")[0]),
            contentType: false,
            processData: false,
            url: url,
            beforeSend: function() {
                $("#respuesta_servidors").html(
                    '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Cargando, por favor espere...</button>'
                );
            },
            success: function(data) {
                setTimeout(function() {
                    $("#respuesta_servidor").html(data);

                }, 2000);
            },
            error: function() {
                alert("Error, por favor intentalo más tarde.");
            },
        });
    } else {
        fadeOut();
    }
}
///// inspecciones /////////////////

function eliminar_inspeccion(id_inspeccion) {
    var url =
        "actions/actions_admin/eliminar_inspeccion.php?id_inspeccion=" +
        id_inspeccion;

    var opcion = confirm("¿Estás seguro de realizar esta acción?");

    if (opcion == true) {
        $.ajax({
            cache: false,
            async: false,
            url: url,
            beforeSend: function() {
                $("#eliminando_inspeccion").html(
                    '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Eliminando, por favor espere...</button>'
                );
                $("#btn_eliminar_inspeccion").hide();
            },
            success: function(data) {
                $("#eliminando_inspeccion").html(data);
            },
            error: function() {
                alert("Error, por favor intentalo más tarde.");
            },
        });
    } else {
        fadeOut();
    }
}
</script>