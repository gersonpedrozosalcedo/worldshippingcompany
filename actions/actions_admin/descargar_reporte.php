<?php

session_start();

include '../../database/database.php';
require "../../vendor/autoload.php";
date_default_timezone_set('America/Bogota');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
//use PhpOffice\PhpSpreadsheet\writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;

$rango_fecha = $_GET["rango_fecha"];
$id_servicio = $_GET["id_servicio"];

$fecha_cortada = explode(" ", $rango_fecha, 3);
$fecha_restada = new DateTime($fecha_cortada[0]);
$fecha_incio = $fecha_restada->format('Y-m-d');

$fecha_sumada = new DateTime($fecha_cortada[2]);
$fecha_fin = $fecha_sumada->format('Y-m-d');


if ($id_servicio == null) {
    $error = "error";
    $mensaje = "Por favor seleccione un servicio.";
    echo "<script> 

ejecutar_boton();
function ejecutar_boton() {
   if('$error' == 'success'){
    toastr.success('$mensaje','Hola!')
    setTimeout(function() {
    }, 1000);
    
   }else{
    toastr.error('$mensaje','Hola!')
   }
}

</script>";
} else if ($rango_fecha == null) {
    $error = "error";
    $mensaje = "Por favor seleccione un rango de fechas";
    echo "<script> 

ejecutar_boton();
function ejecutar_boton() {
   if('$error' == 'success'){
    toastr.success('$mensaje','Hola!')
    setTimeout(function() {
    }, 1000);
    
   }else{
    toastr.error('$mensaje','Hola!')
   }
}

</script>";
} else {

    /* if ($id_servicio != 0) {
        $consultar_servicios = $conn->prepare("SELECT * FROM servicios_control_rutas WHERE id = '$id_servicio' AND fecha_recepcion_doc 
    BETWEEN '$fecha_incio' AND '$fecha_fin' ");
    } else {
        $consultar_servicios = $conn->prepare("SELECT * FROM servicios_control_rutas WHERE id = '$id_servicio'");
    }*/
    $consultar_servicios = $conn->prepare("SELECT * FROM servicios_control_rutas WHERE tipo_servicio = '$id_servicio' AND fecha_recepcion_doc 
    BETWEEN '" . $fecha_incio . "' AND '" . $fecha_fin . "'");
    $consultar_servicios->execute();
    $consultar_servicios = $consultar_servicios->fetchAll(PDO::FETCH_ASSOC);

    foreach ($consultar_servicios as $servicios) {
        $id_servicio_registrado = $servicios["id"];
        $id_cliente = $servicios["id_cliente"];
        $id_sub_cliente = $servicios["id_sub_cliente"];
        $id_tipo_servicios = $servicios["tipo_servicio"];
        $id_tipo_carga = $servicios["tipo_carga"];
        $costo_servicio = $servicios["costo_servicio"];

        $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
        $consultar_cliente->execute();
        $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
        foreach ($consultar_cliente as $cliente) {
        }
        $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
        $consultar_sub_cliente->execute();
        $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
        foreach ($consultar_sub_cliente as $sub_cliente) {
        }

        $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
        $consultar_tipo_servicio->execute();
        $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
        foreach ($consultar_tipo_servicio as $tipo_servicio) {
        }
        $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
        $consultar_tipo_carga->execute();
        $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
        foreach ($consultar_tipo_carga as $tipo_carga) {
        }
    }



    $fecha_generado = date("Y-m-s H:i");
    $nombre_archivo = 'MI-TIR-FO' . $id_servicio_registrado . ' CONTROL DE VEHICULOS EN RUTA ' . $fecha_generado;
    $spreadsheet = new SpreadSheet();
    $spreadsheet->getProperties()->setCreator("WORLDSHIPPINGCOMPANYSAS")->setTitle($nombre_archivo);

    $spreadsheet->setActiveSheetIndex(0);
    $hojaActiva = $spreadsheet->getActiveSheet();

    /////////nombre de la hoja 
    /* $hojaActiva->setTitle('SERVICIO: ' . $tipo_servicio["nombre_servicio"]);*/

    $spreadsheet->getDefaultStyle()->getFont()->setName("Calibri");
    $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
    $spreadsheet->getActiveSheet()->getSheetView()->setZoomScale(90);
    $spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(70);
    $spreadsheet->getActiveSheet()->getRowDimension('3')->setRowHeight(5);
    //$spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(5);

    $spreadsheet->getActiveSheet()->getRowDimension('41')->setRowHeight(41);
    ////altura automatica celdas de observacion
    $spreadsheet->getActiveSheet()->getRowDimension('4,1000')->setRowHeight(-1);

    foreach (range('A', 'Z') as $char) {
        $spreadsheet->getActiveSheet()->getStyle($char)->getAlignment()->setWrapText(true);
    }
    foreach (range('A', 'Z') as $char) {
        foreach (range('A', 'Z') as $char2) {
            $spreadsheet->getActiveSheet()->getStyle($char . $char2)->getAlignment()->setWrapText(true);
        }
    }
    $spreadsheet->getActiveSheet()->getStyle('g')->getAlignment()->setWrapText(true);
    ////////////////
    $hojaActiva->getColumnDimension('A')->setWidth(15);
    $hojaActiva->getColumnDimension('B')->setWidth(10);
    $hojaActiva->getColumnDimension('C')->setWidth(25);
    $hojaActiva->getColumnDimension('D')->setWidth(25);
    $hojaActiva->getColumnDimension('E')->setWidth(25);
    $hojaActiva->getColumnDimension('F')->setWidth(25);

    //bordes

    $styleArray1 = array(
        'borders' => array(
            'allBorders' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => array('argb' => '0000'),
            ),
        ),

    );
    $styleArray2 = array(
        'borders' => array(
            'outline' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => array('argb' => '0000'),
            ),
        ),
        'font' => array(
            'bold' => true
        )

    );
    $styleArray3 = array(
        'borders' => array(
            'allBorders' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => array('argb' => '0000'),
            ),
        ),
        'font' => array(
            'bold' => true
        )

    );
    $style_titles = array(
        'font' => array(
            'bold' => true
        )
    );
    $hojaActiva->getStyle('A1:F1')->applyFromArray($styleArray2);
    $hojaActiva->getStyle('A2:F2')->applyFromArray($styleArray2);

    $hojaActiva->getStyle('A4:z4')->applyFromArray($styleArray3);
    $hojaActiva->getStyle('AA4:ZZ4')->applyFromArray($styleArray3);
    // $hojaActiva->getStyle('A8:F39')->applyFromArray($styleArray1);


    $spreadsheet->getActiveSheet()->getStyle('A1:F1')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('FFFF');


    ///////

    $hojaActiva->mergeCells('D1:f1');
    $hojaActiva->mergeCells('A2:f2');
    $hojaActiva->mergeCells('A3:f3');
    //$hojaActiva->mergeCells('G4:Z4');

    ///// logo titulo

    $hojaActiva->setCellValue('D1', 'CONTROL DE VEHICULOS EN RUTA')->getStyle('D1')->getAlignment()->setHorizontal('center');
    $hojaActiva->setCellValue('D1', 'CONTROL DE VEHICULOS EN RUTA')->getStyle('D1')->getAlignment()->setVertical('center');

    $logo = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
    $logo->setName('Logo');
    $logo->setDescription('Logo');
    $logo->setPath('../../assets/images/logo.png'); // put your path and image here
    $logo->setCoordinates('B1');
    $logo->setOffsetX(80);
    $logo->setOffsetY(8);
    $logo->setRotation(0);
    $logo->setWidthAndHeight(70, 100);
    /*$logo->getShadow()->setVisible(true);
    $logo->getShadow()->setDirection(45);*/
    $logo->setWorksheet($spreadsheet->getActiveSheet());

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////// encabezado 
    $hojaActiva->setCellValue('A2', 'VERSIÓN: 2           CODIGO: MI-TR-FO' . $id_servicio_registrado . '           FECHA: ' . $fecha_generado . '           PAGINA 1 DE 1')->getStyle('A2')->getAlignment()->setHorizontal('center');


    //////// encabezado de tabla de los servicios
    //////// encabezado de tabla de los servicios
    if ($id_tipo_servicios == 1) { // servicio itr importacion
        $encabezado = [
            "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Reserva", "Tipo servicio", "Tipo de carga", "Cantidad recibidos",
            "Tipo de contenedor", "Número contenedor", "Tamaño contenedor", "Linea naviera", "Puerto retiro", "Fecha retiro puerto del vacío",
            "Fecha vencimiento vacío", "Observación", "Número de servicio", "Número de placa del vehículo", "Nombre del conductor del vehículo",
            "Identificación del conductor", "Ruta", "¿Contenedor bajado a piso?", "Arim", "Fecha y hora de descargue", "Observación",
            "Fecha y hora de incio llenado", "Fecha y hora de terminación llenado", "Cantidad de bultos", "Cantidad de pallets/paquetes/carga suelta total", "Aplica almacenamiento",
            "Número de día de almacenaje", "Días de almacenaje libre", "Cubicaje", "Observaciones", "Entrega mercancía",
            "¿Contenedor bajado a piso en patio aliado?", "Fecha bajado a piso", "Lugar de inspección vacío", "Fecha y hora de inspección vacío",
            "Lugar devolución vacío", "Fecha y hora devolución vacío", "Conductor", "identificación", "Placa vehículo", "Observaciones"
        ];
        $hojaActiva->fromArray($encabezado, null, 'A4');
        ///// servicios 1
        $numeroDeFila = 5;
        foreach ($consultar_servicios as $servicios) {
            $id_servicio_registrado = $servicios["id"];
            $id_cliente = $servicios["id_cliente"];
            $id_sub_cliente = $servicios["id_sub_cliente"];
            $id_tipo_servicios = $servicios["tipo_servicio"];
            $id_tipo_carga = $servicios["tipo_carga"];
            if ($servicios["aplica_almacenaje"] == 0) {
                $almacenaje = "No";
            } else {
                $almacenaje = "Si";
            }
            $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_cliente as $cliente) {
            }
            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
            $consultar_sub_cliente->execute();
            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_sub_cliente as $sub_cliente) {
            }

            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
            $consultar_tipo_servicio->execute();
            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_servicio as $tipo_servicio) {
            }
            $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
            $consultar_tipo_carga->execute();
            $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_carga as $tipo_carga) {
            }
            $id_conductor_devolucion = $servicios["id_conductor_d"];
            $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor_devolucion'");
            $consultar_conductor->execute();
            $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

            foreach ($consultar_conductor as $conductor) {
                $nombres_conductor_devolucion = $conductor["nombres_conductor"] . ' ' . $conductor["apellidos_conductor"];
                $identificacion_c_devolucion = $conductor["numero_identificacion"];
                $placa_c_devolucion = $conductor["placa_vehiculo"];
            }
            // inicio serivicico
            $hojaActiva->setCellValueByColumnAndRow(1, $numeroDeFila, $servicios["costo_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(2, $numeroDeFila, $servicios["id"]);
            $hojaActiva->setCellValueByColumnAndRow(3, $numeroDeFila, $cliente["razon_social"]);
            $hojaActiva->setCellValueByColumnAndRow(4, $numeroDeFila, $sub_cliente["nombre_sub_cliente"]);
            $hojaActiva->setCellValueByColumnAndRow(5, $numeroDeFila, $servicios["dons"]);
            $hojaActiva->setCellValueByColumnAndRow(6, $numeroDeFila, $tipo_servicio["nombre_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(7, $numeroDeFila, $tipo_carga["nombre_carga"]);
            $hojaActiva->setCellValueByColumnAndRow(8, $numeroDeFila, $servicios["cantidad_carga_recibidos"]);
            $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["tipo_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(11, $numeroDeFila, $servicios["tamaño_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(12, $numeroDeFila, $servicios["linea_naviera"]);
            $hojaActiva->setCellValueByColumnAndRow(13, $numeroDeFila, $servicios["puerto_origen"]);
            $hojaActiva->setCellValueByColumnAndRow(14, $numeroDeFila, $servicios["fecha_hora_r_p"]);
            $hojaActiva->setCellValueByColumnAndRow(15, $numeroDeFila, $servicios["fecha_vencimiento_b_p_zf"]);
            $hojaActiva->setCellValueByColumnAndRow(16, $numeroDeFila, $servicios["observacion1"]);
            //transporte
            $hojaActiva->setCellValueByColumnAndRow(17, $numeroDeFila, $servicios["id"]);
            $hojaActiva->setCellValueByColumnAndRow(18, $numeroDeFila, $servicios["numero_placa_vehiculo"]);
            $hojaActiva->setCellValueByColumnAndRow(19, $numeroDeFila, $servicios["nombre_conductor"]);
            $hojaActiva->setCellValueByColumnAndRow(20, $numeroDeFila, $servicios["identificacion_conductor"]);
            $hojaActiva->setCellValueByColumnAndRow(21, $numeroDeFila, $servicios["ruta"]);
            $hojaActiva->setCellValueByColumnAndRow(22, $numeroDeFila, "Verificar tabla bajado a piso");
            $hojaActiva->setCellValueByColumnAndRow(23, $numeroDeFila, $servicios["arim"]);
            $hojaActiva->setCellValueByColumnAndRow(24, $numeroDeFila, $servicios["fecha_r_p_p_o"]);
            $hojaActiva->setCellValueByColumnAndRow(25, $numeroDeFila, $servicios["observacion2"]);
            //Descargue mercancia
            $hojaActiva->setCellValueByColumnAndRow(26, $numeroDeFila, $servicios["fecha_hora_incio_descargue"]);
            $hojaActiva->setCellValueByColumnAndRow(27, $numeroDeFila, $servicios["fecha_hora_terminacion_descargue"]);
            $hojaActiva->setCellValueByColumnAndRow(28, $numeroDeFila, $servicios["cantidad_bultos"]);
            $hojaActiva->setCellValueByColumnAndRow(29, $numeroDeFila, $servicios["cantidad_pallets"]);
            $hojaActiva->setCellValueByColumnAndRow(30, $numeroDeFila, $almacenaje);
            $hojaActiva->setCellValueByColumnAndRow(31, $numeroDeFila, $servicios["numero_dia_almacenaje"]);
            $hojaActiva->setCellValueByColumnAndRow(32, $numeroDeFila, $servicios["dia_almacenaje_libre"]);
            $hojaActiva->setCellValueByColumnAndRow(33, $numeroDeFila, $servicios["cubicaje"]);
            $hojaActiva->setCellValueByColumnAndRow(34, $numeroDeFila, $servicios["observaciones_carga"]);
            ///Entrega de mercancia
            $hojaActiva->setCellValueByColumnAndRow(35, $numeroDeFila, "Verificar tabla entrega mercancía");

            //// Devolucion del vacio
            $hojaActiva->setCellValueByColumnAndRow(36, $numeroDeFila, $servicios["patelizo"]);
            $hojaActiva->setCellValueByColumnAndRow(37, $numeroDeFila, $servicios["fecha_bajado_piso"]);
            $hojaActiva->setCellValueByColumnAndRow(38, $numeroDeFila, $servicios["lugar_inspeccion_vacio"]);
            $hojaActiva->setCellValueByColumnAndRow(39, $numeroDeFila, $servicios["fecha_hora_inspeccion"]);
            $hojaActiva->setCellValueByColumnAndRow(40, $numeroDeFila, $servicios["lugar_devolucion_vacio"]);
            $hojaActiva->setCellValueByColumnAndRow(41, $numeroDeFila, $servicios["fecha_devolucion"]);
            $hojaActiva->setCellValueByColumnAndRow(42, $numeroDeFila, $nombres_conductor_devolucion);
            $hojaActiva->setCellValueByColumnAndRow(43, $numeroDeFila, $identificacion_c_devolucion);
            $hojaActiva->setCellValueByColumnAndRow(44, $numeroDeFila,   $placa_c_devolucion);
            $hojaActiva->setCellValueByColumnAndRow(45, $numeroDeFila, $servicios["observacion3"]);

            $numeroDeFila++;
        }

        // CONTENEDOR BAJADO A PISO //////////////////////////////////////////////////////////////////////////////

        $hoja2 = $spreadsheet->createSheet();
        // Generale el nombre de hoja para que confirmes que tienes la hoja creada
        $hoja2->setTitle('CONTENEDOR BAJADO A PISO');
        /////////////////////////////////////////////////////

        $hoja2->getColumnDimension('A')->setWidth(10);
        $hoja2->getColumnDimension('B')->setWidth(25);
        $hoja2->getColumnDimension('C')->setWidth(25);
        $hoja2->getColumnDimension('D')->setWidth(25);
        $hoja2->getColumnDimension('E')->setWidth(28);


        $hoja2->getStyle('A1:e1')->applyFromArray($styleArray2);
        $hoja2->getStyle('A2:e2')->applyFromArray($styleArray2);
        $hoja2->getStyle('A3:e3')->applyFromArray($styleArray3);
        // $hojaActiva->getStyle('A8:F39')->applyFromArray($styleArray1);

        ///////// style

        ///////
        $hoja2->mergeCells('D1:e1');
        $hoja2->mergeCells('A2:e2');
        //$hojaActiva->mergeCells('G4:Z4');
        $hoja2->setCellValue('C1', 'CONTENEDOR BAJADO A PISO')->getStyle('C1')->getAlignment()->setHorizontal('center')->setVertical('center');
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////// encabezado 
        $hoja2->setCellValue('A2', 'VERSIÓN: 2           CODIGO: MI-TR-FO' . $id_servicio_registrado . '           FECHA: ' . $fecha_generado . '           PAGINA 2 DE 3')->getStyle('A2')->getAlignment()->setHorizontal('center');

        $hoja2->setCellValue('A3', 'Id servicio')->getStyle('A3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja2->setCellValue('B3', 'Conductor')->getStyle('B3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja2->setCellValue('C3', 'Identificación conductor')->getStyle('C3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja2->setCellValue('D3', 'Placa vehículo')->getStyle('D3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja2->setCellValue('E3', 'Fecha bajado a piso')->getStyle('E3')->getAlignment()->setHorizontal('center')->setVertical('center');

        $numeroDeFilas = 4;
        foreach ($consultar_servicios as $servicios) {
            $id_servicioss = $servicios["id"];
            $consultar_bajado_a_piso = $conn->prepare("SELECT * FROM servicios_contenedor_bajo_piso WHERE id_servicio = '$id_servicioss'");
            $consultar_bajado_a_piso->execute();
            $consultar_bajado_a_piso = $consultar_bajado_a_piso->fetchAll(PDO::FETCH_ASSOC);


            foreach ($consultar_bajado_a_piso as $bajado_piso) {
                $hoja2->setCellValueByColumnAndRow(1, $numeroDeFilas,  $id_servicioss)->getStyle($numeroDeFilas)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $hoja2->setCellValueByColumnAndRow(2, $numeroDeFilas, $bajado_piso["nombre_conductor"])->getStyle($numeroDeFilas)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $hoja2->setCellValueByColumnAndRow(3, $numeroDeFilas, $bajado_piso["identificacion"])->getStyle($numeroDeFilas)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja2->setCellValueByColumnAndRow(4, $numeroDeFilas, $bajado_piso["placa"])->getStyle($numeroDeFilas)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja2->setCellValueByColumnAndRow(5, $numeroDeFilas, $bajado_piso["fecha_creacion"])->getStyle($numeroDeFilas)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $numeroDeFilas++;
            }
        }

        // ENTREGA DE MERCANCIA //////////////////////////////////////////////////////////////////////////////
        $hoja3 = $spreadsheet->createSheet();
        // Generale el nombre de hoja para que confirmes que tienes la hoja creada
        $hoja3->setTitle('ENTREGA DE MERCANCÍA');
        /////////////////////////////////////////////////////
        $hoja3->getColumnDimension('A')->setWidth(10);
        $hoja3->getColumnDimension('B')->setWidth(22);
        $hoja3->getColumnDimension('C')->setWidth(22);
        $hoja3->getColumnDimension('D')->setWidth(22);
        $hoja3->getColumnDimension('E')->setWidth(22);
        $hoja3->getColumnDimension('F')->setWidth(22);
        $hoja3->getColumnDimension('G')->setWidth(22);
        $hoja3->getColumnDimension('H')->setWidth(28);
        $hoja3->getColumnDimension('I')->setWidth(28);
        $hoja3->getColumnDimension('J')->setWidth(22);
        $hoja3->getColumnDimension('K')->setWidth(22);
        $hoja3->getColumnDimension('L')->setWidth(22);


        $hoja3->getStyle('A1:e1')->applyFromArray($styleArray2);
        $hoja3->getStyle('A2:e2')->applyFromArray($styleArray2);
        $hoja3->getStyle('A3:l3')->applyFromArray($styleArray3);

        // $hojaActiva->getStyle('A8:F39')->applyFromArray($styleArray1);

        ///////
        $hoja3->mergeCells('D1:e1');
        $hoja3->mergeCells('A2:e2');
        //$hojaActiva->mergeCells('G4:Z4');
        $hoja3->setCellValue('C1', 'ENTREGA DE MERCANCÍA')->getStyle('C1')->getAlignment()->setHorizontal('center')->setVertical('center');
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////// encabezado 
        $hoja3->setCellValue('A2', 'VERSIÓN: 2           CODIGO: MI-TR-FO' . $id_servicio_registrado . '           FECHA: ' . $fecha_generado . '           PAGINA 3 DE 3')->getStyle('A2')->getAlignment()->setHorizontal('center');

        $hoja3->setCellValue('A3', 'Id servicio')->getStyle('A3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('B3', 'Fecha y hora')->getStyle('B3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('C3', 'Transportadora')->getStyle('C3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('D3', 'Conductor')->getStyle('D3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('E3', 'Identificación conductor')->getStyle('E3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('F3', 'Placa vehículo')->getStyle('F3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('G3', 'Teléfono conductor')->getStyle('G3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('H3', 'Cantidad de pallets/paquetes/carga suelta despachados')->getStyle('H3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('I3', 'Cantidad de bultos despachados')->getStyle('I3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('J3', 'Peso aprox.')->getStyle('J3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('K3', 'Destino')->getStyle('K3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('L3', 'Observación')->getStyle('L3')->getAlignment()->setHorizontal('center')->setVertical('center');

        $numerosFilaMercancia = 4;

        foreach ($consultar_servicios as $servicios) {
            $id_servicioss = $servicios["id"];
            $consultar_entrega_mercancia = $conn->prepare("SELECT * FROM despachos WHERE id_servicio = '$id_servicioss'");
            $consultar_entrega_mercancia->execute();
            $consultar_entrega_mercancia = $consultar_entrega_mercancia->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_entrega_mercancia as $entrega_mercancia) {
                $hoja3->setCellValueByColumnAndRow(1, $numerosFilaMercancia, $id_servicioss)->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $hoja3->setCellValueByColumnAndRow(2, $numerosFilaMercancia, $entrega_mercancia["fecha_hora_despacho"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $hoja3->setCellValueByColumnAndRow(3, $numerosFilaMercancia, $entrega_mercancia["nombre_transportadora"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(4, $numerosFilaMercancia, $entrega_mercancia["nombres_conductor"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(5, $numerosFilaMercancia, $entrega_mercancia["numero_identificacion"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(6, $numerosFilaMercancia, $entrega_mercancia["placa_vehiculo"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(7, $numerosFilaMercancia, $entrega_mercancia["telefono"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(8, $numerosFilaMercancia, $entrega_mercancia["cantidad_pallets_despacho"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(9, $numerosFilaMercancia, $entrega_mercancia["cantidad_bultos_despachados"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(10, $numerosFilaMercancia, $entrega_mercancia["peso_aprox"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(11, $numerosFilaMercancia, $entrega_mercancia["destino"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(12, $numerosFilaMercancia, $entrega_mercancia["observacion"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;

                $numerosFilaMercancia++;
            }
        }
    } else if ($id_tipo_servicios == 2) { // servicio itr exportacion
        if ($servicios["impoexpo"] == 1) {
            $encabezado = [
                "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Do/Ns", "Tipo servicio", "Lugar de reitro del vacío", "Fecha de retiro del vacío",
                "Tipo de contenedor", "Número contenedor", "Tamaño contenedor", "Linea naviera", "Conductor", "Identificación conductor", "Placa vehículo",
                "¿Contenedor bajado a piso?", "Fecha bajado a piso", "Observaciones", "Recibo de mercancía", "Fecha y hora de cargue", "Observaciones", "Lugar de puerto destino",
                "Fecha y hora de cita de ingreso", "Número de orden de servicio", "Arim (Autorización)", "Observaciones"
            ];
        } else {
            $encabezado = [
                "Costo operación", "Id servicio", "Recibo de mercancía", "Cliente", "Sub cliente", "Do/Ns", "Tipo servicio", "Lugar de reitro del vacío", "Fecha de retiro del vacío",
                "Tipo de contenedor", "Número contenedor", "Tamaño contenedor", "Linea naviera", "Conductor", "Identificación conductor", "Placa vehículo",
                "¿Contenedor bajado a piso?", "Fecha bajado a piso", "Observaciones", "Fecha y hora de cargue", "Observaciones", "Lugar de puerto destino",
                "Fecha y hora de cita de ingreso", "Número de orden de servicio", "Arim (Autorización)", "Observaciones"
            ];
        }
        $hojaActiva->fromArray($encabezado, null, 'A4');
        ///// servicios 2
        $numeroDeFila = 5;
        foreach ($consultar_servicios as $servicios) {
            $id_servicio_registrado = $servicios["id"];
            $id_cliente = $servicios["id_cliente"];
            $id_sub_cliente = $servicios["id_sub_cliente"];
            $id_tipo_servicios = $servicios["tipo_servicio"];
            $id_tipo_carga = $servicios["tipo_carga"];
            if ($servicios["aplica_almacenaje"] == 0) {
                $almacenaje = "No";
            } else {
                $almacenaje = "Si";
            }
            if ($servicios["contenedor_bajado_piso"] == 1) {
                $bajado_piso = "Sí";
            } else if ($servicios["contenedor_bajado_piso"] == 0) {
                $bajado_piso = "No";
            }

            $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_cliente as $cliente) {
            }
            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
            $consultar_sub_cliente->execute();
            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_sub_cliente as $sub_cliente) {
            }

            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
            $consultar_tipo_servicio->execute();
            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_servicio as $tipo_servicio) {
            }
            $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
            $consultar_tipo_carga->execute();
            $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_carga as $tipo_carga) {
            }
            $id_conductor_devolucion = $servicios["id_conductor_d"];
            $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor_devolucion'");
            $consultar_conductor->execute();
            $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

            foreach ($consultar_conductor as $conductor) {
                $nombres_conductor_devolucion = $conductor["nombres_conductor"] . ' ' . $conductor["apellidos_conductor"];
                $identificacion_c_devolucion = $conductor["numero_identificacion"];
                $placa_c_devolucion = $conductor["placa_vehiculo"];
            }
            // inicio serivicico
            if ($servicios["impoexpo"] == 1) {
                $hojaActiva->setCellValueByColumnAndRow(1, $numeroDeFila, $servicios["costo_servicio"]);
                $hojaActiva->setCellValueByColumnAndRow(2, $numeroDeFila, $servicios["id"]);
                $hojaActiva->setCellValueByColumnAndRow(3, $numeroDeFila, $cliente["razon_social"]);
                $hojaActiva->setCellValueByColumnAndRow(4, $numeroDeFila, $sub_cliente["nombre_sub_cliente"]);
                $hojaActiva->setCellValueByColumnAndRow(5, $numeroDeFila, $servicios["dons"]);
                $hojaActiva->setCellValueByColumnAndRow(6, $numeroDeFila, $tipo_servicio["nombre_servicio"]);
                $hojaActiva->setCellValueByColumnAndRow(7, $numeroDeFila, $servicios["lugar_entrega"]);
                $hojaActiva->setCellValueByColumnAndRow(8, $numeroDeFila, $servicios["fecha_lugar_entrega_v"]);
                $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["tipo_contenedor"]);
                $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["contenedor"]);
                $hojaActiva->setCellValueByColumnAndRow(11, $numeroDeFila, $servicios["tamaño_contenedor"]);
                $hojaActiva->setCellValueByColumnAndRow(12, $numeroDeFila, $servicios["linea_naviera"]);
                $hojaActiva->setCellValueByColumnAndRow(13, $numeroDeFila, $servicios["nombre_c_e_v"]);
                $hojaActiva->setCellValueByColumnAndRow(14, $numeroDeFila, $servicios["identificacion_c_e_v"]);
                $hojaActiva->setCellValueByColumnAndRow(15, $numeroDeFila, $servicios["placa_v_e_v"]);
                $hojaActiva->setCellValueByColumnAndRow(16, $numeroDeFila, $bajado_piso);
                $hojaActiva->setCellValueByColumnAndRow(17, $numeroDeFila, $servicios["fecha_bajado_piso"]);
                $hojaActiva->setCellValueByColumnAndRow(18, $numeroDeFila, $servicios["observacion1"]);
                //recibo mercancia
                $hojaActiva->setCellValueByColumnAndRow(19, $numeroDeFila, "Revisar tabla Recibo de mercancía");
                //transporte
                $hojaActiva->setCellValueByColumnAndRow(20, $numeroDeFila, $servicios["fecha_r_p_p_o"]);
                $hojaActiva->setCellValueByColumnAndRow(21, $numeroDeFila, $servicios["observacion2"]);
                // ingreso a puerto
                $hojaActiva->setCellValueByColumnAndRow(22, $numeroDeFila, $servicios["lugar_inspeccion_vacio"]);
                $hojaActiva->setCellValueByColumnAndRow(23, $numeroDeFila, $servicios["fecha_hora_inspeccion"]);
                $hojaActiva->setCellValueByColumnAndRow(24, $numeroDeFila, $servicios["dons"]);
                $hojaActiva->setCellValueByColumnAndRow(25, $numeroDeFila, $servicios["arim_retiro"]);
                $hojaActiva->setCellValueByColumnAndRow(26, $numeroDeFila, $servicios["observacion3"]);
            } else {
                $hojaActiva->setCellValueByColumnAndRow(1, $numeroDeFila, $servicios["costo_servicio"]);
                $hojaActiva->setCellValueByColumnAndRow(2, $numeroDeFila, $servicios["id"]);
                $hojaActiva->setCellValueByColumnAndRow(3, $numeroDeFila, "Revisar tabla Recibo de mercancía");
                $hojaActiva->setCellValueByColumnAndRow(4, $numeroDeFila, $cliente["razon_social"]);
                $hojaActiva->setCellValueByColumnAndRow(5, $numeroDeFila, $sub_cliente["nombre_sub_cliente"]);
                $hojaActiva->setCellValueByColumnAndRow(6, $numeroDeFila, $servicios["dons"]);
                $hojaActiva->setCellValueByColumnAndRow(7, $numeroDeFila, $tipo_servicio["nombre_servicio"]);
                $hojaActiva->setCellValueByColumnAndRow(8, $numeroDeFila, $servicios["lugar_entrega"]);
                $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["fecha_lugar_entrega_v"]);
                $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["tipo_contenedor"]);
                $hojaActiva->setCellValueByColumnAndRow(11, $numeroDeFila, $servicios["contenedor"]);
                $hojaActiva->setCellValueByColumnAndRow(12, $numeroDeFila, $servicios["tamaño_contenedor"]);
                $hojaActiva->setCellValueByColumnAndRow(13, $numeroDeFila, $servicios["linea_naviera"]);
                $hojaActiva->setCellValueByColumnAndRow(14, $numeroDeFila, $servicios["nombre_c_e_v"]);
                $hojaActiva->setCellValueByColumnAndRow(15, $numeroDeFila, $servicios["identificacion_c_e_v"]);
                $hojaActiva->setCellValueByColumnAndRow(16, $numeroDeFila, $servicios["placa_v_e_v"]);

                $hojaActiva->setCellValueByColumnAndRow(17, $numeroDeFila, $bajado_piso);
                $hojaActiva->setCellValueByColumnAndRow(18, $numeroDeFila, $servicios["fecha_bajado_piso"]);
                $hojaActiva->setCellValueByColumnAndRow(19, $numeroDeFila, $servicios["observacion1"]);
                //transporte
                $hojaActiva->setCellValueByColumnAndRow(20, $numeroDeFila, $servicios["fecha_r_p_p_o"]);
                $hojaActiva->setCellValueByColumnAndRow(21, $numeroDeFila, $servicios["observacion2"]);
                // ingreso a puerto
                $hojaActiva->setCellValueByColumnAndRow(22, $numeroDeFila, $servicios["lugar_inspeccion_vacio"]);
                $hojaActiva->setCellValueByColumnAndRow(23, $numeroDeFila, $servicios["fecha_hora_inspeccion"]);
                $hojaActiva->setCellValueByColumnAndRow(24, $numeroDeFila, $servicios["dons"]);
                $hojaActiva->setCellValueByColumnAndRow(25, $numeroDeFila, $servicios["arim_retiro"]);
                $hojaActiva->setCellValueByColumnAndRow(26, $numeroDeFila, $servicios["observacion3"]);
            }
        }
        // RECIBO DE MERCANCIA //////////////////////////////////////////////////////////////////////////////
        $hoja3 = $spreadsheet->createSheet();
        // Generale el nombre de hoja para que confirmes que tienes la hoja creada
        $hoja3->setTitle('RECIBO DE MERCANCÍA');
        /////////////////////////////////////////////////////
        $hoja3->getColumnDimension('A')->setWidth(10);
        $hoja3->getColumnDimension('B')->setWidth(22);
        $hoja3->getColumnDimension('C')->setWidth(22);
        $hoja3->getColumnDimension('D')->setWidth(22);
        $hoja3->getColumnDimension('E')->setWidth(22);
        $hoja3->getColumnDimension('F')->setWidth(22);
        $hoja3->getColumnDimension('G')->setWidth(22);
        $hoja3->getColumnDimension('H')->setWidth(28);
        $hoja3->getColumnDimension('I')->setWidth(28);
        $hoja3->getColumnDimension('J')->setWidth(22);
        $hoja3->getColumnDimension('K')->setWidth(22);
        $hoja3->getColumnDimension('L')->setWidth(22);


        $hoja3->getStyle('A1:e1')->applyFromArray($styleArray2);
        $hoja3->getStyle('A2:e2')->applyFromArray($styleArray2);
        $hoja3->getStyle('A3:l3')->applyFromArray($styleArray3);

        // $hojaActiva->getStyle('A8:F39')->applyFromArray($styleArray1);

        ///////
        $hoja3->mergeCells('D1:e1');
        $hoja3->mergeCells('A2:e2');
        //$hojaActiva->mergeCells('G4:Z4');
        $hoja3->setCellValue('C1', 'ENTREGA DE MERCANCÍA')->getStyle('C1')->getAlignment()->setHorizontal('center')->setVertical('center');
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////// encabezado 
        $hoja3->setCellValue('A2', 'VERSIÓN: 2           CODIGO: MI-TR-FO' . $id_servicio_registrado . '           FECHA: ' . $fecha_generado . '           PAGINA 2 DE 2')->getStyle('A2')->getAlignment()->setHorizontal('center');

        $hoja3->setCellValue('A3', 'Id servicio')->getStyle('A3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('B3', 'Fecha y hora')->getStyle('B3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('C3', 'Transportadora')->getStyle('C3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('D3', 'Conductor')->getStyle('D3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('E3', 'Identificación conductor')->getStyle('E3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('F3', 'Placa vehículo')->getStyle('F3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('G3', 'Teléfono conductor')->getStyle('G3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('H3', 'Cantidad de pallets despachados')->getStyle('H3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('I3', 'Cantidad de bultos despachados')->getStyle('I3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('J3', 'Peso aprox.')->getStyle('J3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('K3', 'Destino')->getStyle('K3')->getAlignment()->setHorizontal('center')->setVertical('center');
        $hoja3->setCellValue('L3', 'Observación')->getStyle('L3')->getAlignment()->setHorizontal('center')->setVertical('center');

        $numerosFilaMercancia = 4;

        foreach ($consultar_servicios as $servicios) {
            $id_servicioss = $servicios["id"];
            $consultar_entrega_mercancia = $conn->prepare("SELECT * FROM despachos WHERE id_servicio = '$id_servicioss'");
            $consultar_entrega_mercancia->execute();
            $consultar_entrega_mercancia = $consultar_entrega_mercancia->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_entrega_mercancia as $entrega_mercancia) {
                $hoja3->setCellValueByColumnAndRow(1, $numerosFilaMercancia, $id_servicioss)->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $hoja3->setCellValueByColumnAndRow(2, $numerosFilaMercancia, $entrega_mercancia["fecha_hora_despacho"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $hoja3->setCellValueByColumnAndRow(3, $numerosFilaMercancia, $entrega_mercancia["nombre_transportadora"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(4, $numerosFilaMercancia, $entrega_mercancia["nombres_conductor"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(5, $numerosFilaMercancia, $entrega_mercancia["numero_identificacion"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(6, $numerosFilaMercancia, $entrega_mercancia["placa_vehiculo"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(7, $numerosFilaMercancia, $entrega_mercancia["telefono"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(8, $numerosFilaMercancia, $entrega_mercancia["cantidad_pallets_despacho"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(9, $numerosFilaMercancia, $entrega_mercancia["cantidad_bultos_despachados"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(10, $numerosFilaMercancia, $entrega_mercancia["peso_aprox"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(11, $numerosFilaMercancia, $entrega_mercancia["destino"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(12, $numerosFilaMercancia, $entrega_mercancia["observacion"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;

                $numerosFilaMercancia++;
            }
        }
    } else if ($id_tipo_servicios == 3) { // servicio retiro de contenedor vacios de puertos
        $encabezado = [
            "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Reserva", "Tipo servicio",
            "Tipo de contenedor", "Número contenedor", "Tamaño contenedor",
            "Puerto retiro del vacío", "Lugar devolución", "Fecha limite de devolución", "Observaciones",
            "Fecha y hora retiro puerto del vacío", "Arim retiro", "Nombre conductor (transportadora)", "Identificación del conductor (transportadora)", "Placa del vehículo (transportadora)",
            "¿Contenedor bajado a piso?", "Fecha de bajado a piso", "¿Contenedor debe ser inspeccionado?", "Lugar de inpsección", "Fecha de inspección", "Número de placa del vehículo",
            "Nombre conductor", "Identificación conductor", "¿Lugar devolución en puerto?", "Arim de ingreso", "Fecha y hora devolución", "Número de placa del vehículo", "Nombre conductor",
            "Identificación conductor"
        ];
        $hojaActiva->fromArray($encabezado, null, 'A4');
        ///// servicios 3 
        $numeroDeFila = 5;
        foreach ($consultar_servicios as $servicios) {
            $id_servicio_registrado = $servicios["id"];
            $id_cliente = $servicios["id_cliente"];
            $id_sub_cliente = $servicios["id_sub_cliente"];
            $id_tipo_servicios = $servicios["tipo_servicio"];
            $id_tipo_carga = $servicios["tipo_carga"];
            if ($servicios["aplica_almacenaje"] == 0) {
                $almacenaje = "No";
            } else {
                $almacenaje = "Si";
            }
            if ($servicios["contenedor_bajado_piso_fecha"] == 0) {
                $contenedor_bajado_piso = "No";
            } else {
                $contenedor_bajado_piso = "Si";
            }
            if ($servicios["contenedor_inspeccion"] == 0) {
                $contenerdor_inspeccion = "No";
            } else {
                $contenerdor_inspeccion = "Si";
            }
            if ($servicios["lugar_devolucion_puerto"] == 0) {
                $lugar_devolucion_puerto = "No";
            } else {
                $lugar_devolucion_puerto = "Si";
            }
            $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_cliente as $cliente) {
            }
            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
            $consultar_sub_cliente->execute();
            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_sub_cliente as $sub_cliente) {
            }

            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
            $consultar_tipo_servicio->execute();
            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_servicio as $tipo_servicio) {
            }
            $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
            $consultar_tipo_carga->execute();
            $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_carga as $tipo_carga) {
            }
            $id_conductor_devolucion = $servicios["id_conductor_d"];
            $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor_devolucion'");
            $consultar_conductor->execute();
            $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

            foreach ($consultar_conductor as $conductor) {
                $nombres_conductor_devolucion = $conductor["nombres_conductor"] . ' ' . $conductor["apellidos_conductor"];
                $identificacion_c_devolucion = $conductor["numero_identificacion"];
                $placa_c_devolucion = $conductor["placa_vehiculo"];
            }
            // inicio serivicico
            $hojaActiva->setCellValueByColumnAndRow(1, $numeroDeFila, $servicios["costo_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(2, $numeroDeFila, $servicios["id"]);
            $hojaActiva->setCellValueByColumnAndRow(3, $numeroDeFila, $cliente["razon_social"]);
            $hojaActiva->setCellValueByColumnAndRow(4, $numeroDeFila, $sub_cliente["nombre_sub_cliente"]);
            $hojaActiva->setCellValueByColumnAndRow(5, $numeroDeFila, $servicios["dons"]);
            $hojaActiva->setCellValueByColumnAndRow(6, $numeroDeFila, $tipo_servicio["nombre_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(7, $numeroDeFila, $servicios["tipo_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(8, $numeroDeFila, $servicios["contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["tamaño_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["puerto_origen"]);
            $hojaActiva->setCellValueByColumnAndRow(11, $numeroDeFila, $servicios["lugar_devolucion_vacio"]);
            $hojaActiva->setCellValueByColumnAndRow(12, $numeroDeFila, $servicios["fecha_limite_devolucion"]);
            $hojaActiva->setCellValueByColumnAndRow(13, $numeroDeFila, $servicios["observacion1"]);

            //mas información
            $hojaActiva->setCellValueByColumnAndRow(14, $numeroDeFila, $servicios["fecha_hora_r_p"]);
            $hojaActiva->setCellValueByColumnAndRow(15, $numeroDeFila, $servicios["arim_retiro"]);
            $hojaActiva->setCellValueByColumnAndRow(16, $numeroDeFila, $servicios["nombre_c_p_a"]);
            $hojaActiva->setCellValueByColumnAndRow(17, $numeroDeFila, $servicios["identificacion_c_p_a"]);
            $hojaActiva->setCellValueByColumnAndRow(18, $numeroDeFila, $servicios["placa_v_p_a"]);
            $hojaActiva->setCellValueByColumnAndRow(19, $numeroDeFila, $contenedor_bajado_piso);
            $hojaActiva->setCellValueByColumnAndRow(20, $numeroDeFila, $servicios["fecha_bajado_piso"]);
            $hojaActiva->setCellValueByColumnAndRow(21, $numeroDeFila, $contenerdor_inspeccion);
            $hojaActiva->setCellValueByColumnAndRow(22, $numeroDeFila, $servicios["lugar_inspeccion_vacio"]);
            $hojaActiva->setCellValueByColumnAndRow(23, $numeroDeFila, $servicios["fecha_hora_inspeccion"]);
            $hojaActiva->setCellValueByColumnAndRow(24, $numeroDeFila, $servicios["placa_v_z_f"]);
            $hojaActiva->setCellValueByColumnAndRow(25, $numeroDeFila, $servicios["nombre_c_z_f"]);
            $hojaActiva->setCellValueByColumnAndRow(26, $numeroDeFila, $servicios["identificacion_c_z_f"]);

            //mas información
            $hojaActiva->setCellValueByColumnAndRow(27, $numeroDeFila, $lugar_devolucion_puerto);
            $hojaActiva->setCellValueByColumnAndRow(28, $numeroDeFila, $servicios["arim"]);
            $hojaActiva->setCellValueByColumnAndRow(29, $numeroDeFila, $servicios["fecha_devolucion"]);
            $hojaActiva->setCellValueByColumnAndRow(30, $numeroDeFila, $servicios["placa_v_e_v"]);
            $hojaActiva->setCellValueByColumnAndRow(31, $numeroDeFila, $servicios["nombre_c_e_v"]);
            $hojaActiva->setCellValueByColumnAndRow(32, $numeroDeFila, $servicios["identificacion_c_e_v"]);
            $numeroDeFila++;
        }
    } else if ($id_tipo_servicios == 4) {  // servicio ingreso de contenedor vacio puertos
        $encabezado = [
            "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Reserva",  "Tipo servicio", "Linea naviera",
            "Tipo de contenedor", "Tamaño de contenedor", "Número contenedor",
            "Patio retiro del vacío", "Fecha limite de entrega del del vacío", "Observaciones",
            "Fecha y hora retiro puerto del vacío", "Número de orden de servicio", "Nombre conductor (transportadora)", "Identificación del conductor (transportadora)", "Placa del vehículo (transportadora)",
            "¿Contenedor bajado a piso?", "Fecha de bajado a piso", "¿Viaje fallido?", "Observación",
            "Fecha y hora de ingreso puerto", "Puerto destino", "Arim de ingreso", "Nombre conductor", "Identificación conductor", "Número de placa del vehículo"

        ];
        $hojaActiva->fromArray($encabezado, null, 'A4');
        ///// servicios 3 
        $numeroDeFila = 5;
        foreach ($consultar_servicios as $servicios) {
            $id_servicio_registrado = $servicios["id"];
            $id_cliente = $servicios["id_cliente"];
            $id_sub_cliente = $servicios["id_sub_cliente"];
            $id_tipo_servicios = $servicios["tipo_servicio"];
            $id_tipo_carga = $servicios["tipo_carga"];
            $dons = $servicios["dons"];
            $donss = $servicios["dons"];
            if ($servicios["aplica_almacenaje"] == 0) {
                $almacenaje = "No";
            } else {
                $almacenaje = "Si";
            }
            if ($servicios["contenedor_bajado_piso_fecha"] == 0) {
                $contenedor_bajado_piso = "No";
            } else {
                $contenedor_bajado_piso = "Si";
            }
            if ($servicios["viaje_fallido"] == 0) {
                $viaje_fallido = "No";
            } else {
                $viaje_fallido = "Si";
            }
            if ($servicios["lugar_devolucion_puerto"] == 0) {
                $lugar_devolucion_puerto = "No";
            } else {
                $lugar_devolucion_puerto = "Si";
            }
            $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_cliente as $cliente) {
            }
            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
            $consultar_sub_cliente->execute();
            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_sub_cliente as $sub_cliente) {
            }

            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
            $consultar_tipo_servicio->execute();
            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_servicio as $tipo_servicio) {
            }
            $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
            $consultar_tipo_carga->execute();
            $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_carga as $tipo_carga) {
            }
            $id_conductor_devolucion = $servicios["id_conductor_d"];
            $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor_devolucion'");
            $consultar_conductor->execute();
            $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

            foreach ($consultar_conductor as $conductor) {
                $nombres_conductor_devolucion = $conductor["nombres_conductor"] . ' ' . $conductor["apellidos_conductor"];
                $identificacion_c_devolucion = $conductor["numero_identificacion"];
                $placa_c_devolucion = $conductor["placa_vehiculo"];
            }
            // inicio serivicico
            $hojaActiva->setCellValueByColumnAndRow(1, $numeroDeFila, $servicios["costo_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(2, $numeroDeFila, $servicios["id"]);
            $hojaActiva->setCellValueByColumnAndRow(3, $numeroDeFila, $cliente["razon_social"]);
            $hojaActiva->setCellValueByColumnAndRow(4, $numeroDeFila, $sub_cliente["nombre_sub_cliente"]);

            $hojaActiva->setCellValueByColumnAndRow(5, $numeroDeFila, $dons);
            $hojaActiva->setCellValueByColumnAndRow(6, $numeroDeFila, $tipo_servicio["nombre_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(7, $numeroDeFila, $servicios["linea_naviera"]);


            $hojaActiva->setCellValueByColumnAndRow(8, $numeroDeFila, $servicios["tipo_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["tamaño_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(11, $numeroDeFila, $servicios["patio_retiro_vacio"]);
            $hojaActiva->setCellValueByColumnAndRow(12, $numeroDeFila, $servicios["fecha_puerto_entrega_vacio"]);
            $hojaActiva->setCellValueByColumnAndRow(13, $numeroDeFila, $servicios["observacion1"]);

            //mas información
            $hojaActiva->setCellValueByColumnAndRow(14, $numeroDeFila, $servicios["fecha_hora_r_p"]);
            $hojaActiva->setCellValueByColumnAndRow(15, $numeroDeFila, $donss);
            $hojaActiva->setCellValueByColumnAndRow(16, $numeroDeFila, $servicios["nombre_c_p_a"]);
            $hojaActiva->setCellValueByColumnAndRow(17, $numeroDeFila, $servicios["identificacion_c_p_a"]);
            $hojaActiva->setCellValueByColumnAndRow(18, $numeroDeFila, $servicios["placa_v_p_a"]);
            $hojaActiva->setCellValueByColumnAndRow(19, $numeroDeFila, $contenedor_bajado_piso);
            $hojaActiva->setCellValueByColumnAndRow(20, $numeroDeFila, $servicios["fecha_bajado_piso"]);
            $hojaActiva->setCellValueByColumnAndRow(21, $numeroDeFila, $viaje_fallido);
            $hojaActiva->setCellValueByColumnAndRow(22, $numeroDeFila, $servicios["observacion2"]);

            //mas información

            $hojaActiva->setCellValueByColumnAndRow(23, $numeroDeFila, $servicios["fecha_ingreso_puerto"]);
            $hojaActiva->setCellValueByColumnAndRow(24, $numeroDeFila, $servicios["puerto_entrega_vacio"]);
            $hojaActiva->setCellValueByColumnAndRow(25, $numeroDeFila, $servicios["arim"]);
            $hojaActiva->setCellValueByColumnAndRow(26, $numeroDeFila, $servicios["nombre_c_e_v"]);
            $hojaActiva->setCellValueByColumnAndRow(27, $numeroDeFila, $servicios["identificacion_c_e_v"]);
            $hojaActiva->setCellValueByColumnAndRow(28, $numeroDeFila, $servicios["placa_v_e_v"]);
            $numeroDeFila++;
        }
    } else if ($id_tipo_servicios == 5) { // servicio Traslado de Carga Contenerizada
        $encabezado = [
            "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Do/Ns", "Tipo servicio",
            "Tipo de contenedor", "Número contenedor", "Tamaño contenedor", "Puerto retiro de vacío", "Fecha retiro puerto del vacío",
            "Lugar de entrega zona franca", "Fecha de entrega en zona franca", "Limite de devolución del vacío", "Observación",
            "Nombre del conductor del vehículo", "Identificación del conductor", "Número de placa del vehículo",
            "Fecha y hora de llegada a destino", "Fecha y hora finalización", "¿Contenedor bajado a piso?", "Fecha de bajado a piso",
            "¿Contenedor debe ser inspeccionado?", "Lugar de inpsección", "Fecha de inspección", "Número de placa del vehículo", "Nombre conductor",
            "Identificación conductor",  "¿Contenedor bajado a piso?", "Fecha de bajado a piso",  "¿Lugar devolución vacío?", "Arim de ingreso",
            "Fecha y hora devolución", "Número de placa del vehículo", "Nombre conductor", "Identificación conductor", "Observaciones"
        ];
        $hojaActiva->fromArray($encabezado, null, 'A4');
        ///// servicios 5
        $numeroDeFila = 5;
        foreach ($consultar_servicios as $servicios) {
            $id_servicio_registrado = $servicios["id"];
            $id_cliente = $servicios["id_cliente"];
            $id_sub_cliente = $servicios["id_sub_cliente"];
            $id_tipo_servicios = $servicios["tipo_servicio"];
            $id_tipo_carga = $servicios["tipo_carga"];

            if ($servicios["aplica_almacenaje"] == 0) {
                $almacenaje = "No";
            } else {
                $almacenaje = "Si";
            }
            if ($servicios["contenedor_inspeccion"] == 0) {
                $contenerdor_inspeccion = "No";
            } else {
                $contenerdor_inspeccion = "Si";
            }
            if ($servicios["contenedor_bajado_piso_fecha"] == 0) {
                $contenedor_bajado_piso = "No";
            } else {
                $contenedor_bajado_piso = "Si";
            }
            if ($servicios["contenedor_bajado_piso_1"] == 0) {
                $contenedor_bajado_piso_1 = "No";
            } else {
                $contenedor_bajado_piso_1 = "Si";
            }
            if ($servicios["lugar_devolucion_puerto"] == 0) {
                $lugar_devolucion_puerto = "No";
            } else {
                $lugar_devolucion_puerto = "Si";
            }

            $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_cliente as $cliente) {
            }
            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
            $consultar_sub_cliente->execute();
            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_sub_cliente as $sub_cliente) {
            }

            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
            $consultar_tipo_servicio->execute();
            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_servicio as $tipo_servicio) {
            }
            $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
            $consultar_tipo_carga->execute();
            $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_carga as $tipo_carga) {
            }
            $id_conductor_devolucion = $servicios["id_conductor_d"];
            $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor_devolucion'");
            $consultar_conductor->execute();
            $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

            foreach ($consultar_conductor as $conductor) {
                $nombres_conductor_devolucion = $conductor["nombres_conductor"] . ' ' . $conductor["apellidos_conductor"];
                $identificacion_c_devolucion = $conductor["numero_identificacion"];
                $placa_c_devolucion = $conductor["placa_vehiculo"];
            }
            // inicio serivicico
            $hojaActiva->setCellValueByColumnAndRow(1, $numeroDeFila, $servicios["costo_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(2, $numeroDeFila, $servicios["id"]);
            $hojaActiva->setCellValueByColumnAndRow(3, $numeroDeFila, $cliente["razon_social"]);
            $hojaActiva->setCellValueByColumnAndRow(4, $numeroDeFila, $sub_cliente["nombre_sub_cliente"]);
            $hojaActiva->setCellValueByColumnAndRow(5, $numeroDeFila, $servicios["dons"]);
            $hojaActiva->setCellValueByColumnAndRow(6, $numeroDeFila, $tipo_servicio["nombre_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(7, $numeroDeFila, $servicios["tipo_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(8, $numeroDeFila, $servicios["contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["tamaño_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["puerto_origen"]);
            $hojaActiva->setCellValueByColumnAndRow(11, $numeroDeFila, $servicios["fecha_hora_r_p"]);
            $hojaActiva->setCellValueByColumnAndRow(12, $numeroDeFila, $servicios["lugar_entrega_zona_f"]);
            $hojaActiva->setCellValueByColumnAndRow(13, $numeroDeFila, $servicios["fecha_entrega_zona_f"]);
            $hojaActiva->setCellValueByColumnAndRow(14, $numeroDeFila, $servicios["fecha_limite_devolucion"]);
            $hojaActiva->setCellValueByColumnAndRow(15, $numeroDeFila, $servicios["observacion1"]);

            //mas informacion
            $hojaActiva->setCellValueByColumnAndRow(16, $numeroDeFila, $servicios["nombre_c_p_a"]);
            $hojaActiva->setCellValueByColumnAndRow(17, $numeroDeFila, $servicios["identificacion_c_p_a"]);
            $hojaActiva->setCellValueByColumnAndRow(18, $numeroDeFila, $servicios["placa_v_p_a"]);
            $hojaActiva->setCellValueByColumnAndRow(19, $numeroDeFila, $servicios["fecha_hora_incio_descargue"]);
            $hojaActiva->setCellValueByColumnAndRow(20, $numeroDeFila, $servicios["fecha_hora_terminacion_descargue"]);
            $hojaActiva->setCellValueByColumnAndRow(21, $numeroDeFila,  $contenedor_bajado_piso);
            $hojaActiva->setCellValueByColumnAndRow(22, $numeroDeFila, $servicios["fecha_bajado_piso"]);
            //mas informacion
            $hojaActiva->setCellValueByColumnAndRow(23, $numeroDeFila, $contenerdor_inspeccion);
            $hojaActiva->setCellValueByColumnAndRow(24, $numeroDeFila, $servicios["lugar_inspeccion_vacio"]);
            $hojaActiva->setCellValueByColumnAndRow(25, $numeroDeFila, $servicios["fecha_hora_inspeccion"]);
            $hojaActiva->setCellValueByColumnAndRow(26, $numeroDeFila, $servicios["placa_v_z_f"]);
            $hojaActiva->setCellValueByColumnAndRow(27, $numeroDeFila, $servicios["nombre_c_z_f"]);
            $hojaActiva->setCellValueByColumnAndRow(28, $numeroDeFila, $servicios["identificacion_c_z_f"]);
            $hojaActiva->setCellValueByColumnAndRow(29, $numeroDeFila, $contenedor_bajado_piso_1);
            $hojaActiva->setCellValueByColumnAndRow(30, $numeroDeFila, $servicios["fecha_bajado_piso_1"]);

            $hojaActiva->setCellValueByColumnAndRow(31, $numeroDeFila, $lugar_devolucion_puerto);
            $hojaActiva->setCellValueByColumnAndRow(32, $numeroDeFila, $servicios["arim"]);
            $hojaActiva->setCellValueByColumnAndRow(33, $numeroDeFila, $servicios["fecha_devolucion"]);
            $hojaActiva->setCellValueByColumnAndRow(34, $numeroDeFila, $servicios["placa_v_e_v"]);
            $hojaActiva->setCellValueByColumnAndRow(35, $numeroDeFila, $servicios["nombre_c_e_v"]);
            $hojaActiva->setCellValueByColumnAndRow(36, $numeroDeFila, $servicios["identificacion_c_e_v"]);
            $hojaActiva->setCellValueByColumnAndRow(37, $numeroDeFila, $servicios["observacion3"]);

            $numeroDeFila++;
        }
    } else if ($id_tipo_servicios == 6) { // servicio Traslado de Carga Suelta
        $encabezado = [
            "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Do/Ns", "Tipo servicio", "Tipo de carga", "Cantidad recibida",
            "Puerto retiro del vacío", "Fecha retiro puerto del vacío", "Lugar entrega", "Observaciones",
            "Nombre del conductor del vehículo", "Identificación del conductor",  "Número de placa del vehículo",
            "Fecha y hora de llegada al lugar de entrega", "Fecha y hora finalización de desacargue", "Observaciones"
        ];
        $hojaActiva->fromArray($encabezado, null, 'A4');
        ///// servicios 6
        $numeroDeFila = 5;
        foreach ($consultar_servicios as $servicios) {
            $id_servicio_registrado = $servicios["id"];
            $id_cliente = $servicios["id_cliente"];
            $id_sub_cliente = $servicios["id_sub_cliente"];
            $id_tipo_servicios = $servicios["tipo_servicio"];
            $id_tipo_carga = $servicios["tipo_carga"];
            if ($servicios["aplica_almacenaje"] == 0) {
                $almacenaje = "No";
            } else {
                $almacenaje = "Si";
            }
            $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_cliente as $cliente) {
            }
            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
            $consultar_sub_cliente->execute();
            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_sub_cliente as $sub_cliente) {
            }

            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
            $consultar_tipo_servicio->execute();
            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_servicio as $tipo_servicio) {
            }
            $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
            $consultar_tipo_carga->execute();
            $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_carga as $tipo_carga) {
            }
            $id_conductor_devolucion = $servicios["id_conductor_d"];
            $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor_devolucion'");
            $consultar_conductor->execute();
            $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

            foreach ($consultar_conductor as $conductor) {
                $nombres_conductor_devolucion = $conductor["nombres_conductor"] . ' ' . $conductor["apellidos_conductor"];
                $identificacion_c_devolucion = $conductor["numero_identificacion"];
                $placa_c_devolucion = $conductor["placa_vehiculo"];
            }
            // inicio serivicico
            $hojaActiva->setCellValueByColumnAndRow(1, $numeroDeFila, $servicios["costo_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(2, $numeroDeFila, $servicios["id"]);
            $hojaActiva->setCellValueByColumnAndRow(3, $numeroDeFila, $cliente["razon_social"]);
            $hojaActiva->setCellValueByColumnAndRow(4, $numeroDeFila, $sub_cliente["nombre_sub_cliente"]);
            $hojaActiva->setCellValueByColumnAndRow(5, $numeroDeFila, $servicios["dons"]);
            $hojaActiva->setCellValueByColumnAndRow(6, $numeroDeFila, $tipo_servicio["nombre_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(7, $numeroDeFila, $tipo_carga["nombre_carga"]);
            $hojaActiva->setCellValueByColumnAndRow(8, $numeroDeFila, $servicios["cantidad_pallets"]);
            $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["puerto_origen"]);
            $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["fecha_hora_r_p"]);
            $hojaActiva->setCellValueByColumnAndRow(11, $numeroDeFila, $servicios["lugar_entrega"]);
            $hojaActiva->setCellValueByColumnAndRow(12, $numeroDeFila, $servicios["observacion1"]);
            //mas informacion
            $hojaActiva->setCellValueByColumnAndRow(13, $numeroDeFila, $servicios["nombre_c_z_f"]);
            $hojaActiva->setCellValueByColumnAndRow(14, $numeroDeFila, $servicios["identificacion_c_z_f"]);
            $hojaActiva->setCellValueByColumnAndRow(15, $numeroDeFila, $servicios["placa_v_z_f"]);
            $hojaActiva->setCellValueByColumnAndRow(16, $numeroDeFila, $servicios["fecha_hora_incio_descargue"]);
            $hojaActiva->setCellValueByColumnAndRow(17, $numeroDeFila, $servicios["fecha_hora_terminacion_descargue"]);
            $hojaActiva->setCellValueByColumnAndRow(18, $numeroDeFila, $servicios["observacion2"]);
            $numeroDeFila++;
        }
    } else if ($id_tipo_servicios == 9) { // servicio Vaciado de Contenedor
        foreach ($consultar_servicios as $servicios) {
            $tipo_carga = $servicios["tipo_carga"];
        }
        if ($tipo_carga == 1) {
            $nombre_carga = "pallets";
            $encabezado = [
                "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Do/Ns", "Tipo servicio", "¿Contenedor o Carga suelta?", "Tipo de carga", "Cantidad pallets total",
                "Peso aprox", "Tipo de contenedor", "Tamaño de contenedor", "Número de contenedor", "Fecha y hora inicio del vaciado", "Fecha y hora finalización del vaciado",
                "Nombre conductor", "Identificación conductor", "Número de placa vehículo", "Días de almacenaje", "Obsevaciones"
            ];
        } else if ($tipo_carga == 2) {
            $nombre_carga = "bultos";
            $encabezado = [
                "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Do/Ns", "Tipo servicio", "¿Contenedor o Carga suelta?", "Tipo de carga", "Cantidad bultos total",
                "Peso aprox", "Tipo de contenedor", "Tamaño de contenedor", "Número de contenedor", "Fecha y hora inicio del vaciado", "Fecha y hora finalización del vaciado",
                "Nombre conductor", "Identificación conductor", "Número de placa vehículo", "Días de almacenaje", "Obsevaciones"
            ];
        } else if ($tipo_carga == 3) {
            $nombre_carga = "carga suelta";
            $encabezado = [
                "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Do/Ns", "Tipo servicio", "¿Contenedor o Carga suelta?", "Tipo de carga", "Cantidad carga suelta total",
                "Peso aprox", "Tipo de contenedor", "Tamaño de contenedor", "Número de contenedor", "Fecha y hora inicio del vaciado", "Fecha y hora finalización del vaciado",
                "Nombre conductor", "Identificación conductor", "Número de placa vehículo", "Días de almacenaje", "Obsevaciones"
            ];
        } else if ($tipo_carga == 4) {
            $nombre_carga = "paquetes";
            $encabezado = [
                "Costo operación", "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Do/Ns", "Tipo servicio", "¿Contenedor o Carga suelta?", "Tipo de carga", "Cantidad paquetes total",
                "Peso aprox", "Tipo de contenedor", "Tamaño de contenedor", "Número de contenedor", "Fecha y hora inicio del vaciado", "Fecha y hora finalización del vaciado",
                "Nombre conductor", "Identificación conductor", "Número de placa vehículo", "Días de almacenaje", "Obsevaciones"
            ];
        } else if ($tipo_carga == 5) {
            $nombre_carga = "Pallets";
            $encabezado = [
                "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Do/Ns", "Tipo servicio", "¿Contenedor o Carga suelta?", "Tipo de carga", "Cantidad pallets total",
                "Cantidad bultos total", "Peso aprox", "Tipo de contenedor", "Tamaño de contenedor", "Número de contenedor", "Fecha y hora inicio del vaciado", "Fecha y hora finalización del vaciado",
                "Nombre conductor", "Identificación conductor", "Número de placa vehículo", "Días de almacenaje", "Obsevaciones"
            ];
        }

        $hojaActiva->fromArray($encabezado, null, 'A4');
        ///// servicios 6
        $numeroDeFila = 5;
        foreach ($consultar_servicios as $servicios) {
            $id_servicio_registrado = $servicios["id"];
            $id_cliente = $servicios["id_cliente"];
            $id_sub_cliente = $servicios["id_sub_cliente"];
            $id_tipo_servicios = $servicios["tipo_servicio"];
            $id_tipo_carga = $servicios["tipo_carga"];

            if ($servicios["contenedor_carga_suelta"] == 0) {
                $contenedor_carga_suelta = "Carga suelta";
            } else {
                $contenedor_carga_suelta = "Contenedor";
            }
            if ($servicios["aplica_almacenaje"] == 0) {
                $almacenaje = "No";
            } else {
                $almacenaje = "Si";
            }

            $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_cliente as $cliente) {
            }
            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
            $consultar_sub_cliente->execute();
            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_sub_cliente as $sub_cliente) {
            }

            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
            $consultar_tipo_servicio->execute();
            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_servicio as $tipo_servicio) {
            }
            $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
            $consultar_tipo_carga->execute();
            $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_carga as $tipo_carga) {
            }
            $id_conductor_devolucion = $servicios["id_conductor_d"];
            $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor_devolucion'");
            $consultar_conductor->execute();
            $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

            foreach ($consultar_conductor as $conductor) {
                $nombres_conductor_devolucion = $conductor["nombres_conductor"] . ' ' . $conductor["apellidos_conductor"];
                $identificacion_c_devolucion = $conductor["numero_identificacion"];
                $placa_c_devolucion = $conductor["placa_vehiculo"];
            }
            // inicio serivicico
            $hojaActiva->setCellValueByColumnAndRow(1, $numeroDeFila, $servicios["costo_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(2, $numeroDeFila, $servicios["id"]);
            $hojaActiva->setCellValueByColumnAndRow(3, $numeroDeFila, $cliente["razon_social"]);
            $hojaActiva->setCellValueByColumnAndRow(4, $numeroDeFila, $sub_cliente["nombre_sub_cliente"]);
            $hojaActiva->setCellValueByColumnAndRow(5, $numeroDeFila, $servicios["dons"]);
            $hojaActiva->setCellValueByColumnAndRow(6, $numeroDeFila, $tipo_servicio["nombre_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(7, $numeroDeFila, $contenedor_carga_suelta);
            $hojaActiva->setCellValueByColumnAndRow(8, $numeroDeFila, $tipo_carga["nombre_carga"]);

            if ($id_tipo_carga == 5) {
                $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["cantidad_bpallets"]);
                $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["cantidad_bultos"]);
                $hojaActiva->setCellValueByColumnAndRow(11, $numeroDeFila, $servicios["peso_aprox"]);
                $hojaActiva->setCellValueByColumnAndRow(12, $numeroDeFila, $servicios["tipo_contenedor"]);
                $hojaActiva->setCellValueByColumnAndRow(13, $numeroDeFila, $servicios["tamaño_contenedor"]);
                $hojaActiva->setCellValueByColumnAndRow(14, $numeroDeFila, $servicios["contenedor"]);
                $hojaActiva->setCellValueByColumnAndRow(15, $numeroDeFila, $servicios["fecha_hora_incio_descargue"]);
                $hojaActiva->setCellValueByColumnAndRow(16, $numeroDeFila, $servicios["fecha_hora_terminacion_descargue"]);
                $hojaActiva->setCellValueByColumnAndRow(17, $numeroDeFila, $servicios["nombre_conductor"]);
                $hojaActiva->setCellValueByColumnAndRow(18, $numeroDeFila, $servicios["identificacion_conductor"]);
                $hojaActiva->setCellValueByColumnAndRow(19, $numeroDeFila, $servicios["numero_placa_vehiculo"]);
                $hojaActiva->setCellValueByColumnAndRow(20, $numeroDeFila, $servicios["dia_almacenaje_libre"]);
                $hojaActiva->setCellValueByColumnAndRow(21, $numeroDeFila, $servicios["observacion1"]);
            } else {
                $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["cantidad_bpallets"]);
                $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["peso_aprox"]);
                $hojaActiva->setCellValueByColumnAndRow(11, $numeroDeFila, $servicios["tipo_contenedor"]);
                $hojaActiva->setCellValueByColumnAndRow(12, $numeroDeFila, $servicios["tamaño_contenedor"]);
                $hojaActiva->setCellValueByColumnAndRow(13, $numeroDeFila, $servicios["contenedor"]);
                $hojaActiva->setCellValueByColumnAndRow(15, $numeroDeFila, $servicios["fecha_hora_incio_descargue"]);
                $hojaActiva->setCellValueByColumnAndRow(16, $numeroDeFila, $servicios["fecha_hora_terminacion_descargue"]);
                $hojaActiva->setCellValueByColumnAndRow(17, $numeroDeFila, $servicios["nombre_conductor"]);
                $hojaActiva->setCellValueByColumnAndRow(18, $numeroDeFila, $servicios["identificacion_conductor"]);
                $hojaActiva->setCellValueByColumnAndRow(19, $numeroDeFila, $servicios["numero_placa_vehiculo"]);
                $hojaActiva->setCellValueByColumnAndRow(20, $numeroDeFila, $servicios["dia_almacenaje_libre"]);
                $hojaActiva->setCellValueByColumnAndRow(21, $numeroDeFila, $servicios["observacion1"]);
            }

            //mas informacion

            // ENTREGA DE MERCANCIA //////////////////////////////////////////////////////////////////////////////
            $hoja3 = $spreadsheet->createSheet();
            // Generale el nombre de hoja para que confirmes que tienes la hoja creada
            $hoja3->setTitle('ENTREGA DE MERCANCÍA');
            /////////////////////////////////////////////////////
            $hoja3->getColumnDimension('A')->setWidth(10);
            $hoja3->getColumnDimension('B')->setWidth(22);
            $hoja3->getColumnDimension('C')->setWidth(22);
            $hoja3->getColumnDimension('D')->setWidth(22);
            $hoja3->getColumnDimension('E')->setWidth(22);
            $hoja3->getColumnDimension('F')->setWidth(22);
            $hoja3->getColumnDimension('G')->setWidth(22);
            $hoja3->getColumnDimension('H')->setWidth(28);
            $hoja3->getColumnDimension('I')->setWidth(28);
            $hoja3->getColumnDimension('J')->setWidth(22);
            $hoja3->getColumnDimension('K')->setWidth(22);
            $hoja3->getColumnDimension('L')->setWidth(22);


            $hoja3->getStyle('A1:e1')->applyFromArray($styleArray2);
            $hoja3->getStyle('A2:e2')->applyFromArray($styleArray2);
            $hoja3->getStyle('A3:l3')->applyFromArray($styleArray3);

            // $hojaActiva->getStyle('A8:F39')->applyFromArray($styleArray1);

            ///////
            $hoja3->mergeCells('D1:e1');
            $hoja3->mergeCells('A2:e2');
            //$hojaActiva->mergeCells('G4:Z4');
            $hoja3->setCellValue('C1', 'ENTREGA DE MERCANCÍA')->getStyle('C1')->getAlignment()->setHorizontal('center')->setVertical('center');
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////// encabezado 
            $hoja3->setCellValue('A2', 'VERSIÓN: 2           CODIGO: MI-TR-FO' . $id_servicio_registrado . '           FECHA: ' . $fecha_generado . '           PAGINA 3 DE 3')->getStyle('A2')->getAlignment()->setHorizontal('center');

            $hoja3->setCellValue('A3', 'Id servicio')->getStyle('A3')->getAlignment()->setHorizontal('center')->setVertical('center');
            $hoja3->setCellValue('B3', 'Fecha y hora')->getStyle('B3')->getAlignment()->setHorizontal('center')->setVertical('center');
            $hoja3->setCellValue('C3', 'Transportadora')->getStyle('C3')->getAlignment()->setHorizontal('center')->setVertical('center');
            $hoja3->setCellValue('D3', 'Conductor')->getStyle('D3')->getAlignment()->setHorizontal('center')->setVertical('center');
            $hoja3->setCellValue('E3', 'Identificación conductor')->getStyle('E3')->getAlignment()->setHorizontal('center')->setVertical('center');
            $hoja3->setCellValue('F3', 'Placa vehículo')->getStyle('F3')->getAlignment()->setHorizontal('center')->setVertical('center');
            $hoja3->setCellValue('G3', 'Teléfono conductor')->getStyle('G3')->getAlignment()->setHorizontal('center')->setVertical('center');

            if ($id_tipo_carga == 5) {
                $hoja3->setCellValue('H3', 'Cantidad de pallets despachados')->getStyle('H3')->getAlignment()->setHorizontal('center')->setVertical('center');
                $hoja3->setCellValue('I3', 'Cantidad de bultos despachados')->getStyle('I3')->getAlignment()->setHorizontal('center')->setVertical('center');
                $hoja3->setCellValue('J3', 'Peso aprox.')->getStyle('J3')->getAlignment()->setHorizontal('center')->setVertical('center');
                $hoja3->setCellValue('K3', 'Destino')->getStyle('K3')->getAlignment()->setHorizontal('center')->setVertical('center');
                $hoja3->setCellValue('L3', 'Observación')->getStyle('L3')->getAlignment()->setHorizontal('center')->setVertical('center');
            } else {
                $hoja3->setCellValue('H3', 'Cantidad de ' . $nombre_carga . ' despachados')->getStyle('H3')->getAlignment()->setHorizontal('center')->setVertical('center');
                $hoja3->setCellValue('I3', 'Peso aprox.')->getStyle('J3')->getAlignment()->setHorizontal('center')->setVertical('center');
                $hoja3->setCellValue('J3', 'Destino')->getStyle('K3')->getAlignment()->setHorizontal('center')->setVertical('center');
                $hoja3->setCellValue('K3', 'Observación')->getStyle('L3')->getAlignment()->setHorizontal('center')->setVertical('center');
            }



            $consultar_entrega_mercancia = $conn->prepare("SELECT * FROM despachos WHERE id_servicio = '$id_servicio_registrado'");
            $consultar_entrega_mercancia->execute();
            $consultar_entrega_mercancia = $consultar_entrega_mercancia->fetchAll(PDO::FETCH_ASSOC);

            $numerosFilaMercancia = 4;
            foreach ($consultar_entrega_mercancia as $entrega_mercancia) {
                $hoja3->setCellValueByColumnAndRow(1, $numerosFilaMercancia, $id_servicio_registrado)->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $hoja3->setCellValueByColumnAndRow(2, $numerosFilaMercancia, $entrega_mercancia["fecha_hora_despacho"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $hoja3->setCellValueByColumnAndRow(3, $numerosFilaMercancia, $entrega_mercancia["nombre_transportadora"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(4, $numerosFilaMercancia, $entrega_mercancia["nombres_conductor"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(5, $numerosFilaMercancia, $entrega_mercancia["numero_identificacion"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(6, $numerosFilaMercancia, $entrega_mercancia["placa_vehiculo"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;
                $hoja3->setCellValueByColumnAndRow(7, $numerosFilaMercancia, $entrega_mercancia["telefono"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);;

                if ($id_tipo_carga == 5) {
                    $hoja3->setCellValueByColumnAndRow(8, $numerosFilaMercancia, $entrega_mercancia["cantidad_pallets_despacho"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $hoja3->setCellValueByColumnAndRow(9, $numerosFilaMercancia, $entrega_mercancia["cantidad_bultos_despachados"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $hoja3->setCellValueByColumnAndRow(10, $numerosFilaMercancia, $entrega_mercancia["peso_aprox"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $hoja3->setCellValueByColumnAndRow(11, $numerosFilaMercancia, $entrega_mercancia["destino"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $hoja3->setCellValueByColumnAndRow(12, $numerosFilaMercancia, $entrega_mercancia["observacion"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                } else {
                    $hoja3->setCellValueByColumnAndRow(8, $numerosFilaMercancia, $entrega_mercancia["cantidad_pallets_despacho"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $hoja3->setCellValueByColumnAndRow(9, $numerosFilaMercancia, $entrega_mercancia["peso_aprox"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $hoja3->setCellValueByColumnAndRow(10, $numerosFilaMercancia, $entrega_mercancia["destino"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $hoja3->setCellValueByColumnAndRow(11, $numerosFilaMercancia, $entrega_mercancia["observacion"])->getStyle($numerosFilaMercancia)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                }


                $numerosFilaMercancia++;
            }

            $numeroDeFila++;
        }
    } else if ($id_tipo_servicios == 10) { // Suministro de Montacargas de 3 Toneladas
        $encabezado = [
            "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Do/Ns", "Tipo servicio", "Tipo contenedor", "Tamaño de contenedor", "Contenedor", "Lugar de operación",
            "Fecha de operación", "Horas de operación", "Observaciones"
        ];
        $hojaActiva->fromArray($encabezado, null, 'A4');
        ///// servicios 1
        $numeroDeFila = 5;
        foreach ($consultar_servicios as $servicios) {
            $id_servicio_registrado = $servicios["id"];
            $id_cliente = $servicios["id_cliente"];
            $id_sub_cliente = $servicios["id_sub_cliente"];
            $id_tipo_servicios = $servicios["tipo_servicio"];
            $id_tipo_carga = $servicios["tipo_carga"];
            if ($servicios["aplica_almacenaje"] == 0) {
                $almacenaje = "No";
            } else {
                $almacenaje = "Si";
            }
            $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_cliente as $cliente) {
            }
            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
            $consultar_sub_cliente->execute();
            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_sub_cliente as $sub_cliente) {
            }

            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
            $consultar_tipo_servicio->execute();
            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_servicio as $tipo_servicio) {
            }
            $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
            $consultar_tipo_carga->execute();
            $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_carga as $tipo_carga) {
            }
            $id_conductor_devolucion = $servicios["id_conductor_d"];
            $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor_devolucion'");
            $consultar_conductor->execute();
            $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

            foreach ($consultar_conductor as $conductor) {
                $nombres_conductor_devolucion = $conductor["nombres_conductor"] . ' ' . $conductor["apellidos_conductor"];
                $identificacion_c_devolucion = $conductor["numero_identificacion"];
                $placa_c_devolucion = $conductor["placa_vehiculo"];
            }
            // inicio serivicico  
            $hojaActiva->setCellValueByColumnAndRow(1, $numeroDeFila, $servicios["costo_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(2, $numeroDeFila, $servicios["id"]);
            $hojaActiva->setCellValueByColumnAndRow(3, $numeroDeFila, $cliente["razon_social"]);
            $hojaActiva->setCellValueByColumnAndRow(4, $numeroDeFila, $sub_cliente["nombre_sub_cliente"]);
            $hojaActiva->setCellValueByColumnAndRow(5, $numeroDeFila, $servicios["dons"]);
            $hojaActiva->setCellValueByColumnAndRow(6, $numeroDeFila, $tipo_servicio["nombre_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(7, $numeroDeFila, $servicios["tipo_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(8, $numeroDeFila, $servicios["tamaño_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["lugar_operacion"]);
            $hojaActiva->setCellValueByColumnAndRow(11, $numeroDeFila, $servicios["fecha_operacion"]);
            $hojaActiva->setCellValueByColumnAndRow(12, $numeroDeFila, $servicios["hora_operacion"]);
            $hojaActiva->setCellValueByColumnAndRow(13, $numeroDeFila, $servicios["observacion1"]);
            $numeroDeFila++;
        }
    } else if ($id_tipo_servicios == 11) { // Suministro de Montacargas de 7 Toneladas
        $encabezado = [
            "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Do/Ns", "Tipo servicio", "Tipo contenedor", "Tamaño de contenedor", "Contenedor", "Lugar de operación",
            "Fecha de operación", "Horas de operación", "Observaciones"
        ];
        $hojaActiva->fromArray($encabezado, null, 'A4');
        ///// servicios 1
        $numeroDeFila = 5;
        foreach ($consultar_servicios as $servicios) {
            $id_servicio_registrado = $servicios["id"];
            $id_cliente = $servicios["id_cliente"];
            $id_sub_cliente = $servicios["id_sub_cliente"];
            $id_tipo_servicios = $servicios["tipo_servicio"];
            $id_tipo_carga = $servicios["tipo_carga"];
            if ($servicios["aplica_almacenaje"] == 0) {
                $almacenaje = "No";
            } else {
                $almacenaje = "Si";
            }
            $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_cliente as $cliente) {
            }
            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
            $consultar_sub_cliente->execute();
            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_sub_cliente as $sub_cliente) {
            }

            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
            $consultar_tipo_servicio->execute();
            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_servicio as $tipo_servicio) {
            }
            $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
            $consultar_tipo_carga->execute();
            $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_carga as $tipo_carga) {
            }
            $id_conductor_devolucion = $servicios["id_conductor_d"];
            $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor_devolucion'");
            $consultar_conductor->execute();
            $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

            foreach ($consultar_conductor as $conductor) {
                $nombres_conductor_devolucion = $conductor["nombres_conductor"] . ' ' . $conductor["apellidos_conductor"];
                $identificacion_c_devolucion = $conductor["numero_identificacion"];
                $placa_c_devolucion = $conductor["placa_vehiculo"];
            }
            // inicio serivicico  ç
            $hojaActiva->setCellValueByColumnAndRow(1, $numeroDeFila, $servicios["costo_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(2, $numeroDeFila, $servicios["id"]);
            $hojaActiva->setCellValueByColumnAndRow(3, $numeroDeFila, $cliente["razon_social"]);
            $hojaActiva->setCellValueByColumnAndRow(4, $numeroDeFila, $sub_cliente["nombre_sub_cliente"]);
            $hojaActiva->setCellValueByColumnAndRow(5, $numeroDeFila, $servicios["dons"]);
            $hojaActiva->setCellValueByColumnAndRow(6, $numeroDeFila, $tipo_servicio["nombre_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(7, $numeroDeFila, $servicios["tipo_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(8, $numeroDeFila, $servicios["tamaño_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["lugar_operacion"]);
            $hojaActiva->setCellValueByColumnAndRow(11, $numeroDeFila, $servicios["fecha_operacion"]);
            $hojaActiva->setCellValueByColumnAndRow(12, $numeroDeFila, $servicios["hora_operacion"]);
            $hojaActiva->setCellValueByColumnAndRow(13, $numeroDeFila, $servicios["observacion1"]);
            $numeroDeFila++;
        }
    } else if ($id_tipo_servicios == 12 || $id_tipo_servicios == 13) { // Suministro de Estibas Certificadas y no Certificadas
        $encabezado = [
            "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Do/Ns", "Tipo servicio", "Tipo contenedor", "Tamaño de contenedor", "Contenedor", "Lugar entrega Estibas",
            "Fecha de entrega Estibas", "Cantidad entrega de Estibas", "¿Se patelizó?", "¿Se llenó el contenedor?", "Horas de montacarga", "Observaciones"
        ];
        $hojaActiva->fromArray($encabezado, null, 'A4');
        ///// servicios 1
        $numeroDeFila = 5;
        foreach ($consultar_servicios as $servicios) {
            $id_servicio_registrado = $servicios["id"];
            $id_cliente = $servicios["id_cliente"];
            $id_sub_cliente = $servicios["id_sub_cliente"];
            $id_tipo_servicios = $servicios["tipo_servicio"];
            $id_tipo_carga = $servicios["tipo_carga"];
            if ($servicios["aplica_almacenaje"] == 0) {
                $almacenaje = "No";
            } else {
                $almacenaje = "Si";
            }
            $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_cliente as $cliente) {
            }
            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
            $consultar_sub_cliente->execute();
            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_sub_cliente as $sub_cliente) {
            }

            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
            $consultar_tipo_servicio->execute();
            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_servicio as $tipo_servicio) {
            }
            $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
            $consultar_tipo_carga->execute();
            $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_carga as $tipo_carga) {
            }
            $id_conductor_devolucion = $servicios["id_conductor_d"];
            $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor_devolucion'");
            $consultar_conductor->execute();
            $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

            foreach ($consultar_conductor as $conductor) {
                $nombres_conductor_devolucion = $conductor["nombres_conductor"] . ' ' . $conductor["apellidos_conductor"];
                $identificacion_c_devolucion = $conductor["numero_identificacion"];
                $placa_c_devolucion = $conductor["placa_vehiculo"];
            }
            // inicio serivicico  
            $hojaActiva->setCellValueByColumnAndRow(1, $numeroDeFila, $servicios["costo_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(2, $numeroDeFila, $servicios["id"]);
            $hojaActiva->setCellValueByColumnAndRow(3, $numeroDeFila, $cliente["razon_social"]);
            $hojaActiva->setCellValueByColumnAndRow(4, $numeroDeFila, $sub_cliente["nombre_sub_cliente"]);
            $hojaActiva->setCellValueByColumnAndRow(5, $numeroDeFila, $servicios["dons"]);
            $hojaActiva->setCellValueByColumnAndRow(6, $numeroDeFila, $tipo_servicio["nombre_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(7, $numeroDeFila, $servicios["tipo_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(8, $numeroDeFila, $servicios["tamaño_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["lugar_entrega_estiba"]);
            $hojaActiva->setCellValueByColumnAndRow(11, $numeroDeFila, $servicios["fecha_entrega_estiba"]);
            $hojaActiva->setCellValueByColumnAndRow(12, $numeroDeFila, $servicios["cantidad_estiba_entregada"]);
            $hojaActiva->setCellValueByColumnAndRow(13, $numeroDeFila, $servicios["patelizo"]);
            $hojaActiva->setCellValueByColumnAndRow(14, $numeroDeFila, $servicios["lleno_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(15, $numeroDeFila, $servicios["hora_montacarga"]);
            $hojaActiva->setCellValueByColumnAndRow(16, $numeroDeFila, $servicios["observacion1"]);
            $numeroDeFila++;
        }
    } else if ($id_tipo_servicios == 14 || $id_tipo_servicios == 15) { // Suministro de cadenas con sus monas y Pencas con sus raches
        $encabezado = [
            "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Do/Ns", "Tipo servicio", "Contenedor", "Lugar entrega",
            "Fecha de entrega", "Cantidad de entrega",  "¿Se llenó el contenedor?", "Horas de montacarga", "Observaciones"
        ];
        $hojaActiva->fromArray($encabezado, null, 'A4');
        ///// servicios 1
        $numeroDeFila = 5;
        foreach ($consultar_servicios as $servicios) {
            $id_servicio_registrado = $servicios["id"];
            $id_cliente = $servicios["id_cliente"];
            $id_sub_cliente = $servicios["id_sub_cliente"];
            $id_tipo_servicios = $servicios["tipo_servicio"];
            $id_tipo_carga = $servicios["tipo_carga"];
            if ($servicios["aplica_almacenaje"] == 0) {
                $almacenaje = "No";
            } else {
                $almacenaje = "Si";
            }
            $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_cliente as $cliente) {
            }
            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
            $consultar_sub_cliente->execute();
            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_sub_cliente as $sub_cliente) {
            }

            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
            $consultar_tipo_servicio->execute();
            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_servicio as $tipo_servicio) {
            }
            $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
            $consultar_tipo_carga->execute();
            $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_carga as $tipo_carga) {
            }
            $id_conductor_devolucion = $servicios["id_conductor_d"];
            $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor_devolucion'");
            $consultar_conductor->execute();
            $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

            foreach ($consultar_conductor as $conductor) {
                $nombres_conductor_devolucion = $conductor["nombres_conductor"] . ' ' . $conductor["apellidos_conductor"];
                $identificacion_c_devolucion = $conductor["numero_identificacion"];
                $placa_c_devolucion = $conductor["placa_vehiculo"];
            }
            // inicio serivicico  
            $hojaActiva->setCellValueByColumnAndRow(1, $numeroDeFila, $servicios["costo_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(2, $numeroDeFila, $servicios["id"]);
            $hojaActiva->setCellValueByColumnAndRow(3, $numeroDeFila, $cliente["razon_social"]);
            $hojaActiva->setCellValueByColumnAndRow(4, $numeroDeFila, $sub_cliente["nombre_sub_cliente"]);
            $hojaActiva->setCellValueByColumnAndRow(5, $numeroDeFila, $servicios["dons"]);
            $hojaActiva->setCellValueByColumnAndRow(6, $numeroDeFila, $tipo_servicio["nombre_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(7, $numeroDeFila, $servicios["contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(8, $numeroDeFila, $servicios["lugar_entrega_estiba"]);
            $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["fecha_entrega_estiba"]);
            $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["cantidad_estiba_entregada"]);
            $hojaActiva->setCellValueByColumnAndRow(11, $numeroDeFila, $servicios["lleno_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(12, $numeroDeFila, $servicios["hora_montacarga"]);
            $hojaActiva->setCellValueByColumnAndRow(13, $numeroDeFila, $servicios["observacion1"]);
            $numeroDeFila++;
        }
    } else if ($id_tipo_servicios == 16 || $id_tipo_servicios == 17) { // Cargue y descargue de Mercancía con Reach Estaker

        $encabezado = [
            "Costo operación", "Id servicio", "Cliente", "Sub cliente", "Do/Ns", "Tipo servicio", "Contenedor", "Lugar operación",
            "Horas de operación", "Observaciones"
        ];
        $hojaActiva->fromArray($encabezado, null, 'A4');
        ///// servicios 1
        $numeroDeFila = 5;
        foreach ($consultar_servicios as $servicios) {
            $id_servicio_registrado = $servicios["id"];
            $id_cliente = $servicios["id_cliente"];
            $id_sub_cliente = $servicios["id_sub_cliente"];
            $id_tipo_servicios = $servicios["tipo_servicio"];
            $id_tipo_carga = $servicios["tipo_carga"];
            if ($servicios["aplica_almacenaje"] == 0) {
                $almacenaje = "No";
            } else {
                $almacenaje = "Si";
            }
            $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_cliente as $cliente) {
            }
            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
            $consultar_sub_cliente->execute();
            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_sub_cliente as $sub_cliente) {
            }

            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
            $consultar_tipo_servicio->execute();
            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_servicio as $tipo_servicio) {
            }
            $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
            $consultar_tipo_carga->execute();
            $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_carga as $tipo_carga) {
            }
            $id_conductor_devolucion = $servicios["id_conductor_d"];
            $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor_devolucion'");
            $consultar_conductor->execute();
            $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

            foreach ($consultar_conductor as $conductor) {
                $nombres_conductor_devolucion = $conductor["nombres_conductor"] . ' ' . $conductor["apellidos_conductor"];
                $identificacion_c_devolucion = $conductor["numero_identificacion"];
                $placa_c_devolucion = $conductor["placa_vehiculo"];
            }
            // inicio serivicico  
            $hojaActiva->setCellValueByColumnAndRow(1, $numeroDeFila, $servicios["costo_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(2, $numeroDeFila, $servicios["id"]);
            $hojaActiva->setCellValueByColumnAndRow(3, $numeroDeFila, $cliente["razon_social"]);
            $hojaActiva->setCellValueByColumnAndRow(4, $numeroDeFila, $sub_cliente["nombre_sub_cliente"]);
            $hojaActiva->setCellValueByColumnAndRow(5, $numeroDeFila, $servicios["dons"]);
            $hojaActiva->setCellValueByColumnAndRow(6, $numeroDeFila, $tipo_servicio["nombre_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(7, $numeroDeFila, $servicios["contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(8, $numeroDeFila, $servicios["lugar_operacion"]);
            $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["hora_operacion"]);
            $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["observacion1"]);
            $numeroDeFila++;
        }
    }


    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $nombre_archivo . '.xlsx"');
    header('Cache-Control: max-age=0');

    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
}