<?php

require "../../vendor/autoload.php";
date_default_timezone_set('America/Bogota');
include '../../database/database.php';

$id_inspeccion = $_GET["id_inspeccion"];

use PhpOffice\PhpSpreadsheet\Spreadsheet;
//use PhpOffice\PhpSpreadsheet\writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;

$fecha_generado = date("Y-m-d H:i");

$nombre_archivo = 'MI-TIR-FO ' . $id_inspeccion . ' INSPECCION VEHICULO ' . $fecha_generado;

$spreadsheet = new SpreadSheet();
$spreadsheet->getProperties()->setCreator("WORLDSHIPPINGCOMPANYSAS")->setTitle($nombre_archivo);

$spreadsheet->setActiveSheetIndex(0);
$hojaActiva = $spreadsheet->getActiveSheet();

$spreadsheet->getDefaultStyle()->getFont()->setName("Calibri");
$spreadsheet->getDefaultStyle()->getFont()->setSize(10);
$spreadsheet->getActiveSheet()->getSheetView()->setZoomScale(90);
$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(70);
$spreadsheet->getActiveSheet()->getRowDimension('3')->setRowHeight(5);
$spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(5);

$spreadsheet->getActiveSheet()->getRowDimension('41')->setRowHeight(41);
////altura automatica celdas de observacion
$spreadsheet->getActiveSheet()->getRowDimension('8,32')->setRowHeight(-1);
$spreadsheet->getActiveSheet()->getStyle('F')->getAlignment()->setWrapText(true);
////////////////
$hojaActiva->getColumnDimension('A')->setWidth(5);
$hojaActiva->getColumnDimension('B')->setWidth(38);
$hojaActiva->getColumnDimension('C')->setWidth(5);
$hojaActiva->getColumnDimension('D')->setWidth(5);
$hojaActiva->getColumnDimension('E')->setWidth(5);
$hojaActiva->getColumnDimension('F')->setWidth(40);

//bordes

$styleArray1 = array(
    'borders' => array(
        'allBorders' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => array('argb' => '0000'),
        ),
    ),

);
$styleArray2 = array(
    'borders' => array(
        'outline' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => array('argb' => '0000'),
        ),
    ),
    'font' => array(
        'bold' => true
    )

);
$styleArray3 = array(
    'borders' => array(
        'allBorders' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => array('argb' => '0000'),
        ),
    ),
    'font' => array(
        'bold' => true
    )

);
$style_titles = array(
    'font' => array(
        'bold' => true
    )
);
$hojaActiva->getStyle('A1:F1')->applyFromArray($styleArray2);
$hojaActiva->getStyle('A2:F2')->applyFromArray($styleArray2);

$hojaActiva->getStyle('A7:f7')->applyFromArray($styleArray3);
$hojaActiva->getStyle('A8:F39')->applyFromArray($styleArray1);


$spreadsheet->getActiveSheet()->getStyle('A1:F1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFFF');


///////

$hojaActiva->mergeCells('D1:f1');
$hojaActiva->mergeCells('A2:f2');
$hojaActiva->mergeCells('A3:f3');
//$hojaActiva->mergeCells('A4:f4');

//consultas
$consultar_inspeccion = $conn->prepare("SELECT * FROM control_inspeccion_vehiculo WHERE id = '$id_inspeccion'");
$consultar_inspeccion->execute();
$consultar_inspeccion = $consultar_inspeccion->fetchAll(PDO::FETCH_ASSOC);

$consultar_control_inspeccion_vehiculo_relacion = $conn->prepare("SELECT * FROM control_inspeccion_vehiculo_relacion WHERE id_inspeccion = '$id_inspeccion'");
$consultar_control_inspeccion_vehiculo_relacion->execute();
$consultar_control_inspeccion_vehiculo_relacion = $consultar_control_inspeccion_vehiculo_relacion->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_inspeccion as $value) {
    $fecha_creacion = $value["fecha_creacion"];
    $nombre_conductor = $value["conductor"];
    $identificacion = $value["identificacion_conductor"];
    $placa_vehiculo = $value["placa_vehiculo"];

    $nombre_empleado = $value["nombre_empleado"];
    $identificacion_empleado = $value["identificacion_empleado"];
}
$separar_fecha_hora = (explode(" ", $fecha_creacion));
$fecha_inspeccion = "Fecha: " . $separar_fecha_hora[0];
$hora_inspeccion = 'Hora: ' . $separar_fecha_hora[1];

//consultas conductor

$consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE numero_identificacion = '$identificacion'");
$consultar_conductor->execute();
$consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_conductor as $conductor) {
    $id_conductor = $conductor["id"];
    $foto_firma_conductor = $conductor["firma_digital"];
}
//consultas empleado

$consultar_empleado = $conn->prepare("SELECT * FROM empleados WHERE numero_identificacion = '$identificacion_empleado'");
$consultar_empleado->execute();
$consultar_empleado = $consultar_empleado->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_empleado as $empleado) {
    $id_empleado = $empleado["id"];
    $foto_firma_empleado = $empleado["firma_digital"];
}

///// logo titulo

$hojaActiva->setCellValue('D1', 'CHECK LIST DE INSPECCIÓN DE VEHICULOS')->getStyle('D1')->getAlignment()->setHorizontal('center');
$hojaActiva->setCellValue('D1', 'CHECK LIST DE INSPECCIÓN DE VEHICULOS')->getStyle('D1')->getAlignment()->setVertical('center');

$logo = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
$logo->setName('Logo');
$logo->setDescription('Logo');
$logo->setPath('../../assets/images/logo.png'); // put your path and image here
$logo->setCoordinates('B1');
$logo->setOffsetX(80);
$logo->setOffsetY(8);
$logo->setRotation(0);
$logo->setWidthAndHeight(70, 100);
/*$logo->getShadow()->setVisible(true);
$logo->getShadow()->setDirection(45);*/
$logo->setWorksheet($spreadsheet->getActiveSheet());

///////
$hojaActiva->setCellValue('A2', 'VERSIÓN: 2           CODIGO: MI-TR-FO' . $id_inspeccion . '           FECHA: ' . $fecha_generado . '           PAGINA 1 DE 1')->getStyle('A2')->getAlignment()->setHorizontal('center');
////// 
$hojaActiva->setCellValue('A4', 'Conductor: ' . $nombre_conductor . '')->getStyle('A2')->getAlignment()->setHorizontal('center');
$hojaActiva->setCellValue('C4', 'Coord/aux: ' . $nombre_empleado . '')->getStyle('A2')->getAlignment()->setHorizontal('center');

//////////
$hojaActiva->setCellValue('A5', $fecha_inspeccion);
$hojaActiva->setCellValue('C5', $hora_inspeccion);
$hojaActiva->setCellValue('F5', 'Placa: ' . $placa_vehiculo)->getStyle('F4')->getAlignment()->setHorizontal('center');
////////
$hojaActiva->setCellValue('A7', 'N°');
$hojaActiva->setCellValue('B7', 'CONCEPTO');
$hojaActiva->setCellValue('C7', 'SI');
$hojaActiva->setCellValue('D7', 'NO');
$hojaActiva->setCellValue('E7', 'NA');
$hojaActiva->setCellValue('F7', 'OBSERVACIONES');
//$hojaActiva->setCellValue('C5', 'MARJO')->setCellValue('D1', 'CDP');

$i = 1;
$j = 8;
foreach ($consultar_control_inspeccion_vehiculo_relacion as $inspeccion_relacion) {
    $hojaActiva->setCellValue('A' . $j, $i);
    $hojaActiva->setCellValue('B' . $j, $inspeccion_relacion["concepto"]);
    $var1 = "Si";
    $var3 = "No";
    $var2 = $inspeccion_relacion["seleccion"];
    if (strcmp($var1, $var2) == 0) {
        $hojaActiva->setCellValue('C' . $j, 'X');
    } else if (strcmp($var3, $var2) == 0) {
        $hojaActiva->setCellValue('D' . $j, 'X');
    } else {
        $hojaActiva->setCellValue('E' . $j, 'X');
    }

    $hojaActiva->setCellValue('F' . $j, $inspeccion_relacion["observacion"]);

    $i++;
    $j++;
}

/////// firmas footer

/* foto*/
$logo = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
$logo->setName('firma_conductor');
$logo->setDescription('firma_conductor');
$logo->setPath('../../foto_firma_conductores/' . $id_conductor . '/' . $foto_firma_conductor); // put your path and image here
$logo->setCoordinates('B41');
$logo->setOffsetX(80);
$logo->setOffsetY(8);
$logo->setRotation(0);
$logo->setWidthAndHeight(70, 100);
/*$logo->getShadow()->setVisible(true);
$logo->getShadow()->setDirection(45);*/
$logo->setWorksheet($spreadsheet->getActiveSheet());

$logo = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
$logo->setName('firma_conductor');
$logo->setDescription('firma_conductor');
$logo->setPath('../../foto_firma_empleados/' . $id_empleado . '/' . $foto_firma_empleado); // put your path and image here
$logo->setCoordinates('F41');
$logo->setOffsetX(80);
$logo->setOffsetY(8);
$logo->setRotation(0);
$logo->setWidthAndHeight(70, 100);
/*$logo->getShadow()->setVisible(true);
$logo->getShadow()->setDirection(45);*/
$logo->setWorksheet($spreadsheet->getActiveSheet());

$hojaActiva->setCellValue('B41', '________________________________')->getStyle('B41')->getAlignment()->setHorizontal('center');
$hojaActiva->setCellValue('F41', '________________________________')->getStyle('F41')->getAlignment()->setHorizontal('center');
$hojaActiva->setCellValue('B42', 'FIRMA CONDUCTOR')->getStyle('B42')->getAlignment()->setHorizontal('center');
$hojaActiva->setCellValue('F42', 'FIRMA COORD/AUX DE TRANSPORTE')->getStyle('F42')->getAlignment()->setHorizontal('center');
$hojaActiva->setCellValue('B43', 'Quien realiza la inspección')->getStyle('B43')->getAlignment()->setHorizontal('center');
$hojaActiva->setCellValue('F43', 'Quien verifica la inspección')->getStyle('F43')->getAlignment()->setHorizontal('center');


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $nombre_archivo . '.xlsx"');
header('Cache-Control: max-age=0');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');

/*
$write = new Xlsx($spreadsheet);
$write->save("mi excel.xlsx");*/