<?php
include '../../database/database.php';
$id_conductor = $_GET["id_conductor"];


$consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor'");
$consultar_conductor->execute();
$consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_identificacion = $conn->prepare("SELECT * FROM tipo_id");
$consultar_tipo_identificacion->execute();
$consultar_tipo_identificacion = $consultar_tipo_identificacion->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_rutas = $conn->prepare("SELECT * FROM rutas");
$consultar_tipo_rutas->execute();
$consultar_tipo_rutas = $consultar_tipo_rutas->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_conductor as $conductor) {
}
$consultar_tipo_vehiculo = $conn->prepare("SELECT * FROM tipo_vehiculo");
$consultar_tipo_vehiculo->execute();
$consultar_tipo_vehiculo = $consultar_tipo_vehiculo->fetchAll(PDO::FETCH_ASSOC);

$consultar_montacargas = $conn->prepare("SELECT * FROM vehiculos_montacarga WHERE estado = 1 ");
$consultar_montacargas->execute();
$consultar_montacargas = $consultar_montacargas->fetchAll(PDO::FETCH_ASSOC);


$consultar_v_transporte = $conn->prepare("SELECT * FROM vehiculos_transporte WHERE estado = 1 ");
$consultar_v_transporte->execute();
$consultar_v_transporte = $consultar_v_transporte->fetchAll(PDO::FETCH_ASSOC);

?>

<form id="form_editar_conductor" enctype="multipart/form-data">
    <input type="hidden" name="id_conductor" value="<?php echo $id_conductor ?>">
    <div class="row">
        <div class="col-12 col-sm-6">
            <label>Nombres: <span class="tx-danger">*</span></label>
            <input type="text" name="nombres" class="form-control" placeholder="Nombres" required=""
                data-parsley-id="11" value="<?php echo $conductor["nombres_conductor"] ?>">
        </div>
        <div class="col-12 col-sm-6">

            <label>Apellidos: <span class="tx-danger">*</span></label>
            <input type="text" name="apellidos" class="form-control" placeholder="Apellidos" required=""
                data-parsley-id="11" value="<?php echo $conductor["apellidos_conductor"] ?>">
        </div>
        <div class="col-12 col-sm-6">
            <br>
            <label>Tipo identifición: <span class="tx-danger">*</span></label>
            <select name="tipo_id" id="tipo_id" class="form-control">
                <?php
                $id_tipo = $conductor["tipo_identificacion"];
                $consultar_rutas = $conn->prepare("SELECT * FROM tipo_id WHERE id = '$id_tipo'");
                $consultar_rutas->execute();
                $consultar_rutas = $consultar_rutas->fetchAll(PDO::FETCH_ASSOC);
                foreach ($consultar_rutas as $tipo_ids) { ?>
                <option value="<?php echo $tipo_ids["id"] ?>" selected><?php echo $tipo_ids["nombre"] ?>
                </option>
                <?php } ?>

                <?php foreach ($consultar_tipo_identificacion as $tipo_id) { ?>
                <option value="<?php echo $tipo_id["id"] ?>"> <?php echo $tipo_id["nombre"] ?>
                </option>
                <?php } ?>

            </select>
        </div>
        <div class="col-12 col-sm-6">
            <br>
            <label>Número identificación: <span class="tx-danger">*</span></label>
            <input type="tel" name="numero_identificacion" class="form-control" placeholder="Número identificación"
                required="" data-parsley-id="11" value="<?php echo $conductor["numero_identificacion"] ?>">
        </div>
        <div class="col-12 col-sm-12">
            <br>
            <label for="">Tipo de vehiculo</label>

            <select name="tipo_vehiculo" id="tipo_vehiculos" class="form-control" onchange="seleccionar_t_v()">
                <option value="<?php echo $conductor["tipo_vehiculo"] ?>" selected>
                    <?php echo $conductor["tipo_vehiculo"] ?>
                </option>
                <?php foreach ($consultar_tipo_vehiculo as $tipo) { ?>
                <option value="<?php echo $tipo["nombre"] ?>"> <?php echo $tipo["nombre"] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-12 col-sm-12">
            <br>
            <label>Placa vehículo/nombre montacarga: <span class="tx-danger">*</span></label>
            <select name="placa_vehiculo" id="selector_placa_nombre_editar" class="form-control "></select>

        </div>
        <!-- <div class="col-12 col-sm-12">
            <br>
            <label>Ruta: <span class="tx-danger">*</span></label>
            <select name="ruta" class="form-control" id="cargo">

                <?php
                $id_ruta = $conductor["id_ruta"];
                $consultar_rutas = $conn->prepare("SELECT * FROM rutas WHERE id = '$id_ruta'");
                $consultar_rutas->execute();
                $consultar_rutas = $consultar_rutas->fetchAll(PDO::FETCH_ASSOC);
                foreach ($consultar_rutas as $rutass) { ?>
                <option value="<?php echo $rutass["id"] ?>" selected><?php echo $rutass["nombre_ruta"] ?>
                </option>
                <?php } ?>

                <?php foreach ($consultar_tipo_rutas as $rutas) { ?>
                <option value="<?php echo $rutas["id"] ?>"> <?php echo $rutas["nombre_ruta"] ?>
                </option>
                <?php } ?>

            </select>
        </div>-->

        <div class="col-12 col-sm-12">
            <br>
            <label>Estado: <span class="tx-danger">*</span></label>
            <select name="estado" id="estado" class="form-control ">

                <option value="<?php echo $conductor["estado"] ?>" selected>
                    <?php
                    if ($conductor["estado"] == 0) {
                        echo "Recien creado";
                    } else if ($conductor["estado"] == 1) {
                        echo "Activado";
                    } else if ($conductor["estado"] == 2) {
                        echo "Desactivado";
                    }
                    ?>
                </option>

                <option value="0">Recien creado</option>
                <option value="1">Activar</option>
                <option value="2">Desactivar</option>

            </select>

        </div>
        <div class="col-12 col-sm-12">
            <br>
            <label>Firma digital (png, jpg) (100x150): <span class="tx-danger">*</span></label>
            <div class="input-group">
                <div class="custom-file"> <input type="file" name="foto_firma" class="form-control form-control-sm"
                        id="inputGroupFile04">
                    <label class="" for="inputGroupFile04"></label>
                </div>
                <br>
                <div class="input-group-append">
                    <span class="input-group-btn">
                        <button class="btn btn-custom-primary file-browser" type="button"><i
                                class="fa fa-upload"></i></button>
                    </span>
                </div>
            </div>
            <h6 style="font-size:8px">Firma: <?php echo $conductor["firma_digital"] ?></h6>

        </div>


    </div>
</form>

<script>
seleccionar_t_v();

function seleccionar_t_v() {

    var tipo_de_vehiculo = document.getElementById("tipo_vehiculos").value;

    //alert(tipo_de_vehiculo);

    if (tipo_de_vehiculo === "Transporte") {
        document.getElementById("selector_placa_nombre_editar").innerHTML =
            '<option value="<?php echo $conductor["placa_vehiculo"] ?>" selected><?php echo $conductor["placa_vehiculo"] ?></option><?php foreach ($consultar_v_transporte as $v_transporte) { ?><option value="<?php echo $v_transporte["nombre"]; ?> "> <?php echo $v_transporte["nombre"]; ?> </option> <?php } ?>';

    } else {
        document.getElementById("selector_placa_nombre_editar").innerHTML =
            '<option value="<?php echo $conductor["placa_vehiculo"] ?>" selected><?php echo $conductor["placa_vehiculo"] ?></option><?php foreach ($consultar_montacargas as $montacarga) { ?><option value = "<?php echo $montacarga["nombre"]; ?>"> <?php echo $montacarga["nombre"]; ?> </option> <?php } ?>';
    }

}
</script>