<?php

include '../../database/database.php';
session_start();
$id_servicio = $_SESSION["id_servicio"];

$consultar_servicio = $conn->prepare("SELECT * FROM servicios_control_rutas WHERE id = '$id_servicio'");
$consultar_servicio->execute();
$consultar_servicio = $consultar_servicio->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_servicio as $servicio) {
    $id_tipo_servicio = $servicio["tipo_servicio"];
    $costo_servicios = $servicio["costo_servicio"];
}

?>

<div class="col-12">
    <h6 class="tx-dark tx-13 tx-semibold">ID SERVICIO: #<?php echo $id_servicio ?> - Resumen del servicio</h6>
    <ul class="list-unstyled">
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Cliente:</b>
                <?php $id_cliente = $servicio["id_cliente"];
                $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
                $consultar_cliente->execute();
                $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                foreach ($consultar_cliente as $cliente) {
                    echo $cliente["razon_social"];
                }
                ?>
            </a>
        </li>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Sub cliente:</b>
                <?php $id_cliente = $servicio["id_cliente"];
                $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_cliente WHERE id_cliente = '$id_cliente'");
                $consultar_sub_cliente->execute();
                $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                foreach ($consultar_sub_cliente as $sub_cliente) {
                    echo $sub_cliente["nombre_sub_cliente"];
                }
                ?>
            </a>
        </li>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Do/Ns: </b>
                <?php echo $servicio["dons"] ?>
            </a>
        </li>

        <?php if ($id_tipo_servicio == 3) { ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Número contenedor: </b>
                <?php echo $servicio["contenedor"] ?>
            </a>
        </li>

        <?php
            if ($servicio["lugar_entrega"] == "") {
            ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>
                Acarreo de ´´Puerto de retiro del vacío´´ el “Fecha de retiro del vacío” a “Lugar de entrega del vacío”
            </a>
        </li>
        <?php } else {
            } ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Descripción: </b>
                Acarreo de ´´ Puerto retiro del vacío ´´ el “Fecha de retiro del vacío” a “Lugar de almacenamiento”,
                descargue y cargue del contenedor a piso con montacargas de 7 toneladas, “X” días de almacenamiento
                (Calcular diferencia de estas dos fechas: Fecha de ingreso al patio del naviero - Fecha de ingreso al
                patio de almacenaje) y acarreo a “Lugar de entrega del vacío “ el Fecha de entrega del vacío.
            </a>
        </li>
        <?php } else {
        } ?>
        <li>
            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Costo del servicios: </b>
                <?php if ($costo_servicios != null) {
                    echo '$' . number_format($costo_servicios);
                } else {
                    echo "No se ha definido el costo del servicio.";
                }
                ?>
            </a>


        </li>

    </ul>
</div>