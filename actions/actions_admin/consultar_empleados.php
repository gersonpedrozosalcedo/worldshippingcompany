<?php

include '../../database/database.php';

$consultar_empleados = $conn->prepare("SELECT * FROM empleados ORDER BY id desc");
$consultar_empleados->execute();
$consultar_empleados = $consultar_empleados->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipos_estados = $conn->prepare("SELECT * FROM estados ORDER BY id desc");
$consultar_tipos_estados->execute();
$consultar_tipos_estados = $consultar_tipos_estados->fetchAll(PDO::FETCH_ASSOC);
?>

<div id="eliminando_empleado"></div>

<table id="foo-filtering" class="table table-bordered table-hover toggle-circle" data-page-size="7">
    <thead>
        <tr>
            <th data-toggle="true">Nombres</th>
            <th>Apellidos</th>
            <th data-hide="phone, tablet">Identificación</th>
            <th data-hide="phone, tablet">Email</th>
            <th data-hide="phone, tablet">Teléfono</th>
            <th data-hide="phone, tablet">Cargo</th>
            <th data-hide="phone, tablet">Estado</th>
            <th data-hide="phone, tablet">Acción</th>
        </tr>
    </thead>
    <tbody>
        <tr>

            <?php foreach ($consultar_empleados as $empleado) { ?>
            <td id="responsive"><?php echo $empleado["nombres"] ?></td>
            <td id="responsive"><?php echo $empleado["apellidos"] ?></td>
            <td id="responsive">
                <?php
                    $id_tipo = $empleado["tipo_identificacion"];
                    $consultar_identificacion = $conn->prepare("SELECT codigo FROM tipo_id WHERE id = '$id_tipo'");
                    $consultar_identificacion->execute();
                    $consultar_identificacion = $consultar_identificacion->fetch(PDO::FETCH_OBJ);

                    echo $consultar_identificacion->codigo;
                    echo ' ';
                    echo $empleado["numero_identificacion"];
                    ?>

            </td>
            <td id="responsive"><?php echo $empleado["email"] ?></td>
            <td id="responsive"><?php echo $empleado["telefono"] ?></td>
            <td id="responsive">
                <?php
                    $id_cargo = $empleado["cargo"];
                    $consultar_cargo = $conn->prepare("SELECT nombre_cargo FROM tipo_cargos WHERE id = '$id_cargo'");
                    $consultar_cargo->execute();
                    $consultar_cargo = $consultar_cargo->fetch(PDO::FETCH_OBJ);
                    echo $consultar_cargo->nombre_cargo;
                    ?>
            </td>
            <td id="responsive">
                <?php
                    if ($empleado["estado"] == 0) {
                        echo '<span class="label label-table label-dark ">Recien creado</span>';
                    } else if ($empleado["estado"] == 1) {
                        echo '<span class="label label-table label-success">Activado</span>';
                    } else if ($empleado["estado"] == 2) {
                        echo '<span class="label label-table label-danger">Desactivado</span>';
                    }
                    ?>


            </td>
            <td id="responsive">
                <button type="button" data-toggle="modal" data-target="#editar_empleado" role="button"
                    onclick="editar_empleado(<?php echo $empleado['id'] ?>)"
                    class="btn btn-outline-primary btn-icon mg-r-5"><i data-feather="edit-3" data-toggle="tooltip"
                        data-trigger="hover" data-placement="top" title=""
                        data-original-title="Ver/Editar información sobre el empleado"></i></button>


                <?php
                    if ($empleado["estado"] == 0) {
                        echo ' <button type="button" data-toggle="modal" href="#formulario1" role="button"
                                onclick="cambiar_estado_empleado(' . $empleado["id"] . ',0)"
                class="btn btn-outline-danger btn-icon mg-r-5"><i data-toggle="tooltip" data-trigger="hover"
                    data-placement="top" title="" data-original-title="Eliminar empleado"
                    data-feather="x-circle"></i></button>';
                    } else if ($empleado["estado"] == 1) {
                        echo ' <button type="button" data-toggle="modal" href="#formulario1" role="button"
                    onclick="cambiar_estado_empleado(' . $empleado["id"] . ',2)"
                class="btn btn-outline-warning btn-icon mg-r-5"><i data-feather="user-x" data-toggle="tooltip"
                    data-trigger="hover" data-placement="top" title="" data-original-title="Desactivar empleado"></i>
                </button>';
                    } else if ($empleado["estado"] == 2) {
                        echo ' <button type="button" data-toggle="modal" href="#formulario1" role="button"
                    onclick="cambiar_estado_empleado(' . $empleado["id"] . ',1)"
                    class="btn btn-outline-success btn-icon mg-r-5"><i data-feather="user-check" data-toggle="tooltip" data-trigger="hover"
                        data-placement="top" title="" data-original-title="Activar empleado" ></i></button>';
                    }
                    ?>
                <button type="button" data-toggle="modal" href="#" role="button"
                    onclick="eliminar_empleado(<?php echo $empleado['id'] ?>)"
                    class="btn btn-outline-danger btn-icon mg-r-5"><i data-toggle="tooltip" data-trigger="hover"
                        data-placement="top" title="" data-original-title="Eliminar empleado"
                        data-feather="x-circle"></i></button>
            </td>
        </tr>
        <?php } ?>

    </tbody>
    <tfoot>
        <tr>
            <td colspan="5">
                <div class="ft-right">
                    <ul class="pagination"></ul>
                </div>
            </td>
        </tr>
    </tfoot>
</table>