<?php

include '../../database/database.php';
session_start();

$id_servicio = $_GET["id_servicio"];

$id_cargo = $_SESSION["cargo"];

if ($id_cargo == 1) {
    $disabled_carga = '';
} else if ($id_cargo == 2) {
    $disabled_transporte = '';
}

$consultar_servicios = $conn->prepare("SELECT * FROM servicios_control_rutas  WHERE id = '$id_servicio'");
$consultar_servicios->execute();
$consultar_servicios = $consultar_servicios->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_servicios as $servicios) {
    $id_tipo_servicio = $servicios["tipo_servicio"];
    $tipo_carga = $servicios["tipo_carga"];
}

$consultar_clientes = $conn->prepare("SELECT * FROM clientes  WHERE estado = 1 ORDER BY id DESC");
$consultar_clientes->execute();
$consultar_clientes = $consultar_clientes->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_servicios = $conn->prepare("SELECT * FROM tipo_servicios WHERE estado = 1");
$consultar_tipo_servicios->execute();
$consultar_tipo_servicios = $consultar_tipo_servicios->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_cargas = $conn->prepare("SELECT * FROM tipo_cargas WHERE estado = 1 ORDER BY id DESC");
$consultar_tipo_cargas->execute();
$consultar_tipo_cargas = $consultar_tipo_cargas->fetchAll(PDO::FETCH_ASSOC);

$consultar_conductores = $conn->prepare("SELECT * FROM conductores WHERE estado = 1 ORDER BY id DESC");
$consultar_conductores->execute();
$consultar_conductores = $consultar_conductores->fetchAll(PDO::FETCH_ASSOC);


$tipo_de_carga_servicio = $conn->prepare("SELECT * FROM tipo_cargas WHERE tipo_carga = '$tipo_carga'");
$tipo_de_carga_servicio->execute();
$tipo_de_carga_servicio = $tipo_de_carga_servicio->fetchAll(PDO::FETCH_ASSOC);

foreach ($tipo_de_carga_servicio as $tipos_cargas_servicios) {
    $nombre_carga = $tipos_cargas_servicios["nombre_carga"];
    # code.
}


?>
<link href="../../assets/plugins/select2.css" rel="stylesheet" />
<script src="../../assets/plugins//select2.js"></script>


<style>
@media only screen and (max-width: 768px) {

    .wizard.vertical>.steps {
        display: inline;
        float: left;
        width: 100%;
    }

    .wizard ul,
    .tabcontrol ul {
        list-style: none !important;
        /*  display: flex;*/
        padding: 0;
        margin: 0;
    }

    .wizard.vertical>.content {
        display: inline;
        float: left;
        margin: 0 2.5% 0.5em 2.5%;
        width: 100%;
    }

    #step_modificacion {
        display: flex !important;
    }
}
</style>

<script>
$(document).ready(function() {
    'use strict';

    $('#wizard2_editar_servicios').steps({
        headerTag: 'h3',
        bodyTag: 'section',
        autoFocus: true,
        titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
        onStepChanging: function(event, currentIndex, newIndex) {
            if (currentIndex < newIndex) {
                // Step 1 form validation
                if (currentIndex === 0) {
                    var fname = $('#id_cliente').parsley();
                    var lname = $('#id_sub_clientes').parsley();

                    if (fname.isValid() && lname.isValid()) {
                        return true;
                    } else {
                        fname.validate();
                        lname.validate();
                    }
                }

                // Step 2 form validation
                if (currentIndex === 1) {
                    var email = $('#fecha_r_p_p_o').parsley();
                    if (email.isValid()) {
                        return true;
                    } else {
                        email.validate();
                    }
                }
                // Step 3 form validation
                if (currentIndex === 2) {
                    var email = $('#fecha_hora_descargue').parsley();
                    if (email.isValid()) {
                        return true;
                    } else {
                        email.validate();
                    }
                }
                // Step 4 form validation
                if (currentIndex === 3) {
                    var email = $('#despacho_parcial_total').parsley();
                    if (email.isValid()) {
                        return true;
                    } else {
                        email.validate();
                    }
                }
                // Step 5 form validation
                if (currentIndex === 4) {
                    var email = $('#fecha_hora_inspeccion').parsley();
                    if (email.isValid()) {
                        return true;
                    } else {
                        email.validate();
                    }
                }
                // Always allow step back to the previous step even if the current step is not valid.
            } else {
                return true;
            }
        }

    });

    $('#wizard2_editar_servicios_itr_expo').steps({
        headerTag: 'h3',
        bodyTag: 'section',
        autoFocus: true,
        titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
        onStepChanging: function(event, currentIndex, newIndex) {
            if (currentIndex < newIndex) {
                // Step 1 form validation
                if (currentIndex === 0) {
                    var fname = $('#id_cliente').parsley();
                    var lname = $('#id_sub_clientes').parsley();

                    if (fname.isValid() && lname.isValid()) {
                        return true;
                    } else {
                        fname.validate();
                        lname.validate();
                    }
                }

                // Step 2 form validation
                if (currentIndex === 1) {
                    var email = $('#dons').parsley();
                    if (email.isValid()) {
                        return true;
                    } else {
                        email.validate();
                    }
                }

                // Step 4 form validation
                if (currentIndex === 2) {
                    var email = $('#observacion2').parsley();
                    if (email.isValid()) {
                        return true;
                    } else {
                        email.validate();
                    }
                }
                // Step 5 form validation
                if (currentIndex === 3) {
                    var email = $('#lugar_inspeccion_vacio').parsley();
                    if (email.isValid()) {
                        return true;
                    } else {
                        email.validate();
                    }
                }
                // Always allow step back to the previous step even if the current step is not valid.
            } else {
                return true;
            }
        }

    });
    $('#wizard3').steps({
        headerTag: 'h3',
        bodyTag: 'section',
        autoFocus: true,
        titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
        stepsOrientation: 1
    });


});
</script>

<div id="respuesta_form_servicio_edit"></div>
<div class="row">
    <div class="col-12 col-sm-6">
        <center><a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal">Ver resumen
                (Facturación)</a>
        </center>
    </div>
    <div class="col-12 col-sm-6">
        <center>
            <?php
            if ($servicios["estado"] == 0 || $servicios["estado"] == 1) {
            ?>
            <a href="javascript:void(0)" class="btn btn-sm btn-success"
                onclick="finalizar_servicio(<?php echo $id_servicio ?>)">Finalizar servicio</a>

            <?php } else if ($servicios["estado"] == 2) { ?>
            <a href="javascript:void(0)" class="btn btn-sm btn-secondary" disabled>Servicio completado</a>
            <?php } ?>
        </center>
    </div>
</div>


<?php
if ($id_tipo_servicio == 1) {
?>
<div data-scrollbar-shown="true" data-scrollable="true" style=" padding: 0px;   overflow: hidden; overflow-y: auto;">
    <div class="card-body collapse show" id="collapse2">
        <div id="wizard2_editar_servicios" class="wizard wizard-style-1 clearfix">

            <h3>Creación servicio</h3>
            <section>
                <form id="form_editar_1">

                    <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                    <input type="hidden" name="mensaje_correo"
                        value="SE HA ACTUALIZADO LA INFORMACIÓN PRINCIPAL DEL SERVICIO <b>ITR IMPORTACIÓN</b>.">
                    <div class="row">
                        <!--<div class="col-12 col-sm-6">
                            <label for="">Fecha recepción documento</label>

                            <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc" class="form-control"
                                placeholder="Seleccionar una fecha" value="<?php
                                                                            if ($servicios['fecha_recepcion_doc'] == null) {
                                                                            } else {
                                                                                echo date('Y-m-d', strtotime($servicios["fecha_recepcion_doc"]));
                                                                            }
                                                                            ?>" <?php echo $disabled_transporte ?>>
                        </div>-->
                        <div class=" col-12 col-sm-6">
                            <label for="">Cliente</label>
                            <select class="form-control" onchange="consultar_subcliente()" name="id_cliente"
                                id="id_cliente" <?php echo $disabled_transporte ?>>
                                <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                    <?php
                                        $id_cliente = $servicios["id_cliente"];
                                        $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                        $consultar_cliente->execute();
                                        $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($consultar_cliente as $cliente) {
                                            echo $cliente["razon_social"];
                                        }
                                        ?>
                                </option>
                                <?php
                                    foreach ($consultar_clientes as $clientes) {
                                    ?>
                                <option value="<?php echo $clientes["id"] ?>"><?php echo $clientes["razon_social"] ?>
                                </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Subcliente</label>

                            <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                                <?php echo $disabled_transporte ?>>

                                <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                    <?php
                                        $id_sub_cliente = $servicios["id_sub_cliente"];
                                        if ($id_sub_cliente == 0) {
                                            echo "no hay subcliente registrado";
                                        }
                                        $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                        $consultar_sub_cliente->execute();
                                        $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($consultar_sub_cliente as $sub_cliente) {
                                            echo $sub_cliente["nombre_sub_cliente"];
                                        }
                                        ?>
                                </option>

                            </select>

                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Do/Ns</label>
                            <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                                class="form-control" <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Tipo de servicio</label>
                            <select class="form-control"
                                oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                name="tipo_servicio" id="tipo_servicios" <?php echo $disabled_transporte ?>>
                                <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                    <?php
                                        $id_tipo_servicio = $servicios["tipo_servicio"];
                                        $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                        $consultar_tipo_servicio->execute();
                                        $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                            echo $tipo_servicio["nombre_servicio"];
                                        }

                                        ?>
                                </option>
                                <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                                <option value="<?php echo $tipo_servicios['id'] ?>">
                                    <?php echo $tipo_servicios['nombre_servicio'] ?>
                                </option>
                                <?php } ?>
                            </select>

                        </div>

                        <div class="col-12 col-sm-6">
                            <label for="">Tipo de carga</label>
                            <select class="form-control" name="tipo_carga" id="tipo_carga"
                                <?php echo $disabled_transporte ?>>
                                <option value="<?php echo $servicios["tipo_carga"] ?>" selected>
                                    <?php
                                        $id_tipo_carga = $servicios["tipo_carga"];
                                        $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                        $consultar_tipo_carga->execute();
                                        $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($consultar_tipo_carga as $tipo_carga) {
                                            echo $tipo_carga["nombre_carga"];
                                        }
                                        ?>
                                </option>
                                <?php foreach ($consultar_tipo_cargas as $tipo_cargas) { ?>
                                <option value="<?php echo $tipo_cargas['id'] ?>">
                                    <?php echo $tipo_cargas['nombre_carga'] ?>
                                </option>
                                <?php } ?>
                            </select>

                        </div>
                        <div class="col-12 col-sm-6" id="cantidad_carga_recibidos">
                            <label for="">Cantidad total recibidos</label>
                            <input type="text" class="form-control" id="cantidad_carga_recibidos"
                                value="<?= $servicios["cantidad_carga_recibidos"] ?>" name="cantidad_carga_recibidos"
                                <?php echo $disabled_carga ?>>

                        </div>
                        <!-- <div class="col-12 col-sm-6">
                            <label for="">Seleccionar imporetiro o expoingreso</label>
                            <select name="impoexpo" class="form-control" id="impoexpo"
                                <?php echo $disabled_transporte ?>>
                                <option value="<?php echo $servicios["impoexpo"] ?>" selected>
                                    <?php echo $servicios["impoexpo"] ?>
                                </option>
                                <option value="IMPORETIRO">Imporetiro</option>
                                <option value="EXPOINGRESO">Expoingreso</option>
                            </select>

                        </div>-->
                        <div class="col-12 col-sm-6" id="tipo_contenedor">
                            <label for="">Tipo de contenedor</label>
                            <select name="tipo_contenedor" id="tipo_contenedor" class="form-control">
                                <option value="<?php echo $servicios["tipo_contenedor"] ?>">
                                    <?php echo $servicios["tipo_contenedor"] ?></option>
                                <option value="HC">HC</option>
                                <option value="DRY">DRY</option>
                                <option value="OPEN TOP">OPEN TOP</option>
                                <option value="REEFER HC">REEFER HC</option>
                                <option value="REERFER">REERFER</option>
                                <option value="FALT RACK">FALT RACK</option>
                                <option value="TANQUE">TANQUE</option>

                            </select>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Número de contenedor</label>
                            <input type="text" name="contenedor" value="<?php echo $servicios["contenedor"] ?>"
                                id="contenedor" class="form-control" <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Tamaño contenedor</label>
                            <input type="text" name="tamaño_contenedor"
                                value="<?php echo $servicios["tamaño_contenedor"] ?>" id="tamaño_contenedor"
                                class="form-control" <?php echo $disabled_transporte ?>>

                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Linea naviera</label>
                            <input type="text" name="linea_naviera" value="<?php echo $servicios["linea_naviera"] ?>"
                                id="linea_naviera" class="form-control" <?php echo $disabled_transporte ?>>

                        </div>
                        <!--  <div class="col-12 col-sm-6">
                            <label for="">Medio recepcion documentos</label>
                            <input type="text" name="recepcion_doc" id="recepcion_doc"
                                value="<?php echo $servicios["medio_recepcion_doc"] ?>" class="form-control"
                                <?php echo $disabled_transporte ?>>

                        </div>-->
                        <div class="col-12 col-sm-6">
                            <label for="">Puerto de retiro</label>
                            <input type="text" value="<?php echo $servicios["puerto_origen"] ?>" name="puerto_origen"
                                id="puerto_origen" class="form-control" <?php echo $disabled_transporte ?>>

                        </div>
                        <div class="col-12 col-sm-6" id="fecha_hora_r_p">
                            <label for="">Fecha y hora retiro puerto del vacío</label>
                            <input type="datetime-local" class="form-control " name="fecha_hora_r_p" id="fecha_hora_r_p"
                                value="<?php $servicios['fecha_hora_r_p'] = preg_replace("/\s/", 'T', $servicios['fecha_hora_r_p']);
                                                                                                                                    echo $servicios['fecha_hora_r_p'] ?>"
                                <?php echo $disabled_transporte ?>>
                        </div>

                        <!--<div class="col-12 col-sm-6" id="fecha_devolucion_v">
                            <label for="">Fecha de entrega del vacío</label>
                            <input type="date" class="form-control " name="fecha_lugar_entrega_v"
                                value="<?= $servicios["fecha_lugar_entrega_v"] ?>" id="fecha_devolucion_v"
                                <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-6" id="lugar_descargue">
                            <label for="">Lugar de entrega del vacío</label>
                            <input type="text" name="lugar_entrega" id="lugar_descargue" class="form-control"
                                value="<?= $servicios["lugar_entrega"] ?>" <?php echo $disabled_transporte ?>>

                        </div>-->
                        <div id="fechas_bodegaje_traslado_zf_edit" class="col-12 col-sm-6"></div>
                        <!-- 
                            <div class="col-12 col-sm-6">
                            <label for="">Fecha vencimiento bodegaje puerto</label>
                            <input type="date" name="fecha_vencimiento_b_p"
                                value="<?php echo  date('Y-m-d', strtotime($servicios['fecha_v_b_p'])); ?>"
                                id="fecha_vencimiento_b_p" class="form-control " placeholder="Seleccionar una fecha"
                                <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Fecha vencimiento traslado ZF</label>
                            <input type="datetime-local"
                                value="<?php $servicios['fecha_v_t_zf'] = preg_replace("/\s/", 'T', $servicios['fecha_v_t_zf']);
                                        echo $servicios['fecha_v_t_zf'] ?>"
                                name="fecha_vencimiento_t_zf" id="fecha_vencimiento_t_zf" class="form-control "
                                placeholder="Seleccionar una fecha" <?php echo $disabled_transporte ?>>



                        </div>-->

                        <div class="col-12 col-sm-6" id="linea_naviera">
                            <label for="">Observación</label>
                            <input type="text" name="observacion1" id="observacion1" class="form-control"
                                value="<?php echo $servicios["observacion1"] ?>" <?php echo $disabled_transporte ?>>

                        </div>

                        <div class="col-12 col-sm-6">
                            <hr>
                            <a href="javascript:void(0)" onclick="actualizar_form_1()"
                                style="width: 40%;margin-top: -7px;"
                                class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                                <div class="ht-40 justify-content-between">
                                    <span class="pd-x-15">Actualizar datos</span>
                                    <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                                </div>
                            </a>

                        </div>

                        <!--<div class="col-12 col-sm-12">

                        <center><br><a href="javascript:void(0)" onclick="guardar_form_1()"
                                class="btn btn-primary">Guardar
                                datos</a></center>

                    </div>-->
                    </div>
                </form>
                <!-- form-group -->
            </section>

            <h3>Transporte</h3>
            <section>
                <form id="form_2">

                    <input type="hidden" name="id_servicio_form2" class="form-control"
                        value="<?php echo $id_servicio ?>">
                    <input type="hidden" name="mensaje_correo"
                        value="SE HA ACTUALIZADO LA INFORMACIÓN DEL TRANSPORTE DEL SERVICIO <b>ITR IMPORTACIÓN</b>.">
                    <div class="row">

                        <div class="col-12 col-sm-6">

                            <div class="col-12">
                                <label for="">Número del servicio</label>
                                <input type="text" class="form-control" value="<?php echo $servicios["dons"] ?>">
                            </div>
                            <div class="col-12 ">
                                <label for="">Número de placa del vehículo</label>
                                <input type="text" class="form-control" id="placa_vehiculo_c"
                                    value="<?php echo $servicios["numero_placa_vehiculo"] ?>"
                                    name="numero_placa_vehiculo" <?php echo $disabled_transporte ?>>
                            </div>
                            <div class="col-12">
                                <label for="">Nombre del conductor del vehículo</label>
                                <input type="text" class="form-control" name="nombre_conductor"
                                    value="<?php echo $servicios["nombre_conductor"] ?>" id="nombre_conductor"
                                    <?php echo $disabled_transporte ?>>
                            </div>
                            <div class="col-12 ">
                                <label for="">Identificación del conductor</label>
                                <input type="text" class="form-control" name="identificacion_conductor"
                                    value="<?php echo $servicios["identificacion_conductor"] ?>"
                                    id="identificacion_conductor" <?php echo $disabled_transporte ?>>
                            </div>
                            <div class="col-12 ">
                                <label for="">Ruta</label>
                                <input type="text" class="form-control" name="ruta" id="ruta"
                                    value="<?php echo $servicios["ruta"] ?>" <?php echo $disabled_transporte ?>>
                            </div>

                            <div class="col-12 ">
                                <label for="">¿Contenedor bajado a piso?</label>
                                <select name="contenedor_bajado_piso" id="contenedor_bajado_piso" class="form-control"
                                    onchange="ejecutar_contenedor_bajado_piso()">
                                    <option value="">Seleciones una opción</option>
                                    <option value="1">Sí</option>
                                    <option value="0">No</option>
                                </select>
                                <p id="campos_bajado_piso"></p>
                            </div>

                            <div class="col-12 ">
                                <label for="">Arim</label>
                                <input type="text" class="form-control" name="arim" id="arim"
                                    value="<?php echo $servicios["arim"] ?>" <?php echo $disabled_transporte ?>>
                            </div>

                            <div class="col-12 ">
                                <?php if ($id_servicio == 1) {
                                        echo '<label for="">Fecha y hora de retiro del producto (origen)</label>';
                                    } else {
                                        echo '<label for="">Fecha y hora de descargue</label>';
                                    }
                                    ?>

                                <input type="datetime-local" value="<?php if ($servicios['fecha_r_p_p_o'] != null) {
                                                                            echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_r_p_p_o']));
                                                                        } else {
                                                                            echo "";
                                                                        } ?>" name="fecha_r_p_p_o" id="fecha_r_p_p_o"
                                    class="form-control " placeholder="Seleccionar una fecha"
                                    <?php echo $disabled_transporte ?>>
                            </div>
                            <div class="col-12 ">
                                <label for="">Observación</label>
                                <input type="text" class="form-control" name="observacion2" id="observacion2"
                                    value="<?php echo $servicios["observacion2"] ?>" <?php echo $disabled_transporte ?>>
                            </div>
                            <div class="col-12 col-sm-12">
                                <hr>
                                <a href="javascript:void(0)" onclick="guardar_form_2()"
                                    style="width: 40%;margin-top: -7px;"
                                    class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                                    <div class="ht-40 justify-content-between">
                                        <span class="pd-x-15">Actualizar datos</span>
                                        <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                                    </div>
                                </a>

                            </div>


                        </div>
                        <div class="col-12 col-sm-6">

                            <div class="col-12">
                                <center>Contenedor bajado a piso</center>
                                <div id="bajado_piso"></div>
                            </div>


                        </div>
                    </div>
                </form>
                <!-- form-group -->

            </section>
            <h3>Descargue mercancía</h3>
            <section>
                <form id="form_3">
                    <div class="row">
                        <input type="hidden" name="id_servicio_form3" class="form-control"
                            value="<?php echo $id_servicio ?>">
                        <input type="hidden" name="mensaje_correo"
                            value="SE HA ACTUALIZADO LA INFORMACIÓN SOBRE EL DESCARGUE DE LA MERCANCÍA DEL SERVICIO <b>ITR IMPORTACIÓN</b>.">
                        <div class="col-12 col-sm-6">

                            <?php if ($id_tipo_servicio == 1) {
                                    echo '  <label for="">Fecha y hora de inicio descargue</label>';
                                } else {
                                    echo '  <label for="">Fecha y hora de inicio llenado</label>';
                                }
                                ?>
                            <input type="datetime-local" class="form-control" id="fecha_hora_descargue"
                                name="fecha_hora_descargue"
                                value="<?php

                                                                                                                                                if ($servicios['fecha_hora_incio_descargue'] == null) {
                                                                                                                                                } else {
                                                                                                                                                    echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_incio_descargue']));
                                                                                                                                                }

                                                                                                                                                ?>"
                                <?php echo $disabled_carga ?>>
                        </div>
                        <div class="col-12 col-sm-6">

                            <?php if ($id_tipo_servicio == 1) {
                                    echo '  <label for="">Fecha y hora de terminación descargue</label>';
                                } else {
                                    echo '  <label for="">Fecha y hora de terminación llenado</label>';
                                }
                                ?>
                            <input type="datetime-local" class="form-control" id="fecha_hora_t_descargue"
                                name="fecha_hora_t_descargue" <?php echo $disabled_carga ?>
                                value="<?php
                                                                                                                                                                                    if ($servicios['fecha_hora_terminacion_descargue'] == null) {
                                                                                                                                                                                    } else {
                                                                                                                                                                                        echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_terminacion_descargue']));
                                                                                                                                                                                    }
                                                                                                                                                                                    ?>">
                        </div>

                        <?php if ($servicios["tipo_carga"] == 5) { ?>

                        <div class="col-12 col-sm-6">
                            <label for="">Cantidad pallets total</label>
                            <input type="text" class="form-control" id="cantidad_bpallets" <?php echo $disabled_carga ?>
                                value="<?php echo $servicios["cantidad_pallets"] ?>" name="cantidad_bpallets">
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Cantidad bultos total</label>
                            <input type="text" class="form-control" <?php echo $disabled_carga ?>
                                value="<?php echo $servicios["cantidad_bultos"] ?>" id="cantidad_bultos"
                                name="cantidad_bultos">
                        </div>

                        <?php } else if ($servicios["tipo_carga"] == 1) { ?>
                        <div class="col-12 col-sm-6">
                            <label for="">Cantidad pallets total
                                (<?php echo $servicios["cantidad_carga_recibidos"] ?>)</label>
                            <input type="text" class="form-control" id="cantidad_bpallets" <?php echo $disabled_carga ?>
                                value="<?php echo $servicios["cantidad_pallets"] ?>" name="cantidad_bpallets">
                        </div>
                        <?php } else if ($servicios["tipo_carga"] == 2) { ?>
                        <div class="col-12 col-sm-6" style="display:none">
                            <label for="">Cantidad pallets total</label>
                            <input type="text" class="form-control" id="cantidad_bpallets" <?php echo $disabled_carga ?>
                                value="<?php echo $servicios["cantidad_pallets"] ?>" name="cantidad_bpallets">
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Cantidad bultos total</label>
                            <input type="text" class="form-control" <?php echo $disabled_carga ?>
                                value="<?php echo $servicios["cantidad_bultos"] ?>" id="cantidad_bultos"
                                name="cantidad_bultos">
                        </div>
                        <?php } else if ($servicios["tipo_carga"] == 3) { ?>
                        <div class="col-12 col-sm-6">
                            <label for="">Cantidad carga suelta total
                                (<?php echo $servicios["cantidad_carga_recibidos"] ?>)</label>
                            <input type="text" class="form-control" id="cantidad_bpallets" <?php echo $disabled_carga ?>
                                value="<?php echo $servicios["cantidad_pallets"] ?>" name="cantidad_bpallets">
                        </div>
                        <?php } else if ($servicios["tipo_carga"] == 4) { ?>
                        <div class="col-12 col-sm-6">
                            <label for="">Cantidad paquetes total
                                (<?php echo $servicios["cantidad_carga_recibidos"] ?>)</label>
                            <input type="text" class="form-control" id="cantidad_bpallets" <?php echo $disabled_carga ?>
                                value="<?php echo $servicios["cantidad_pallets"] ?>" name="cantidad_bpallets">
                        </div>
                        <?php } ?>


                        <div class="col-12 col-sm-6">
                            <label for="">Aplica almacenamiento</label>
                            <select name="aplica_almacenamiento" id="aplica_almacenamiento"
                                <?php echo $disabled_carga ?> class="form-control" id="">
                                <option value="<?php echo $servicios["aplica_almacenaje"] ?>" selected>
                                    <?php
                                        if ($servicios["aplica_almacenaje"] == 0) {
                                            echo "No";
                                        } else {
                                            echo "Si";
                                        }
                                        ?>

                                </option>
                                <option value="1">Sí</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Número de día de almacenaje</label>
                            <input type="text" class="form-control" <?php echo $disabled_carga ?>
                                value="<?php echo $servicios["numero_dia_almacenaje"] ?>" name="numero_dia_almacenaje"
                                id="numero_dia_almacenaje">
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Días de almacenaje libre</label>
                            <input type="text" class="form-control" placeholder="Días de almacenaje"
                                <?php echo $disabled_carga ?> value="<?php echo $servicios["dia_almacenaje_libre"] ?>"
                                id="dia_almacenaje_libre" name="dia_almacenaje_libre">
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Cubicaje</label>
                            <input type="text" value="<?php echo $servicios["cubicaje"] ?>" class="form-control"
                                name="cubicaje" id="cubicaje" <?php echo $disabled_carga ?>>
                        </div>
                        <div class="col-12 col-sm-12">
                            <label for="">Observaciones</label>
                            <textarea name="observaciones_carga" id="observaciones_carga" class="form-control"
                                <?php echo $disabled_carga ?>
                                value="<?php echo $servicios["observaciones_carga"] ?>"><?php echo $servicios["observaciones_carga"] ?></textarea>

                        </div>
                        <div class="col-12 col-sm-12">
                            <label for="">Adjuntar una o varias fotos</label>
                            <div class="input-group">

                                <div class="custom-file">

                                    <input type="file" name="evidencias[]" class="form-control" id="inputGroupFile04"
                                        multiple>
                                    <label class="" for="inputGroupFile04"></label>
                                </div>
                                <br>
                                <div class="input-group-append">
                                    <button class="btn btn-success" onclick="guardar_form_3()" type="button">Guardar
                                        datos y
                                        evidencias <i class="fa fa-upload"></i></button>
                                </div>
                            </div>
                            <a href="../../admin/evidencias_servicio?type=3&id_servicios=<?php echo $id_servicio ?>"
                                target="_blank">Ver
                                evidencias agregadas</a>
                        </div>

                    </div>
                </form>
            </section>
            <h3>Entrega mercancía</h3>
            <section>

                <form id="form_4">
                    <input type="hidden" name="id_servicio_form4" id="id_servicio_form4" class="form-control"
                        value="<?php echo $id_servicio ?>">
                    <input type="hidden" name="mensaje_correo"
                        value="SE HA AGREGADO UNA NUEVA ENTREGA MERCANCÍA DEL SERVICIO <b>ITR IMPORTACIÓN</b>.">
                    <div class="row">
                        <div class="col-12 col-sm-7">

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    <label for="">¿Despacho parcial o total?</label>
                                    <select name="despacho_parcial_total" class="form-control"
                                        id="despacho_parcial_total" <?php echo $disabled_carga ?>>

                                        <option value="0" selected>Despacho parcial</option>
                                        <option value="1">Despacho total</option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Fecha y hora de despacho</label>
                                    <input type="datetime-local" class="form-control " name="fecha_hora_despacho"
                                        id="fecha_hora_despacho" <?php echo $disabled_carga ?>>
                                </div>


                                <?php if ($servicios["tipo_carga"] == 5) {  ?>
                                <div class="col-12 col-sm-6">

                                    <label for="">Cantidad pallets recibidos</label>
                                    <input type="number" oninput="validar_cantidad_pallets()" class="form-control"
                                        id="cantidad_pallets_despacho_item" name="cantidad_pallets_despacho"
                                        max="<?php echo $suma_de_pallets_despachados; ?>" placeholder=""
                                        <?php echo $disabled_carga ?>>
                                    <p id="pallets"></p>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Cantidad de bultos despachados</label>
                                    <input type="number" class="form-control" oninput="validar_cantidad_bultos()"
                                        name="cantidad_bulto_despachado" id="cantidad_bultos_despacho_item"
                                        <?php echo $disabled_carga ?>>
                                    <p id="bultos_contador"></p>
                                </div>
                                <?php  } else if ($servicios["tipo_carga"] == 2) { ?>
                                <div class="col-12 col-sm-6" style="display:none">

                                    <label for="">Cantidad pallets recibidos</label>
                                    <input type="hidden" oninput="validar_cantidad_pallets()" class="form-control"
                                        id="cantidad_pallets_despacho_item" name="cantidad_pallets_despacho"
                                        max="<?php echo $suma_de_pallets_despachados; ?>" placeholder=""
                                        <?php echo $disabled_carga ?>>
                                    <p id="pallets"></p>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Cantidad de bultos despachados</label>
                                    <input type="number" oninput="validar_cantidad_bultos()" class="form-control"
                                        name="cantidad_bulto_despachado" id="cantidad_bultos_despacho_item"
                                        <?php echo $disabled_carga ?>>
                                    <p id="bultos_contador"></p>
                                </div>

                                <?php } else { ?>
                                <div class="col-12 col-sm-6">

                                    <label for="">Cantidad
                                        <?php
                                                if ($servicios["tipo_carga"] == 1) {
                                                    echo "pallets";
                                                } else if ($servicios["tipo_carga"] == 3) {
                                                    echo "carga suelta";
                                                } else if ($servicios["tipo_carga"] == 4) {
                                                    echo "paquetes";
                                                }
                                                ?> recibidos</label>
                                    <input type="number" oninput="validar_cantidad_pallets()" class="form-control"
                                        id="cantidad_pallets_despacho_item" name="cantidad_pallets_despacho"
                                        max="<?php echo $suma_de_pallets_despachados; ?>" placeholder=""
                                        <?php echo $disabled_carga ?>>
                                    <p id="pallets"></p>
                                </div>
                                <?php } ?>

                                <div class="col-12 col-sm-6">
                                    <label for="">Peso aproximado</label>
                                    <input type="text" class="form-control" name="peso_aprox" id="peso_aprox"
                                        <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Destino</label>
                                    <input type="text" class="form-control" name="destino" id="destino"
                                        <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Transportadora</label>
                                    <input type="text" class="form-control" id="transportadora" name="transportadora"
                                        <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Nombre conductor (transportadora)</label>
                                    <input type="text" class="form-control" name="nombre_conductor_t"
                                        id="nombre_conductor_t" <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Teléfono conductor</label>
                                    <input type="text" class="form-control" name="tel_conductor" id="tel_conductor"
                                        <?php echo $disabled_carga ?>>
                                </div>

                                <div class="col-12 col-sm-6">
                                    <label for="">Identificación del conductor (transportadora)</label>
                                    <input type="text" class="form-control" name="doc_conductor_t" id="doc_conductor_t"
                                        <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Placa del vehículo (transportadora)</label>
                                    <input type="text" class="form-control" name="placa_v_conductor_t"
                                        id="placa_v_conductor_t" <?php echo $disabled_carga ?>>
                                </div>

                                <div class="col-12 col-sm-6">
                                    <label for="">Observación</label>
                                    <input type="text" class="form-control" name="observacion4" id="observacion4"
                                        oninput="convertir_mayus()" <?php echo $disabled_carga ?>>
                                </div>

                                <div class="col-12 col-sm-12">
                                    <label for="">Adjuntar una o varias fotos</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" name="evidencias_despachos[]" class="form-control"
                                                id="inputGroupFile04" multiple>
                                            <label class="" for="inputGroupFile04"></label>
                                        </div>
                                        <div class="input-group-append">
                                            <button id="btn_agregar_datos" class="btn btn-success"
                                                onclick="guardar_form_4()" type="button">Guardar
                                                datos y
                                                evidencias <i class="fa fa-upload"></i></button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-12 col-sm-5">
                            <?php if ($servicios["tipo_carga"] == 5) { ?>
                            <div id="total_pallets"></div>
                            <div id="total_bultos"></div>
                            <?php } else if ($servicios["tipo_carga"] == 2) { ?>
                            <div id="total_bultos"></div>
                            <?php } else { ?>
                            <div id="total_pallets"></div>
                            <?php } ?>

                            <div id="despacho_parcial_totals_servicios"></div>
                        </div>
                    </div>

                </form>
            </section>

            <h3>Devolución del vacío</h3>
            <section>
                <form id="form_5" enctype="multipart/form-data">
                    <input type="hidden" name="id_servicio_form5" id="id_servicio_form5" class="form-control"
                        value="<?php echo $id_servicio ?>">
                    <input type="hidden" name="mensaje_correo"
                        value="SE HA AGREGADO O ACTUALIZADO LA INFORMACION DE LA DEVOLUCIÓN DEL VACÍO SOBRE EL SERVICIO <b>ITR IMPORTACIÓN</b>.">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <label for="">¿Contenedor fue bajado a piso en patio aliado?</label>
                            <select name="patelizo" id="patelizo" class="form-control">
                                <option value="<?php echo $servicios["patelizo"] ?>" selected>
                                    <?php if ($servicios["patelizo"] === null) {
                                            echo "Seleccione una opción";
                                        } else {
                                            echo $servicios["patelizo"];
                                        }
                                        ?></option>
                                <option value="Sí">Sí</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Fecha bajado a piso</label>
                            <input type="date" class="form-control " name="fecha_bajado_piso" id="fecha_bajado_piso"
                                value="<?php echo $servicios["fecha_bajado_piso"] ?>" <?php echo $disabled_carga ?>>
                        </div>

                        <div class="col-12 col-sm-6">
                            <label for="">Lugar de inspección del vacío</label>
                            <input type="text" class="form-control" name="lugar_inspeccion_vacio"
                                id="lugar_inspeccion_vacio" value="<?php echo $servicios["lugar_inspeccion_vacio"] ?>"
                                <?php echo $disabled_transporte ?>>
                        </div>

                        <div class="col-12 col-sm-6">
                            <label for="">Fecha y hora de inspección del vacío</label>
                            <input type="datetime-local" class="form-control " name="fecha_hora_inspeccion"
                                id="fecha_hora_inspeccion"
                                value="<?php
                                                                                                                                                    if ($servicios['fecha_hora_inspeccion'] == null) {
                                                                                                                                                    } else {
                                                                                                                                                        echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_inspeccion']));
                                                                                                                                                    }
                                                                                                                                                    ?>"
                                <?php echo $disabled_transporte ?>>
                        </div>

                        <div class="col-12 col-sm-6">
                            <label for="">Lugar devolucion del vacío</label>
                            <input type="text" class="form-control" name="lugar_devolucion_vacio"
                                id="lugar_devolucion_vacio" value="<?php echo $servicios["lugar_devolucion_vacio"] ?>"
                                <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Fecha y hora devolucion del vacío</label>
                            <input type="datetime-local" class="form-control " name="fecha_devolucion"
                                onchange="comparar_fechas_devolucion()" id="fecha_devolucion"
                                value="<?php
                                                                                                                                                                                if ($servicios['fecha_devolucion'] == null) {
                                                                                                                                                                                } else {
                                                                                                                                                                                    echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_devolucion']));
                                                                                                                                                                                }
                                                                                                                                                                                ?>"
                                <?php echo $disabled_transporte ?>>
                        </div>

                        <div class="col-12 col-sm-4">
                            <label for="">Nombre conductor (devolución del vacío)</label>
                            <!-- <input type="text" class="form-control" id="nombre_condcutor_v"
                                value="<?php echo $servicios["id_conductor_d"] ?>" name="nombre_condcutor_v"
                                <?php echo $disabled_transporte ?>>-->
                            <select name="nombre_condcutor_v" oninput="seleccionar_conductor()" id="nombre_condcutor_v"
                                class="form-control" style="width:100% !important;" <?php echo $disabled_transporte ?>>
                                <option value=" <?php echo $servicios["id_conductor_d"] ?>" selected>
                                    <?php $id_conductor = $servicios["id_conductor_d"];
                                        $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor'");
                                        $consultar_conductor->execute();
                                        $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

                                        foreach ($consultar_conductor as $conductor) {
                                            echo $conductor["nombres_conductor"] . ' ' . $conductor["apellidos_conductor"];
                                        }
                                        ?></option>
                                <?php foreach ($consultar_conductores as $conductor) { ?>
                                <option value="<?php echo $conductor["id"] ?>">
                                    <?php echo $conductor["nombres_conductor"] ?></option>
                                <?php  } ?>
                            </select>
                        </div>
                        <!-- <div class=" col-12 col-sm-6">
                            <label for="">Identificación del conductor (devolución del vacío)</label>
                            <input type="text" class="form-control" id="doc_condcutor_v"
                                value="<?php echo $servicios["identificacion_conductor_d"] ?>" name="doc_condcutor_v"
                                <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Placa del vehículo (devolución del vacío)</label>
                            <input type="text" class="form-control" value="<?php echo $servicios["placa_vehiculo_d"] ?>"
                                id="palca_condcutor_v" name="placa_condcutor_v" <?php echo $disabled_transporte ?>>
                        </div>-->
                        <div class="col-12 col-sm-8" id="campos_conductor"></div>

                        <div class="col-12 col-sm-12">

                            <label for="">Observaciones</label>
                            <input type="text" class="form-control" name="observacion3" id="observacion3"
                                value="<?php echo $servicios["observacion3"] ?>" <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-12">
                            <label for="">Adjuntar una o varias fotos</label>
                            <div class="input-group">

                                <div class="custom-file">

                                    <input type="file" name="evidencias_devolucion[]" class="form-control"
                                        id="inputGroupFile04" multiple="">
                                    <label class="" for="inputGroupFile04"></label>
                                </div>
                                <br>
                                <div class="input-group-append">
                                    <button class="btn btn-success" onclick="guardar_form_5()" type="button">Guardar
                                        datos y
                                        evidencias <i class="fa fa-upload"></i></button>
                                </div>
                            </div>
                            <a href="../../admin/evidencias_servicio?type=5&id_servicios=<?php echo $id_servicio ?>"
                                target="_blank">Ver
                                evidencias agregadas</a>
                        </div>


                    </div>
                </form>
            </section>

            <!--<h3>Cierre del servicio</h3>
            <section>
                <div id="resumen_servicio_editar"></div>
            </section>-->
        </div>
    </div>
</div>

<?php
} else if ($id_tipo_servicio == 2) {
?>
<div data-scrollbar-shown="true" data-scrollable="true" style=" padding: 0px;   overflow: hidden; overflow-y: auto;">
    <div class="card-body collapse show" id="collapse2">
        <div id="wizard2_editar_servicios_itr_expo" class="wizard wizard-style-1 clearfix">


            <?php if ($servicios["impoexpo"] == 1) { ?>
            <h3>Retiro de unidad vacía</h3>
            <section>
                <form id="form_editar_1">

                    <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                    <input type="hidden" name="mensaje_correo"
                        value="SE HA ACTULIZADO LA INFORMACIÓN DE RETIRO DE UNIDAD VACÍA DEL SERVICIO <b>ITR EXPORTACIÓN</b>.">

                    <div class="row">
                        <div class=" col-12 col-sm-6">
                            <label for="">Cliente</label>
                            <select class="form-control" onchange="consultar_subcliente()" name="id_cliente"
                                id="id_cliente" <?php echo $disabled_transporte ?>>
                                <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                    <?php
                                            $id_cliente = $servicios["id_cliente"];
                                            $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                            $consultar_cliente->execute();
                                            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                            foreach ($consultar_cliente as $cliente) {
                                                echo $cliente["razon_social"];
                                            }
                                            ?>
                                </option>
                                <?php
                                        foreach ($consultar_clientes as $clientes) {
                                        ?>
                                <option value="<?php echo $clientes["id"] ?>"><?php echo $clientes["razon_social"] ?>
                                </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Subcliente</label>

                            <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                                <?php echo $disabled_transporte ?>>
                                <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                    <?php
                                            $id_sub_cliente = $servicios["id_sub_cliente"];
                                            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                            $consultar_sub_cliente->execute();
                                            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                            foreach ($consultar_sub_cliente as $sub_cliente) {
                                                echo $sub_cliente["nombre_sub_cliente"];
                                            }
                                            ?>

                                </option>
                            </select>

                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Do/Ns</label>
                            <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                                class="form-control" <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Tipo de servicio</label>
                            <select class="form-control"
                                oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                name="tipo_servicio" id="tipo_servicios" <?php echo $disabled_transporte ?>>
                                <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                    <?php
                                            $id_tipo_servicio = $servicios["tipo_servicio"];
                                            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                            $consultar_tipo_servicio->execute();
                                            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                            foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                                echo $tipo_servicio["nombre_servicio"];
                                            }

                                            ?>
                                </option>
                                <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                                <option value="<?php echo $tipo_servicios['id'] ?>">
                                    <?php echo $tipo_servicios['nombre_servicio'] ?>
                                </option>
                                <?php } ?>
                            </select>

                        </div>

                        <div class="col-12 col-sm-6" id="lugar_descargue">
                            <label for="">Lugar de retiro del vacío</label>
                            <input type="text" name="lugar_entrega" id="lugar_descargue" class="form-control"
                                value="<?= $servicios["lugar_entrega"] ?>" <?php echo $disabled_transporte ?>>

                        </div>
                        <div class="col-12 col-sm-6" id="fecha_devolucion_v">
                            <label for="">Fecha de retiro del vacío</label>
                            <input type="date" class="form-control " name="fecha_lugar_entrega_v"
                                value="<?= $servicios["fecha_lugar_entrega_v"] ?>" id="fecha_devolucion_v"
                                <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-6" id="tipo_contenedor">
                            <label for="">Tipo de contenedor</label>
                            <select name="tipo_contenedor" id="tipo_contenedor" class="form-control">
                                <option value="<?php echo $servicios["tipo_contenedor"] ?>">
                                    <?php echo $servicios["tipo_contenedor"] ?></option>
                                <option value="HC">HC</option>
                                <option value="DRY">DRY</option>
                                <option value="OPEN TOP">OPEN TOP</option>
                                <option value="REEFER HC">REEFER HC</option>
                                <option value="REERFER">REERFER</option>
                                <option value="FALT RACK">FALT RACK</option>
                                <option value="TANQUE">TANQUE</option>

                            </select>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Número de contenedor</label>
                            <input type="text" name="contenedor" value="<?php echo $servicios["contenedor"] ?>"
                                id="contenedor" class="form-control" <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Tamaño contenedor</label>
                            <input type="text" name="tamaño_contenedor"
                                value="<?php echo $servicios["tamaño_contenedor"] ?>" id="tamaño_contenedor"
                                class="form-control" <?php echo $disabled_transporte ?>>

                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Linea naviera</label>
                            <input type="text" name="linea_naviera" value="<?php echo $servicios["linea_naviera"] ?>"
                                id="linea_naviera" class="form-control" <?php echo $disabled_transporte ?>>

                        </div>

                        <div class="col-12 col-sm-6" id="nombre_conductors">
                            <label for="">Nombre del conductor del vehículo</label>
                            <input type="text" class="form-control" name="nombre_c_e_v" id="nombre_c_e_v"
                                value="<?php echo $servicios["nombre_c_e_v"] ?>" <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12  col-sm-6" id="identificacion_conductors">
                            <label for="">Identificación del conductor</label>
                            <input type="text" class="form-control" name="identificacion_c_e_v"
                                id="identificacion_c_e_v" value="<?php echo $servicios["identificacion_c_e_v"] ?>"
                                <?php echo $disabled_transporte ?>>
                        </div>

                        <div class="col-12  col-sm-12" id="placa_vehiculo_cs">
                            <label for="">Número de placa del vehículo</label>
                            <input type="text" class="form-control" id="placa_v_e_v" name="placa_v_e_v"
                                value="<?php echo $servicios["placa_v_e_v"] ?>" <?php echo $disabled_transporte ?>>
                        </div>

                        <div class="col-12  ">
                            <label for="">¿Contenedor bajado a piso?</label>
                            <select name="contenedor_bajado_piso_fecha" id="contenedor_bajado_piso_fecha"
                                class="form-control" onchange="ejecutar_contenedor_bajado_piso_fecha()">
                                <option value="<?php if ($servicios["contenedor_bajado_piso_fecha"] == 1) {
                                                            echo "1";
                                                        } else if ($servicios["contenedor_bajado_piso_fecha"] == 0) {
                                                            echo "0";
                                                        } else {
                                                            echo "";
                                                        } ?>" selected>
                                    <?php if ($servicios["contenedor_bajado_piso_fecha"] == 1) {
                                                echo "Sí";
                                            } else if ($servicios["contenedor_bajado_piso_fecha"] == 0) {
                                                echo "No";
                                            } else {
                                                echo "Seleccione una opción";
                                            }
                                            ?>
                                </option>
                                <option value="1">Sí</option>
                                <option value="0">No</option>
                            </select>
                            <p id="campos_bajado_piso_fecha"></p>
                        </div>

                        <div class="col-12 col-sm-6" id="linea_naviera">
                            <label for="">Observación</label>
                            <input type="text" name="observacion1" id="observacion1" class="form-control"
                                value="<?php echo $servicios["observacion1"] ?>" <?php echo $disabled_transporte ?>>

                        </div>

                        <div class="col-12 col-sm-6">
                            <hr>
                            <a href="javascript:void(0)" onclick="actualizar_form_1()"
                                style="width: 40%;margin-top: -7px;"
                                class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                                <div class="ht-40 justify-content-between">
                                    <span class="pd-x-15">Actualizar datos</span>
                                    <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                                </div>
                            </a>

                        </div>

                        <!--<div class="col-12 col-sm-12">

                        <center><br><a href="javascript:void(0)" onclick="guardar_form_1()"
                                class="btn btn-primary">Guardar
                                datos</a></center>

                    </div>-->
                    </div>
                </form>
                <!-- form-group -->
            </section>
            <?php  } else if ($servicios["impoexpo"] == 0) { ?>
            <h3>Recibo de mercancia</h3>
            <section>
                <form id="form_4">
                    <input type="hidden" name="mensaje_correo"
                        value="SE HA ACTULIZADO LA INFORMACIÓN DE RECIBO DE MERCANCÍA DEL SERVICIO <b>ITR EXPORTACIÓN</b>.">

                    <input type="hidden" name="id_servicio_form4" id="id_servicio_form4" class="form-control"
                        value="<?php echo $id_servicio ?>">
                    <div class="row">
                        <div class="col-12 col-sm-7">

                            <div class="row">
                                <div class=" col-12 col-sm-6">
                                    <label for="">Cliente</label>
                                    <select class="form-control" onchange="consultar_subcliente()" name="id_cliente"
                                        id="id_cliente" <?php echo $disabled_transporte ?>>
                                        <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                            <?php
                                                    $id_cliente = $servicios["id_cliente"];
                                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                                    $consultar_cliente->execute();
                                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach ($consultar_cliente as $cliente) {
                                                        echo $cliente["razon_social"];
                                                    }
                                                    ?>
                                        </option>
                                        <?php
                                                foreach ($consultar_clientes as $clientes) {
                                                ?>
                                        <option value="<?php echo $clientes["id"] ?>">
                                            <?php echo $clientes["razon_social"] ?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Subcliente</label>

                                    <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                                        <?php echo $disabled_transporte ?>>
                                        <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                            <?php
                                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                                    $consultar_sub_cliente->execute();
                                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach ($consultar_sub_cliente as $sub_cliente) {
                                                        echo $sub_cliente["nombre_sub_cliente"];
                                                    }
                                                    ?>

                                        </option>
                                    </select>

                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Do/Ns</label>
                                    <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                                        class="form-control" <?php echo $disabled_transporte ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Tipo de servicio</label>
                                    <select class="form-control"
                                        oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                        name="tipo_servicio" id="tipo_servicios" <?php echo $disabled_transporte ?>>
                                        <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                            <?php
                                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                                    $consultar_tipo_servicio->execute();
                                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                                        echo $tipo_servicio["nombre_servicio"];
                                                    }

                                                    ?>
                                        </option>
                                        <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                                        <option value="<?php echo $tipo_servicios['id'] ?>">
                                            <?php echo $tipo_servicios['nombre_servicio'] ?>
                                        </option>
                                        <?php } ?>
                                    </select>

                                </div>

                                <div class="col-12 col-sm-6">
                                    <label for="">Tipo de carga</label>
                                    <select class="form-control" name="tipo_carga" onchange="ocultar_capo_cantidad()"
                                        id="tipo_carga" <?php echo $disabled_transporte ?>>
                                        <option value="<?php echo $servicios["tipo_carga"]; ?>" selected>
                                            <?php
                                                    $id_tipo_carga = $servicios["tipo_carga"];

                                                    if ($id_tipo_carga == 0) {
                                                        echo "Por favor seleccionar una opción";
                                                    } else {
                                                        $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                                        $consultar_tipo_carga->execute();
                                                        $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                                        foreach ($consultar_tipo_carga as $tipo_carga) {
                                                            echo $tipo_carga["nombre_carga"];
                                                        }
                                                    }
                                                    ?>

                                        </option>
                                        <?php foreach ($consultar_tipo_cargas as $tipo_cargas) { ?>
                                        <option value="<?php echo $tipo_cargas['id'] ?>">
                                            <?php echo $tipo_cargas['nombre_carga'] ?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                    <p>Campo importante*</p>

                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Fecha y hora de recibido</label>
                                    <input type="datetime-local" class="form-control " name="fecha_hora_despacho"
                                        id="fecha_hora_despacho" <?php echo $disabled_carga ?>>
                                </div>

                                <?php if ($servicios["tipo_carga"] == 5) { ?>
                                <div class="col-12 col-sm-6">

                                    <label for="">Cantidad pallets recibidos</label>
                                    <input type="number" class="form-control" id="cantidad_pallets_despacho_item"
                                        name="cantidad_pallets_despacho" placeholder="" <?php echo $disabled_carga ?>>

                                </div>

                                <div class="col-12 col-sm-6">
                                    <label for="">Cantidad de bultos recibidos</label>
                                    <input type="text" class="form-control" name="cantidad_bulto_despachado"
                                        id="cantidad_bulto_despachado" <?php echo $disabled_carga ?>>
                                </div>
                                <?php } else if ($servicios["tipo_carga"] == 2) { ?>
                                <div class="col-12 col-sm-6">
                                    <label for="">Cantidad de bultos recibidos</label>
                                    <input type="text" class="form-control" name="cantidad_bulto_despachado"
                                        id="cantidad_bulto_despachado" <?php echo $disabled_carga ?>>
                                </div>
                                <?php } else if ($servicios["tipo_carga"] == 0 || $servicios["tipo_carga"] == "") { ?>

                                <p class="col-12" id="tipo_carga_form_2_campos"></p>

                                <?php } else { ?>
                                <div class="col-12 col-sm-6">
                                    <label for=""><?php if ($servicios["tipo_carga"] == 1) {
                                                                    echo 'Cantidad pallets recibidos';
                                                                } else if ($servicios["tipo_carga"] == 3) {
                                                                    echo 'Cantidad carga suelta recibidos';
                                                                }
                                                                ?></label>
                                    <input type="number" class="form-control" id="cantidad_pallets_despacho_item"
                                        name="cantidad_pallets_despacho" placeholder="" <?php echo $disabled_carga ?>>
                                </div>
                                <?php } ?>

                                <div class="col-12 col-sm-6">
                                    <label for="">Peso aproximado</label>
                                    <input type="text" class="form-control" name="peso_aprox" id="peso_aprox"
                                        <?php echo $disabled_carga ?>>
                                </div>

                                <div class="col-12 col-sm-6">
                                    <label for="">Transportadora</label>
                                    <input type="text" class="form-control" id="transportadora" name="transportadora"
                                        <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Nombre conductor (transportadora)</label>
                                    <input type="text" class="form-control" name="nombre_conductor_t"
                                        id="nombre_conductor_t" <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Teléfono conductor</label>
                                    <input type="text" class="form-control" name="tel_conductor" id="tel_conductor"
                                        <?php echo $disabled_carga ?>>
                                </div>

                                <div class="col-12 col-sm-6">
                                    <label for="">Identificación del conductor (transportadora)</label>
                                    <input type="text" class="form-control" name="doc_conductor_t" id="doc_conductor_t"
                                        <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Placa del vehículo (transportadora)</label>
                                    <input type="text" class="form-control" name="placa_v_conductor_t"
                                        id="placa_v_conductor_t" <?php echo $disabled_carga ?>>
                                </div>

                                <div class="col-12 col-sm-6">
                                    <label for="">Observación</label>
                                    <input type="text" class="form-control" name="observacion4" id="observacion4"
                                        oninput="convertir_mayus()" <?php echo $disabled_carga ?>>
                                </div>

                                <div class="col-12 col-sm-12">
                                    <label for="">Adjuntar una o varias fotos</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" name="evidencias_despachos[]" class="form-control"
                                                id="inputGroupFile04" multiple>
                                            <label class="" for="inputGroupFile04"></label>
                                        </div>
                                        <div class="input-group-append">
                                            <button id="btn_agregar_datos" class="btn btn-success"
                                                onclick="guardar_form_4()" type="button">Guardar
                                                datos y
                                                evidencias <i class="fa fa-upload"></i></button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-12 col-sm-5">
                            <center>
                                <h6>Mercancia recibida</h6>
                            </center>
                            <div id="despacho_parcial_totals_servicios"></div>
                        </div>
                    </div>

                </form>
                <!-- form-group -->
            </section>
            <?php } ?>

            <?php if ($servicios["impoexpo"] != 1) { ?>
            <h3>Retiro de unidad vacía</h3>
            <section>
                <form id="form_editar_1">

                    <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                    <input type="hidden" name="mensaje_correo"
                        value="SE HA ACTULIZADO LA INFORMACIÓN DE RETIRO DE UNIDAD VACÍA DEL SERVICIO <b>ITR EXPORTACIÓN</b>.">

                    <div class="row">
                        <div class=" col-12 col-sm-6">
                            <label for="">Cliente</label>
                            <select class="form-control" onchange="consultar_subcliente()" name="id_cliente"
                                id="id_cliente" <?php echo $disabled_transporte ?>>
                                <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                    <?php
                                            $id_cliente = $servicios["id_cliente"];
                                            $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                            $consultar_cliente->execute();
                                            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                            foreach ($consultar_cliente as $cliente) {
                                                echo $cliente["razon_social"];
                                            }
                                            ?>
                                </option>
                                <?php
                                        foreach ($consultar_clientes as $clientes) {
                                        ?>
                                <option value="<?php echo $clientes["id"] ?>"><?php echo $clientes["razon_social"] ?>
                                </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Subcliente</label>

                            <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                                <?php echo $disabled_transporte ?>>
                                <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                    <?php
                                            $id_sub_cliente = $servicios["id_sub_cliente"];
                                            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                            $consultar_sub_cliente->execute();
                                            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                            foreach ($consultar_sub_cliente as $sub_cliente) {
                                                echo $sub_cliente["nombre_sub_cliente"];
                                            }
                                            ?>

                                </option>
                            </select>

                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Do/Ns</label>
                            <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                                class="form-control" <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Tipo de servicio</label>
                            <select class="form-control"
                                oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                name="tipo_servicio" id="tipo_servicios" <?php echo $disabled_transporte ?>>
                                <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                    <?php
                                            $id_tipo_servicio = $servicios["tipo_servicio"];
                                            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                            $consultar_tipo_servicio->execute();
                                            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                            foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                                echo $tipo_servicio["nombre_servicio"];
                                            }

                                            ?>
                                </option>
                                <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                                <option value="<?php echo $tipo_servicios['id'] ?>">
                                    <?php echo $tipo_servicios['nombre_servicio'] ?>
                                </option>
                                <?php } ?>
                            </select>

                        </div>

                        <div class="col-12 col-sm-6" id="lugar_descargue">
                            <label for="">Lugar de retiro del vacío</label>
                            <input type="text" name="lugar_entrega" id="lugar_descargue" class="form-control"
                                value="<?= $servicios["lugar_entrega"] ?>" <?php echo $disabled_transporte ?>>

                        </div>
                        <div class="col-12 col-sm-6" id="fecha_devolucion_v">
                            <label for="">Fecha de retiro del vacío</label>
                            <input type="date" class="form-control " name="fecha_lugar_entrega_v"
                                value="<?= $servicios["fecha_lugar_entrega_v"] ?>" id="fecha_devolucion_v"
                                <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-6" id="tipo_contenedor">
                            <label for="">Tipo de contenedor</label>
                            <select name="tipo_contenedor" id="tipo_contenedor" class="form-control">
                                <option value="<?php echo $servicios["tipo_contenedor"] ?>">
                                    <?php echo $servicios["tipo_contenedor"] ?></option>
                                <option value="HC">HC</option>
                                <option value="DRY">DRY</option>
                                <option value="OPEN TOP">OPEN TOP</option>
                                <option value="REEFER HC">REEFER HC</option>
                                <option value="REERFER">REERFER</option>
                                <option value="FALT RACK">FALT RACK</option>
                                <option value="TANQUE">TANQUE</option>

                            </select>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Número de contenedor</label>
                            <input type="text" name="contenedor" value="<?php echo $servicios["contenedor"] ?>"
                                id="contenedor" class="form-control" <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Tamaño contenedor</label>
                            <input type="text" name="tamaño_contenedor"
                                value="<?php echo $servicios["tamaño_contenedor"] ?>" id="tamaño_contenedor"
                                class="form-control" <?php echo $disabled_transporte ?>>

                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Linea naviera</label>
                            <input type="text" name="linea_naviera" value="<?php echo $servicios["linea_naviera"] ?>"
                                id="linea_naviera" class="form-control" <?php echo $disabled_transporte ?>>

                        </div>

                        <div class="col-12 col-sm-6" id="nombre_conductors">
                            <label for="">Nombre del conductor del vehículo</label>
                            <input type="text" class="form-control" name="nombre_c_e_v" id="nombre_c_e_v"
                                value="<?php echo $servicios["nombre_c_e_v"] ?>" <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12  col-sm-6" id="identificacion_conductors">
                            <label for="">Identificación del conductor</label>
                            <input type="text" class="form-control" name="identificacion_c_e_v"
                                id="identificacion_c_e_v" value="<?php echo $servicios["identificacion_c_e_v"] ?>"
                                <?php echo $disabled_transporte ?>>
                        </div>

                        <div class="col-12  col-sm-12" id="placa_vehiculo_cs">
                            <label for="">Número de placa del vehículo</label>
                            <input type="text" class="form-control" id="placa_v_e_v" name="placa_v_e_v"
                                value="<?php echo $servicios["placa_v_e_v"] ?>" <?php echo $disabled_transporte ?>>
                        </div>

                        <div class="col-12  ">
                            <label for="">¿Contenedor bajado a piso?</label>
                            <select name="contenedor_bajado_piso_fecha" id="contenedor_bajado_piso_fecha"
                                class="form-control" onchange="ejecutar_contenedor_bajado_piso_fecha()">
                                <option value="<?php if ($servicios["contenedor_bajado_piso_fecha"] == 1) {
                                                            echo "1";
                                                        } else if ($servicios["contenedor_bajado_piso_fecha"] == 0) {
                                                            echo "0";
                                                        } else {
                                                            echo "";
                                                        } ?>" selected>
                                    <?php if ($servicios["contenedor_bajado_piso_fecha"] == 1) {
                                                echo "Sí";
                                            } else if ($servicios["contenedor_bajado_piso_fecha"] == 0) {
                                                echo "No";
                                            } else {
                                                echo "Seleccione una opción";
                                            }
                                            ?>
                                </option>
                                <option value="1">Sí</option>
                                <option value="0">No</option>
                            </select>
                            <p id="campos_bajado_piso_fecha"></p>
                        </div>


                        <div class="col-12 col-sm-6" id="linea_naviera">
                            <label for="">Observación</label>
                            <input type="text" name="observacion1" id="observacion1" class="form-control"
                                value="<?php echo $servicios["observacion1"] ?>" <?php echo $disabled_transporte ?>>

                        </div>

                        <div class="col-12 col-sm-6">
                            <hr>
                            <a href="javascript:void(0)" onclick="actualizar_form_1()"
                                style="width: 40%;margin-top: -7px;"
                                class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                                <div class="ht-40 justify-content-between">
                                    <span class="pd-x-15">Actualizar datos</span>
                                    <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                                </div>
                            </a>

                        </div>


                    </div>
                </form>
                <!-- form-group -->
            </section>
            <?php  } else if ($servicios["impoexpo"] != 0) { ?>
            <h3>Recibo de mercancia</h3>
            <section>
                <form id="form_4">
                    <input type="hidden" name="mensaje_correo"
                        value="SE HA ACTULIZADO LA INFORMACIÓN DE RECIBO DE MERCANCÍA DEL SERVICIO <b>ITR EXPORTACIÓN</b>.">

                    <input type="hidden" name="id_servicio_form4" id="id_servicio_form4" class="form-control"
                        value="<?php echo $id_servicio ?>">
                    <div class="row">
                        <div class="col-12 col-sm-7">

                            <div class="row">
                                <div class=" col-12 col-sm-6">
                                    <label for="">Cliente</label>
                                    <select class="form-control" onchange="consultar_subcliente()" name="id_cliente"
                                        id="id_cliente" <?php echo $disabled_transporte ?>>
                                        <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                            <?php
                                                    $id_cliente = $servicios["id_cliente"];
                                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                                    $consultar_cliente->execute();
                                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach ($consultar_cliente as $cliente) {
                                                        echo $cliente["razon_social"];
                                                    }
                                                    ?>
                                        </option>
                                        <?php
                                                foreach ($consultar_clientes as $clientes) {
                                                ?>
                                        <option value="<?php echo $clientes["id"] ?>">
                                            <?php echo $clientes["razon_social"] ?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Subcliente</label>

                                    <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                                        <?php echo $disabled_transporte ?>>
                                        <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                            <?php
                                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                                    $consultar_sub_cliente->execute();
                                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach ($consultar_sub_cliente as $sub_cliente) {
                                                        echo $sub_cliente["nombre_sub_cliente"];
                                                    }
                                                    ?>

                                        </option>
                                    </select>

                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Do/Ns</label>
                                    <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                                        class="form-control" <?php echo $disabled_transporte ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Tipo de servicio</label>
                                    <select class="form-control"
                                        oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                        name="tipo_servicio" id="tipo_servicios" <?php echo $disabled_transporte ?>>
                                        <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                            <?php
                                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                                    $consultar_tipo_servicio->execute();
                                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                                        echo $tipo_servicio["nombre_servicio"];
                                                    }

                                                    ?>
                                        </option>
                                        <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                                        <option value="<?php echo $tipo_servicios['id'] ?>">
                                            <?php echo $tipo_servicios['nombre_servicio'] ?>
                                        </option>
                                        <?php } ?>
                                    </select>

                                </div>

                                <div class="col-12 col-sm-6">
                                    <label for="">Tipo de carga</label>
                                    <select class="form-control" name="tipo_carga" onchange="ocultar_capo_cantidad()"
                                        id="tipo_carga" <?php echo $disabled_transporte ?>>
                                        <option value="<?php echo $servicios["tipo_carga"]; ?>" selected>
                                            <?php
                                                    $id_tipo_carga = $servicios["tipo_carga"];

                                                    if ($id_tipo_carga == 0) {
                                                        echo "Por favor seleccionar una opción";
                                                    } else {
                                                        $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                                        $consultar_tipo_carga->execute();
                                                        $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                                        foreach ($consultar_tipo_carga as $tipo_carga) {
                                                            echo $tipo_carga["nombre_carga"];
                                                        }
                                                    }
                                                    ?>

                                        </option>
                                        <?php foreach ($consultar_tipo_cargas as $tipo_cargas) { ?>
                                        <option value="<?php echo $tipo_cargas['id'] ?>">
                                            <?php echo $tipo_cargas['nombre_carga'] ?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                    <p>Campo importante*</p>

                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Fecha y hora de recibido</label>
                                    <input type="datetime-local" class="form-control " name="fecha_hora_despacho"
                                        id="fecha_hora_despacho" <?php echo $disabled_carga ?>>
                                </div>

                                <?php if ($servicios["tipo_carga"] == 5) { ?>
                                <div class="col-12 col-sm-6">

                                    <label for="">Cantidad pallets recibidos</label>
                                    <input type="number" class="form-control" id="cantidad_pallets_despacho_item"
                                        name="cantidad_pallets_despacho" placeholder="" <?php echo $disabled_carga ?>>

                                </div>

                                <div class="col-12 col-sm-6">
                                    <label for="">Cantidad de bultos recibidos</label>
                                    <input type="text" class="form-control" name="cantidad_bulto_despachado"
                                        id="cantidad_bulto_despachado" <?php echo $disabled_carga ?>>
                                </div>
                                <?php } else if ($servicios["tipo_carga"] == 2) { ?>
                                <div class="col-12 col-sm-6">
                                    <label for="">Cantidad de bultos recibidos</label>
                                    <input type="text" class="form-control" name="cantidad_bulto_despachado"
                                        id="cantidad_bulto_despachado" <?php echo $disabled_carga ?>>
                                </div>
                                <?php } else if ($servicios["tipo_carga"] == 0 || $servicios["tipo_carga"] == "") { ?>

                                <p class="col-12" id="tipo_carga_form_2_campos"></p>

                                <?php } else { ?>
                                <div class="col-12 col-sm-6">
                                    <label for=""><?php if ($servicios["tipo_carga"] == 1) {
                                                                    echo 'Cantidad pallets recibidos';
                                                                } else if ($servicios["tipo_carga"] == 3) {
                                                                    echo 'Cantidad carga suelta recibidos';
                                                                }
                                                                ?></label>
                                    <input type="number" class="form-control" id="cantidad_pallets_despacho_item"
                                        name="cantidad_pallets_despacho" placeholder="" <?php echo $disabled_carga ?>>
                                </div>
                                <?php } ?>

                                <div class="col-12 col-sm-6">
                                    <label for="">Peso aproximado</label>
                                    <input type="text" class="form-control" name="peso_aprox" id="peso_aprox"
                                        <?php echo $disabled_carga ?>>
                                </div>

                                <div class="col-12 col-sm-6">
                                    <label for="">Transportadora</label>
                                    <input type="text" class="form-control" id="transportadora" name="transportadora"
                                        <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Nombre conductor (transportadora)</label>
                                    <input type="text" class="form-control" name="nombre_conductor_t"
                                        id="nombre_conductor_t" <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Teléfono conductor</label>
                                    <input type="text" class="form-control" name="tel_conductor" id="tel_conductor"
                                        <?php echo $disabled_carga ?>>
                                </div>

                                <div class="col-12 col-sm-6">
                                    <label for="">Identificación del conductor (transportadora)</label>
                                    <input type="text" class="form-control" name="doc_conductor_t" id="doc_conductor_t"
                                        <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Placa del vehículo (transportadora)</label>
                                    <input type="text" class="form-control" name="placa_v_conductor_t"
                                        id="placa_v_conductor_t" <?php echo $disabled_carga ?>>
                                </div>

                                <div class="col-12 col-sm-6">
                                    <label for="">Observación</label>
                                    <input type="text" class="form-control" name="observacion4" id="observacion4"
                                        oninput="convertir_mayus()" <?php echo $disabled_carga ?>>
                                </div>

                                <div class="col-12 col-sm-12">
                                    <label for="">Adjuntar una o varias fotos</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" name="evidencias_despachos[]" class="form-control"
                                                id="inputGroupFile04" multiple>
                                            <label class="" for="inputGroupFile04"></label>
                                        </div>
                                        <div class="input-group-append">
                                            <button id="btn_agregar_datos" class="btn btn-success"
                                                onclick="guardar_form_4()" type="button">Guardar
                                                datos y
                                                evidencias <i class="fa fa-upload"></i></button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-12 col-sm-5">
                            <center>
                                <h6>Mercancia recibida</h6>
                            </center>
                            <div id="despacho_parcial_totals_servicios"></div>
                        </div>
                    </div>

                </form>
                <!-- form-group -->
            </section>
            <?php } ?>

            <h3>Transporte</h3>
            <section>
                <form id="form_2">
                    <input type="hidden" name="mensaje_correo"
                        value="SE HA ACTUALIZADO LA INFORMACIÓN DEL TRANSPORTE DEL SERVICIO <b>ITR EXPORTACIÓN</b>.">

                    <input type="hidden" name="id_servicio_form2" id="transporte" class="form-control"
                        value="<?php echo $id_servicio ?>">
                    <div class="row">

                        <div class="col-12 ">
                            <?php if ($id_servicio == 1) {
                                    echo '<label for="">Fecha y hora de retiro del producto (origen)</label>';
                                } else {
                                    echo '<label for="">Fecha y hora de cargue</label>';
                                }
                                ?>
                            <input type="datetime-local" value="<?php if ($servicios['fecha_r_p_p_o'] != null) {
                                                                        echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_r_p_p_o']));
                                                                    } else {
                                                                        echo "";
                                                                    } ?>" name="fecha_r_p_p_o" id="fecha_r_p_p_o"
                                class="form-control " placeholder="Seleccionar una fecha"
                                <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 ">
                            <label for="">Observación</label>
                            <input type="text" class="form-control" name="observacion2" id="observacion2"
                                value="<?php echo $servicios["observacion2"] ?>" <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-12">
                            <hr>
                            <a href="javascript:void(0)" onclick="guardar_form_2()" style="width: 40%;margin-top: -7px;"
                                class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                                <div class="ht-40 justify-content-between">
                                    <span class="pd-x-15">Actualizar datos</span>
                                    <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                                </div>
                            </a>

                        </div>
                    </div>
                </form>
                <!-- form-group -->

            </section>
            <h3>Ingreso a puerto</h3>
            <section>
                <form id="form_5" enctype="multipart/form-data">
                    <input type="hidden" name="mensaje_correo"
                        value="SE HA ACTUALIZADO LA INFORMACIÓN INGRESO A PUERTO DEL SERVICIO <b>ITR EXPORTACIÓN</b>.">

                    <input type="hidden" name="id_servicio_form5" id="id_servicio_form5" class="form-control"
                        value="<?php echo $id_servicio ?>">
                    <div class="row">


                        <div class="col-12 col-sm-6">
                            <label for="">Lugar de puerto destino</label>
                            <input type="text" class="form-control" name="lugar_inspeccion_vacio"
                                id="lugar_inspeccion_vacio" value="<?php echo $servicios["lugar_inspeccion_vacio"] ?>"
                                <?php echo $disabled_transporte ?>>
                        </div>

                        <div class="col-12 col-sm-6">
                            <label for="">Fecha y hora de cita de ingreso </label>
                            <input type="datetime-local" class="form-control " name="fecha_hora_inspeccion"
                                id="fecha_hora_inspeccion"
                                value="<?php
                                                                                                                                                    if ($servicios['fecha_hora_inspeccion'] == null) {
                                                                                                                                                    } else {
                                                                                                                                                        echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_inspeccion']));
                                                                                                                                                    }
                                                                                                                                                    ?>"
                                <?php echo $disabled_transporte ?>>
                        </div>

                        <div class="col-12 ">
                            <label for="">Número de orden de servicio</label>
                            <input type="text" class="form-control" name="dons" id="dons"
                                value="<?php echo $servicios["dons"] ?>" <?php echo $disabled_carga ?>>
                        </div>
                        <div class="col-12 ">
                            <label for="">Arim (Autorización)</label>
                            <input type="text" class="form-control" name="arim_retiro" id="arim_retiro"
                                value="<?php echo $servicios["arim_retiro"] ?>" <?php echo $disabled_carga ?>>
                        </div>

                        <div class="col-12 col-sm-12">

                            <label for="">Observaciones</label>
                            <input type="text" class="form-control" name="observacion3" id="observacion3"
                                value="<?php echo $servicios["observacion3"] ?>" <?php echo $disabled_transporte ?>>
                        </div>
                        <div class="col-12 col-sm-12">
                            <label for="">Adjuntar una o varias fotos</label>
                            <div class="input-group">

                                <div class="custom-file">

                                    <input type="file" name="evidencias_devolucion[]" class="form-control"
                                        id="inputGroupFile04" multiple="">
                                    <label class="" for="inputGroupFile04"></label>
                                </div>
                                <br>
                                <div class="input-group-append">
                                    <button class="btn btn-success" onclick="guardar_form_5()" type="button">Guardar
                                        datos y
                                        evidencias <i class="fa fa-upload"></i></button>
                                </div>
                            </div>
                            <a href="../../admin/evidencias_servicio?type=5&id_servicios=<?php echo $id_servicio ?>"
                                target="_blank">Ver
                                evidencias agregadas</a>
                        </div>


                    </div>
                </form>
            </section>

        </div>
    </div>
</div>

<?php
} else if ($id_tipo_servicio == 3) {
?>

<center>
    <h6 id="nombre_servicio"></h6>
    <hr>
</center>

<div style="padding: 0px;  height: 75vh !important; overflow-x: hidden;overflow-y: auto;">
    <div id="wizard3">
        <h3>Datos principales</h3>
        <section>
            <form id="form_editar_1">
                <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                <input type="hidden" name="mensaje_correo"
                    value="SE HA ACTUALIZADO LA INFORMACIÓN PRINCIPAL DEL SERVICIO <b>RETIRO DE CONTENEDOR VACÍO DE PUERTOS</b>.">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <label for="">Fecha recepción documento</label>

                        <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc" class="form-control"
                            placeholder="Seleccionar una fecha"
                            value="<?php if ($servicios['fecha_recepcion_doc'] == null) {
                                                                                                                                                                        echo "";
                                                                                                                                                                    } else {
                                                                                                                                                                        echo date('Y-m-d', strtotime($servicios["fecha_recepcion_doc"]));
                                                                                                                                                                    }
                                                                                                                                                                    ?>"
                            <?php echo $disabled_transporte ?>>
                    </div>
                    <div class=" col-12 col-sm-6">
                        <label for="">Cliente</label>
                        <select class="form-control" onchange="consultar_subcliente()" name="id_cliente" id="id_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                <?php
                                    $id_cliente = $servicios["id_cliente"];
                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                    $consultar_cliente->execute();
                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_cliente as $cliente) {
                                        echo $cliente["razon_social"];
                                    }
                                    ?>
                            </option>
                            <?php
                                foreach ($consultar_clientes as $clientes) {
                                ?>
                            <option value="<?php echo $clientes["id"] ?>"><?php echo $clientes["razon_social"] ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Subcliente</label>

                        <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                <?php
                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                    $consultar_sub_cliente->execute();
                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_sub_cliente as $sub_cliente) {
                                        echo $sub_cliente["nombre_sub_cliente"];
                                    }
                                    ?>

                            </option>
                        </select>

                    </div>
                    <!--<div class="col-12 col-sm-6">
                        <label for="">Booking/reserva</label>
                        <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                            class="form-control" <?php echo $disabled_transporte ?>>
                    </div>-->
                    <div class="col-12 col-sm-6">
                        <label for="">Tipo de servicio</label>
                        <select class="form-control"
                            oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)" name="tipo_servicio"
                            id="tipo_servicios" <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                <?php
                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                    $consultar_tipo_servicio->execute();
                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                        echo $tipo_servicio["nombre_servicio"];
                                    }

                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                            <option value="<?php echo $tipo_servicios['id'] ?>">
                                <?php echo $tipo_servicios['nombre_servicio'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>

                    <div class="col-12 col-sm-6">
                        <label for="">Linea naviera</label>
                        <input type="text" name="linea_naviera" value="<?php echo $servicios["linea_naviera"] ?>"
                            id="linea_naviera" class="form-control" <?php echo $disabled_transporte ?>>

                    </div>

                    <div class="col-12 col-sm-6" style="display:none">
                        <label for="">Tipo de carga</label>
                        <select class="form-control" name="tipo_carga" id="tipo_carga"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_carga"] ?>">
                                <?php
                                    $id_tipo_carga = $servicios["tipo_carga"];
                                    $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                    $consultar_tipo_carga->execute();
                                    $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_carga as $tipo_carga) {
                                        echo $tipo_carga["nombre_carga"];
                                    }
                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_cargas as $tipo_cargas) { ?>
                            <option value="<?php echo $tipo_cargas['id'] ?>" selected>
                                <?php echo $tipo_cargas['nombre_carga'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>
                    <div class="col-12 col-sm-6" id="tipo_contenedor">
                        <label for="">Tipo de contenedor</label>
                        <select name="tipo_contenedor" id="tipo_contenedor" class="form-control">
                            <option value="<?php echo $servicios["tipo_contenedor"] ?>">
                                <?php echo $servicios["tipo_contenedor"] ?></option>
                            <option value="HC">HC</option>
                            <option value="DRY">DRY</option>
                            <option value="OPEN TOP">OPEN TOP</option>
                            <option value="REEFER HC">REEFER HC</option>
                            <option value="REERFER">REERFER</option>
                            <option value="FALT RACK">FALT RACK</option>
                            <option value="TANQUE">TANQUE</option>

                        </select>
                    </div>
                    <div class="col-12 col-sm-6" id="tamaño_contenedor_1">
                        <label for="">Tamaño contenedor</label>
                        <select name="tamaño_contenedor" id="tamaño_contenedors" class="form-control"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tamaño_contenedor"] ?>">
                                <?php echo $servicios["tamaño_contenedor"] ?></option>
                            <option value="20">20</option>
                            <option value="40">40</option>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Número de contenedor</label>
                        <input type="text" name="contenedor" value="<?php echo $servicios["contenedor"] ?>"
                            id="contenedor" class="form-control" <?php echo $disabled_transporte ?>>
                    </div>


                    <div class="col-12 col-sm-6">
                        <label for="">Puerto de retiro del vacío</label>
                        <input type="text" name="puerto_origen" id="puerto_origen" class="form-control"
                            value="<?php echo $servicios["puerto_origen"] ?>" <?php echo $disabled_transporte ?>>

                    </div>

                    <div class="col-12 col-sm-6" id="lugar_devolucion_vacios">
                        <label for="">Lugar de devolución</label>
                        <input type="text" name="lugar_devolucion_vacio" id="lugar_devolucion_vacio"
                            value="<?php echo $servicios["lugar_devolucion_vacio"] ?>" class="form-control"
                            <?php echo $disabled_transporte ?>>

                    </div>
                    <div class="col-12 col-sm-6" id="fecha_devolucion_vacios">
                        <label for="">Fecha limite de devolución</label>
                        <input type="datetime-local" class="form-control " name="fecha_limite_devolucion"
                            id="fecha_limite_devolucion" <?php echo $disabled_transporte ?>
                            value="<?php $servicios['fecha_limite_devolucion'] = preg_replace("/\s/", 'T', $servicios['fecha_limite_devolucion']);
                                                                                                                                                                                        echo $servicios['fecha_limite_devolucion'] ?>">
                    </div>


                    <?php if ($servicios["lugar_entrega"] != ""  && $servicios["fecha_lugar_entrega_v"] != "0000-00-00") { ?>
                    <div class="col-12 col-sm-6" id="lugar_descargue">
                        <label for="">Lugar de entrega del vacío</label>
                        <input type="text" name="lugar_entrega" id="lugar_descargue"
                            value="<?php echo $servicios["lugar_entrega"] ?>" class="form-control"
                            <?php echo $disabled_transporte ?>>

                    </div>

                    <div class="col-12 col-sm-6" id="fecha_devolucion_v">
                        <label for="">Fecha de entrega del vacío</label>
                        <input type="date" class="form-control"
                            value="<?php echo $servicios["fecha_lugar_entrega_v"] ?>" name="fecha_lugar_entrega_v"
                            id="fecha_devolucion_v" <?php echo $disabled_transporte ?>>
                    </div>
                    <?php  } ?>

                    <div class="col-12 col-sm-6">
                        <label for="">Observación</label>
                        <input type="text" name="observacion1" id="observacion1"
                            value="<?php echo $servicios["observacion1"] ?>" class="form-control">
                    </div>

                    <div class="col-12 col-sm-6">

                        <a href="javascript:void(0)" onclick="actualizar_form_1()" style="width: 100%; margin-top:15px;"
                            class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                            <div class="ht-40 justify-content-between">
                                <span class="pd-x-15">Actualizar</span>
                                <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                            </div>
                        </a>

                    </div>
                </div>
            </form>
        </section>
        <h3>Más información</h3>
        <section>
            <form id="formulario_servicio_2">
                <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                <input type="hidden" name="mensaje_correo"
                    value="SE HA ACTUALIZADO/AGREGADO LA INFORMACIÓN DE RETIRO PUERTO DEL VACÍO DEL SERVICIO <b>RETIRO DE CONTENEDOR VACÍO DE PUERTOS</b>.">
                <div class="col-12 " id="fecha_hora_r_p">
                    <label for="">Fecha y hora retiro puerto del vacío</label>
                    <input type="datetime-local" class="form-control " name="fecha_hora_r_p" id="fecha_hora_r_p"
                        value="<?php $servicios['fecha_hora_r_p'] = preg_replace("/\s/", 'T', $servicios['fecha_hora_r_p']);
                                                                                                                            echo $servicios['fecha_hora_r_p'] ?>" <?php echo $disabled_transporte ?>>
                </div>
                <div class="col-12 ">
                    <label for="">Arim retiro</label>
                    <input type="text" class="form-control" name="arim_retiro" id="arim_retiro"
                        value="<?php echo $servicios["arim_retiro"] ?>" <?php echo $disabled_carga ?>>
                </div>


                <div class="col-12 ">
                    <label for="">Nombre conductor (transportadora)</label>
                    <input type="text" class="form-control" name="nombre_c_p_a" id="nombre_conductor_t"
                        value="<?php echo $servicios["nombre_c_p_a"] ?>" <?php echo $disabled_carga ?>>
                </div>
                <div class="col-12">
                    <label for="">Identificación del conductor (transportadora)</label>
                    <input type="text" class="form-control" name="identificacion_c_p_a" id="doc_conductor_t"
                        value="<?php echo $servicios["identificacion_c_p_a"] ?>" <?php echo $disabled_carga ?>>
                </div>
                <div class="col-12 ">
                    <label for="">Placa del vehículo (transportadora)</label>
                    <input type="text" class="form-control" name="placa_v_p_a" id="placa_v_conductor_t"
                        value="<?php echo $servicios["placa_v_p_a"] ?>" <?php echo $disabled_carga ?>>
                </div>
                <div class="col-12 ">
                    <label for="">¿Contenedor bajado a piso?</label>
                    <select name="contenedor_bajado_piso_fecha" id="contenedor_bajado_piso_fecha" class="form-control"
                        onchange="ejecutar_contenedor_bajado_piso_fecha()">
                        <option value="<?php if ($servicios["contenedor_bajado_piso_fecha"] == 1) {
                                                echo "1";
                                            } else if ($servicios["contenedor_bajado_piso_fecha"] == 0) {
                                                echo "0";
                                            } else {
                                                echo "";
                                            } ?>" selected>
                            <?php if ($servicios["contenedor_bajado_piso_fecha"] == 1) {
                                    echo "Sí";
                                } else if ($servicios["contenedor_bajado_piso_fecha"] == 0) {
                                    echo "No";
                                } else {
                                    echo "Seleccione una opción";
                                }
                                ?>
                        </option>
                        <option value="1">Sí</option>
                        <option value="0">No</option>
                    </select>
                    <p id="campos_bajado_piso_fecha"></p>
                </div>

                <div class="col-12 ">
                    <label for="">¿Contenedor debe ser inspeccionado?</label>
                    <select name="contenedor_inspeccion" id="contenedor_inspeccion" class="form-control"
                        onchange="ejecutar_contenedor_inspeccion()">
                        <option value="<?php if ($servicios["contenedor_inspeccion"] == 1) {
                                                echo "1";
                                            } else if ($servicios["contenedor_inspeccion"] == 0) {
                                                echo "0";
                                            } else {
                                                echo "";
                                            } ?>" selected>
                            <?php if ($servicios["contenedor_inspeccion"] == 1) {
                                    echo "Sí";
                                } else if ($servicios["contenedor_inspeccion"] == 0) {
                                    echo "No";
                                } else {
                                    echo "Seleccione  una opción";
                                }
                                ?>
                        </option>
                        <option value="1">Sí</option>
                        <option value="0">No</option>
                    </select>
                    <p id="campos_contenedor_inspeccion"></p>
                </div>

                <div class="col-12 ">

                    <a href="javascript:void(0)" onclick="actualizar_servicios(2)" style="width: 100%; margin-top:15px;"
                        class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                        <div class="ht-40 justify-content-between">
                            <span class="pd-x-15">Guardar/actualizar</span>
                            <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                        </div>
                    </a>

                </div>
            </form>
        </section>
        <h3>Más información</h3>
        <section>
            <form id="formulario_servicio_3">
                <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">

                <input type="hidden" name="mensaje_correo"
                    value="SE HA ACTUALIZADO/AGREGADO LA INFORMACIÓN SOBRE LA DEVOLUCIÓN EN PUERTO DEL SERVICIO <b>RETIRO DE CONTENEDOR VACÍO DE PUERTOS</b>.">

                <div class="col-12 ">
                    <label for="">¿Lugar devolución en puerto?</label>
                    <select name="lugar_devolucion_puerto" id="lugar_devolucion_puerto" class="form-control"
                        onchange="ejecutar_lugar_devolucion()">
                        <option value="<?php if ($servicios["lugar_devolucion_puerto"] == 1) {
                                                echo "1";
                                            } else if ($servicios["lugar_devolucion_puerto"] == 0) {
                                                echo "0";
                                            } else {
                                                echo " ";
                                            } ?>" selected>
                            <?php if ($servicios["lugar_devolucion_puerto"] == 1) {
                                    echo "Sí";
                                } else if ($servicios["lugar_devolucion_puerto"] == 0) {
                                    echo "No";
                                } else {
                                    echo "Seleccione  una opción";
                                }
                                ?>
                        </option>
                        <option value="1">Sí</option>
                        <option value="0">No</option>
                    </select>
                    <p id="campos_lugar_devolucion"></p>
                </div>
                <!-- <div class="col-12 ">
                    <label for="">Observación</label>
                    <input type="text" class="form-control" name="observacion3" id="observacion3"
                        value="<?php echo $servicios["observacion3"] ?>" <?php echo $disabled_carga ?>>
                </div>-->
                <div class="col-12" style="width: 100%; margin-top:15px;">
                    <a href="javascript:void(0)" onclick="actualizar_servicios(3)" style="width: 100%; margin-top:15px;"
                        class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                        <div class="ht-40 justify-content-between">
                            <span class="pd-x-15">Guardar/actualizar</span>
                            <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                        </div>
                    </a>

                </div>
            </form>
        </section>
    </div>
</div>

<?php
} else if ($id_tipo_servicio == 4) {
?>
<center>
    <h6 id="nombre_servicio"></h6>
    <hr>
</center>

<div style="padding: 0px;  height: 80vh !important; overflow-x: hidden;overflow-y: auto;">
    <div id="wizard3">
        <h3>Datos principales </h3>
        <section>
            <form id="form_editar_1">

                <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                <input type="hidden" name="mensaje_correo"
                    value="SE HA ACTUALIZADO LA INFORMACIÓN PRINCIPAL DEL SERVICIO <b>INGRESO DE CONTENEDOR VACÍO DE PUERTOS</b>.">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <label for="">Fecha recepción documento</label>

                        <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc" class="form-control"
                            placeholder="Seleccionar una fecha"
                            value="<?php
                                                                                                                                                                    if ($servicios['fecha_recepcion_doc'] == null) {
                                                                                                                                                                    } else {
                                                                                                                                                                        echo date('Y-m-d', strtotime($servicios["fecha_recepcion_doc"]));
                                                                                                                                                                    }
                                                                                                                                                                    ?>"
                            <?php echo $disabled_transporte ?>>
                    </div>
                    <div class=" col-12 col-sm-6">
                        <label for="">Cliente</label>
                        <select class="form-control" onchange="consultar_subcliente()" name="id_cliente" id="id_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                <?php
                                    $id_cliente = $servicios["id_cliente"];
                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                    $consultar_cliente->execute();
                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_cliente as $cliente) {
                                        echo $cliente["razon_social"];
                                    }
                                    ?>
                            </option>
                            <?php
                                foreach ($consultar_clientes as $clientes) {
                                ?>
                            <option value="<?php echo $clientes["id"] ?>"><?php echo $clientes["razon_social"] ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Subcliente</label>

                        <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                <?php
                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                    $consultar_sub_cliente->execute();
                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_sub_cliente as $sub_cliente) {
                                        echo $sub_cliente["nombre_sub_cliente"];
                                    }
                                    ?>

                            </option>
                        </select>

                    </div>

                    <div class="col-12 col-sm-6">
                        <label for="">Booking/reserva</label>
                        <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                            class="form-control" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Tipo de servicio</label>
                        <select class="form-control"
                            oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)" name="tipo_servicio"
                            id="tipo_servicios" <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                <?php
                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                    $consultar_tipo_servicio->execute();
                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                        echo $tipo_servicio["nombre_servicio"];
                                    }

                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                            <option value="<?php echo $tipo_servicios['id'] ?>">
                                <?php echo $tipo_servicios['nombre_servicio'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Linea naviera</label>
                        <input type="text" name="linea_naviera" value="<?php echo $servicios["linea_naviera"] ?>"
                            id="linea_naviera" class="form-control" <?php echo $disabled_transporte ?>>

                    </div>


                    <div class="col-12 col-sm-6" id="tipo_contenedor">
                        <label for="">Tipo de contenedor</label>
                        <select name="tipo_contenedor" id="tipo_contenedor" class="form-control">
                            <option value="<?php echo $servicios["tipo_contenedor"] ?>">
                                <?php echo $servicios["tipo_contenedor"] ?></option>
                            <option value="HC">HC</option>
                            <option value="DRY">DRY</option>
                            <option value="OPEN TOP">OPEN TOP</option>
                            <option value="REEFER HC">REEFER HC</option>
                            <option value="REERFER">REERFER</option>
                            <option value="FALT RACK">FALT RACK</option>
                            <option value="TANQUE">TANQUE</option>

                        </select>
                    </div>
                    <div class="col-12 col-sm-6" id="tamaño_contenedor_1">
                        <label for="">Tamaño contenedor</label>
                        <select name="tamaño_contenedor" id="tamaño_contenedors" class="form-control"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tamaño_contenedor"] ?>">
                                <?php echo $servicios["tamaño_contenedor"] ?></option>
                            <option value="20">20</option>
                            <option value="40">40</option>
                        </select>
                    </div>

                    <div class="col-12 col-sm-6">
                        <label for="">Número de contenedor</label>
                        <input type="text" name="contenedor" value="<?php echo $servicios["contenedor"] ?>"
                            id="contenedor" class="form-control" <?php echo $disabled_transporte  ?>>
                    </div>


                    <div class="col-12 col-sm-6" id="patio_retiro">
                        <label for="">Patio retiro del vacío</label>
                        <input type="text" name="patio_retiro_vacio" id="patio_retiro_vacio" class="form-control"
                            <?php echo $disabled_transporte ?> value="<?php echo $servicios["patio_retiro_vacio"] ?>">
                    </div>

                    <div class="col-12 col-sm-6" id="fecha_puerto_entrega_vacio">
                        <label for="">Fecha de entrega del vacío</label>
                        <input type="date" class="form-control"
                            value="<?php echo $servicios["fecha_puerto_entrega_vacio"] ?>"
                            name="fecha_puerto_entrega_vacio" id="fecha_puerto_entrega_vacio"
                            <?php echo $disabled_transporte ?>>
                    </div>

                    <div class="col-12 col-sm-6">
                        <label for="">Observación</label>
                        <input type="text" name="observacion1" id="observacion1"
                            value="<?php echo $servicios["observacion1"] ?>" class="form-control">
                    </div>


                    <div class="col-12 col-sm-6">

                        <a href="javascript:void(0)" onclick="actualizar_form_1()" style="width: 100%; margin-top:15px;"
                            class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                            <div class="ht-40 justify-content-between">
                                <span class="pd-x-15">Actualizar</span>
                                <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                            </div>
                        </a>

                    </div>
                </div>
            </form>
        </section>
        <h3>Más información</h3>
        <section>
            <form id="formulario_servicio_2">
                <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                <input type="hidden" name="mensaje_correo"
                    value="SE HA ACTUALIZADO/AGREGADO LA INFORMACIÓN SOBRE EL RETIRO PUERTO DEL VACÍO DEL SERVICIO <b>INGRESO DE CONTENEDOR VACÍO DE PUERTOS</b>.">
                <div class="col-12 " id="fecha_hora_r_p">
                    <label for="">Fecha y hora retiro puerto de vacío</label>
                    <input type="datetime-local" class="form-control " name="fecha_hora_r_p" id="fecha_hora_r_p"
                        value="<?php $servicios['fecha_hora_r_p'] = preg_replace("/\s/", 'T', $servicios['fecha_hora_r_p']);
                                                                                                                            echo $servicios['fecha_hora_r_p'] ?>" <?php echo $disabled_transporte ?>>
                </div>

                <div class="col-12 ">
                    <label for="">Número de orden de servicio</label>
                    <input type="text" class="form-control" name="dons" id="dons"
                        value="<?php echo $servicios["dons"] ?>" <?php echo $disabled_carga ?>>
                </div>

                <div class="col-12 ">
                    <label for="">Nombre conductor (transportadora)</label>
                    <input type="text" class="form-control" name="nombre_c_p_a" id="nombre_conductor_t"
                        value="<?php echo $servicios["nombre_c_p_a"] ?>" <?php echo $disabled_carga ?>>
                </div>
                <div class="col-12">
                    <label for="">Identificación del conductor (transportadora)</label>
                    <input type="text" class="form-control" name="identificacion_c_p_a" id="doc_conductor_t"
                        value="<?php echo $servicios["identificacion_c_p_a"] ?>" <?php echo $disabled_carga ?>>
                </div>
                <div class="col-12 ">
                    <label for="">Placa del vehículo (transportadora)</label>
                    <input type="text" class="form-control" name="placa_v_p_a" id="placa_v_conductor_t"
                        value="<?php echo $servicios["placa_v_p_a"] ?>" <?php echo $disabled_carga ?>>
                </div>

                <div class="col-12 ">
                    <label for="">¿Contenedor bajado a piso?</label>
                    <select name="contenedor_bajado_piso_fecha" id="contenedor_bajado_piso_fecha" class="form-control"
                        onchange="ejecutar_contenedor_bajado_piso_fecha()">
                        <option value="<?php if ($servicios["contenedor_bajado_piso_fecha"] == 1) {
                                                echo "1";
                                            } else if ($servicios["contenedor_bajado_piso_fecha"] == 0) {
                                                echo "0";
                                            } else {
                                                echo "";
                                            } ?>" selected>
                            <?php if ($servicios["contenedor_bajado_piso_fecha"] == 1) {
                                    echo "Sí";
                                } else if ($servicios["contenedor_bajado_piso_fecha"] == 0) {
                                    echo "No";
                                } else {
                                    echo "Seleccione una opción";
                                }
                                ?>
                        </option>
                        <option value="1">Sí</option>
                        <option value="0">No</option>
                    </select>
                    <p id="campos_bajado_piso_fecha"></p>
                </div>

                <div class="col-12 ">
                    <label for=""> ¿Viaje fallido?</label>
                    <select name="viaje_fallido" onchange="observacion_viaje()" id="viaje_fallido" class="form-control">
                        <option value="<?php if ($servicios["viaje_fallido"] == 1) {
                                                echo "1";
                                            } else if ($servicios["viaje_fallido"] == 0) {
                                                echo "0";
                                            } else if ($servicios["viaje_fallido"] == null) {
                                                echo "";
                                            } ?>" selected>
                            <?php if ($servicios["viaje_fallido"] == 1) {
                                    echo "Sí";
                                } else if ($servicios["viaje_fallido"] == 0) {
                                    echo "No";
                                } else {
                                    echo "Seleccione una opción";
                                } ?>
                        </option>
                        <option value="1">Sí</option>
                        <option value="0">No</option>
                    </select>
                    <p id="observacion2_campo"></p>
                </div>
                <div class="col-12 ">
                    <label for="">Evidencias</label>
                    <div class="custom-file"><input type="file" name="evidencias[]" class="form-control"
                            id="inputGroupFile04" multiple=""><a
                            href="../../admin/evidencias_servicio?type=3&id_servicios=<?php echo $servicios['id'] ?>"
                            target="_blank">Ver evidencias agregadas</a></div>

                </div>
                <div class="col-12 ">

                    <a href="javascript:void(0)" onclick="actualizar_servicios(2)" style="width: 100%; margin-top:15px;"
                        class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                        <div class="ht-40 justify-content-between">
                            <span class="pd-x-15">Guardar/actualizar</span>
                            <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                        </div>
                    </a>

                </div>
            </form>
        </section>
        <h3>Más información</h3>
        <section>
            <form id="formulario_servicio_3">
                <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                <input type="hidden" name="mensaje_correo"
                    value="SE HA ACTUALIZADO/AGREGADO LA INFORMACIÓN SOBRE EL INGRESO PUERTO DEL SERVICIO <b>INGRESO DE CONTENEDOR VACÍO DE PUERTOS</b>.">
                <div class="col-12" id="fecha_hora_r_p">
                    <label for="">Fecha y hora de ingreso puerto</label>
                    <input type="datetime-local" class="form-control " name="fecha_ingreso_puerto"
                        id="fecha_ingreso_puerto"
                        value="<?php $servicios['fecha_ingreso_puerto'] = preg_replace("/\s/", 'T', $servicios['fecha_ingreso_puerto']);
                                                                                                                                        echo $servicios['fecha_ingreso_puerto'] ?>"
                        <?php echo $disabled_transporte ?>>
                </div>
                <div class="col-12">
                    <label for="">Puerto destino</label>
                    <input type="text" name="puerto_entrega_vacio" id="puerto_entrega_vacio" class="form-control"
                        value="<?php echo $servicios["puerto_entrega_vacio"] ?>" <?php echo $disabled_transporte ?>>

                </div>

                <div class="col-12">
                    <label for="">Arim de ingreso</label> <input type="text" class="form-control" id="arim" name="arim"
                        value="<?php echo $servicios["arim"] ?>">
                </div>
                <div class="col-12">
                    <label for="">Nombre conductor (transportadora)</label>
                    <input type="text" class="form-control" name="nombre_c_e_v" id="nombre_conductor_t"
                        value="<?php echo $servicios["nombre_c_e_v"] ?>" <?php echo $disabled_carga ?>>
                </div>
                <div class="col-12">
                    <label for="">Identificación del conductor (transportadora)</label>
                    <input type="text" class="form-control" name="identificacion_c_e_v" id="doc_conductor_t"
                        value="<?php echo $servicios["identificacion_c_e_v"] ?>" <?php echo $disabled_carga ?>>
                </div>
                <div class="col-12">
                    <label for="">Placa del vehículo (transportadora)</label>
                    <input type="text" class="form-control" name="placa_v_e_v" id="placa_v_conductor_t"
                        value="<?php echo $servicios["placa_v_e_v"] ?>" <?php echo $disabled_carga ?>>
                </div>
                <div class="col-12">
                    <label for="">Evidencias</label>
                    <div class="custom-file"><input type="file" name="evidencias_devolucion[]" class="form-control"
                            id="inputGroupFile04" multiple=""><a
                            href="../../admin/evidencias_servicio?type=5&id_servicios=<?php echo $servicios['id'] ?>"
                            target="_blank">Ver evidencias agregadas</a></div>
                </div>
                <div class="col-12" style="width: 100%; margin-top:15px;">
                    <a href="javascript:void(0)" onclick="actualizar_servicios(3)" style="width: 100%; margin-top:15px;"
                        class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                        <div class="ht-40 justify-content-between">
                            <span class="pd-x-15">Guardar/actualizar</span>
                            <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                        </div>
                    </a>

                </div>
            </form>
        </section>
    </div>


</div>
<?php } else if ($id_tipo_servicio == 5) { ?>
<center>
    <h6 id="nombre_servicio"></h6>
    <hr>
</center>
<div style="padding: 0px;  height: 80vh !important; overflow-x: hidden;overflow-y: auto;">

    <div id="wizard3">
        <h3>Datos principales</h3>
        <section>
            <form id="form_editar_1">

                <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                <input type="hidden" name="mensaje_correo"
                    value="SE HA ACTUALIZADO LA INFORMACIÓN PRINCIPAL DEL SERVICIO <b>TRASLADO DE CARGA CONTENERIZADA</b>.">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <label for="">Fecha recepción documento</label>

                        <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc" class="form-control"
                            placeholder="Seleccionar una fecha"
                            value="<?php
                                                                                                                                                                    if ($servicios['fecha_recepcion_doc'] == null) {
                                                                                                                                                                    } else {
                                                                                                                                                                        echo date('Y-m-d', strtotime($servicios["fecha_recepcion_doc"]));
                                                                                                                                                                    }
                                                                                                                                                                    ?>"
                            <?php echo $disabled_transporte ?>>
                    </div>
                    <div class=" col-12 col-sm-6">
                        <label for="">Cliente</label>
                        <select class="form-control" onchange="consultar_subcliente()" name="id_cliente" id="id_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                <?php
                                    $id_cliente = $servicios["id_cliente"];
                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                    $consultar_cliente->execute();
                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_cliente as $cliente) {
                                        echo $cliente["razon_social"];
                                    }
                                    ?>
                            </option>
                            <?php
                                foreach ($consultar_clientes as $clientes) {
                                ?>
                            <option value="<?php echo $clientes["id"] ?>"><?php echo $clientes["razon_social"] ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Subcliente</label>

                        <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                <?php
                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                    $consultar_sub_cliente->execute();
                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_sub_cliente as $sub_cliente) {
                                        echo $sub_cliente["nombre_sub_cliente"];
                                    }
                                    ?>

                            </option>
                        </select>

                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Do/Ns</label>
                        <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                            class="form-control" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Tipo de servicio</label>
                        <select class="form-control"
                            oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)" name="tipo_servicio"
                            id="tipo_servicios" <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                <?php
                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                    $consultar_tipo_servicio->execute();
                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                        echo $tipo_servicio["nombre_servicio"];
                                    }

                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                            <option value="<?php echo $tipo_servicios['id'] ?>">
                                <?php echo $tipo_servicios['nombre_servicio'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>

                    <div class="col-12 col-sm-6" style="display:none">
                        <label for="">Tipo de carga</label>
                        <select class="form-control" name="tipo_carga" id="tipo_carga"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_carga"] ?>">
                                <?php
                                    $id_tipo_carga = $servicios["tipo_carga"];
                                    $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                    $consultar_tipo_carga->execute();
                                    $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_carga as $tipo_carga) {
                                        echo $tipo_carga["nombre_carga"];
                                    }
                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_cargas as $tipo_cargas) { ?>
                            <option value="<?php echo $tipo_cargas['id'] ?>" selected>
                                <?php echo $tipo_cargas['nombre_carga'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>

                    <div class="col-12 col-sm-6" id="tipo_contenedor">
                        <label for="">Tipo de contenedor</label>
                        <select name="tipo_contenedor" id="tipo_contenedor" class="form-control">
                            <option value="<?php echo $servicios["tipo_contenedor"] ?>">
                                <?php echo $servicios["tipo_contenedor"] ?></option>
                            <option value="HC">HC</option>
                            <option value="DRY">DRY</option>
                            <option value="OPEN TOP">OPEN TOP</option>
                            <option value="REEFER HC">REEFER HC</option>
                            <option value="REERFER">REERFER</option>
                            <option value="FALT RACK">FALT RACK</option>
                            <option value="TANQUE">TANQUE</option>

                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Contenedor</label>
                        <input type="text" name="contenedor" value="<?php echo $servicios["contenedor"] ?>"
                            id="contenedor" class="form-control" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6" id="tamaño_contenedor_1">
                        <label for="">Tamaño contenedor</label>
                        <select name="tamaño_contenedor" id="tamaño_contenedor" class="form-control"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tamaño_contenedor"] ?>">
                                <?php echo $servicios["tamaño_contenedor"] ?></option>
                            <option value="20">20</option>
                            <option value="40">40</option>
                        </select>
                    </div>


                    <div class="col-12 col-sm-6" id="puerto_origen">
                        <label for="">Puerto de retiro del vacío</label>
                        <input type="text" name="puerto_origen" id="puerto_origen" class="form-control"
                            value="<?php echo $servicios["puerto_origen"] ?>" <?php echo $disabled_transporte ?>>

                    </div>
                    <div class="col-12 col-sm-6" id="fecha_hora_r_p">
                        <label for="">Fecha retiro puerto del vacío</label>
                        <input type="datetime-local" class="form-control "
                            value="<?php echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_r_p'])); ?>"
                            name="fecha_hora_r_p" id="fecha_hora_r_p" <?php echo $disabled_transporte ?>>
                    </div>

                    <div class="col-12 col-sm-6" id="lugar_entrega_zona_f_1">
                        <label for="">Lugar de entrega zona franca</label>
                        <input type="text" name="lugar_entrega_zona_f" id="lugar_entrega_zona_f" class="form-control"
                            value="<?php echo $servicios["lugar_entrega_zona_f"] ?>" <?php echo $disabled_transporte ?>>

                    </div>

                    <div class="col-12 col-sm-6" id="fecha_entrega_zona_f_1">
                        <label for="">Fecha de entrega en zona franca</label>
                        <input type="date" class="form-control " name="fecha_entrega_zona_f" id="fecha_entrega_zona_f"
                            value="<?php echo $servicios["fecha_entrega_zona_f"] ?>" <?php echo $disabled_transporte ?>>
                    </div>

                    <div class="col-12 col-sm-6">
                        <label for="">Limite de devolución del vacío</label>
                        <input type="datetime-local" id="fecha_limite_devolucion" name="fecha_limite_devolucion"
                            class="form-control" placeholder="Seleccionar una fecha"
                            value="<?php
                                                                                                                                                                                        if ($servicios['fecha_limite_devolucion'] == null) {
                                                                                                                                                                                        } else {
                                                                                                                                                                                            echo date('Y-m-d H:i:s', strtotime($servicios["fecha_limite_devolucion"]));
                                                                                                                                                                                        }
                                                                                                                                                                                        ?>"
                            <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Observación</label>
                        <input type="text" name="observacion1" id="observacion1"
                            value="<?php echo $servicios["observacion1"] ?>" class="form-control">
                    </div>
                    <div class="col-12 col-sm-6">

                        <a href="javascript:void(0)" onclick="actualizar_form_1()" style="width: 100%; margin-top:15px;"
                            class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                            <div class="ht-40 justify-content-between">
                                <span class="pd-x-15">Actualizar</span>
                                <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                            </div>
                        </a>

                    </div>
                </div>
            </form>
        </section>
        <h3>Más información</h3>
        <section>
            <form id="formulario_servicio_2">
                <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                <input type="hidden" name="mensaje_correo"
                    value="SE HA ACTUALIZADO/AGREGADO LA INFORMACIÓN SOBRE LA LLEGADA AL DESTINO DEL SERVICIO <b>TRASLADO DE CARGA CONTENERIZADA</b>.">

                <div class="col-12">
                    <label for="">Nombre conductor (transportadora)</label>
                    <input type="text" class="form-control" name="nombre_c_p_a" id="nombre_conductor_t"
                        value="<?php echo $servicios["nombre_c_p_a"] ?>" <?php echo $disabled_carga ?>>
                </div>
                <div class="col-12">
                    <label for="">Identificación del conductor (transportadora)</label>
                    <input type="text" class="form-control" name="identificacion_c_p_a" id="doc_conductor_t"
                        value="<?php echo $servicios["identificacion_c_p_a"] ?>" <?php echo $disabled_carga ?>>
                </div>
                <div class="col-12 ">
                    <label for="">Placa del vehículo (transportadora)</label>
                    <input type="text" class="form-control" name="placa_v_p_a" id="placa_v_conductor_t"
                        value="<?php echo $servicios["placa_v_p_a"] ?>" <?php echo $disabled_carga ?>>
                </div>
                <div class="col-12 " id="fecha_hora_incio_descargue">
                    <label for="">Fecha y hora de llegada a destino</label>
                    <input type="datetime-local" class="form-control " name="fecha_hora_incio_descargue"
                        id="fecha_hora_incio_descargue"
                        value="<?php $servicios['fecha_hora_incio_descargue'] = preg_replace("/\s/", 'T', $servicios['fecha_hora_incio_descargue']);
                                                                                                                                                    echo $servicios['fecha_hora_incio_descargue'] ?>"
                        <?php echo $disabled_transporte ?>>
                </div>
                <div class="col-12 " id="fecha_hora_terminacion_descargue">
                    <label for="">Fecha y hora finalización</label>
                    <input type="datetime-local" class="form-control " name="fecha_hora_terminacion_descargue"
                        id="fecha_hora_terminacion_descargue"
                        value="<?php $servicios['fecha_hora_terminacion_descargue'] = preg_replace("/\s/", 'T', $servicios['fecha_hora_terminacion_descargue']);
                                                                                                                                                                echo $servicios['fecha_hora_terminacion_descargue'] ?>"
                        <?php echo $disabled_transporte ?>>
                </div>



                <div class="col-12 ">
                    <label for="">¿Contenedor bajado a piso?</label>
                    <select name="contenedor_bajado_piso_fecha" id="contenedor_bajado_piso_fecha" class="form-control"
                        onchange="ejecutar_contenedor_bajado_piso_fecha()">
                        <option value="<?php if ($servicios["contenedor_bajado_piso"] == 1) {
                                                echo "1";
                                            } else if ($servicios["contenedor_bajado_piso"] == 0) {
                                                echo "0";
                                            } else {
                                                echo "";
                                            } ?>" selected>
                            <?php if ($servicios["contenedor_bajado_piso"] == 1) {
                                    echo "Sí";
                                } else if ($servicios["contenedor_bajado_piso"] == 0) {
                                    echo "No";
                                } else {
                                    echo "Seleccione una opción";
                                }
                                ?>
                        </option>
                        <option value="1">Sí</option>
                        <option value="0">No</option>
                    </select>
                    <p id="campos_bajado_piso_fecha"></p>
                </div>

                <div class="col-12 ">
                    <label for="">Evidencias</label>
                    <div class="custom-file"><input type="file" name="evidencias[]" class="form-control"
                            id="inputGroupFile04" multiple=""><a
                            href="../../admin/evidencias_servicio?type=3&id_servicios=<?php echo $servicios['id'] ?>"
                            target="_blank">Ver evidencias agregadas</a>
                    </div>
                </div>

                <div class="col-12 ">

                    <a href="javascript:void(0)" onclick="actualizar_servicios(2)" style="width: 100%; margin-top:15px;"
                        class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                        <div class="ht-40 justify-content-between">
                            <span class="pd-x-15">Guardar/actualizar</span>
                            <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                        </div>
                    </a>

                </div>

            </form>
        </section>
        <h3>Más información</h3>
        <section>
            <form id="formulario_servicio_3">
                <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                <input type="hidden" name="mensaje_correo"
                    value="SE HA ACTUALIZADO/AGREGADO LA INFORMACIÓN SOBRE LA DEVOLUCIÓN DEL VACÍO DEL SERVICIO <b>TRASLADO DE CARGA CONTENERIZADA</b>.">

                <div class="col-12 ">
                    <label for="">¿Contenedor debe ser inspeccionado?</label>
                    <select name="contenedor_inspeccion" id="contenedor_inspeccion" class="form-control"
                        onchange="ejecutar_contenedor_inspeccion()">
                        <option value="<?php if ($servicios["contenedor_inspeccion"] == 1) {
                                                echo "1";
                                            } else if ($servicios["contenedor_inspeccion"] == 0) {
                                                echo "0";
                                            } else {
                                                echo "";
                                            } ?>" selected>
                            <?php if ($servicios["contenedor_inspeccion"] == 1) {
                                    echo "Sí";
                                } else if ($servicios["contenedor_inspeccion"] == 0) {
                                    echo "No";
                                } else {
                                    echo "Seleccione  una opción";
                                }
                                ?>
                        </option>
                        <option value="1">Sí</option>
                        <option value="0">No</option>
                    </select>
                    <p id="campos_contenedor_inspeccion"></p>
                </div>

                <div class="col-12 ">
                    <label for="">¿Contenedor bajado a piso?</label>
                    <select name="contenedor_bajado_piso_1" id="contenedor_bajado_piso_1" class="form-control"
                        onchange="ejecutar_contenedor_bajado_piso_fecha_1()">
                        <option value="<?php if ($servicios["contenedor_bajado_piso_1"] == 1) {
                                                echo "1";
                                            } else if ($servicios["contenedor_bajado_piso_1"] == 0) {
                                                echo "0";
                                            } else {
                                                echo "";
                                            } ?>" selected>
                            <?php if ($servicios["contenedor_bajado_piso_1"] == 1) {
                                    echo "Sí";
                                } else if ($servicios["contenedor_bajado_piso_1"] == 0) {
                                    echo "No";
                                } else {
                                    echo "Seleccione una opción";
                                }
                                ?>
                        </option>
                        <option value="1">Sí</option>
                        <option value="0">No</option>
                    </select>
                    <p id="campos_bajado_piso_fecha_1"></p>
                </div>


                <div class="col-12 ">
                    <label for="">¿Lugar devolución vacío?</label>
                    <select name="lugar_devolucion_puerto" id="lugar_devolucion_puerto" class="form-control"
                        onchange="ejecutar_lugar_devolucion()">
                        <option value="<?php if ($servicios["lugar_devolucion_puerto"] == 1) {
                                                echo "1";
                                            } else if ($servicios["lugar_devolucion_puerto"] == 0) {
                                                echo "0";
                                            } else {
                                                echo " ";
                                            } ?>" selected>
                            <?php if ($servicios["lugar_devolucion_puerto"] == 1) {
                                    echo "Sí";
                                } else if ($servicios["lugar_devolucion_puerto"] == 0) {
                                    echo "No";
                                } else {
                                    echo "Seleccione  una opción";
                                }
                                ?>
                        </option>
                        <option value="1">Sí</option>
                        <option value="0">No</option>
                    </select>
                    <p id="campos_lugar_devolucion"></p>
                </div>
                <div class="col-12">
                    <label for="">Observación</label>
                    <input type="text" class="form-control" name="observacion3" id="observacion3"
                        value="<?php echo $servicios["observacion3"] ?>" <?php echo $disabled_carga ?>>
                </div>
                <div class="col-12" style="width: 100%; margin-top:15px;">
                    <a href="javascript:void(0)" onclick="actualizar_servicios(3)" style="width: 100%; margin-top:15px;"
                        class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                        <div class="ht-40 justify-content-between">
                            <span class="pd-x-15">Guardar/actualizar</span>
                            <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                        </div>
                    </a>

                </div>
            </form>
        </section>
    </div>
</div>


<?php } else if ($id_tipo_servicio == 6) { ?>

<center>
    <h6 id="nombre_servicio"></h6>
    <hr>
</center>


<div style="padding: 0px;  height: 80vh !important; overflow-x: hidden;overflow-y: auto;">
    <div id="wizard3">
        <h3>Datos principales</h3>
        <section>
            <form id="form_editar_1">

                <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                <input type="hidden" name="mensaje_correo"
                    value="SE HA ACTUALIZADO LA INFORMACIÓN PRINCIPAL DEL SERVICIO <b>TRASLADO DE CARGA SUELTA</b>.">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <label for="">Fecha recepción documento</label>

                        <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc" class="form-control"
                            placeholder="Seleccionar una fecha"
                            value="<?php
                                                                                                                                                                    if ($servicios['fecha_recepcion_doc'] == null) {
                                                                                                                                                                    } else {
                                                                                                                                                                        echo date('Y-m-d', strtotime($servicios["fecha_recepcion_doc"]));
                                                                                                                                                                    }
                                                                                                                                                                    ?>"
                            <?php echo $disabled_transporte ?>>
                    </div>
                    <div class=" col-12 col-sm-6">
                        <label for="">Cliente</label>
                        <select class="form-control" onchange="consultar_subcliente()" name="id_cliente" id="id_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                <?php
                                    $id_cliente = $servicios["id_cliente"];
                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                    $consultar_cliente->execute();
                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_cliente as $cliente) {
                                        echo $cliente["razon_social"];
                                    }
                                    ?>
                            </option>
                            <?php
                                foreach ($consultar_clientes as $clientes) {
                                ?>
                            <option value="<?php echo $clientes["id"] ?>"><?php echo $clientes["razon_social"] ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Subcliente</label>

                        <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                <?php
                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                    $consultar_sub_cliente->execute();
                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_sub_cliente as $sub_cliente) {
                                        echo $sub_cliente["nombre_sub_cliente"];
                                    }
                                    ?>

                            </option>
                        </select>

                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Do/Ns</label>
                        <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                            class="form-control" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Tipo de servicio</label>
                        <select class="form-control"
                            oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)" name="tipo_servicio"
                            id="tipo_servicios" <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                <?php
                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                    $consultar_tipo_servicio->execute();
                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                        echo $tipo_servicio["nombre_servicio"];
                                    }

                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                            <option value="<?php echo $tipo_servicios['id'] ?>">
                                <?php echo $tipo_servicios['nombre_servicio'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>

                    <div class="col-12 col-sm-6">
                        <label for="">Tipo de carga</label>
                        <select class="form-control" name="tipo_carga" id="tipo_carga"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_carga"] ?>" selected>
                                <?php
                                    $id_tipo_carga = $servicios["tipo_carga"];
                                    $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                    $consultar_tipo_carga->execute();
                                    $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_carga as $tipo_carga) {
                                        $nombre_cargas = $tipo_carga["nombre_carga"];
                                        echo $tipo_carga["nombre_carga"];
                                    }
                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_cargas as $tipo_cargas) { ?>
                            <option value="<?php echo $tipo_cargas['id'] ?>">
                                <?php echo $tipo_cargas['nombre_carga'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>

                    <div class="col-12 col-sm-6" id="cantidad_pallets_despacho">
                        <label for="">Cantidad de <?php echo $nombre_cargas; ?></label>
                        <input type="text" class="form-control" id="cantidad_pallets"
                            value="<?= $servicios["cantidad_pallets"] ?>" name="cantidad_pallets"
                            <?php echo $disabled_carga ?>>

                    </div>
                    <div class="col-12 col-sm-6" id="puerto_origen">
                        <label for="">Puerto de retiro del vacío</label>
                        <input type="text" name="puerto_origen" id="puerto_origen" class="form-control"
                            value="<?php echo $servicios["puerto_origen"] ?>" <?php echo $disabled_transporte ?>>

                    </div>
                    <div class="col-12 col-sm-6" id="fecha_hora_r_p">
                        <label for="">Fecha retiro puerto del vacío</label>
                        <input type="datetime-local" class="form-control "
                            value="<?php echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_r_p'])); ?>"
                            name="fecha_hora_r_p" id="fecha_hora_r_p" <?php echo $disabled_transporte ?>>
                    </div>

                    <div class="col-12 col-sm-6">
                        <label for="">Lugar entrega</label>
                        <input type="text" class="form-control" value="<?php echo $servicios["lugar_entrega"] ?>"
                            name="lugar_entrega" id="lugar_entrega" <?php echo $disabled_transporte ?>>
                    </div>

                    <div class="col-12 col-sm-6">
                        <label for="">Observación</label>
                        <input type="text" name="observacion1" id="observacion1"
                            value="<?php echo $servicios["observacion1"] ?>" class="form-control">
                    </div>

                    <div class="col-12 col-sm-6">

                        <a href="javascript:void(0)" onclick="actualizar_form_1()" style="width: 100%; margin-top:15px;"
                            class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                            <div class="ht-40 justify-content-between">
                                <span class="pd-x-15">Actualizar</span>
                                <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                            </div>
                        </a>

                    </div>
                </div>
            </form>
        </section>
        <h3>Más información</h3>
        <section>

            <form id="formulario_servicio_2">
                <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                <input type="hidden" name="mensaje_correo"
                    value="SE HA ACTUALIZADO LA INFORMACIÓN DE LLEGADA AL LUGAR DE ENTREGA DEL SERVICIO <b>TRASLADO DE CARGA SUELTA</b>.">
                <div class="col-12">
                    <label for="">Nombre conductor (transportadora)</label>
                    <input type="text" class="form-control" name="nombre_c_z_f" id="nombre_c_z_f"
                        value="<?php echo $servicios["nombre_c_z_f"] ?>" <?php echo $disabled_carga ?>>
                </div>
                <div class="col-12">
                    <label for="">Identificación del conductor (transportadora)</label>
                    <input type="text" class="form-control" name="identificacion_c_z_f" id="doc_conductor_t"
                        value="<?php echo $servicios["identificacion_c_z_f"] ?>" <?php echo $disabled_carga ?>>
                </div>
                <div class="col-12">
                    <label for="">Placa del vehículo (transportadora)</label>
                    <input type="text" class="form-control" name="placa_v_z_f" id="placa_v_conductor_t"
                        value="<?php echo $servicios["placa_v_z_f"] ?>" <?php echo $disabled_carga ?>>
                </div>

                <div class="col-12 " id="fecha_hora_incio_descargue">
                    <label for="">Fecha y hora de llegada al lugar de entrega</label>
                    <input type="datetime-local" class="form-control " name="fecha_hora_incio_descargue"
                        id="fecha_hora_incio_descargue"
                        value="<?php $servicios['fecha_hora_incio_descargue'] = preg_replace("/\s/", 'T', $servicios['fecha_hora_incio_descargue']);
                                                                                                                                                    echo $servicios['fecha_hora_incio_descargue'] ?>"
                        <?php echo $disabled_transporte ?>>
                </div>
                <div class="col-12 " id="fecha_hora_terminacion_descargue">
                    <label for="">Fecha y hora finalización de descargue</label>
                    <input type="datetime-local" class="form-control " name="fecha_hora_terminacion_descargue"
                        id="fecha_hora_terminacion_descargue"
                        value="<?php $servicios['fecha_hora_terminacion_descargue'] = preg_replace("/\s/", 'T', $servicios['fecha_hora_terminacion_descargue']);
                                                                                                                                                                echo $servicios['fecha_hora_terminacion_descargue'] ?>"
                        <?php echo $disabled_transporte ?>>
                </div>


                <div class="col-12 ">
                    <label for="">Evidencias</label>
                    <div class="custom-file"><input type="file" name="evidencias[]" class="form-control"
                            id="inputGroupFile04" multiple=""><a
                            href="../../admin/evidencias_servicio?type=3&id_servicios=<?php echo $servicios['id'] ?>"
                            target="_blank">Ver evidencias agregadas</a>
                    </div>
                </div>
                <div class="col-12 col-sm-12">
                    <label for="">Observación</label>
                    <input type="text" name="observacion2" id="observacion2"
                        value="<?php echo $servicios["observacion2"] ?>" class="form-control">
                </div>
                <div class="col-12" style="width: 100%; margin-top:15px;">
                    <a href="javascript:void(0)" onclick="actualizar_servicios(2)" style="width: 100%; margin-top:15px;"
                        class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                        <div class="ht-40 justify-content-between">
                            <span class="pd-x-15">Guardar/actualizar</span>
                            <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                        </div>
                    </a>

                </div>



            </form>



        </section>

    </div>
</div>

<?php } else if ($id_tipo_servicio == 7) { ?>
<center>
    <h6 id="nombre_servicio"></h6>
    <hr>
</center>
<div style="padding: 0px;  height: 80vh !important; overflow-x: hidden;overflow-y: auto;">



    <div class="row">
        <div class="col-12 col-sm-4">

            <form id="form_editar_1">
                <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                <input type="hidden" name="mensaje_correo"
                    value="SE HA ACTUALIZADO LA INFORMACIÓN PRINCIPAL DEL SERVICIO <b>VACIADO DE CONTENEDOR</b>.">
                <div class="row">
                    <!--<div class="col-12 col-sm-6">
                        <label for="">Fecha recepción documento</label>

                        <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc" class="form-control"
                            placeholder="Seleccionar una fecha"
                            value="<?php
                                    if ($servicios['fecha_recepcion_doc'] == null) {
                                    } else {
                                        echo date('Y-m-d', strtotime($servicios["fecha_recepcion_doc"]));
                                    }
                                    ?>"
                            <?php echo $disabled_transporte ?>>
                    </div>-->
                    <div class=" col-12 col-sm-6">
                        <label for="">Cliente</label>
                        <select class="form-control" onchange="consultar_subcliente()" name="id_cliente" id="id_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                <?php
                                    $id_cliente = $servicios["id_cliente"];
                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                    $consultar_cliente->execute();
                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_cliente as $cliente) {
                                        echo $cliente["razon_social"];
                                    }
                                    ?>
                            </option>
                            <?php
                                foreach ($consultar_clientes as $clientes) {
                                ?>
                            <option value="<?php echo $clientes["id"] ?>"><?php echo $clientes["razon_social"] ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Subcliente</label>

                        <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                <?php
                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                    $consultar_sub_cliente->execute();
                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_sub_cliente as $sub_cliente) {
                                        echo $sub_cliente["nombre_sub_cliente"];
                                    }
                                    ?>

                            </option>
                        </select>

                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Do/Ns</label>
                        <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                            class="form-control" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Tipo de servicio</label>
                        <select class="form-control"
                            oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)" name="tipo_servicio"
                            id="tipo_servicios" <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                <?php
                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                    $consultar_tipo_servicio->execute();
                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                        echo $tipo_servicio["nombre_servicio"];
                                    }

                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                            <option value="<?php echo $tipo_servicios['id'] ?>">
                                <?php echo $tipo_servicios['nombre_servicio'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>

                    <div class="col-12 col-sm-6" id="puerto_origen">
                        <label for="">Puerto de retiro del vacío</label>
                        <input type="text" name="puerto_origen" id="puerto_origen" class="form-control"
                            value="<?php echo $servicios["puerto_origen"] ?>" <?php echo $disabled_transporte ?>>

                    </div>
                    <div class="col-12 col-sm-6" id="fecha_hora_r_p">
                        <label for="">Fecha retiro puerto del vacío</label>
                        <input type="datetime-local" class="form-control "
                            value="<?php echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_r_p'])); ?>"
                            name="fecha_hora_r_p" id="fecha_hora_r_p" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6" id="peso_retiros">
                        <label for="">Peso retirado</label>
                        <input type="text" name="peso_retiro" id="peso_retiro" class="form-control"
                            value="<?php echo $servicios["peso_retiro"] ?>" <?php echo $disabled_transporte ?>>

                    </div>
                    <div class="col-12 col-sm-6" id="tipo_vehiculo">
                        <label for="">Seleccionar vehículo acarreo</label>
                        <select name="vehiculo_acarreo" id="vehiculo_acarreo" class="form-control">
                            <option value="<?= $servicios["vehiculo_acarreo"] ?><">
                                <?= $servicios["vehiculo_acarreo"] ?>
                            </option>
                            <option value="Furgón">Furgón</option>
                            <option value="Patineta">Patineta</option>
                            <option value="Mula">Mula</option>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6" id="lugar_ingresos">
                        <label for="">Lugar ingreso</label>
                        <input type="text" name="lugar_ingreso" id="lugar_ingreso" class="form-control"
                            value="<?= $servicios["lugar_ingreso"] ?>" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6">

                        <a href="javascript:void(0)" onclick="actualizar_form_1()" style="width: 100%; margin-top:15px;"
                            class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                            <div class="ht-40 justify-content-between">
                                <span class="pd-x-15">Actualizar</span>
                                <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                            </div>
                        </a>

                    </div>
                </div>
            </form>
        </div>

        <div class="col-12 col-sm-8">
            <form id="form_4">
                <input type="hidden" name="id_servicio_form4" id="id_servicio_form4" class="form-control"
                    value="<?php echo $id_servicio ?>">
                <input type="hidden" name="mensaje_correo"
                    value="SE HA AGREGADO UN NUEVO DESPACHO DEL SERVICIO <b>VACIADO DE CONTENEDOR</b>.">
                <div class="row">
                    <div class="col-12 col-sm-6">

                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <label for="">¿Despacho parcial o total?</label>
                                <select name="despacho_parcial_total" class="form-control" id="despacho_parcial_total"
                                    <?php echo $disabled_carga ?>>

                                    <option value="0" selected>Despacho parcial</option>
                                    <option value="1">Despacho total</option>
                                </select>
                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="">Fecha y hora de despacho</label>
                                <input type="datetime-local" class="form-control " name="fecha_hora_despacho"
                                    id="fecha_hora_despacho" <?php echo $disabled_carga ?>>
                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="">Cantidad de peso despachado</label>
                                <input type="number" oninput="validar_cantidad_pesos()" class="form-control"
                                    id="cantidad_pallets_despacho" name="cantidad_pallets_despacho"
                                    max="<?php echo $suma_de_pallets_despachados; ?>" placeholder=""
                                    <?php echo $disabled_carga ?>>
                                <h6 style="font-size:10px"><b>
                                        <p id="suma_peso"></p>
                                    </b></h6>

                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="">Transportadora</label>
                                <input type="text" class="form-control" id="transportadora" name="transportadora"
                                    <?php echo $disabled_carga ?>>
                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="">Nombre conductor (transportadora)</label>
                                <input type="text" class="form-control" name="nombre_conductor_t"
                                    id="nombre_conductor_t" <?php echo $disabled_carga ?>>
                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="">Identificación del conductor (transportadora)</label>
                                <input type="text" class="form-control" name="doc_conductor_t" id="doc_conductor_t"
                                    <?php echo $disabled_carga ?>>
                            </div>
                            <div class="col-12 col-sm-12">
                                <label for="">Placa del vehículo (transportadora)</label>
                                <input type="text" class="form-control" name="placa_v_conductor_t"
                                    id="placa_v_conductor_t" <?php echo $disabled_carga ?>>
                            </div>

                            <div class="col-12 col-sm-12">
                                <label for="">Adjuntar una o varias fotos</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="evidencias_despachos[]" class="form-control"
                                            id="inputGroupFile04" multiple>
                                        <label class="" for="inputGroupFile04"></label>
                                    </div>
                                    <div class="input-group-append">
                                        <button id="btn_agregar_datos" class="btn btn-success"
                                            onclick="guardar_form_4()" type="button">Guardar
                                            datos y
                                            evidencias <i class="fa fa-upload"></i></button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div id="total_pallets"></div>

                        <div id="despacho_parcial_totals_servicios"></div>
                    </div>
                </div>

            </form>
        </div>

        <!--<div class="col-12 col-sm-4">
            <h6>Resumen facturación</h6>
            <div id="resumen_servicio_editar"></div>
        </div>-->
    </div>
</div>

<?php } else if ($id_tipo_servicio == 8) { ?>
<center>
    <h6 id="nombre_servicio"></h6>
    <hr>
</center>

<div style="padding: 0px; height: 80vh !important; overflow-x: hidden;overflow-y: auto;">
    <div class="row">
        <div class="col-12 col-sm-6">

            <form id="form_editar_1">

                <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">

                <input type="hidden" name="mensaje_correo"
                    value="SE HA ACTUALIZADO LA INFORMACIÓN PRINCIPAL DEL SERVICIO <b>VACIADO DE CONTENEDOR</b>.">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <label for="">Fecha recepción documento</label>

                        <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc" class="form-control"
                            placeholder="Seleccionar una fecha"
                            value="<?php
                                                                                                                                                                    if ($servicios['fecha_recepcion_doc'] == null) {
                                                                                                                                                                    } else {
                                                                                                                                                                        echo date('Y-m-d', strtotime($servicios["fecha_recepcion_doc"]));
                                                                                                                                                                    }
                                                                                                                                                                    ?>"
                            <?php echo $disabled_transporte ?>>
                    </div>
                    <div class=" col-12 col-sm-6">
                        <label for="">Cliente</label>
                        <select class="form-control" onchange="consultar_subcliente()" name="id_cliente" id="id_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                <?php
                                    $id_cliente = $servicios["id_cliente"];
                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                    $consultar_cliente->execute();
                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_cliente as $cliente) {
                                        echo $cliente["razon_social"];
                                    }
                                    ?>
                            </option>
                            <?php
                                foreach ($consultar_clientes as $clientes) {
                                ?>
                            <option value="<?php echo $clientes["id"] ?>"><?php echo $clientes["razon_social"] ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Subcliente</label>

                        <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                <?php
                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                    $consultar_sub_cliente->execute();
                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_sub_cliente as $sub_cliente) {
                                        echo $sub_cliente["nombre_sub_cliente"];
                                    }
                                    ?>

                            </option>
                        </select>

                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Do/Ns</label>
                        <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                            class="form-control" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Tipo de servicio</label>
                        <select class="form-control"
                            oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)" name="tipo_servicio"
                            id="tipo_servicios" <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                <?php
                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                    $consultar_tipo_servicio->execute();
                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                        echo $tipo_servicio["nombre_servicio"];
                                    }

                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                            <option value="<?php echo $tipo_servicios['id'] ?>">
                                <?php echo $tipo_servicios['nombre_servicio'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>

                    <div class="col-12 col-sm-6" style="display:none">
                        <label for="">Tipo de carga</label>
                        <select class="form-control" name="tipo_carga" id="tipo_carga"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_carga"] ?>">
                                <?php
                                    $id_tipo_carga = $servicios["tipo_carga"];
                                    $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                    $consultar_tipo_carga->execute();
                                    $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_carga as $tipo_carga) {
                                        echo $tipo_carga["nombre_carga"];
                                    }
                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_cargas as $tipo_cargas) { ?>
                            <option value="<?php echo $tipo_cargas['id'] ?>" selected>
                                <?php echo $tipo_cargas['nombre_carga'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Contenedor</label>
                        <input type="text" name="contenedor" value="<?php echo $servicios["contenedor"] ?>"
                            id="contenedor" class="form-control" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6" id="tamaño_contenedor_1">
                        <label for="">Tamaño contenedor</label>
                        <select name="tamaño_contenedor" id="tamaño_contenedor" class="form-control"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tamaño_contenedor"] ?>">
                                <?php echo $servicios["tamaño_contenedor"] ?></option>
                            <option value="20">20</option>
                            <option value="40">40</option>
                        </select>
                    </div>


                    <div class="col-12 col-sm-6" id="puerto_origen">
                        <label for="">Puerto de retiro del vacío</label>
                        <input type="text" name="puerto_origen" id="puerto_origen" class="form-control"
                            value="<?php echo $servicios["puerto_origen"] ?>" <?php echo $disabled_transporte ?>>

                    </div>
                    <div class="col-12 col-sm-6" id="fecha_hora_r_p">
                        <label for="">Fecha retiro puerto del vacío</label>
                        <input type="datetime-local" class="form-control "
                            value="<?php echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_r_p'])); ?>"
                            name="fecha_hora_r_p" id="fecha_hora_r_p" <?php echo $disabled_transporte ?>>
                    </div>

                    <div class="col-12 col-sm-6" id="lugar_entrega_zona_f_1">
                        <label for="">Lugar de entrega zona franca</label>
                        <input type="text" name="lugar_entrega_zona_f" id="lugar_entrega_zona_f" class="form-control"
                            value="<?php echo $servicios["lugar_entrega_zona_f"] ?>" <?php echo $disabled_transporte ?>>

                    </div>

                    <div class="col-12 col-sm-6" id="fecha_entrega_zona_f_1">
                        <label for="">Fecha de entrega en zona franca</label>
                        <input type="date" class="form-control " name="fecha_entrega_zona_f" id="fecha_entrega_zona_f"
                            value="<?php echo $servicios["fecha_entrega_zona_f"] ?>" <?php echo $disabled_transporte ?>>
                    </div>

                    <div class="col-12 col-sm-6">

                        <a href="javascript:void(0)" onclick="actualizar_form_1()" style="width: 100%; margin-top:15px;"
                            class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                            <div class="ht-40 justify-content-between">
                                <span class="pd-x-15">Actualizar</span>
                                <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                            </div>
                        </a>

                    </div>
                </div>
            </form>
        </div>
        <div class="col-12 col-sm-6">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <form id="formulario_servicio_2">
                        <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                        <input type="hidden" name="mensaje_correo"
                            value="SE HA AGREGADO UN NUEVO DESPACHO DEL SERVICIO <b>VACIADO DE CONTENEDOR</b>.">
                        <div class="col-12">
                            <label for="">Nombre conductor (transportadora)</label>
                            <input type="text" class="form-control" name="nombre_c_z_f" id="nombre_c_z_f"
                                value="<?php echo $servicios["nombre_c_z_f"] ?>" <?php echo $disabled_carga ?>>
                        </div>
                        <div class="col-12">
                            <label for="">Identificación del conductor (transportadora)</label>
                            <input type="text" class="form-control" name="identificacion_c_z_f" id="doc_conductor_t"
                                value="<?php echo $servicios["identificacion_c_z_f"] ?>" <?php echo $disabled_carga ?>>
                        </div>
                        <div class="col-12">
                            <label for="">Placa del vehículo (transportadora)</label>
                            <input type="text" class="form-control" name="placa_v_z_f" id="placa_v_conductor_t"
                                value="<?php echo $servicios["placa_v_z_f"] ?>" <?php echo $disabled_carga ?>>
                        </div>
                        <div class="col-12" style="width: 100%; margin-top:15px;">
                            <a href="javascript:void(0)" onclick="actualizar_servicios(2)"
                                style="width: 100%; margin-top:15px;"
                                class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                                <div class="ht-40 justify-content-between">
                                    <span class="pd-x-15">Guardar/actualizar</span>
                                    <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                                </div>
                            </a>

                        </div>
                    </form>
                </div>
                <div class="col-12 col-sm-6">
                    <?php if ($servicios["patio_almacenaje"] == "" || $servicios["lugar_devolucion_vacio"] == "") { ?>

                    <div id="opcion">
                        <label for="">¿Se generará almacenamiento del vacío?</label>
                        <select name="almacenamiento_vacio" onchange="mostrar_opciones_form_5()" class="form-control"
                            id="almacenamiento_vacio">
                            <option value="3">Seleccione una opción</option>
                            <option value="1">Sí</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div id="mensaje_almacenamiento"></div>

                    <?php } ?>
                    <div id="mostrar_opcion_no" style="display:none">

                        <form id="formulario_servicio_53">
                            <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                            <div class="row">
                                <div class="col-12 ">
                                    <label for="">Lugar devolucion del vacío </label>
                                    <input type="text" class="form-control"
                                        value="<?php echo $servicios["lugar_devolucion_vacio"] ?>"
                                        name="lugar_devolucion_vacio" id="lugar_devolucion_v"
                                        <?php echo $disabled_transporte ?>>
                                </div>
                                <div class="col-12 ">
                                    <label for="">Fecha devolución del vacío</label>
                                    <input type="date" class="form-control " name="fecha_devolucion"
                                        onchange="comparar_fechas_devolucion()" id="fecha_devolucion"
                                        value="<?php
                                                                                                                                                                                if ($servicios['fecha_devolucion'] == null) {
                                                                                                                                                                                } else {
                                                                                                                                                                                    echo  date('Y-m-d', strtotime($servicios['fecha_devolucion']));
                                                                                                                                                                                }
                                                                                                                                                                                ?>"
                                        <?php echo $disabled_transporte ?>>
                                </div>
                                <div class="col-12">
                                    <label for="">Nombre conductor (transportadora)</label>
                                    <input type="text" class="form-control" name="nombre_c_e_v" id="nombre_conductor_t"
                                        value="<?php echo $servicios["nombre_c_e_v"] ?>" <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12">
                                    <label for="">Identificación del conductor (transportadora)</label>
                                    <input type="text" class="form-control" name="identificacion_c_e_v"
                                        id="doc_conductor_t" value="<?php echo $servicios["identificacion_c_e_v"] ?>"
                                        <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12">
                                    <label for="">Placa del vehículo (transportadora)</label>
                                    <input type="text" class="form-control" name="placa_v_e_v" id="placa_v_conductor_t"
                                        value="<?php echo $servicios["placa_v_e_v"] ?>" <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12">
                                    <label for="">¿Se realizó descarga y carga con montacargas de? </label>
                                    <select name="montacarga" id="montacarga" class="form-control">
                                        <?= $montacarga = $servicios["montacarga"];
                                            if ($montacarga == "") {
                                                echo '<option value="">Seleccione una opción</option>';
                                            } else {
                                                echo '<option value="' . $servicios["montacarga"] . '">' . $servicios["montacarga"] . '</option>';
                                            }
                                            ?>
                                        <option value="7 toneladas">7 toneladas</option>
                                        <option value="8 toneladas">8 toneladas</option>
                                    </select>
                                </div>
                                <div class="col-12" style="width: 100%; margin-top:15px;">
                                    <a href="javascript:void(0)" onclick="actualizar_servicios(53)"
                                        style="width: 100%; margin-top:15px;"
                                        class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                                        <div class="ht-40 justify-content-between">
                                            <span class="pd-x-15">Guardar/actualizar</span>
                                            <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                                        </div>
                                    </a>

                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="mostrar_opcion_si" style="display:none">
                        <form id="formulario_servicio_54">
                            <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
                            <div class="row">
                                <div class="col-12 ">
                                    <label for="">Patio almacenaje</label>
                                    <input type="text" name="patio_almacenaje" id="patio_almacenaje"
                                        class="form-control" value="<?php echo $servicios["patio_almacenaje"] ?>">
                                </div>
                                <div class="col-12 ">
                                    <label for="">Fecha de ingreso al patio de almacenaje</label>
                                    <input type="date" class="form-control" name="fecha_ingreso_patio_almacenaje"
                                        value="<?php echo $servicios["fecha_ingreso_patio_almacenaje"] ?>">
                                </div>
                                <div class="col-12 ">
                                    <label for="">Nombre conductor (transportadora)</label>
                                    <input type="text" class="form-control" name="nombre_c_p_a" id="nombre_conductor_t"
                                        value="<?php echo $servicios["nombre_c_p_a"] ?>" <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12">
                                    <label for="">Identificación del conductor (transportadora)</label>
                                    <input type="text" class="form-control" name="identificacion_c_p_a"
                                        id="doc_conductor_t" value="<?php echo $servicios["identificacion_c_p_a"] ?>"
                                        <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 ">
                                    <label for="">Placa del vehículo (transportadora)</label>
                                    <input type="text" class="form-control" name="placa_v_p_a" id="placa_v_conductor_t"
                                        value="<?php echo $servicios["placa_v_p_a"] ?>" <?php echo $disabled_carga ?>>
                                </div>
                                <hr>
                                <div class="col-12 ">
                                    <label for="">Lugar entrega del vacío (naviero) </label>
                                    <input type="text" class="form-control"
                                        value="<?php echo $servicios["lugar_entrega"] ?>" name="lugar_entrega"
                                        id="lugar_entrega" <?php echo $disabled_transporte ?>>
                                </div>
                                <div class="col-12 ">
                                    <label for="">Fecha de ingreso al patio del naviero</label>
                                    <input type="date" class="form-control " name="fecha_ingreso_patio_naviero"
                                        onchange="comparar_fechas_devolucion()" id="fecha_ingreso_patio_naviero"
                                        value="<?php
                                                                                                                                                                                                    if ($servicios['fecha_ingreso_patio_naviero'] == null) {
                                                                                                                                                                                                    } else {
                                                                                                                                                                                                        echo  date('Y-m-d', strtotime($servicios['fecha_ingreso_patio_naviero']));
                                                                                                                                                                                                    }
                                                                                                                                                                                                    ?>"
                                        <?php echo $disabled_transporte ?>>
                                </div>
                                <div class="col-12">
                                    <label for="">Nombre conductor (transportadora)</label>
                                    <input type="text" class="form-control" name="nombre_c_e_v" id="nombre_conductor_t"
                                        value="<?php echo $servicios["nombre_c_e_v"] ?>" <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12">
                                    <label for="">Identificación del conductor (transportadora)</label>
                                    <input type="text" class="form-control" name="identificacion_c_e_v"
                                        id="doc_conductor_t" value="<?php echo $servicios["identificacion_c_e_v"] ?>"
                                        <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12">
                                    <label for="">Placa del vehículo (transportadora)</label>
                                    <input type="text" class="form-control" name="placa_v_e_v" id="placa_v_conductor_t"
                                        value="<?php echo $servicios["placa_v_e_v"] ?>" <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12">
                                    <label for="">¿Se realizó descarga y carga con montacargas de? </label>
                                    <select name="montacarga" id="montacarga" class="form-control">
                                        <?= $montacarga = $servicios["montacarga"];
                                            if ($montacarga == "") {
                                                echo '<option value="">Seleccione una opción</option>';
                                            } else {
                                                echo '<option value="' . $servicios["montacarga"] . '">' . $servicios["montacarga"] . '</option>';
                                            }
                                            ?>
                                        <option value="7 toneladas">7 toneladas</option>
                                        <option value="8 toneladas">8 toneladas</option>
                                    </select>
                                </div>
                                <div class="col-12" style="width: 100%; margin-top:15px;">
                                    <a href="javascript:void(0)" onclick="actualizar_servicios(54)"
                                        style="width: 100%; margin-top:15px;"
                                        class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                                        <div class="ht-40 justify-content-between">
                                            <span class="pd-x-15">Guardar/actualizar</span>
                                            <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                                        </div>
                                    </a>

                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div>
        <!--<div class="col-12 col-sm-4">
            <h6>Resumen facturación</h6>
            <div id="resumen_servicio_editar"></div>
        </div>-->
    </div>
</div>
<?php } else if ($id_tipo_servicio == 9) { ?>
<center>
    <h6 id="nombre_servicio"></h6>
    <hr>
</center>

<div style="padding: 0px; height: 80vh !important; overflow-x: hidden;overflow-y: auto;">

    <div id="wizard3">
        <h3>Datos principales</h3>
        <section>
            <form id="form_editar_1">

                <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">

                <input type="hidden" name="mensaje_correo"
                    value="SE HA ACTUALIZADO LA INFORMACIÓN PRINCIPAL DEL SERVICIO <b>VACIADO DE CONTENEDOR</b>.">
                <div class="row">
                    <!--<div class="col-12 col-sm-6">
                        <label for="">Fecha recepción documento</label>

                        <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc" class="form-control"
                            placeholder="Seleccionar una fecha"
                            value="<?php
                                    if ($servicios['fecha_recepcion_doc'] == null) {
                                    } else {
                                        echo date('Y-m-d', strtotime($servicios["fecha_recepcion_doc"]));
                                    }
                                    ?>"
                            <?php echo $disabled_transporte ?>>
                    </div>-->
                    <div class=" col-12 col-sm-6">
                        <label for="">Cliente</label>
                        <select class="form-control" onchange="consultar_subcliente()" name="id_cliente" id="id_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                <?php
                                    $id_cliente = $servicios["id_cliente"];
                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                    $consultar_cliente->execute();
                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_cliente as $cliente) {
                                        echo $cliente["razon_social"];
                                    }
                                    ?>
                            </option>
                            <?php
                                foreach ($consultar_clientes as $clientes) {
                                ?>
                            <option value="<?php echo $clientes["id"] ?>"><?php echo $clientes["razon_social"] ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Subcliente</label>

                        <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                <?php
                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                    $consultar_sub_cliente->execute();
                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_sub_cliente as $sub_cliente) {
                                        echo $sub_cliente["nombre_sub_cliente"];
                                    }
                                    ?>

                            </option>
                        </select>

                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Do/Ns</label>
                        <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                            class="form-control" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Tipo de servicio</label>
                        <select class="form-control"
                            oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)" name="tipo_servicio"
                            id="tipo_servicios" <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                <?php
                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                    $consultar_tipo_servicio->execute();
                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                        echo $tipo_servicio["nombre_servicio"];
                                    }

                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                            <option value="<?php echo $tipo_servicios['id'] ?>">
                                <?php echo $tipo_servicios['nombre_servicio'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>

                    <div class="col-12 col-sm-6" id="contenedor_carga_sueltas">
                        <label for="">¿Contenedor o Carga suelta? <b>Campo obligatorio*</b></label>
                        <select class="form-control" name="contenedor_carga_suelta"
                            onchange="contenedor_carga_sueltas()" id="contenedor_carga_suelta"
                            style="width:100% !important;" <?php echo $disabled_transporte ?>>
                            <option value="<?php if ($servicios["contenedor_carga_suelta"] == 0) {
                                                    echo "0";
                                                } else if ($servicios["contenedor_carga_suelta"] == 1) {
                                                    echo "1";
                                                } else {
                                                    echo "";
                                                } ?>" seleted>
                                <?php
                                    if ($servicios["contenedor_carga_suelta"] == 1) {
                                        echo "Contenedor";
                                    } else if ($servicios["contenedor_carga_suelta"] == 0) {
                                        echo "Carga suelta";
                                    } else {
                                        echo "Seleccione una opción";
                                    }
                                    ?>
                            </option>
                            <option value="1">Contenedor</option>
                            <option value="0">Carga suelta</option>
                        </select>

                    </div>

                    <div class="col-12 col-sm-6">
                        <label for="">Tipo de carga</label>
                        <select class="form-control" name="tipo_carga" id="tipo_carga" onchange="tipo_cargas_campos()"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_carga"] ?>" selected>
                                <?php
                                    $id_tipo_carga = $servicios["tipo_carga"];
                                    $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                    $consultar_tipo_carga->execute();
                                    $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_carga as $tipo_carga) {
                                        echo $tipo_carga["nombre_carga"];
                                    }
                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_cargas as $tipo_cargas) { ?>
                            <option value="<?php echo $tipo_cargas['id'] ?>">
                                <?php echo $tipo_cargas['nombre_carga'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>

                    <div class="col-12 col-sm-6" id="bultos_total" style="display:none">
                        <label for="">Cantidad bultos total</label>
                        <input type="text" class="form-control" <?php echo $disabled_carga ?> id="cantidad_bultos"
                            name="cantidad_bultos" value="<?php echo $servicios["cantidad_bultos"] ?>">
                    </div>
                    <div class="col-12 col-sm-6" id="pallets_total" style="display:none">
                        <label for="" id="label_tipo_carga"></label>
                        <input type="text" class="form-control" id="cantidad_bpallets" name="cantidad_bpallets"
                            value="<?php echo $servicios["cantidad_bpallets"] ?>" <?php echo $disabled_carga ?>>
                    </div>

                    <div class="col-12 col-sm-6">
                        <label for="">Peso aproximado</label>
                        <input type="text" class="form-control" name="peso_aprox" id="peso_aprox"
                            value="<?php echo $servicios["peso_aprox"] ?>" <?php echo $disabled_carga ?>>
                    </div>


                    <div class="col-12 col-sm-6" id="tipo_contenedor" style="display:none">
                        <label for="">Tipo de contenedor</label>
                        <select name="tipo_contenedor" id="tipo_contenedor" class="form-control">
                            <option value="<?php echo $servicios["tipo_contenedor"] ?>">
                                <?php echo $servicios["tipo_contenedor"] ?></option>
                            <option value="HC">HC</option>
                            <option value="DRY">DRY</option>
                            <option value="OPEN TOP">OPEN TOP</option>
                            <option value="REEFER HC">REEFER HC</option>
                            <option value="REERFER">REERFER</option>
                            <option value="FALT RACK">FALT RACK</option>
                            <option value="TANQUE">TANQUE</option>

                        </select>
                    </div>
                    <div class="col-12 col-sm-6" id="tamaño_contenedor" style="display:none">
                        <label for="">Tamaño contenedor</label>
                        <input type="text" name="tamaño_contenedor"
                            value="<?php echo $servicios["tamaño_contenedor"] ?>" id="tamaño_contenedor"
                            class="form-control" <?php echo $disabled_transporte ?>>

                    </div>

                    <div class="col-12 col-sm-6" id="contenedors" style="display:none">
                        <label for="">Número de contenedor</label>
                        <input type="text" name="contenedor" value="<?php echo $servicios["contenedor"] ?>"
                            id="contenedor" class="form-control" <?php echo $disabled_transporte ?>>
                    </div>



                    <div class="col-12   col-sm-6" id="fecha_hora_incio_descargue">
                        <label for="">Fecha y hora inicio del vaciado</label>
                        <input type="datetime-local" class="form-control " name="fecha_hora_incio_descargue"
                            id="fecha_hora_incio_descargue"
                            value="<?php $servicios['fecha_hora_incio_descargue'] = preg_replace("/\s/", 'T', $servicios['fecha_hora_incio_descargue']);
                                                                                                                                                        echo $servicios['fecha_hora_incio_descargue'] ?>"
                            <?php echo $disabled_transporte ?>>
                    </div>

                    <div class="col-12  col-sm-6" id="fecha_hora_terminacion_descargue">
                        <label for="">Fecha y hora finalización de vaciado</label>
                        <input type="datetime-local" class="form-control " name="fecha_hora_terminacion_descargue"
                            id="fecha_hora_terminacion_descargue"
                            value="<?php $servicios['fecha_hora_terminacion_descargue'] = preg_replace("/\s/", 'T', $servicios['fecha_hora_terminacion_descargue']);
                                                                                                                                                                    echo $servicios['fecha_hora_terminacion_descargue'] ?>">
                    </div>

                    <div class="col-12  col-sm-6" id="placa_vehiculo_cs">
                        <label for="">Número de placa del vehículo</label>
                        <input type="text" class="form-control" id="placa_vehiculo_c" name="numero_placa_vehiculo"
                            value="<?php echo $servicios["numero_placa_vehiculo"]; ?>">
                    </div>
                    <div class="col-12 col-sm-6" id="nombre_conductors">
                        <label for="">Nombre del conductor del vehículo</label>
                        <input type="text" class="form-control" name="nombre_conductor" id="nombre_conductor"
                            value="<?php echo $servicios["nombre_conductor"]; ?>">
                    </div>
                    <div class="col-12  col-sm-6" id="identificacion_conductors">
                        <label for="">Identificación del conductor</label>
                        <input type="text" class="form-control" name="identificacion_conductor"
                            id="identificacion_conductor" value="<?php echo $servicios["identificacion_conductor"]; ?>">
                    </div>



                    <div class="col-12 col-sm-6">
                        <label for="">Días de almacenaje libre</label>
                        <input type="text" class="form-control" placeholder="Días de almacenaje"
                            <?php echo $disabled_carga ?> value="<?php echo $servicios["dia_almacenaje_libre"] ?>"
                            id="dia_almacenaje_libre" name="dia_almacenaje_libre">
                        <h6 style="font-size:10px"><b>Dejar el campo vacío si no tiene días de almacenaje libre*</b>
                        </h6>
                    </div>
                    <div class="col-12 col-sm-6" id="evidencias">
                        <label for="">Evidencias</label>
                        <div class="custom-file"><input type="file" name="evidencias[]" class="form-control"
                                id="inputGroupFile04" multiple=""><a
                                href="../../admin/evidencias_servicio?type=3&id_servicios=<?php echo $servicios['id'] ?>"
                                target="_blank">Ver evidencias agregadas</a></div>

                    </div>

                    <div class="col-12 col-sm-6">
                        <label for="">Observación</label>
                        <input type="text" name="observacion1" id="observacion1" class="form-control"
                            value="<?php echo $servicios["observacion1"] ?>">
                    </div>

                    <div class="col-12 col-sm-6">
                        <a href="javascript:void(0)" onclick="actualizar_form_1()" style="width: 100%; margin-top:15px;"
                            class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                            <div class="ht-40 justify-content-between">
                                <span class="pd-x-15">Actualizar</span>
                                <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                            </div>
                        </a>

                    </div>
                </div>

            </form>
        </section>
        <h3>Más información</h3>
        <section>
            <form id="form_4">
                <input type="hidden" name="id_servicio_form4" id="id_servicio_form4" class="form-control"
                    value="<?php echo $id_servicio ?>">
                <input type="hidden" name="mensaje_correo"
                    value="SE HA AGREGADO UN NUEVO DESPACHO DEL SERVICIO <b>VACIADO DE CONTENEDOR</b>.">
                <div class="row">
                    <div class="col-12 col-sm-7">

                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <label for="">¿Despacho parcial o total?</label>
                                <select name="despacho_parcial_total" class="form-control" id="despacho_parcial_total"
                                    <?php echo $disabled_carga ?>>

                                    <option value="0" selected>Despacho parcial</option>
                                    <option value="1">Despacho total</option>
                                </select>
                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="">Fecha y hora de despacho</label>
                                <input type="datetime-local" class="form-control " name="fecha_hora_despacho"
                                    id="fecha_hora_despacho" <?php echo $disabled_carga ?>>
                            </div>

                            <?php if ($servicios["tipo_carga"] == 5) { ?>
                            <div class="col-12 col-sm-6">
                                <label for="">Cantidad pallets despachados</label>
                                <input type="number" oninput="validar_cantidad_pallets()" class="form-control"
                                    id="cantidad_pallets_despacho_item" name="cantidad_pallets_despacho"
                                    max="<?php echo $suma_de_pallets_despachados; ?>" placeholder=""
                                    <?php echo $disabled_carga ?>>
                                <p id="pallets"></p>
                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="">Cantidad de bultos despachados</label>
                                <input type="number" class="form-control" oninput="validar_cantidad_bultos()"
                                    name="cantidad_bulto_despachado" id="cantidad_bultos_despacho_item"
                                    <?php echo $disabled_carga ?>>
                                <p id="bultos_contador"></p>
                            </div>
                            <?php } else { ?>
                            <div class="col-12 col-sm-6">
                                <label for=""><?php if ($servicios["tipo_carga"] == 1) {
                                                            echo 'Cantidad pallets despachados';
                                                        } else if ($servicios["tipo_carga"] == 2) {
                                                            echo 'Cantidad bultos despachados';
                                                        } else if ($servicios["tipo_carga"] == 3) {
                                                            echo 'Cantidad carga suelta despachados';
                                                        } else if ($servicios["tipo_carga"] == 4) {
                                                            echo 'Cantidad paquetes despachados';
                                                        }
                                                        ?></label>
                                <input type="number" oninput="validar_cantidad_pallets()" class="form-control"
                                    id="cantidad_pallets_despacho_item" name="cantidad_pallets_despacho"
                                    max="<?php echo $suma_de_pallets_despachados; ?>" placeholder=""
                                    <?php echo $disabled_carga ?>>
                                <p id="pallets"></p>
                            </div>
                            <?php } ?>


                            <div class="col-12 col-sm-6">
                                <label for="">Peso aproximado</label>
                                <input type="text" class="form-control" name="peso_aprox" id="peso_aprox"
                                    <?php echo $disabled_carga ?>>
                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="">Destino</label>
                                <input type="text" class="form-control" name="destino" id="destino"
                                    <?php echo $disabled_carga ?>>
                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="">Transportadora</label>
                                <input type="text" class="form-control" id="transportadora" name="transportadora"
                                    <?php echo $disabled_carga ?>>
                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="">Nombre conductor (transportadora)</label>
                                <input type="text" class="form-control" name="nombre_conductor_t"
                                    id="nombre_conductor_t" <?php echo $disabled_carga ?>>
                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="">Teléfono conductor</label>
                                <input type="text" class="form-control" name="tel_conductor" id="tel_conductor"
                                    <?php echo $disabled_carga ?>>
                            </div>

                            <div class="col-12 col-sm-6">
                                <label for="">Identificación del conductor (transportadora)</label>
                                <input type="text" class="form-control" name="doc_conductor_t" id="doc_conductor_t"
                                    <?php echo $disabled_carga ?>>
                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="">Placa del vehículo (transportadora)</label>
                                <input type="text" class="form-control" name="placa_v_conductor_t"
                                    id="placa_v_conductor_t" <?php echo $disabled_carga ?>>
                            </div>

                            <div class="col-12 col-sm-6">
                                <label for="">Observación</label>
                                <input type="text" class="form-control" name="observacion4" id="observacion4"
                                    oninput="convertir_mayus()" <?php echo $disabled_carga ?>>
                            </div>

                            <div class="col-12 col-sm-12">
                                <label for="">Adjuntar una o varias fotos</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="evidencias_despachos[]" class="form-control"
                                            id="inputGroupFile04" multiple>
                                        <label class="" for="inputGroupFile04"></label>
                                    </div>
                                    <div class="input-group-append">
                                        <button id="btn_agregar_datos" class="btn btn-success"
                                            onclick="guardar_form_4()" type="button">Guardar
                                            datos y
                                            evidencias <i class="fa fa-upload"></i></button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-12 col-sm-5">
                        <div id="total_pallets"></div>
                        <div id="total_bultos"></div>
                        <div id="despacho_parcial_totals_servicios"></div>
                    </div>
                </div>

            </form>
        </section>
    </div>


</div>
<?php } else if ($id_tipo_servicio == 10 || $id_tipo_servicio == 11) { ?>
<center>
    <h6 id="nombre_servicio"></h6>
    <hr>
</center>
<form id="form_editar_1" style="padding: 0px; height: 80vh !important; overflow-x: hidden;overflow-y: auto;">

    <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
    <input type="hidden" name="mensaje_correo"
        value="SE HA ACTUALIZADO LA INFORMACIÓN PRINCIPAL DEL SERVICIO <b>SUMINISTRO DE MONTACARGAS</b>.">
    <div class="row">
        <div class="col-12 col-sm-12">

            <form>
                <div class="row">
                    <!-- <div class="col-12 col-sm-6">
                        <label for="">Fecha recepción documento</label>

                        <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc" class="form-control"
                            placeholder="Seleccionar una fecha"
                            value="<?php
                                    if ($servicios['fecha_recepcion_doc'] == null) {
                                    } else {
                                        echo date('Y-m-d', strtotime($servicios["fecha_recepcion_doc"]));
                                    }
                                    ?>"
                            <?php echo $disabled_transporte ?>>
                    </div>-->
                    <div class=" col-12 col-sm-6">
                        <label for="">Cliente</label>
                        <select class="form-control" onchange="consultar_subcliente()" name="id_cliente" id="id_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                <?php
                                    $id_cliente = $servicios["id_cliente"];
                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                    $consultar_cliente->execute();
                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_cliente as $cliente) {
                                        echo $cliente["razon_social"];
                                    }
                                    ?>
                            </option>
                            <?php
                                foreach ($consultar_clientes as $clientes) {
                                ?>
                            <option value="<?php echo $clientes["id"] ?>"><?php echo $clientes["razon_social"] ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Subcliente</label>

                        <select class="form-control" name="id_sub_cliente" id="id_sub_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                <?php
                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                    $consultar_sub_cliente->execute();
                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_sub_cliente as $sub_cliente) {
                                        echo $sub_cliente["nombre_sub_cliente"];
                                    }
                                    ?>

                            </option>
                        </select>

                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Do/Ns</label>
                        <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                            class="form-control" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Tipo de servicio</label>
                        <select class="form-control"
                            oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)" name="tipo_servicio"
                            id="tipo_servicios" <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                <?php
                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                    $consultar_tipo_servicio->execute();
                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                        echo $tipo_servicio["nombre_servicio"];
                                    }

                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                            <option value="<?php echo $tipo_servicios['id'] ?>">
                                <?php echo $tipo_servicios['nombre_servicio'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>

                    <div class="col-12 col-sm-6" style="display:none">
                        <label for="">Tipo de carga</label>
                        <select class="form-control" name="tipo_carga" id="tipo_carga"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_carga"] ?>">
                                <?php
                                    $id_tipo_carga = $servicios["tipo_carga"];
                                    $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                    $consultar_tipo_carga->execute();
                                    $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_carga as $tipo_carga) {
                                        echo $tipo_carga["nombre_carga"];
                                    }
                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_cargas as $tipo_cargas) { ?>
                            <option value="<?php echo $tipo_cargas['id'] ?>" selected>
                                <?php echo $tipo_cargas['nombre_carga'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>

                    <div class="col-12 col-sm-6" id="tipo_contenedor">
                        <label for="">Tipo de contenedor</label>
                        <select name="tipo_contenedor" id="tipo_contenedor" class="form-control">
                            <option value="<?php echo $servicios["tipo_contenedor"] ?>">
                                <?php echo $servicios["tipo_contenedor"] ?></option>
                            <option value="HC">HC</option>
                            <option value="DRY">DRY</option>
                            <option value="OPEN TOP">OPEN TOP</option>
                            <option value="REEFER HC">REEFER HC</option>
                            <option value="REERFER">REERFER</option>
                            <option value="FALT RACK">FALT RACK</option>
                            <option value="TANQUE">TANQUE</option>

                        </select>
                    </div>
                    <div class="col-12 col-sm-6" id="">
                        <label for="">Tamaño contenedor</label>
                        <input type=" text" name="tamaño_contenedor"
                            value="<?php echo $servicios["tamaño_contenedor"] ?>" id="" class="form-control"
                            <?php echo $disabled_transporte ?>>

                    </div>

                    <div class="col-12 col-sm-6" id="contenedors">
                        <label for="">Número de contenedor</label>
                        <input type="text" name="contenedor" value="<?php echo $servicios["contenedor"] ?>"
                            id="contenedor" class="form-control" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6" id="lugar_operacions">
                        <label for="">Lugar operación</label>
                        <input type="text" name="lugar_operacion" id="lugar_operacion" class="form-control"
                            value="<?php echo $servicios["lugar_operacion"] ?>" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6" id="fecha_operacions">
                        <label for="">Fecha de operación</label>
                        <input type="date" name="fecha_operacion" id="fecha_operacion" class="form-control"
                            value="<?php echo $servicios["fecha_operacion"] ?>">
                    </div>
                    <div class="col-12 col-sm-6" id="hora_operacions">
                        <label for="">Horas de operación</label>
                        <input type="text" name="hora_operacion" id="hora_operacion" class="form-control"
                            value="<?php echo $servicios["hora_operacion"] ?>">
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Observación</label>
                        <input type="text" name="observacion1" id="observacion1" class="form-control"
                            value="<?php echo $servicios["observacion1"] ?>">
                    </div>

                    <div class="col-12 col-sm-6">
                        <a href="javascript:void(0)" onclick="actualizar_form_1()" style="width: 100%; margin-top:15px;"
                            class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                            <div class="ht-40 justify-content-between">
                                <span class="pd-x-15">Actualizar</span>
                                <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                            </div>
                        </a>

                    </div>
                </div>
            </form>
        </div>
        <!--<div class="col-12 col-sm-4">
            <h6>Resumen facturación</h6>
            <div id="resumen_servicio_editar"></div>
        </div>-->
    </div>
</form>
<?php } else if ($id_tipo_servicio == 12 || $id_tipo_servicio == 13) { ?>
<center>
    <h6 id="nombre_servicio"></h6>
    <hr>
</center>
<form id="form_editar_1" style="padding: 0px; height: 80vh !important; overflow-x: hidden;overflow-y: auto;">

    <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
    <input type="hidden" name="mensaje_correo"
        value="SE HA ACTUALIZADO LA INFORMACIÓN PRINCIPAL DEL SERVICIO <b>SUMINISTRO DE ESTIBAS</b>.">
    <div class="row">
        <div class="col-12 col-sm-12">

            <form>
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <label for="">Fecha recepción documento</label>

                        <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc" class="form-control"
                            placeholder="Seleccionar una fecha"
                            value="<?php
                                                                                                                                                                    if ($servicios['fecha_recepcion_doc'] == null) {
                                                                                                                                                                    } else {
                                                                                                                                                                        echo date('Y-m-d', strtotime($servicios["fecha_recepcion_doc"]));
                                                                                                                                                                    }
                                                                                                                                                                    ?>"
                            <?php echo $disabled_transporte ?>>
                    </div>
                    <div class=" col-12 col-sm-6">
                        <label for="">Cliente</label>
                        <select class="form-control" onchange="consultar_subcliente()" name="id_cliente" id="id_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                <?php
                                    $id_cliente = $servicios["id_cliente"];
                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                    $consultar_cliente->execute();
                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_cliente as $cliente) {
                                        echo $cliente["razon_social"];
                                    }
                                    ?>
                            </option>
                            <?php
                                foreach ($consultar_clientes as $clientes) {
                                ?>
                            <option value="<?php echo $clientes["id"] ?>"><?php echo $clientes["razon_social"] ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Subcliente</label>

                        <select class="form-control" name="id_sub_cliente" id="id_sub_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                <?php
                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                    $consultar_sub_cliente->execute();
                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_sub_cliente as $sub_cliente) {
                                        echo $sub_cliente["nombre_sub_cliente"];
                                    }
                                    ?>

                            </option>
                        </select>

                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Do/Ns</label>
                        <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                            class="form-control" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Tipo de servicio</label>
                        <select class="form-control"
                            oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)" name="tipo_servicio"
                            id="tipo_servicios" <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                <?php
                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                    $consultar_tipo_servicio->execute();
                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                        echo $tipo_servicio["nombre_servicio"];
                                    }

                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                            <option value="<?php echo $tipo_servicios['id'] ?>">
                                <?php echo $tipo_servicios['nombre_servicio'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>


                    <div class="col-12 col-sm-6" id="tipo_contenedor">
                        <label for="">Tipo de contenedor</label>
                        <select name="tipo_contenedor" id="tipo_contenedor" class="form-control">
                            <option value="<?php echo $servicios["tipo_contenedor"] ?>">
                                <?php echo $servicios["tipo_contenedor"] ?></option>
                            <option value="HC">HC</option>
                            <option value="DRY">DRY</option>
                            <option value="OPEN TOP">OPEN TOP</option>
                            <option value="REEFER HC">REEFER HC</option>
                            <option value="REERFER">REERFER</option>
                            <option value="FALT RACK">FALT RACK</option>
                            <option value="TANQUE">TANQUE</option>

                        </select>
                    </div>
                    <div class="col-12 col-sm-6" id="">
                        <label for="">Tamaño contenedor</label>
                        <input type=" text" name="tamaño_contenedor"
                            value="<?php echo $servicios["tamaño_contenedor"] ?>" id="" class="form-control"
                            <?php echo $disabled_transporte ?>>

                    </div>

                    <div class="col-12 col-sm-6">
                        <label for="">Número contenedor</label>
                        <input type="text" name="contenedor" value="<?php echo $servicios["contenedor"] ?>"
                            id="contenedor" class="form-control" <?php echo $disabled_transporte ?>>
                    </div>

                    <div class="col-12 col-sm-6" id="lugar_entrega_estibas">
                        <label for="">Lugar entrega Estibas</label>
                        <input type="text" name="lugar_entrega_estiba" id="lugar_entrega_estiba" class="form-control"
                            value="<?php echo $servicios["lugar_entrega_estiba"] ?>" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6" id="fecha_entrega_estibas">
                        <label for="">Fecha de entrega Estibas</label>
                        <input type="date" name="fecha_entrega_estiba" id="fecha_entrega_estiba" class="form-control"
                            value="<?php echo $servicios["fecha_entrega_estiba"] ?>">
                    </div>

                    <div class="col-12 col-sm-6" id="cantidad_estibas">
                        <label for="">Cantidad entrega de Estibas</label>
                        <input type="text" name="cantidad_estiba_entregada" id="cantidad_estiba_entregada"
                            value="<?php echo $servicios["cantidad_estiba_entregada"] ?>" class="form-control"
                            <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6" id="patelizos">
                        <label for="">¿Se patelizó?</label>
                        <select name="patelizo" id="patelizo" class="form-control">
                            <option value="<?php echo $servicios["patelizo"] ?>"><?php echo $servicios["patelizo"] ?>
                            </option>
                            <option value="Sí">Sí</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6" id="lleno_contenedors">
                        <label for="">¿Se llenó el contenedor?</label>
                        <select name="lleno_contenedor" id="lleno_contenedor" class="form-control">
                            <option value="<?php echo $servicios["lleno_contenedor"] ?>">
                                <?php echo $servicios["lleno_contenedor"] ?></option>
                            <option value="Sí">Sí</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Horas de montacarga</label>
                        <input type="text" class="form-control" placeholder="" <?php echo $disabled_carga ?>
                            value="<?php echo $servicios["hora_montacarga"] ?>" id="hora_montacarga"
                            name="hora_montacarga">
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Observación</label>
                        <input type="text" name="observacion1" id="observacion1" class="form-control"
                            value="<?php echo $servicios["observacion1"] ?>">
                    </div>
                    <div class="col-12 col-sm-6">
                        <a href="javascript:void(0)" onclick="actualizar_form_1()" style="width: 100%; margin-top:15px;"
                            class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                            <div class="ht-40 justify-content-between">
                                <span class="pd-x-15">Actualizar</span>
                                <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                            </div>
                        </a>

                    </div>
                </div>
            </form>
        </div>
        <!--<div class="col-12 col-sm-4">
            <h6>Resumen facturación</h6>
            <div id="resumen_servicio_editar"></div>
        </div>-->
    </div>
</form>
<?php } else if ($id_tipo_servicio == 14 || $id_tipo_servicio == 15) { ?>
<center>
    <h6 id="nombre_servicio"></h6>
    <hr>
</center>
<form id="form_editar_1" style="padding: 0px; height: 80vh !important; overflow-x: hidden;overflow-y: auto;">

    <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
    <input type="hidden" name="mensaje_correo"
        value="SE HA ACTUALIZADO LA INFORMACIÓN PRINCIPAL DEL SERVICIO <b>SUMINISTRO DE PENCAS CON SUS RACHES O CADENAS CON SUS MONAS</b>.">
    <div class="row">
        <div class="col-12 col-sm-12">

            <form>
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <label for="">Fecha recepción documento</label>

                        <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc" class="form-control"
                            placeholder="Seleccionar una fecha"
                            value="<?php
                                                                                                                                                                    if ($servicios['fecha_recepcion_doc'] == null) {
                                                                                                                                                                    } else {
                                                                                                                                                                        echo date('Y-m-d', strtotime($servicios["fecha_recepcion_doc"]));
                                                                                                                                                                    }
                                                                                                                                                                    ?>"
                            <?php echo $disabled_transporte ?>>
                    </div>
                    <div class=" col-12 col-sm-6">
                        <label for="">Cliente</label>
                        <select class="form-control" onchange="consultar_subcliente()" name="id_cliente" id="id_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                <?php
                                    $id_cliente = $servicios["id_cliente"];
                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                    $consultar_cliente->execute();
                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_cliente as $cliente) {
                                        echo $cliente["razon_social"];
                                    }
                                    ?>
                            </option>
                            <?php
                                foreach ($consultar_clientes as $clientes) {
                                ?>
                            <option value="<?php echo $clientes["id"] ?>"><?php echo $clientes["razon_social"] ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Subcliente</label>

                        <select class="form-control" name="id_sub_cliente" id="id_sub_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                <?php
                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                    $consultar_sub_cliente->execute();
                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_sub_cliente as $sub_cliente) {
                                        echo $sub_cliente["nombre_sub_cliente"];
                                    }
                                    ?>

                            </option>
                        </select>

                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Do/Ns</label>
                        <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                            class="form-control" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Tipo de servicio</label>
                        <select class="form-control"
                            oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)" name="tipo_servicio"
                            id="tipo_servicios" <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                <?php
                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                    $consultar_tipo_servicio->execute();
                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                        echo $tipo_servicio["nombre_servicio"];
                                    }

                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                            <option value="<?php echo $tipo_servicios['id'] ?>">
                                <?php echo $tipo_servicios['nombre_servicio'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Contenedor</label>
                        <input type="text" name="contenedor" value="<?php echo $servicios["contenedor"] ?>"
                            id="contenedor" class="form-control" <?php echo $disabled_transporte ?>>
                    </div>

                    <div class="col-12 col-sm-6" id="lugar_entrega_estibas">
                        <label for="">Lugar entrega </label>
                        <input type="text" name="lugar_entrega_estiba" id="lugar_entrega_estiba" class="form-control"
                            value="<?php echo $servicios["lugar_entrega_estiba"] ?>" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6" id="fecha_entrega_estibas">
                        <label for="">Fecha de entrega </label>
                        <input type="date" name="fecha_entrega_estiba" id="fecha_entrega_estiba" class="form-control"
                            value="<?php echo $servicios["fecha_entrega_estiba"] ?>">
                    </div>

                    <div class="col-12 col-sm-6" id="cantidad_estibas">
                        <label for="">Cantidad entrega</label>
                        <input type="text" name="cantidad_estiba_entregada" id="cantidad_estiba_entregada"
                            value="<?php echo $servicios["cantidad_estiba_entregada"] ?>" class="form-control"
                            <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6" id="lleno_contenedors">
                        <label for="">¿Se llenó el contenedor?</label>
                        <select name="lleno_contenedor" id="lleno_contenedor" class="form-control">
                            <option value="<?php echo $servicios["lleno_contenedor"] ?>">
                                <?php echo $servicios["lleno_contenedor"] ?></option>
                            <option value="Sí">Sí</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Horas de montacarga</label>
                        <input type="text" class="form-control" placeholder="" <?php echo $disabled_carga ?>
                            value="<?php echo $servicios["hora_montacarga"] ?>" id="hora_montacarga"
                            name="hora_montacarga">
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Observación</label>
                        <input type="text" name="observacion1" id="observacion1" class="form-control"
                            value="<?php echo $servicios["observacion1"] ?>">
                    </div>
                    <div class="col-12 col-sm-6">
                        <a href="javascript:void(0)" onclick="actualizar_form_1()" style="width: 100%; margin-top:15px;"
                            class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                            <div class="ht-40 justify-content-between">
                                <span class="pd-x-15">Actualizar</span>
                                <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                            </div>
                        </a>

                    </div>
                </div>
            </form>
        </div>
        <!--<div class="col-12 col-sm-4">
            <h6>Resumen facturación</h6>
            <div id="resumen_servicio_editar"></div>
        </div>-->
    </div>
</form>
<?php } else if ($id_tipo_servicio == 16 || $id_tipo_servicio == 17) { ?>
<center>
    <h6 id="nombre_servicio"></h6>
    <hr>
</center>
<form id="form_editar_1" style="padding: 0px; height: 80vh !important; overflow-x: hidden;overflow-y: auto;">

    <input type="hidden" name="id_servicio" value="<?php echo $id_servicio ?>">
    <input type="hidden" name="mensaje_correo"
        value="SE HA ACTUALIZADO LA INFORMACIÓN PRINCIPAL DEL SERVICIO <b>DESCARGUE O CARGUE DE MERCANCÍA CON REACH ESTAKER</b>.">

    <div class="row">
        <div class="col-12 col-sm-12">

            <form>
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <label for="">Fecha recepción documento</label>

                        <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc" class="form-control"
                            placeholder="Seleccionar una fecha"
                            value="<?php
                                                                                                                                                                    if ($servicios['fecha_recepcion_doc'] == null) {
                                                                                                                                                                    } else {
                                                                                                                                                                        echo date('Y-m-d', strtotime($servicios["fecha_recepcion_doc"]));
                                                                                                                                                                    }
                                                                                                                                                                    ?>"
                            <?php echo $disabled_transporte ?>>
                    </div>
                    <div class=" col-12 col-sm-6">
                        <label for="">Cliente</label>
                        <select class="form-control" onchange="consultar_subcliente()" name="id_cliente" id="id_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                <?php
                                    $id_cliente = $servicios["id_cliente"];
                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                    $consultar_cliente->execute();
                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_cliente as $cliente) {
                                        echo $cliente["razon_social"];
                                    }
                                    ?>
                            </option>
                            <?php
                                foreach ($consultar_clientes as $clientes) {
                                ?>
                            <option value="<?php echo $clientes["id"] ?>"><?php echo $clientes["razon_social"] ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Subcliente</label>

                        <select class="form-control" name="id_sub_cliente" id="id_sub_cliente"
                            <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                <?php
                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                    $consultar_sub_cliente->execute();
                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_sub_cliente as $sub_cliente) {
                                        echo $sub_cliente["nombre_sub_cliente"];
                                    }
                                    ?>

                            </option>
                        </select>

                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Do/Ns</label>
                        <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"] ?>"
                            class="form-control" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Tipo de servicio</label>
                        <select class="form-control"
                            oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)" name="tipo_servicio"
                            id="tipo_servicios" <?php echo $disabled_transporte ?>>
                            <option value="<?php echo $servicios["tipo_servicio"] ?>" selected>
                                <?php
                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                    $consultar_tipo_servicio->execute();
                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($consultar_tipo_servicio as $tipo_servicio) {
                                        echo $tipo_servicio["nombre_servicio"];
                                    }

                                    ?>
                            </option>
                            <?php foreach ($consultar_tipo_servicios as $tipo_servicios) { ?>
                            <option value="<?php echo $tipo_servicios['id'] ?>">
                                <?php echo $tipo_servicios['nombre_servicio'] ?>
                            </option>
                            <?php } ?>
                        </select>

                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Contenedor</label>
                        <input type="text" name="contenedor" value="<?php echo $servicios["contenedor"] ?>"
                            id="contenedor" class="form-control" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6" id="lugar_operacions">
                        <label for="">Lugar operación</label>
                        <input type="text" name="lugar_operacion" id="lugar_operacion" class="form-control"
                            value="<?php echo $servicios["lugar_operacion"] ?>" <?php echo $disabled_transporte ?>>
                    </div>
                    <div class="col-12 col-sm-6" id="hora_operacions">
                        <label for="">Horas de operación</label>
                        <input type="text" name="hora_operacion" id="hora_operacion" class="form-control"
                            value="<?php echo $servicios["hora_operacion"] ?>">
                    </div>
                    <div class="col-12 col-sm-6">
                        <label for="">Observación</label>
                        <input type="text" name="observacion1" id="observacion1" class="form-control"
                            value="<?php echo $servicios["observacion1"] ?>">
                    </div>

                    <div class="col-12 col-sm-6">
                        <a href="javascript:void(0)" onclick="actualizar_form_1()" style="width: 100%; margin-top:15px;"
                            class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                            <div class="ht-40 justify-content-between">
                                <span class="pd-x-15">Actualizar</span>
                                <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                            </div>
                        </a>

                    </div>
                </div>
            </form>
        </div>
        <!--<div class="col-12 col-sm-4">
            <h6>Resumen facturación</h6>
            <div id="resumen_servicio_editar"></div>
        </div>-->
    </div>
</form>
<?php } ?>
<div id="respuesta"></div>


<style>
.modal-backdrop {
    z-index: 100 !important
}
</style>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Servicio #<?php echo $id_servicio ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <center>
                    <h6>Resumen facturación</h6>
                </center>
                <div id="resumen_servicio_editar"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <!-- <button type="button" class="btn btn-primary">Descargar resumen (PDF)</button>-->
            </div>
        </div>
    </div>
</div>



<script>
function ejecutar_contenedor_bajado_piso() {
    //   alert("entro");
    var opcion = document.getElementById("contenedor_bajado_piso").value;

    if (opcion == 1) {

        document.getElementById("campos_bajado_piso").innerHTML =
            '<label for="">Número de placa del vehículo</label> <input type="text" oninput="convertir_mayus()"  class="form-control" id="placa_vehiculo_c_piso" name="placa_vehiculo_c_piso">' +
            '<label for="">Nombre conductor</label> <input type="text" oninput="convertir_mayus()" class="form-control" id="nombre_conductor_piso" name="nombre_conductor_piso">' +
            '<label for="">Identificación conductor</label> <input type="text" oninput="convertir_mayus()" class="form-control" id="identificacion_piso" name="identificacion_piso">';

    } else {
        document.getElementById("campos_bajado_piso").innerHTML = '';
    }

}
mostrar_contenedor_bajado_piso();

function mostrar_contenedor_bajado_piso() {
    var id_servicio = '<?php echo $id_servicio ?>';
    var url = '../../actions/actions_admin/acordeon_contenedor_bajado_piso.php?id_servicio=' + id_servicio;

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#bajado_piso").html("Por favor espere...");
        },
        success: function(data) {
            $("#bajado_piso").html(data);
        },
        error: function() {
            alert("Hubo un error, intentelo más tarde");
        }
    })

}
ejecutar_contenedor_bajado_piso_fecha();

function ejecutar_contenedor_bajado_piso_fecha() {
    var opcion = document.getElementById("contenedor_bajado_piso_fecha").value;

    if (opcion == 1) {

        document.getElementById("campos_bajado_piso_fecha").innerHTML =
            '<label for="">Fecha de bajado a piso</label> <input type="date" class="form-control" id="fecha_bajado_piso" value="<?php echo $servicios["fecha_bajado_piso"] ?>" name="fecha_bajado_piso">';

    } else {
        document.getElementById("campos_bajado_piso_fecha").innerHTML = '';
    }
    ejecutar_contenedor_inspeccion();
    ejecutar_lugar_devolucion();
}
ejecutar_contenedor_bajado_piso_fecha_1();

function ejecutar_contenedor_bajado_piso_fecha_1() {
    var opcion = document.getElementById("contenedor_bajado_piso_1").value;
    //alert(opcion)
    if (opcion == 1) {

        document.getElementById("campos_bajado_piso_fecha_1").innerHTML =
            '<label for="">Fecha de bajado a piso</label> <input type="date" class="form-control" id="fecha_bajado_piso_1" value="<?php echo $servicios["fecha_bajado_piso_1"] ?>" name="fecha_bajado_piso_1">';

    } else {
        document.getElementById("campos_bajado_piso_fecha_1").innerHTML = '';
    }
}
ejecutar_contenedor_inspeccion();

function ejecutar_contenedor_inspeccion() {
    //alert("hola");
    var opcion = document.getElementById("contenedor_inspeccion").value;

    if (opcion == 1) {
        // alert("hola");
        document.getElementById("campos_contenedor_inspeccion").innerHTML =
            '<label for="">Lugar de inpsección</label> <input type="text" class="form-control" id="lugar_inspeccion_vacio" name="lugar_inspeccion_vacio" value="<?php echo $servicios["lugar_inspeccion_vacio"] ?>" >' +
            '<label for="">Fecha de inspección</label> <input type="datetime-local" class="form-control" id="fecha_hora_inspeccion" name="fecha_hora_inspeccion"  value="<?php $servicios['fecha_hora_inspeccion'] = preg_replace("/\s/", 'T', $servicios['fecha_hora_inspeccion']);
                                                                                                                                                                                echo $servicios['fecha_hora_inspeccion'] ?>">' +
            '<label for="">Número de placa del vehículo</label> <input type="text" class="form-control" id="placa_v_z_f" name="placa_v_z_f" value="<?php echo $servicios["placa_v_z_f"] ?>" >' +
            '<label for="">Nombre conductor</label> <input type="text" class="form-control" id="nombre_c_z_f" name="nombre_c_z_f" value="<?php echo $servicios["nombre_c_z_f"] ?>">' +
            '<label for="">Identificación conductor</label> <input type="text" class="form-control" id="identificacion_c_z_f" name="identificacion_c_z_f"  value="<?php echo $servicios["identificacion_c_z_f"] ?>">' +
            '<label for="">Evidencias</label><div class="custom-file"><input type="file" name="evidencias[]" class="form-control" id="inputGroupFile04" multiple=""><a href="../../admin/evidencias_servicio?type=3&id_servicios=<?php echo $servicios['id'] ?>" target="_blank">Ver evidencias agregadas</a></div>';

    } else {
        document.getElementById("campos_contenedor_inspeccion").innerHTML = '';
    }
}
ejecutar_lugar_devolucion();

function ejecutar_lugar_devolucion() {
    var opcion = document.getElementById("lugar_devolucion_puerto").value;

    if (opcion == 1) {
        document.getElementById("campos_lugar_devolucion").innerHTML =
            '<label for="">Arim de ingreso</label> <input type="text" class="form-control" id="arim" name="arim" value="<?php echo $servicios["arim"] ?>">' +
            '<label for="">Fecha y hora devolución</label> <input type="datetime-local" class="form-control" id="fecha_devolucion" name="fecha_devolucion" value="<?php $servicios['fecha_devolucion'] = preg_replace("/\s/", 'T', $servicios['fecha_devolucion']);
                                                                                                                                                                        echo $servicios['fecha_devolucion'] ?>">' +
            '<label for="">Número de placa del vehículo</label> <input type="text" class="form-control" id="placa_v_e_v" name="placa_v_e_v" value="<?php echo $servicios["placa_v_e_v"] ?>">' +
            '<label for="">Nombre conductor</label> <input type="text" class="form-control" id="nombre_c_e_v" name="nombre_c_e_v" value="<?php echo $servicios["nombre_c_e_v"] ?>">' +
            '<label for="">Identificación conductor</label> <input type="text" class="form-control" id="identificacion_c_e_v" name="identificacion_c_e_v" value="<?php echo $servicios["identificacion_c_e_v"] ?>">' +
            '<label for="">Evidencias</label><div class="custom-file"><input type="file" name="evidencias_devolucion[]" class="form-control" id="inputGroupFile04" multiple=""><a href="../../admin/evidencias_servicio?type=5&id_servicios=<?php echo $servicios['id'] ?>" target="_blank">Ver evidencias agregadas</a></div>';
    } else {
        document.getElementById("campos_lugar_devolucion").innerHTML = '';
    }
}

function eliminar_contenedor_bajado_piso(id) {
    var url = '../../actions/actions_admin/eliminar_contenedor_bajado_piso.php?id=' + id;
    var opcion = confirm("¿Estás seguro de realizar esta acción?");

    if (opcion == true) {
        $.ajax({
            cache: false,
            async: false,
            url: url,
            beforeSend: function() {
                $("#eliminando_contenedor").html("Por favor espere...");
            },
            success: function(data) {
                $("#eliminando_contenedor").html(data);
            },
            error: function() {
                alert("Hubo un error, intentelo más tarde");
            }
        })

    } else {
        fadeOut();
    }
}
</script>

<script>
mostrar_opciones_form_5()

function mostrar_opciones_form_5() {

    var opcion = document.getElementById("almacenamiento_vacio").value;
    var opcion_seleccionada = '<?php echo $servicios["patio_almacenaje"] ?>';
    var opcion_seleccionada2 = '<?= $servicios["lugar_devolucion_vacio"] ?>';

    // alert(opcion_seleccionada);
    if (opcion_seleccionada != "") {
        $("#mostrar_opcion_si").show();
        $("#mostrar_opcion_no").hide();
        $("#opcion").hide();
        // alert("si");
        $("#mensaje_almacenamiento").html("Si tiene almacenamiento<hr>");
    } else if (opcion_seleccionada2 != "") {
        $("#mostrar_opcion_si").hide();
        $("#mostrar_opcion_no").show();
        $("#opcion").hide();
        //alert("no");
        $("#mensaje_almacenamiento").html("No tiene almacenamiento<hr>");
    } else if (opcion == 1) {
        $("#mostrar_opcion_si").show();
        $("#mostrar_opcion_no").hide();
    } else if (opcion == 0) {
        $("#mostrar_opcion_si").hide();
        $("#mostrar_opcion_no").show();
    } else if (opcion == 3) {
        $("#mostrar_opcion_si").hide();
        $("#mostrar_opcion_no").hide();
    }

}
</script>
<script src="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.js"></script>
<link rel="stylesheet" href="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.css">
<script src="http://worldshippingcompany.com.co/assets/plugins/bootstrap-select/js/bootstrap-select.min.js">
</script>

<script>
function validar_cantidad_pallets() {

    var suma_pallets_despachados = document.getElementById("suma_pallets_despachados").value;
    var cantidad_p_total = document.getElementById("cantidad_bpallets").value;
    var cantidad_p_ingresada = document.getElementById("cantidad_pallets_despacho_item").value;
    var pallets_disponibles_por_despachar = cantidad_p_total - suma_pallets_despachados;
    // alert(cantidad_p_ingresada + " > " + pallets_disponibles_por_despachar)
    if (parseInt(cantidad_p_ingresada) > parseInt(pallets_disponibles_por_despachar)) {
        alerta("Atención!",
            "La cantidad de pallets ingresados es mayor al total de pallets por despachar, total de palllets disponibles por despachar: " +
            pallets_disponibles_por_despachar,
            "warning")
    } else {
        document.getElementById("total_pallets").innerHTML =
            '<div class="oaerror warning"><strong>Cantidad de pallets por despachar: <b></strong>' +
            pallets_disponibles_por_despachar + '</b> </div>';
    }
}

function validar_cantidad_bultos() {

    var suma_bultos_despachados = document.getElementById("suma_bultos_despachados").value;
    var cantidad_b_total = document.getElementById("cantidad_bultos").value;
    var cantidad_p_ingresada = document.getElementById("cantidad_bultos_despacho_item").value;
    var bultos_disponibles_por_despachar = cantidad_b_total - suma_bultos_despachados;
    // alert(cantidad_p_ingresada + " > " + bultos_disponibles_por_despachar)
    if (parseInt(cantidad_p_ingresada) > parseInt(bultos_disponibles_por_despachar)) {
        alerta("Atención!",
            "La cantidad de bultos ingresados es mayor al total de bultos por despachar, total de bultos disponibles por despachar: " +
            bultos_disponibles_por_despachar,
            "warning");
        bultos();
    } else {
        document.getElementById("total_bultos").innerHTML =
            '<div class="oaerror warning"><strong>Cantidad de bultos por despachar: <b></strong>' +
            bultos_disponibles_por_despachar + '</b> </div>';
    }
}
</script>
<script>
$('#tipo_servicio').selectpicker({
    liveSearch: true,
    maxOptions: 1
});

$('#basic33').selectpicker({
    liveSearch: true,
    maxOptions: 1
});



function comparar_fechas_devolucion() {
    var fecha_devolucion_vacio = document.getElementById("fecha_devolucion_v").value;
    var fecha_cita_devolucion_vacio = document.getElementById("fecha_cita_devolucion_v").value;

    // alert(fecha_devolucion_vacio + ' ------- ' + fecha_cita_devolucion_vacio);

    if (fecha_devolucion_vacio > fecha_cita_devolucion_vacio) {

        alerta("Atención!", "Fecha de cita devolución no sobre pasa la fecha devolucion del vacio!", "warning")
    } else if (fecha_devolucion_vacio < fecha_cita_devolucion_vacio) {

        alerta("Atención!", "Fecha de cita devolucion sobre pasa la fecha devolución del vacio!", "warning")

    } else if (fecha_devolucion_vacio == fecha_cita_devolucion_vacio) {


        alerta("Atención!", "Fecha de cita devolucion es igual a la fecha devolución del vacio!", "warning")



    }
}
fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>);

function fechas_bodegaje_traslado_zf_edit(id_servicio) {
    var tipo_servicio = document.getElementById("tipo_servicios").value;

    var url = "../../actions/actions_admin/consultar_fechas_bodegaje_p_traslado_zf.php?tipo_servicio=" +
        tipo_servicio +
        '&id_servicio=' + id_servicio + "&estado=0";

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#fechas_bodegaje_traslado_zf_edit").html("Cargando...");
        },
        success: function(data) {
            $("#fechas_bodegaje_traslado_zf_edit").html(data);
        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });

    var nombre_servicio = $("#tipo_servicios option:selected").text();
    $("#nombre_servicio").html("Servicio: " + nombre_servicio);
}

seleccionar_conductor();

function seleccionar_conductor() {
    var id_conductor = document.getElementById("nombre_condcutor_v").value;

    var url = "../../actions/actions_admin/consultar_conductores_servicios.php?id_conductor=" + id_conductor +
        "&estado=0";

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#campos_conductor").html("Cargando...");
        },
        success: function(data) {
            $("#campos_conductor").html(data);
        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });

}

tipos_despachos();

function tipos_despachos() {
    var select = document.getElementById("tipo_despacho").value;
    if (select == 1) {

        $("#mostrar_tipo_despacho").show();
        $("#opcion").html("Cantidad de metros cuadros almacenados");
    } else if (select == 2) {
        $("#mostrar_tipo_despacho").show();
        $("#opcion").html("Cantidad de tolenadas almacenados");
    } else if (select == 3) {
        $("#mostrar_tipo_despacho").show();
        $("#opcion").html("Cantidad de número de pallets");
    }
}


function validar_cantidad_pesos() {
    $('#form_1').remove();
    var suma_pallets_despachados = document.getElementById("suma_pallets_despachados").value;
    var cantidad_p_total = document.getElementById("peso_retiro").value;
    var cantidad_p_ingresada = document.getElementById("cantidad_pallets_despacho").value;

    //alert(cantidad_p_total)
    console.log(cantidad_p_total);
    var pallets_disponibles_por_despachar = cantidad_p_total - suma_pallets_despachados;
    var resta_total = pallets_disponibles_por_despachar - cantidad_p_ingresada;



    if (cantidad_p_ingresada > pallets_disponibles_por_despachar) {
        alerta("Atención!",
            "La cantidad de peso ingresado es mayor al total de peso por despachar, total de peso disponibles por despachar: " +
            pallets_disponibles_por_despachar,
            "warning")
    } else {
        document.getElementById("total_pallets").innerHTML =
            '<div class="oaerror warning"><strong>Cantidad de peso por despachar: <b></strong>' +
            resta_total + '</b> </div>';

        document.getElementById("suma_peso").innerHTML = 'Por despachar: ' + resta_total;
    }
}

function validar_cantidad_despachos() {
    $('#form_1').remove();
    var suma_pallets_despachados = document.getElementById("suma_pallets_despachados").value;
    var cantidad_p_ingresada = document.getElementById("cantidad_pallets_despacho").value;
    var cantidad_p_total = document.getElementById("cantidad_despacho").value;
    // alert(cantidad_p_total)
    console.log(cantidad_p_total);
    var pallets_disponibles_por_despachar = cantidad_p_total - suma_pallets_despachados;
    var resta_total = pallets_disponibles_por_despachar - cantidad_p_ingresada;

    if (cantidad_p_ingresada > pallets_disponibles_por_despachar) {
        alerta("Atención!",
            "La cantidad ingresada es mayor al total faltante por despachar, total disponibles por despachar: " +
            pallets_disponibles_por_despachar,
            "warning")
    } else {
        document.getElementById("total_pallets").innerHTML =
            '<div class="oaerror warning"><strong>Cantidad por despachar: <b></strong>' +
            resta_total + '</b> </div>';

        document.getElementById("suma_peso").innerHTML = 'Por despachar: ' + resta_total;
    }
}

function alerta(titulo, mensaje, tipo_alerta) {

    swal(titulo, mensaje, tipo_alerta)

}

$(document).ready(function() {
    $('#impoexpo').select2({
        width: 'resolve',
        dropdownParent: "#editar_servicio"

    });
    $('#tipo_servicios').select2({
        width: 'resolve',
        dropdownParent: "#editar_servicio"

    });
    $('#sub_cliente').select2({
        width: 'resolve',
        dropdownParent: "#editar_servicio"

    });
    $('#cliente').select2({
        width: 'resolve',
        dropdownParent: "#editar_servicio"

    });
    $('#tipo_carga').select2({
        width: 'resolve',
        dropdownParent: "#editar_servicio"

    });
    $('#nombre_condcutor_v').select2({
        width: 'resolve',
        dropdownParent: "#editar_servicio"

    });

});

function actualizar_servicios(id) {
    //alert("entro");
    var identificador = id;
    var url = "../../actions/actions_admin/actualizar_formularios_actions.php";

    $.ajax({
        cache: false,
        async: false,
        type: 'POST',
        url: url,
        data: new FormData($("#formulario_servicio_" + identificador)[0]),
        contentType: false,
        processData: false,
        beforeSend: function() {
            $("#respuesta_form_servicio_edit").html(
                '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Guardando, por favor espere...</button>'
            );
        },
        success: function(data) {
            setTimeout(function() {
                $("#respuesta_form_servicio_edit").html(data);
            }, 3000);
        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });
}

observacion_viaje();

function observacion_viaje() {

    var opcion = document.getElementById("viaje_fallido").value;
    if (opcion == 1) {
        document.getElementById("observacion2_campo").innerHTML =
            '<input name="observacion2" id="observacion2" value="<?php echo $servicios["observacion2"] ?>" class="form-control" placeholder="Por favor escriba una observación">';

    } else {
        document.getElementById("observacion2_campo").innerHTML = '';

    }

}
</script>

<script>
contenedor_carga_sueltas();

function contenedor_carga_sueltas() {
    var opcion = document.getElementById("contenedor_carga_suelta").value;
    //   alert(opcion);

    if (opcion == 1) {
        $("#tamaño_contenedor").show();
        $("#contenedors").show();
        $("#tipo_contenedor").show();
    } else if (opcion == 0) {
        $("#tamaño_contenedor").hide();
        $("#contenedors").hide();
        $("#tipo_contenedor").hide();
    } else {
        alert("Por favor selecicona si es contenedor o carga suelta");
    }
}

tipo_cargas_campos();

function tipo_cargas_campos() {
    // alert("tipo de cargas");
    var opcion = document.getElementById("tipo_carga").value;

    //   alert();
    if (opcion == 1) {
        $("#bultos_total").hide();
        $("#pallets_total").show();

        document.getElementById("label_tipo_carga").innerHTML = "Cantidad pallets total";

    } else if (opcion == 2) {
        $("#bultos_total").show();
        $("#pallets_total").hide();
        document.getElementById("label_tipo_carga").innerHTML = "Cantidad bultos total";
    } else if (opcion == 3) {
        $("#bultos_total").hide();
        $("#pallets_total").show();
        document.getElementById("label_tipo_carga").innerHTML = "Cantidad carga suelta total";
    } else if (opcion == 5) {
        $("#bultos_total").show();
        $("#pallets_total").show();
        document.getElementById("label_tipo_carga").innerHTML = "Cantidad pallets total";
    } else {
        $("#bultos_total").hide();
        $("#pallets_total").show();
        document.getElementById("label_tipo_carga").innerHTML = "Cantidad paquetes total";
    }

}



$(document).ready(function() {
    $(".form-control").on("keypress", function() {
        $input = $(this);
        setTimeout(function() {
            $input.val($input.val().toUpperCase());
        }, 50);
    })
})


function convertir_mayus() {

    $(".form-control").on("keypress", function() {
        $input = $(this);
        setTimeout(function() {
            $input.val($input.val().toUpperCase());
        }, 50);
    })
}
</script>