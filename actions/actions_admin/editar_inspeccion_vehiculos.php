<?php
session_start();
include '../../database/database.php';

$id_inspeccion = $_GET["id_inspeccion"];

$consultar_inspeccion = $conn->prepare("SELECT * FROM control_inspeccion_vehiculo WHERE id = '$id_inspeccion'");
$consultar_inspeccion->execute();
$consultar_inspeccion = $consultar_inspeccion->fetchAll(PDO::FETCH_ASSOC);



$consultar_control_inspeccion_vehiculo_relacion = $conn->prepare("SELECT * FROM control_inspeccion_vehiculo_relacion WHERE id_inspeccion = '$id_inspeccion'");
$consultar_control_inspeccion_vehiculo_relacion->execute();
$consultar_control_inspeccion_vehiculo_relacion = $consultar_control_inspeccion_vehiculo_relacion->fetchAll(PDO::FETCH_ASSOC);

$identificacion_empleado = $_SESSION["numero_identificacion"];
?>

<div class="form-layout form-layout-2">
    <div class="row no-gutters">
        <?php foreach ($consultar_control_inspeccion_vehiculo_relacion as $value) { ?>
        <div class="col-md-4">
            <div class="form-group">
                <label class="form-control-label active">¿<?php echo $value["concepto"] ?>?
                    <span class="tx-danger">*</span></label>
                <input class="form-control" type="hidden" name="concepto[]" value="<?php echo $value["concepto"] ?>">
                <select name="seleccion[]" class="form-control form-control-sm" id="" disabled>
                    <option value="<?php echo $value["seleccion"] ?>"><?php echo $value["seleccion"] ?></option>
                    <option value="No">No</option>
                    <option value="Na">Na</option>
                </select>
            </div>
        </div>
        <!-- col-4 -->
        <div class="col-md-4 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1">
                <label class="form-control-label active">Observaciones: <span class="tx-danger">*</span></label>
                <textarea name="observacion[]" rows="2" class="form-control form-control-sm"
                    placeholder=" Observaciones" value="Ninguna" disabled><?php echo $value["observacion"] ?></textarea>
            </div>
        </div>
        <!-- col-4 -->
        <div class="col-md-4 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1">
                <label for="">Descargar evidencia</label>
                <div class="input-group">
                    <?php
                        $id_inspeccion_relacion = $value["id"];
                        $consultar_inspeccion_evidencia = $conn->prepare("SELECT * FROM control_inspeccion_vehiculo_evidencias WHERE id_inspeccion = '$id_inspeccion_relacion'");
                        $consultar_inspeccion_evidencia->execute();
                        $consultar_inspeccion_evidencia = $consultar_inspeccion_evidencia->fetchAll(PDO::FETCH_ASSOC);
                        $contador = count($consultar_inspeccion_evidencia);
                        foreach ($consultar_inspeccion_evidencia as $values) {

                            if ($values["nombre_file"] != "") {

                        ?>
                    <h6 style="font-size:10px;"><?php echo $values["nombre_file"] ?></h6><br>
                    <a class="btn btn-custom-primary btn-sm file-browser" download
                        href="../foto_evidencia_inspeccion_vehiculo/<?php echo $values["id_inspeccion"] ?>/<?php echo $values["nombre_file"] ?>">Descargar
                        <i class="fa fa-download"></i></a>
                    <?php
                            }
                        }
                        if ($contador == 0) {
                            echo '<span class="badge badge-outlined badge-danger">No se agregaron evidencias.</span>';
                        }
                        ?>
                </div>
            </div>
        </div>
        <?php  } ?>
    </div>


</div>