<?php 
include '../../database/database.php';
$id_despachos = $_GET["id_despachos"];

$consultar_despachos = $conn->prepare("SELECT * FROM evidencias_despachos WHERE id_despacho = '$id_despachos'");
$consultar_despachos->execute();
$consultar_despachos = $consultar_despachos->fetchAll(PDO::FETCH_ASSOC);


?>

<link type="text/css" rel="stylesheet" href="../../../../assets/plugins/baguetteBox/baguetteBox.min.css">
<link type="text/css" rel="stylesheet" href="../../../../assets/plugins/viewer/css/viewer.css">
<link type="text/css" rel="stylesheet" href="../../../../assets/plugins/viewer/css/main.css">
<link type="text/css" rel="stylesheet" href="../../../../assets/plugins/photoswipe/photoswipe.css">
<link type="text/css" rel="stylesheet" href="../../../../assets/plugins/photoswipe/default-skin/default-skin.css">

<div class="tz-gallery">
    <div style="height: 90vh;
    overflow-y: auto;
    overflow-x: auto;">
        <center>
            <a href="javascript:void(0)" onclick="descargar_todo()"><span
                    class="badge badge-pill mg-t-10 badge-success">Descargar
                    todo <i class="fa fa-download"></i></span></a>
        </center>
        <div class="row">

            <?php foreach($consultar_despachos as $evidencia_despacho){
                 $contador= $contador + 1; 
            ?>
            <div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                    <a class="lightbox" data-toggle="modal"
                        data-target="#exampleModal_<?php echo $evidencia_despacho['id'] ?>" href="#">
                        <img class="img-thumbnail img-fluid mg-5" style="height: 350px;width: 350px;object-fit: cover;"
                            src="./../foto_evidencia_despacho/<?php echo $evidencia_despacho['id_despacho'] ?>/<?php echo $evidencia_despacho['nombre_file']?>"
                            alt="">
                    </a>
                    <div class="caption">
                        <h3
                            style=" text-overflow: ellipsis; /* puntos suspensivos */ white-space: nowrap; /* no permite multilinea */ overflow: hidden; /* ocultar el resto */">
                            <?php echo $evidencia_despacho['nombre_file'] ?></h3>
                        <center>
                            <a href="./../foto_evidencia_despacho/<?php echo $evidencia_despacho['id_despacho'] ?>/<?php echo $evidencia_despacho['nombre_file']?>"
                                download id="enlace_<?= $contador ?>"><span
                                    class="badge badge-pill mg-t-10 badge-primary">Descargar
                                    <i class="fa fa-download"></i></span></a>
                        </center>
                    </div>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal_<?php echo $evidencia_despacho['id'] ?>" tabindex="-1"
                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Evidencia/foto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <img style="width:100%"
                                src="./../foto_evidencia_despacho/<?php echo $evidencia_despacho['id_despacho'] ?>/<?php echo $evidencia_despacho['nombre_file']?>"
                                alt="">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
    </div>

</div>

<script>
function descargar_todo() {

    for (var i = 0; i < 100; i++) {
        i++;
        document.getElementById("enlace_" + i).click();
    }
}
</script>
<!--

<script src="../../assets/plugins/baguetteBox/baguetteBox.min.js"></script>
<script src="../../assets/plugins/viewer/js/viewer.js"></script>
<script src="../../assets/plugins/viewer/js/common.js"></script>
<script src="../../assets/plugins/viewer/js/main.js"></script>
<script src="../../assets/plugins/photoswipe/photoswipe.min.js"></script>
<script src="../../assets/plugins/photoswipe/photoswipe-ui-default.min.js"></script>-->



<script>
//baguetteBox Gallery 
baguetteBox.run('.tz-gallery', {
    noScrollbars: true
});
</script>