<?php

include '../../database/database.php';

$id_conductor = $_POST["id_conductor"];
$nombres = $_POST["nombres"];
$apellidos = $_POST["apellidos"];
$tipo_id = $_POST["tipo_id"];
$numero_identificacion = $_POST["numero_identificacion"];
$placa_vehiculo = $_POST["placa_vehiculo"];
//$ruta = $_POST["ruta"];
$estado = $_POST["estado"];
$foto_firma = $_FILES["foto_firma"]["name"];
$tipo_vehiculo = $_POST["tipo_vehiculo"];


if ($nombres == null) {
   $error = "error";
   $mensaje = "Por favor ingrese el nombre del empleado.";
} else if ($apellidos == null) {
   $error = "error";
   $mensaje = "Por favor ingrese los apellidos del empleado.";
} else if ($tipo_id == null) {
   $error = "error";
   $mensaje = "Por favor ingrese el tipo de documento.";
} else if ($numero_identificacion == null) {
   $error = "error";
   $mensaje = "Por favor ingrese un número de identificación.";
} else if ($placa_vehiculo == null) {
   $error = "error";
   $mensaje = "Por favor ingrese la placa del vehiculo.";
}/*else if($ruta == null){
   $error= "error";
   $mensaje = "Por favor seleccione una rutas.";
}*/ else if ($tipo_vehiculo == null) {
   $error = "error";
   $mensaje = "Por favor seleccione un tipo vehículo.";
} else {


   $actualizar_conductor = $conn->prepare("UPDATE  conductores SET 
    nombres_conductor = '$nombres',apellidos_conductor = '$apellidos',tipo_identificacion = '$tipo_id',numero_identificacion = '$numero_identificacion',placa_vehiculo = '$placa_vehiculo', tipo_vehiculo='$tipo_vehiculo', estado = '$estado' WHERE id = '$id_conductor'");

   $actualizar_conductor->execute();

   if ($actualizar_conductor === null) {
      $error = "error";
      $mensaje = "Hubo un error, inténtelo más tarde.";
   } else {
      $error = "success";
      $mensaje = "Conductor actualizado exitosamente.";

      if ($foto_firma == null) {
      } else {
         $directorio = "../../foto_firma_conductores/$id_conductor";
         if (!file_exists($directorio)) {
            mkdir($directorio, 0777) or die("No se puede crear el directorio de extracci&oacute;n");
         }

         $dir = opendir($directorio);
         $filename = $foto_firma;
         $source = $_FILES["foto_firma"]["tmp_name"];
         $target_path = $directorio . '/' . $filename;
         move_uploaded_file($source, $target_path);

         if ($filename != null) {
            $guardar_foto = $conn->prepare("UPDATE conductores SET firma_digital = '$foto_firma'  where id= '$id_conductor'");
            $guardar_foto->execute();
         }
      }
   }
}



echo "<script> 

 ejecutar_boton();
 function ejecutar_boton() {
    if('$error' == 'success'){
     toastr.success('$mensaje','Hola!')
     setTimeout(function() {
 
     window.location.href = 'conductores';
     
     }, 1700);
    }else{
     toastr.error('$mensaje','Hola!')
    }
 }
 
 </script>";