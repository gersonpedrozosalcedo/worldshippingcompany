<?php

include '../../database/database.php';
$consultar_tipos_servicios = $conn->prepare("SELECT * FROM tipo_servicios ORDER BY nombre_servicio ASC ");
$consultar_tipos_servicios->execute();
$consultar_tipos_servicios = $consultar_tipos_servicios->fetchAll(PDO::FETCH_ASSOC);


?>
<table id="foo-filtering" class="table table-bordered table-hover toggle-circle" data-page-size="7">

    <thead>
        <tr>
            <th>Nombre servicio</th>
            <th>ITR</th>
            <th data-hide="all">Costo</th>
            <th data-hide="all">Estado</th>
            <th data-hide="all">Acción</th>
        </tr>
    </thead>
    <tbody>

        <?php 
            foreach($consultar_tipos_servicios as $tipos_servicios){
            ?> <tr>
            <td id="responsive"><?php echo $tipos_servicios["nombre_servicio"] ?></td>
            <td id="responsive"><?php 
            if($tipos_servicios["itreitri"] == 0){
                echo "ITR de Importación";
                            }else{
                                echo "ITR de Exportación";
                            }
            ?></td>
            <td><?php echo $tipos_servicios["costo_servicio"]?></td>
            <td>
                <?php

                if($tipos_servicios["estado"] == 0){
                    echo ' <span class="label label-table label-danger">Desactivado</span>';
                }else if($tipos_servicios["estado"] == 1){
                    echo ' <span class="label label-table label-success">Activado</span>';
                }
               ?>
            </td>

            <td>
                <span style="margin: 0px; padding:0px"
                    onclick="editar_tipo_servicio(<?php echo $tipos_servicios['id'] ?>)">
                    <button type="button" data-toggle="modal" data-target="#editar_tipo_servicios"
                        class="btn btn-outline-primary btn-icon mg-r-5">

                        <i class="fa fa-edit" data-toggle=" tooltip" data-trigger="hover" data-placement="top" title=""
                            data-original-title="Ver/Actualizar tipo de servicio"></i>
                    </button>
                </span>
            </td>

        </tr>
        <?php
            }
            ?>


    </tbody>
    <tfoot>
        <tr>
            <td colspan="5">
                <div class="ft-right">
                    <ul class="pagination"></ul>
                </div>
            </td>
        </tr>
    </tfoot>
</table>

<script>
// Row Toggler
$("#foo-row-toggler").footable();

// Accordion
$("#foo-accordion")
    .footable()
    .on("footable_row_expanded", function(e) {
        $("#foo-accordion tbody tr.footable-detail-show")
            .not(e.row)
            .each(function() {
                $("#foo-accordion").data("footable").toggleDetail(this);
            });
    });
// Filtering
var filtering = $("#foo-filtering");
filtering.footable().on("footable_filtering", function(e) {
    var selected = $("#foo-filter-status").find(":selected").val();
    e.filter += e.filter && e.filter.length > 0 ? " " + selected : selected;
    e.clear = !e.filter;
});

// Filter status
$("#foo-filter-status").change(function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});

// Search input
$("#foo-search").on("input", function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});
</script>