<?php
include '../../database/database.php';
$id_empleado = $_GET["id_empleado"];


$consultar_empleados = $conn->prepare("SELECT * FROM empleados WHERE id = '$id_empleado'");
$consultar_empleados->execute();
$consultar_empleados = $consultar_empleados->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_identificacion = $conn->prepare("SELECT * FROM tipo_id");
$consultar_tipo_identificacion->execute();
$consultar_tipo_identificacion = $consultar_tipo_identificacion->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_cargos = $conn->prepare("SELECT * FROM tipo_cargos");
$consultar_tipo_cargos->execute();
$consultar_tipo_cargos = $consultar_tipo_cargos->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_empleados as $empleado) {
}

?>

<form id="form_editar_empleado" enctype="multipart/form-data">
    <input type="hidden" name="id_empleado" value="<?php echo $empleado["id"] ?>">
    <div class="row">
        <div class="col-12 col-sm-12">
            <label>Nombres: <span class="tx-danger">*</span></label>
            <input type="text" name="nombres" class="form-control" placeholder="Nombres" required=""
                data-parsley-id="11" value="<?php echo $empleado["nombres"] ?>">
        </div>
        <div class="col-12 col-sm-12">
            <br>
            <label>Apellidos: <span class="tx-danger">*</span></label>
            <input type="text" name="apellidos" class="form-control" placeholder="Apellidos" required=""
                data-parsley-id="11" value="<?php echo $empleado["apellidos"] ?>">
        </div>
        <div class="col-12 col-sm-12">
            <br>
            <label>Tipo identifición: <span class="tx-danger">*</span></label>
            <select name="tipo_id" id="tipo_id" class="form-control">
                <?php
                $id_tipo = $empleado["tipo_identificacion"];
                $consultar_tipo_identificacions = $conn->prepare("SELECT * FROM tipo_id WHERE id = '$id_tipo'");
                $consultar_tipo_identificacions->execute();
                $consultar_tipo_identificacions = $consultar_tipo_identificacions->fetchAll(PDO::FETCH_ASSOC);
                foreach ($consultar_tipo_identificacions as $tipo_ids) { ?>
                <option value="<?php echo $tipo_ids["id"] ?>" selected><?php echo $tipo_ids["nombre"] ?>
                </option>
                <?php } ?>

                <?php foreach ($consultar_tipo_identificacion as $tipo_id) { ?>
                <option value="<?php echo $tipo_id["id"] ?>"> <?php echo $tipo_id["nombre"] ?>
                </option>
                <?php } ?>

            </select>
        </div>
        <div class="col-12 col-sm-12">
            <br>
            <label>Número identificación: <span class="tx-danger">*</span></label>
            <input type="tel" name="numero_identificacion" class="form-control" placeholder="Número identificación"
                required="" data-parsley-id="11" value="<?php echo $empleado["numero_identificacion"] ?>">
        </div>
        <div class="col-12 col-sm-12">
            <br>
            <label>Email: <span class="tx-danger">*</span></label>
            <input type="email" name="email" class="form-control " placeholder="Email" required="" data-parsley-id="11"
                value="<?php echo $empleado["email"] ?>">
        </div>
        <div class="col-12 col-sm-12">
            <br>
            <label>Teléfono: <span class="tx-danger">*</span></label>
            <input type="tel" name="telefono" class="form-control " placeholder="Teléfono" required=""
                data-parsley-id="11" value="<?php echo $empleado["telefono"] ?>">
        </div>

        <div class="col-12 col-sm-12">
            <br>
            <label>Contraseña: <span class="tx-danger">*</span></label>
            <input type="password" name="contraseña" class="form-control" placeholder="Contraseña" required=""
                data-parsley-id="11">
        </div>

        <div class="col-12 col-sm-12">
            <br>
            <label>Repetir contraseña: <span class="tx-danger">*</span></label>
            <input type="password" name="contraseña1" class="form-control" placeholder="Repetir contraseña" required=""
                data-parsley-id="11">
        </div>
        <div class="col-12 col-sm-12">
            <br>
            <label>Cargo: <span class="tx-danger">*</span></label>
            <select name="cargo" class="form-control" id="cargo">
                <?php
                $id_cargo = $empleado["cargo"];
                $consultar_tipo_cargoss = $conn->prepare("SELECT * FROM tipo_cargos WHERE id ='$id_cargo' ");
                $consultar_tipo_cargoss->execute();
                $consultar_tipo_cargoss = $consultar_tipo_cargoss->fetchAll(PDO::FETCH_ASSOC);
                foreach ($consultar_tipo_cargoss as $cargoss) { ?>
                <option value="<?php echo $cargoss["id"] ?>" selected><?php echo $cargoss["nombre_cargo"] ?></option>
                <?php } ?>
                <?php foreach ($consultar_tipo_cargos as $cargos) { ?>
                <option value="<?php echo $cargos["id"] ?>"> <?php echo $cargos["nombre_cargo"] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-12 col-sm-12">
            <br>
            <label>Estado: <span class="tx-danger">*</span></label>
            <select name="estado" id="estado" class="form-control ">

                <option value="<?php echo $empleado["estado"] ?>" selected>
                    <?php
                    if ($empleado["estado"] == 0) {
                        echo "Recien creado";
                    } else if ($empleado["estado"] == 1) {
                        echo "Activado";
                    } else if ($empleado["estado"] == 2) {
                        echo "Desactivado";
                    }
                    ?>
                </option>

                <option value="0">Recien creado</option>
                <option value="1">Activar</option>
                <option value="2">Desactivar</option>

            </select>

        </div>
        <div class="col-12 col-sm-12">
            <br>
            <label>Firma digital (png, jpg) (100x150): <span class="tx-danger">*</span></label>
            <div class="input-group">
                <div class="custom-file"> <input type="file" name="foto_firma" class="form-control form-control-sm"
                        id="inputGroupFile04">
                    <label class="" for="inputGroupFile04"></label>
                </div>
                <br>
                <div class="input-group-append">
                    <span class="input-group-btn">
                        <button class="btn btn-custom-primary file-browser" type="button"><i
                                class="fa fa-upload"></i></button>
                    </span>
                </div>
            </div>
            <h6 style="font-size:8px">Firma: <?php echo $empleado["firma_digital"] ?></h6>

        </div>

    </div>
</form>