<?php
include '../../database/database.php';
session_start();

$id_servicio = $_GET["id_servicio"];
$opcion = $_GET["opcion"];

$consultar_despachos = $conn->prepare("SELECT * FROM despachos WHERE id_servicio = '$id_servicio'");
$consultar_despachos->execute();
$consultar_despachos = $consultar_despachos->fetchAll(PDO::FETCH_ASSOC);

$consultar_servicios = $conn->prepare("SELECT * FROM servicios_control_rutas  WHERE id = '$id_servicio'");
$consultar_servicios->execute();
$consultar_servicios = $consultar_servicios->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_servicios as $servicios) {
    $id_tipo_servicio = $servicios["tipo_servicio"];
    $tipo_carga = $servicios["tipo_carga"];
}

if ($id_tipo_servicio == 2) {
    $tipo_de_despacho = "recibidos";
} else {
    $tipo_de_despacho = "despachados";
}

?>

<style>
/* The Modal (background) */
.modal_evidencia {
    display: none;
    /* Hidden by default */
    position: fixed;
    /* Stay in place */
    z-index: 902;
    /* Sit on top */
    padding-top: 100px;
    /* Location of the box */
    left: 0;
    top: 0;
    width: 100%;
    /* Full width */
    height: 100vh;
    /* Full height */
    /*  overflow: auto;*/
    /* Enable scroll if needed */
    background-color: rgb(0, 0, 0);
    /* Fallback color */
    background-color: rgba(0, 0, 0, 0.4);
    /* Black w/ opacity */
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Modal Content */
.modal_evidencia-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 100%;
    height: 100vh;
    border-radius: 20px 20px 0px 0px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {
        top: 300px;
        opacity: 0
    }

    to {
        top: 0;
        opacity: 1
    }
}

@keyframes animatetop {
    from {
        top: 300px;
        opacity: 0
    }

    to {
        top: 0;
        opacity: 1
    }
}


/* The Close Button */
.close_evidencia {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close_evidencia:hover,
.close_evidencia:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

#cerrar {
    margin-top: -20px;
    margin-bottom: 15px;
    border-bottom: solid 1px #0000001f;
    pointer-events: auto;
    /* border-radius: 20px; */
    cursor: pointer;
}

#cerrar:hover {
    background: #00000036;
    border-radius: 0px 0px 20px 20px;

    color: white;
}
</style>

<div id="accordion">
    <?php
    foreach ($consultar_despachos as $despachos) {
        $suma_de_pallets_despachados += $despachos["cantidad_pallets_despacho"];
        $suma_de_bultos_despachos += $despachos["cantidad_bultos_despachados"];

        if ($tipo_carga == 5) {
            $nombre_depacho = $despachos["fecha_hora_despacho"] . ' / # pallets: ' . $despachos["cantidad_pallets_despacho"] . ' / # bultos: ' . $despachos["cantidad_bultos_despachados"];
        } else if ($tipo_carga == 1) {
            $nombre_depacho = $despachos["fecha_hora_despacho"] . ' / # pallets: ' . $despachos["cantidad_pallets_despacho"];
        } else if ($tipo_carga == 2) {
            $nombre_depacho = $despachos["fecha_hora_despacho"] . ' / # bultos: ' . $despachos["cantidad_bultos_despachados"];
        } else if ($tipo_carga == 3) {
            $nombre_depacho = $despachos["fecha_hora_despacho"] . ' / # carga suelta: ' . $despachos["cantidad_pallets_despacho"];
        } else if ($tipo_carga == 4) {
            $nombre_depacho = $despachos["fecha_hora_despacho"] . ' / # paquetes: ' . $despachos["cantidad_pallets_despacho"];
        }
    ?>

    <div class="card mb-2">
        <div class="card-header">
            <a class="text-dark collapsed" data-toggle="collapse" href="#accordion<?php echo $despachos["id"] ?>"
                aria-expanded="false" data-original-title="" title="" data-init="true">
                <?php echo  $nombre_depacho; ?>
            </a>
        </div>
        <div id="accordion<?php echo $despachos["id"] ?>" class="collapse" data-parent="#accordion" style="">
            <div class="card-body">
                <div class="col-12">
                    <h6 class="tx-dark tx-13 tx-semibold">Detalles</h6>
                    <ul class="list-unstyled">
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <b>Fecha y hora:</b>
                                <?php echo $despachos["fecha_hora_despacho"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <b>Transportadora: </b>
                                <?php echo $despachos["nombre_transportadora"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <b>Nombre condcutor:
                                </b><?php echo $despachos["nombres_conductor"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <b>Identificación conductor:
                                </b><?php echo $despachos["numero_identificacion"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <b>Teléfono conductor:
                                </b><?php echo $despachos["telefono"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <b>Placa vehículo:
                                </b><?php echo $despachos["placa_vehiculo"] ?>
                            </a>
                        </li>
                        <?php if ($tipo_carga == 5) { ?>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <b>Cantidad pallets <?php echo $tipo_de_despacho ?>:
                                </b><?php echo $despachos["cantidad_pallets_despacho"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <b>Cantidad bultos <?php echo $tipo_de_despacho ?>:
                                </b><?php echo $despachos["cantidad_bultos_despachados"] ?>
                            </a>
                        </li>
                        <?php } else {
                                if ($tipo_carga == 1) {
                                    $nombre_carga = "pallets";
                                } else if ($tipo_carga == 2) {
                                    $nombre_carga = "bultos";
                                } else if ($tipo_carga == 3) {
                                    $nombre_carga = "carga suelta";
                                } else if ($tipo_carga == 4) {
                                    $nombre_carga = "paquetes";
                                }
                            ?>
                        <li>
                            <?php if ($tipo_carga == 2) { ?>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <b>Cantidad <?php echo  $nombre_carga; ?> <?php echo $tipo_de_despacho ?>:
                                </b><?php echo $despachos["cantidad_bultos_despachados"] ?>
                            </a>
                            <?php } else { ?>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <b>Cantidad <?php echo  $nombre_carga; ?> <?php echo $tipo_de_despacho ?>:
                                </b><?php echo $despachos["cantidad_pallets_despacho"] ?>
                            </a>
                            <?php } ?>

                        </li>
                        <?php } ?>

                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <b>Peso aproximado:
                                </b><?php echo $despachos["peso_aprox"] . ' Kg' ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <b>Destino:
                                </b><?php echo $despachos["destino"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <b>Observación:
                                </b><?php echo $despachos["observacion"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="btn btn-primary btn-sm" href="#" data-toggle="modal"
                                data-target="#ver_evidencias_despachos"
                                onclick="ver_evidencias(<?php echo $despachos['id'] ?>)">
                                <b>Ver evidencias
                                </b></a> &nbsp; <a target="_blank"
                                href="../../actions/actions_admin/descargar_pdf_despachos.php?id_despacho=<?php echo $despachos["id"] ?>"
                                class="btn btn-success btn-sm">Pdf
                            </a>
                            <?php if ($opcion != 1) { ?>
                            <a href="javascript:void(0)" class="btn btn-danger btn-sm"
                                id="btn_eliminar_despacho_<?= $despachos['id'] ?>"
                                onclick="eliminar_despacho(<?php echo $despachos['id'] ?>,<?php echo $id_servicio ?>)">Eliminar
                            </a>

                            <?php } ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>

<div id="descargar_pdf_despachos"></div>

<div id="eliminando_despacho"></div>
<input type="hidden" id="suma_pallets_despachados" value="<?php echo  $suma_de_pallets_despachados; ?>">
<input type="hidden" id="suma_bultos_despachados" value="<?php echo  $suma_de_bultos_despachos; ?>">
<!-- The editar servicios -->
<div id="ver_evidencia_despachos" class="modal_evidencia">
    <div onclick="cerrar_modal_evidencia()" style="padding: 31px; pointer-events: auto; cursor: pointer;
    margin-top: -109px;"></div>
    <!-- Modal content -->
    <div class="modal_evidencia-content">
        <div onclick="cerrar_modal_evidencia()" id="cerrar">
            <center><i class="fa fa-window-minimize"></i><br>
                <h6 style="font-size:8px">Cerrar</h6>
            </center>
            <!-- <span class="close_evidencia" onclick="cerrar_modal_evidencia()">&times;</span>-->
        </div>


        <div id="evidencia_despacio_fotos"> </div>
    </div>
</div>

<script>
function cerrar_modal_evidencia() {
    var modal_evidencia = document.getElementById("ver_evidencia_despachos");
    modal_evidencia.style.display = "none";
}

function ver_evidencias(id_evidencia_despacho) {

    var modal_evidencia = document.getElementById("ver_evidencia_despachos");
    modal_evidencia.style.display = "block";

    var url = "../../actions/actions_admin/ver_evidencias_despachos.php?id_despachos=" + id_evidencia_despacho;

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#evidencia_despacio_fotos").html("Cargando...");
        },
        success: function(data) {
            $("#evidencia_despacio_fotos").html(data);

        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });
}

function descargar_pdf_despachos(id_evidencia_despacho) {
    var url = "../../actions/actions_admin/descargar_pdf_despachos.php?id_despacho=" + id_evidencia_despacho;

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#descargar_pdf_despachos").html("Cargando...");
        },
        success: function(data) {
            $("#descargar_pdf_despachos").html(data);

        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });
}

function ver_evidencias(id_evidencia_despacho) {

    var modal_evidencia = document.getElementById("ver_evidencia_despachos");
    modal_evidencia.style.display = "block";

    var url = "../../actions/actions_admin/ver_evidencias_despachos.php?id_despachos=" + id_evidencia_despacho;

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#evidencia_despacio_fotos").html("Cargando...");
        },
        success: function(data) {
            $("#evidencia_despacio_fotos").html(data);

        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });
}

function eliminar_despacho(id_despacho, id_servicio) {
    var opcion = confirm("¿Estás seguro de realizar esta acción?");

    if (opcion == true) {
        var url =
            "../actions/actions_admin/eliminar_despachos.php?id_despacho=" +
            id_despacho + '&id_servicio=' + id_servicio;

        $.ajax({
            cache: false,
            async: false,
            url: url,
            beforeSend: function() {
                $("#eliminando_despacho").html("Cargando...");
            },
            success: function(data) {
                $("#eliminando_despacho").html(data);
            },
            error: function() {
                alert("Error, por favor intentalo más tarde.");
            },
        });
    } else {
        fadeOut();
    }
}
</script>