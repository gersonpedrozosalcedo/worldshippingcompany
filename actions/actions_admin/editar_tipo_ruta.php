<?php 

include '../../database/database.php';

$id_tipo_ruta = $_GET["id_tipo_ruta"];

$consultar_tipos_rutas = $conn->prepare("SELECT * FROM rutas WHERE id = '$id_tipo_ruta' ");
$consultar_tipos_rutas->execute();
$consultar_tipos_rutas = $consultar_tipos_rutas->fetchAll(PDO::FETCH_ASSOC);
foreach($consultar_tipos_rutas as $tipo_ruta){
    
}
?>

<form id="editar_form_tipo_ruta">

    <input type="hidden" name="id_tipo_ruta" value="<?php echo $id_tipo_ruta?>">
    <div class="form-group">
        <label for="recipient-name-2" class="form-control-label">Nombre ruta</label>
        <input type="text" class="form-control" name="nombre_ruta" value="<?php echo $tipo_ruta["nombre_ruta"]?>"
            id="recipient-name-2">
    </div>

    <div class="form-group">
        <label for="recipient-name-2" class="form-control-label">Estado ruta</label>
        <select name="estado" class="form-control" id="estado">

            <option value="<?php echo $tipo_ruta["estado"]?>">
                <?php 
            if($tipo_ruta["estado"] == 0){
                echo "Desactivado";

            }else{
                echo "Activado";
            }
            ?>

            </option>
            <option value="0">Desactivar</option>
            <option value="1">Activar</option>

        </select>


    </div>
</form>

<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
    <button type="button" class="btn btn-primary" onclick="actualizar_tipos_rutas()">Actualizar</button>
</div>