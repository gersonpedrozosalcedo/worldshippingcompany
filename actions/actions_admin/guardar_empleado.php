<?php

include '../../database/database.php';


$nombres = $_POST["nombres"];
$apellidos = $_POST["apellidos"];
$tipo_id = $_POST["tipo_id"];
$numero_identificacion = $_POST["numero_identificacion"];
$email = $_POST["email"];
$telefono = $_POST["telefono"];
$contraseña = $_POST["contraseña"];
$contraseña1 = $_POST["contraseña1"];
$cargo = $_POST["cargo"];
$estado = $_POST["cargo"];
$foto_firma = $_FILES["foto_firma"]["name"];

if ($nombres == null) {
   $error = "error";
   $mensaje = "Por favor ingrese el nombre del empleado.";
} else if ($apellidos == null) {
   $error = "error";
   $mensaje = "Por favor ingrese los apellidos del empleado.";
} else if ($tipo_id == null) {
   $error = "error";
   $mensaje = "Por favor ingrese el tipo de documento.";
} else if ($numero_identificacion == null) {
   $error = "error";
   $mensaje = "Por favor ingrese un número de identificación.";
} else if ($email == null) {
   $error = "error";
   $mensaje = "Por favor ingrese email del empleado.";
} else if ($telefono == null) {
   $error = "error";
   $mensaje = "Por favor ingrese el número de teléfono del empleado .";
} else if ($contraseña == null) {
   $error = "error";
   $mensaje = "Por favor ingrese una contraseña.";
} else if ($contraseña1 == null) {
   $error = "error";
   $mensaje = "Por favor repita la contraseña.";
} else if ($contraseña != $contraseña1) {
   $error = "error";
   $mensaje = "Las contraseñas no coinciden.";
} else if ($cargo == null) {
   $error = "error";
   $mensaje = "Por favor ingrese el cargo del empleado.";
} else if ($estado == null) {
   $error = "error";
   $mensaje = "Por favor seleccione el estado del usuario.";
} else if ($foto_firma == null) {
   $error = "error";
   $mensaje = "Por favor ingrese la firma digital del empleado.";
} else {


   $guardar_empleados = $conn->prepare("INSERT INTO empleados (nombres,apellidos,tipo_identificacion,numero_identificacion,email,contraseña,telefono,cargo,estado,fecha_creacion)
     VALUES ('$nombres','$apellidos','$tipo_id','$numero_identificacion','$email','$contraseña','$telefono','$cargo','$estado',NOW())");

   $guardar_empleados->execute();

   if ($guardar_empleados === null) {
      $error = "error";
      $mensaje = "Hubo un error, inténtelo más tarde.";
   } else {
      $error = "success";
      $mensaje = "Empleado guardado exitosamente.";

      $id_empleado = $conn->lastInsertId();
      $directorio = "../../foto_firma_empleados/$id_empleado";
      if (!file_exists($directorio)) {
         mkdir($directorio, 0777) or die("No se puede crear el directorio de extracci&oacute;n");
      }

      $dir = opendir($directorio);
      $filename = $foto_firma;
      $source = $_FILES["foto_firma"]["tmp_name"];
      $target_path = $directorio . '/' . $filename;
      move_uploaded_file($source, $target_path);

      if ($filename != null) {
         $guardar_foto = $conn->prepare("UPDATE empleados SET firma_digital = '$foto_firma'  where id= '$id_empleado'");
         $guardar_foto->execute();
      }
   }
}

echo "<script> 

 ejecutar_boton();
 function ejecutar_boton() {
    if('$error' == 'success'){
     toastr.success('$mensaje','Hola!')
     setTimeout(function() {
 
     window.location.href = 'empleados';
     
     }, 1700);
    }else{
     toastr.error('$mensaje','Hola!')
    }
 }
 
 </script>";