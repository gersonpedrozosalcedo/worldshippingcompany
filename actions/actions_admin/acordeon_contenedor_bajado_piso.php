<?php
include '../../database/database.php';
session_start();

$id_servicio = $_GET["id_servicio"];
$mostrar = $_GET["mostrar"];
$consultar_contenedor_bajado_piso = $conn->prepare("SELECT * FROM servicios_contenedor_bajo_piso WHERE id_servicio = '$id_servicio'");
$consultar_contenedor_bajado_piso->execute();
$consultar_contenedor_bajado_piso = $consultar_contenedor_bajado_piso->fetchAll(PDO::FETCH_ASSOC);


?>



<div id="accordion">
    <?php
    foreach ($consultar_contenedor_bajado_piso as $contendor_piso) {
        //$suma_de_pallets_despachados += $contendor_piso["cantidad_pallets_despacho"];
    ?>
    <!--<input type="hidden" id="suma_pallets_despachados" value="10">-->
    <div class="card mb-2">
        <div class="card-header">
            <a class="text-dark collapsed" data-toggle="collapse" href="#accordion<?php echo $contendor_piso["id"] ?>"
                aria-expanded="false" data-original-title="" title="" data-init="true">
                <?php echo $contendor_piso["nombre_conductor"] . ' / ' . $contendor_piso["placa"] ?>
            </a>
        </div>
        <div id="accordion<?php echo $contendor_piso["id"] ?>" class="collapse" data-parent="#accordion" style="">
            <div class="card-body">
                <div class="col-12">
                    <h6 class="tx-dark tx-13 tx-semibold">Detalles</h6>
                    <ul class="list-unstyled">
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Nombre conductor:</b>
                                <?php echo $contendor_piso["nombre_conductor"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Identificación: </b>
                                <?php echo $contendor_piso["identificacion"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Placa vehículo:
                                </b><?php echo $contendor_piso["placa"] ?>
                            </a>
                        </li>
                        <?php if ($mostrar != 1) { ?>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <i class="icon-arrow-right-circle pl-1 pr-2"></i> <a href="javascript:void(0)"
                                    onclick="eliminar_contenedor_bajado_piso(<?php echo $contendor_piso['id'] ?>)"
                                    class="btn btn-danger btn-sm">Eliminar</a>
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<div id="eliminando_contenedor"> </div>