<?php
include '../../database/database.php';
session_start();

$id_servicio = $_SESSION["id_servicio"];

$consultar_despachos = $conn->prepare("SELECT * FROM despachos WHERE id_servicio = '$id_servicio'");
$consultar_despachos->execute();
$consultar_despachos = $consultar_despachos->fetchAll(PDO::FETCH_ASSOC);


?>


<div id="accordion">
    <?php
    foreach ($consultar_despachos as $despachos) {
        $suma_de_pallets_despachados += $despachos["cantidad_pallets_despacho"];
        $suma_de_bultos_despachos += $despachos["cantidad_bultos_despachados"];
    ?>
        <input type="hidden" id="suma_pallets_despachados" value="<?php echo $suma_de_pallets_despachados ?>">
        <input type="hidden" id="suma_bultos_despachados" value="<?php echo $suma_de_bultos_despachos ?>">
        <div class="card mb-2">
            <div class="card-header">
                <a class="text-dark collapsed" data-toggle="collapse" href="#accordion<?php echo $despachos["id"] ?>" aria-expanded="false" data-original-title="" title="" data-init="true">g
                    <?php echo $despachos["nombre_transportadora"] . ' / ' . $despachos["fecha_hora_despacho"] ?>
                </a>
            </div>
            <div id="accordion<?php echo $despachos["id"] ?>" class="collapse" data-parent="#accordion" style="">
                <div class="card-body">
                    <div class="col-12">
                        <h6 class="tx-dark tx-13 tx-semibold">Detalles</h6>
                        <ul class="list-unstyled">
                            <li>
                                <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                    <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Fecha y hora de despacho:</b>
                                    <?php echo $despachos["fecha_hora_despacho"] ?>
                                </a>
                            </li>
                            <li>
                                <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                    <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Transportadora: </b>
                                    <?php echo $despachos["nombre_transportadora"] ?>
                                </a>
                            </li>
                            <li>
                                <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                    <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Nombre condcutor:
                                    </b><?php echo $despachos["nombres_conductor"] ?>
                                </a>
                            </li>
                            <li>
                                <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                    <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Identificación conductor:
                                    </b><?php echo $despachos["numero_identificacion"] ?>
                                </a>
                            </li>
                            <li>
                                <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                    <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Placa vehículo:
                                    </b><?php echo $despachos["placa_vehiculo"] ?>
                                </a>
                            </li>
                            <li>
                                <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                    <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Cantidad de pallets despachados:
                                    </b><?php echo $despachos["cantidad_pallets_despacho"] ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>