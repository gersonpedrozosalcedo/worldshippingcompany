<?php

include '../../database/database.php';
include "actualizar_servicio.class.php";

$datos = $_POST;

$id_servicio = $_POST["id_servicio"];

$mensaje_correo = $_POST["mensaje_correo"];

$actualizar_servicio = new actualizar_servicio();
$update = actualizar_servicio::update_servicio($datos, $conn, $id_servicio);
$envio_correo = actualizar_servicio::enviar_correo($datos, $conn, $mensaje_correo);

$evidencias = $_FILES["evidencias"];
$evidencias_devolucion = $_FILES["evidencias_devolucion"];
if ($evidencias == null) {
} else {
   $directorio = "../../foto_evidencia/$id_servicio";

   if (!file_exists($directorio)) {
      mkdir($directorio, 0777) or die("No se puede crear el directorio de extracci&oacute;n");
   }

   $dir = opendir($directorio);
   for ($i = 0; $i < count($evidencias); $i++) {
      $filename = $evidencias["name"][$i];
      $source = $evidencias["tmp_name"][$i];
      $target_path = $directorio . '/' . $filename;
      move_uploaded_file($source, $target_path);

      if ($filename != null) {
         $guardar_evidencias = $conn->prepare("INSERT INTO evidencia_servicios (id_servicio,foto,fecha_creacion) VALUES ('$id_servicio','$filename',NOW())");
         $guardar_evidencias->execute();
      }
   }
}


if ($evidencias_devolucion == null) {
} else {
   $directorio = "../../foto_evidencia_devolucion_vacio/$id_servicio";

   if (!file_exists($directorio)) {
      mkdir($directorio, 0777) or die("No se puede crear el directorio de extracci&oacute;n");
   }

   $dir = opendir($directorio);
   for ($i = 0; $i < count($evidencias_devolucion); $i++) {
      $filename = $evidencias_devolucion["name"][$i];
      $source = $evidencias_devolucion["tmp_name"][$i];
      $target_path = $directorio . '/' . $filename;
      move_uploaded_file($source, $target_path);

      if ($filename != null) {
         $guardar_evidencias = $conn->prepare("INSERT INTO evidencia_servicios_devolucion_v (id_servicio,foto,fecha_creacion) VALUES ('$id_servicio','$filename',NOW())");
         $guardar_evidencias->execute();
      }
   }
}



$error = $update["error"];
$mensaje = $update["mensaje"];
$mensaje_error = $update["mensaje_error"];


$mensaje_respuesta = "
<script> 

ejecutar_boton();
function ejecutar_boton() {
   if('$error' == 'success'){
      
    toastr.success('$mensaje','Hola!')
    setTimeout(function() {
      $('#form_1').remove();
      tabla_servicios();
   editar_servicio($id_servicio);
    }, 1700);
   }else{
    toastr.error('$mensaje','Hola!');
  

   }
}

</script>
";

echo $mensaje_respuesta;