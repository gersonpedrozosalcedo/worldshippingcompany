<?php
include '../../database/database.php';
$id_cliente = $_GET["id_cliente"];


$consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
$consultar_cliente->execute();
$consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);

foreach ($consultar_cliente as $cliente) {
}
$consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id_cliente = '$id_cliente'");
$consultar_sub_cliente->execute();
$consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
$contador = count($consultar_sub_cliente);

$consultar_emails_copia = $conn->prepare("SELECT * FROM email_copia_clientes WHERE id_cliente = '$id_cliente'");
$consultar_emails_copia->execute();
$consultar_emails_copia = $consultar_emails_copia->fetchAll(PDO::FETCH_ASSOC);
$contador_email_cco = count($consultar_emails_copia);

?>

<form id="form_editar_cliente">
    <input type="hidden" name="id_cliente" value="<?php echo $cliente["id"] ?>">
    <div class="row">
        <!-- info cliente -->
        <div class="col-12 col-sm-6">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <label>Razón social: <span class="tx-danger">*</span></label>
                    <input type="text" name="razon_social" class="form-control" placeholder="Razón social" required=""
                        data-parsley-id="11" value='<?php echo $cliente["razon_social"] ?>'>
                </div>
                <div class="col-12 col-sm-12">
                    <br>
                    <label>Nit: <span class="tx-danger">*</span></label>
                    <input type="tel" name="nit" class="form-control " placeholder="nit" required=""
                        data-parsley-id="20" value='<?php echo $cliente["nit"] ?>'>
                </div>

                <div class="col-12 col-sm-12">
                    <br>
                    <label>Email principal: <span class="tx-danger">*</span></label>
                    <input type="email" name="email" class="form-control " placeholder="Email" required=""
                        data-parsley-id="11" value='<?php echo $cliente["email"] ?>'>
                </div>


                <br>
                <div class="col-12 col-sm-12">
                    <label>Estado: <span class="tx-danger">*</span></label>
                    <select name="estado" id="estado" class="form-control ">

                        <option value="<?php echo $cliente["estado"] ?>" selected><?php
                                                                                    if ($cliente["estado"] == 0) {
                                                                                        echo "Recien creado";
                                                                                    } else if ($cliente["estado"] == 1) {
                                                                                        echo "Activado";
                                                                                    } else if ($cliente["estado"] == 2) {
                                                                                        echo "Desactivado";
                                                                                    }
                                                                                    ?></option>

                        <option value="0">Recien creado</option>
                        <option value="1">Activar</option>
                        <option value="2">Desactivar</option>

                    </select>
                </div>
                <div class="col-12 col-sm-12">
                    <input type="hidden" name="email_clientes" id="email_clientes" value="" readonly="true" required />
                    <div class="form-group">
                        <br>
                        <label>Email copias CCO: <span class="tx-danger">*</span></label>
                        <div id="tabla_email_clientes"></div>

                        <button type="button" class="btn btn-primary" onclick="ejecutar_email_clientes()">Agregar otro
                            email
                            copia CCO</button>

                    </div>
                </div>
                <div class="col-12 col-sm-12">

                    <input type="hidden" name="form_editar_sub_cliente" id="form_editar_sub_cliente" value=""
                        readonly="true" required />
                    <div id="servicios_profesional"></div>
                    <div style="color: white" class="form-group">
                        <input type="hidden" name="id_cliente" value="<?php echo  $id_cliente ?>">
                        <div class="table-responsive" style="width:100%">
                            <table class="table table-bordered" id="tabla_form_editar_sub_cliente">
                                <tr>
                                    <td><button type="button" style="width: 100%" name="add_form_editar_cliente"
                                            id="add_form_editar_cliente" class="btn btn-warning">Añadir
                                            subcliente</button></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- emails cco -->
        <div class="col-12 col-sm-6">
            <center>
                <p>Emails CCO</p>

            </center>
            <div id="accordion">
                <?php
                foreach ($consultar_emails_copia as $email_copia) {



                ?>
                <div class="card mb-2">
                    <div class="card-header">
                        <a class="text-dark collapsed" data-toggle="collapse"
                            href="#accordion_emails<?php echo $email_copia["id"] ?>" aria-expanded="false"
                            data-original-title="" title="" data-init="true">
                            <?php echo $email_copia["email"] ?>
                        </a>
                    </div>
                    <div id="accordion_emails<?php echo $email_copia["id"] ?>" class="collapse"
                        data-parent="#accordion">
                        <div class="card-body">
                            <div class="col-12">
                                <h6 class="tx-dark tx-13 tx-semibold">Detalles</h6>
                                <ul class="list-unstyled">
                                    <li>
                                        <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                            <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Email CCO:</b>
                                            <?php echo $email_copia["email"] ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                            <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Fecha y hora creado:</b>
                                            <?php echo $email_copia["fecha_creacion"] ?>
                                        </a>
                                    </li>

                                    <li>
                                        <div class="row  mg-t-10 mg-b-10">
                                            <div class="col-md-6">
                                                <div class="custom-control custom-radio">
                                                    <input
                                                        onclick="actualizar_estado_email_cco('<?php echo $email_copia['id']; ?>',0)"
                                                        name="radio_<?php echo $email_copia["id"]; ?>" type="radio"
                                                        value="admin_<?php echo $email_copia["id"]; ?>"
                                                        <?php
                                                                                                                                                                                                                                                        if ($email_copia["estado"] == 0) {
                                                                                                                                                                                                                                                            echo 'checked';
                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                        ?>
                                                        class="custom-control-input"
                                                        id="radio1_<?php echo $email_copia["id"]; ?>">
                                                    <label class="custom-control-label"
                                                        for="radio1_<?php echo $email_copia["id"]; ?>">Desactivar</label>
                                                </div>
                                            </div>
                                            <!-- col-3 -->
                                            <div class="col-md-6 mg-t-20 mg-lg-t-0">
                                                <div class="custom-control custom-radio ">
                                                    <input name="radio_<?php echo $email_copia["id"]; ?>" type="radio"
                                                        value="admin_<?php echo $email_copia["id"]; ?>"
                                                        onclick="actualizar_estado_email_cco('<?php echo $email_copia['id']; ?>',1)"
                                                        class="custom-control-input"
                                                        <?php
                                                                                                                                                                                                                                                                                        if ($email_copia["estado"] == 1) {
                                                                                                                                                                                                                                                                                            echo 'checked';
                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                        ?>
                                                        id="radio2_<?php echo $email_copia["id"]; ?>">
                                                    <label class=" custom-control-label"
                                                        for="radio2_<?php echo $email_copia["id"]; ?>">Activar</label>
                                                </div>
                                            </div>

                                        </div>

                                    </li>
                                    <li>
                                        <span style="margin: 0px; padding:0px" data-toggle="tooltip"
                                            data-trigger="hover" data-placement="top" title=""
                                            data-original-title="Eliminar email copia">
                                            <a id="btn_eliminar_email_copia" href="#"
                                                onclick="eliminar_email_copia(<?php echo $email_copia['id'] ?>)"
                                                class="btn btn-outline-danger  btn-sm btn-icon mg-r-5">Eliminar
                                            </a>
                                            <p id="eliminando_email_copia"></p>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php
                if ($contador_email_cco == 0) {
                    echo "No hay emails CCO regsitrados.";
                }
                ?>
            </div>

            <!-- sub clientes -->

            <center>
                <p>Sub-clientes</p>

            </center>
            <div id="accordion">
                <?php
                foreach ($consultar_sub_cliente as $sub_cliente) {



                ?>
                <div class="card mb-2">
                    <div class="card-header">
                        <a class="text-dark collapsed" data-toggle="collapse"
                            href="#accordion_sub_cliente<?php echo $sub_cliente["id"] ?>" aria-expanded="false"
                            data-original-title="" title="" data-init="true">
                            <?php echo $sub_cliente["nombre_sub_cliente"] ?>
                        </a>
                    </div>
                    <div id="accordion_sub_cliente<?php echo $sub_cliente["id"] ?>" class="collapse"
                        data-parent="#accordion">
                        <div class="card-body">
                            <div class="col-12">
                                <h6 class="tx-dark tx-13 tx-semibold">Detalles</h6>
                                <ul class="list-unstyled">
                                    <li>
                                        <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                            <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Nombre del
                                                subcliente:</b>
                                            <?php echo $sub_cliente["nombre_sub_cliente"] ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                            <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Email del
                                                subcliente:</b>
                                            <?php echo $sub_cliente["email"] ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                            <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Fecha y hora creado:</b>
                                            <?php echo $sub_cliente["fecha_creacion"] ?>
                                        </a>
                                    </li>

                                    <li>
                                        <div class="row  mg-t-10 mg-b-10">
                                            <div class="col-md-6">
                                                <div class="custom-control custom-radio">
                                                    <input
                                                        onclick="actualizar_estado_sub_cliente('<?php echo $sub_cliente['id']; ?>',0)"
                                                        name="radio_<?php echo $sub_cliente["id"]; ?>" type="radio"
                                                        value="admin_<?php echo $sub_cliente["id"]; ?>"
                                                        <?php
                                                                                                                                                                                                                                                            if ($sub_cliente["estado"] == 0) {
                                                                                                                                                                                                                                                                echo 'checked';
                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                            ?>
                                                        class="custom-control-input"
                                                        id="radio1_<?php echo $sub_cliente["id"]; ?>">
                                                    <label class="custom-control-label"
                                                        for="radio1_<?php echo $sub_cliente["id"]; ?>">Desactivar</label>
                                                </div>
                                            </div>
                                            <!-- col-3 -->
                                            <div class="col-md-6 mg-t-20 mg-lg-t-0">
                                                <div class="custom-control custom-radio ">
                                                    <input name="radio_<?php echo $sub_cliente["id"]; ?>" type="radio"
                                                        value="admin_<?php echo $sub_cliente["id"]; ?>"
                                                        onclick="actualizar_estado_sub_cliente('<?php echo $sub_cliente['id']; ?>',1)"
                                                        class="custom-control-input"
                                                        <?php
                                                                                                                                                                                                                                                                                        if ($sub_cliente["estado"] == 1) {
                                                                                                                                                                                                                                                                                            echo 'checked';
                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                        ?>
                                                        id="radio2_<?php echo $sub_cliente["id"]; ?>">
                                                    <label class=" custom-control-label"
                                                        for="radio2_<?php echo $sub_cliente["id"]; ?>">Activar</label>
                                                </div>
                                            </div>

                                        </div>

                                    </li>
                                    <li>
                                        <span style="margin: 0px; padding:0px" data-toggle="tooltip"
                                            data-trigger="hover" data-placement="top" title=""
                                            data-original-title="Eliminar sub cliente">
                                            <a id="btn_eliminar_sub_cliente" href="#"
                                                onclick="eliminar_sub_cliente(<?php echo $sub_cliente['id'] ?>)"
                                                class="btn btn-outline-danger  btn-sm btn-icon mg-r-5">Eliminar
                                            </a>
                                            <p id="eliminando_sub_cliente"></p>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php
                if ($contador == 0) {
                    echo "No hay subclientes regitrados.";
                }
                ?>
            </div>
        </div>

    </div>
</form>
<div id="cambiar_estado_sub_clientes"></div>

<script>
$(document).ready(function() {
    J = 0;
    aux = 0;
    $('#add_form_editar_cliente').click(function() {
        J++;
        aux++;
        document.getElementById("form_editar_sub_cliente").value = aux;
        $('#tabla_form_editar_sub_cliente').append(

            '<table style="width:100%" id="tabla_form_editar_sub_cliente' + J + '" >  ' +
            '<tr id="row' +
            J + '">' +
            '<td><input type="text"  id="nombre_sub_cliente' + J +
            '" name="nombre_sub_cliente[]" placeholder="Nombre del subcliente" class="form-control name_list"  required />' +
            '<td><input type="email"  id="email_sub_cliente' + J +
            '" name="email_sub_cliente[]" placeholder="Email del subcliente" class="form-control name_list"  required />' +
            '<td><button type="button" name="remove" id="' + J +
            '" class="btn btn-danger btn_remove" required>X</button></td>' +
            '</tr>' + '</table>');
    });
    $(document).on('click', '.btn_remove', function() {
        aux--;

        document.getElementById("form_editar_sub_cliente").value = aux;

        var id = $(this).attr('id');
        $('#tabla_form_editar_sub_cliente' + id).remove();
    });

})
</script>

<script>
var J = 0;
var aux = 0;

function ejecutar_email_clientes() {
    J++;
    aux++;
    document.getElementById("email_clientes").value = aux;
    $('#tabla_email_clientes').append(

        '<table style="width:100%" id="tabla_email_clientes' + J + '" >  ' + '<tr id="row' +
        J + '">' +
        '<td style="width:100%"><input type="text"  id="email_clientes' + J +
        '" name="email_copia_clientes[]" placeholder="Ingrese email(s) del cliente" class="form-control name_list"  required />' +
        '<td><button type="button" name="remove" id="' + J +
        '" class="btn btn-danger btn_remove" required>X</button></td>' +
        '</tr>' + '</table>');

}

$(document).on('click', '.btn_remove', function() {
    aux--;
    document.getElementById("email_clientes").value = aux;
    var id = $(this).attr('id');
    $('#tabla_email_clientes' + id).remove();


});
</script>