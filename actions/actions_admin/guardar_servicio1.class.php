<?php


include '../../database/database.php';


class guardar_servicio
{

  public static $tablename = "servicios_control_rutas";

  public static function  add_servicio($datos, $conn)
  {

    $campo_bd = "";
    $campo_value = "";
    $id_servicio = "";
    foreach ($datos as $key => $value) {
      $campo_bd .= $key . ",";
      $campo_value .= "'" . $value . "',";
      $formateado_bd = trim($campo_bd, ',');
      $formateado_campo_value = trim($campo_value, ',');
    }
    $sql = 'INSERT INTO servicios_control_rutas (fecha_recepcion_doc,' . $formateado_bd . ') VALUES(NOW(),' . $formateado_campo_value . ')';
    $guardar_servicio = $conn->prepare($sql);
    $guardar_servicio->execute();

    $id_servicio = $conn->lastInsertId();

    $sql_actualizar = "UPDATE  servicios_control_rutas  SET estado = 0 WHERE id = '$id_servicio' ";
    $actualizar_servicio = $conn->prepare($sql_actualizar);
    $actualizar_servicio->execute();

    $count = $guardar_servicio->rowCount(); // validando que s ehaya ejecutado la sentencia.

    if ($count == '0') {
      $confirmacion = [
        "error" => "error",
        "mensaje" => "Hubo un error, intentelo más tarde o ya has ingresado estos datos.",
        "sql" => $sql
      ];
    } else {
      $confirmacion = [
        "error" => "success",
        "mensaje" => "Servicio guardado correctamente.",
        "id_servicio" => $id_servicio
      ];
    }
    return $confirmacion;
  }
}